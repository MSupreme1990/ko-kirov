<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return \Mindy\Utils\Settings::override(include('settings.php'), [
    'components' => [
        'db' => [
            'class' => '\Mindy\Query\ConnectionManager',
            'databases' => [
                'default' => [
                    'class' => '\Mindy\Query\Connection',
                    'dsn' => 'mysql:host=172.16.1.3;dbname=u1028_1',
                    'username' => 'u1028',
                    'password' => 'Z0SpYvQJGxvioaHq',
                    'charset' => 'utf8',
                ],
            ],
        ],
    ],
]);
