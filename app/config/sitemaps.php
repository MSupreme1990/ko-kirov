<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Modules\Catalog\Sitemap\CategorySitemap;
use Modules\Catalog\Sitemap\ProductSitemap;
use Modules\Pages\Sitemap\PageSitemap;

return [
    'pages' => new PageSitemap(),
    'catalog-category' => new CategorySitemap(),
    'catalog-product' => new ProductSitemap(),
];
