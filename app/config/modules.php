<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    'Core',
    'Orders',
    'Admin',
    'Ko',
    'Catalog',
    'Pages',
    'Offices' => [
        'imageSizes' => [
            'thumb' => [94, 62],
        ],
    ],
    'Slider' => [
        'imageSizes' => [
            'resize' => [990, 514],
        ],
    ],
    'Geo',
    'Mail',
    'Meta',
    'Redirect',
    'Sites',
    'Comments',
    'Files',
    'Menu',
    'Currency',
    'Cart',
    'Sitemap',
    'Translate',
    'User',
];
