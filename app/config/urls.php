<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Mindy\Router\Patterns;

return [
    '/user' => new Patterns('User.urls', 'user'),
    '/core' => new Patterns('Modules.Admin.urls', 'admin'),
    '/core/files' => new Patterns('Modules.Files.urls', 'files'),
    '/core/translate' => new Patterns('Modules.Translate.urls', 'translate'),
    '/core/engine' => new Patterns('Modules.Core.urls', 'core'),
    '/mail' => new Patterns('Modules.Mail.urls', 'mail'),
    '' => new Patterns('Modules.Sitemap.urls', 'sitemap'),
    '/robots.txt' => new Patterns('Modules.Sites.urls', 'sites'),
    '/page/' => new Patterns('Modules.Pages.urls', 'page'),
    '/order/' => new Patterns('Modules.Orders.urls', 'orders'),
    '/c/' => new Patterns('Modules.Catalog.urls', 'catalog'),

    '/api/currency' => new Patterns('Modules.Currency.urls_api', 'currency_api'),
    '/api/mail' => new Patterns('Modules.Mail.urls_api', 'mail_api'),
    '/api/menu' => new Patterns('Modules.Menu.urls_api', 'menu_api'),
    '/api/catalog' => new Patterns('Modules.Catalog.urls_api', 'catalog_api'),
    '/api/page' => new Patterns('Modules.Pages.urls_api', 'page_api'),
    '/api/ko' => new Patterns('Modules.Ko.urls_api', 'ko_api'),
    '/api/geo' => new Patterns('Modules.Geo.urls_api', 'geo_api'),
    '/api/slider' => new Patterns('Modules.Slider.urls_api', 'slider_api'),
    '/api/offices' => new Patterns('Modules.Offices.urls_api', 'offices_api'),

    '/' => new Patterns('Modules.Ko.urls', 'ko'),
];
