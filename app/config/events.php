<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    [
        '\Mindy\Orm\Orm', 'beforeValidate',
        ['\Mindy\Orm\Orm', 'beforeValidate'],
    ],
    [
        '\Mindy\Orm\Orm', 'afterValidate',
        ['\Mindy\Orm\Orm', 'afterValidate'],
    ],
    [
        '\Mindy\Orm\Orm', 'beforeSave',
        ['\Mindy\Orm\Orm', 'beforeSave'],
    ],
    [
        '\Mindy\Orm\Orm', 'afterSave',
        ['\Mindy\Orm\Orm', 'afterSave'],
    ],
    [
        '\Mindy\Orm\Orm', 'beforeDelete',
        ['\Mindy\Orm\Orm', 'beforeDelete'],
    ],
    [
        '\Mindy\Orm\Orm', 'afterDelete',
        ['\Mindy\Orm\Orm', 'afterDelete'],
    ],

    [
        '\Mindy\Form\BaseForm', 'beforeValidate',
        ['\Mindy\Form\BaseForm', 'beforeValidate'],
    ],
    [
        '\Mindy\Form\BaseForm', 'afterValidate',
        ['\Mindy\Form\BaseForm', 'afterValidate'],
    ],
    [
        '\Mindy\Form\BaseForm', 'beforeSetAttributes',
        ['\Mindy\Form\BaseForm', 'beforeSetAttributes'],
    ],
];
