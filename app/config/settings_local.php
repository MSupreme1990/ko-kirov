<?php

/*
 * (c) Studio107 <mail@studio107.ru> http://studio107.ru
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

return \Mindy\Utils\Settings::override(include('settings.php'), [
    'components' => [
        'db' => [
            'class' => '\Mindy\Query\ConnectionManager',
            'databases' => [
                'default' => [
                    'class' => '\Mindy\Query\Connection',
                    'dsn' => 'mysql:host=localhost;dbname=ko-kirov',
                    'username' => 'root',
                    'password' => '',
                    'charset' => 'utf8',
                ],
            ],
        ],
    ],
]);
