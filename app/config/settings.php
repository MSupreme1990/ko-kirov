<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    'basePath' => dirname(__FILE__).'/../',
    'name' => 'Mindy',
    'behaviors' => [
        'ParamsCollectionBehavior' => [
            'class' => '\Mindy\Base\ParamsCollectionBehavior',
        ],
    ],
    'managers' => [
        'aamt@mail.ru',
        'mail@studio107.ru',
    ],
    'locale' => [
        'language' => 'ru',
        'sourceLanguage' => 'en',
        'charset' => 'utf-8',
    ],
    'components' => [
        'middleware' => [
            'class' => '\Mindy\Middleware\MiddlewareManager',
            'middleware' => [
                'UtmMiddleware' => [
                    'class' => '\Modules\Ko\Middleware\UtmMiddleware',
                ],
                'CurrentSiteMiddleware' => [
                    'class' => '\Modules\Sites\Middleware\CurrentSiteMiddleware',
                ],
                'AmoCrmMiddleware' => [
                    'class' => '\Modules\Ko\Middleware\AmoCrmMiddleware',
                ],
                'RedirectMiddleware' => [
                    'class' => '\Modules\Redirect\Middleware\RedirectMiddleware',
                ],
                'DetectCityMiddleware' => [
                    'class' => '\Modules\Ko\Middleware\DetectCityMiddleware',
                ],
                'CorsMiddleware' => [
                    'class' => '\Modules\Core\Middleware\CorsMiddleware',
                ],
            ],
        ],
        'orion' => function () {
            return new Orion\Component\Client\Client([
                'token' => '24e8ed655ef6834',
            ]);
        },
        'signal' => [
            'class' => '\Mindy\Event\EventManager',
            'events' => dirname(__FILE__).DIRECTORY_SEPARATOR.'events.php',
        ],
        'db' => [
            'class' => '\Mindy\Query\ConnectionManager',
            'databases' => [
                'default' => [
                    'class' => '\Mindy\Query\Connection',
                    'dsn' => 'mysql:host=localhost;dbname=u1249_dev',
                    'username' => 'u1249_dev',
                    'password' => 'kN8|pU8$',
//                    'dsn' => 'mysql:host=192.168.2.2;dbname=c288_0',
//                    'username' => 'c288_0',
//                    'password' => 'fnrMtLUQO4CTwo',
                    'charset' => 'utf8',
                ],
            ],
        ],
        'permissions' => [
            'class' => '\Modules\User\Components\Permissions',
        ],
        'mail' => [
            'class' => '\Modules\Mail\Components\DbMailer',
            'defaultFrom' => 'aamt@ko-kirov.ru',
            'messageConfig' => [
                'from' => 'aamt@ko-kirov.ru',
            ],
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => 'aamt@ko-kirov.ru',
                'password' => 'erebozuhi1821',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
        'finder' => [
            'class' => '\Mindy\Finder\FinderFactory',
        ],
        'authManager' => [
            'class' => '\Modules\User\Components\Permissions\PermissionManager',
        ],
        'request' => [
            'class' => '\Mindy\Http\Request',
            'enableCsrfValidation' => false,
        ],
        'auth' => [
            'class' => '\Modules\User\Components\Auth',
            'allowAutoLogin' => true,
            'autoRenewCookie' => true,
        ],
        'cache' => [
            'class' => '\Mindy\Cache\FileCache',
        ],
        'storage' => [
            'class' => '\Mindy\Storage\FileSystemStorage',
            'baseUrl' => 'http://media.ko-kirov.ru/',
        ],
        'template' => [
            'class' => '\Mindy\Template\Renderer',
            'mode' => MINDY_DEBUG ? \Mindy\Template\Renderer::RECOMPILE_ALWAYS : \Mindy\Template\Renderer::RECOMPILE_NEVER,
            'target' => '../app/runtime/cache_templates/',
        ],
        'session' => [
            'class' => '\Modules\User\Components\UserSession',
            'sessionName' => 'mindy',
            'autoStart' => true,
            'cookieMode' => 'only',
            'cookieParams' => [
                'domain' => '.'.str_replace('http://', '', MAIN_DOMAIN),
            ],
        ],
        'generator' => [
            'class' => '\Mindy\Base\Generator',
        ],
        'logger' => [
            'class' => '\Mindy\Logger\LoggerManager',
            'handlers' => [
                'default' => [
                    'class' => '\Mindy\Logger\Handler\NullHandler',
                    'level' => MINDY_DEBUG ? 'DEBUG' : 'ERROR',
                ],
                'null' => [
                    'class' => '\Mindy\Logger\Handler\NullHandler',
                    'level' => 'ERROR',
                ],
                'console' => [
                    'class' => '\Mindy\Logger\Handler\StreamHandler',
                ],
                'users' => [
                    'class' => '\Mindy\Logger\Handler\RotatingFileHandler',
                    'alias' => 'application.runtime.users',
                    'level' => 'INFO',
                    'formatter' => 'users',
                ],
                'mail_admins' => [
                    'class' => '\Mindy\Logger\Handler\SwiftMailerHandler',
                ],
            ],
            'formatters' => [
                'users' => [
                    'class' => '\Mindy\Logger\Formatters\LineFormatter',
                    'format' => "%datetime% %message%\n",
                ],
            ],
            'loggers' => [
                'users' => [
                    'class' => '\Monolog\Logger',
                    'handlers' => ['users'],
                ],
            ],
        ],
    ],
    'preload' => [
        'sentry',
        'logger',
        'db',
    ],
    'modules' => include(__DIR__.'/modules.php'),
];
