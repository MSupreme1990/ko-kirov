<?php
// ko/index.html 2018-10-22 13:31:17 GMT

use \Mindy\Template\Template;

class __MindyTemplate_b64633105b54064432e7f070a526fb82 extends Template
{
    const NAME = 'ko/index.html';

    public function __construct($loader, $helpers = array())
    {
        parent::__construct($loader, $helpers);
    }

    public function display($context = array(), $blocks = array(), $macros = array(), $imports = array())
    {
        /* line 1 -> 18 */
        $this->parent = $this->loadExtends('app.html');
        if (isset($this->parent)) {
            return $this->parent->display($context, $blocks + $this->blocks, $macros + $this->macros, $imports + $this->imports);
        }
    }

    protected static $lines = array(18=>1,);
}
// end of ko/index.html
