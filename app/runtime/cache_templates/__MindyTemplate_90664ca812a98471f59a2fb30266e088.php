<?php
// base.html 2018-10-22 13:31:17 GMT

use \Mindy\Template\Template;

class __MindyTemplate_90664ca812a98471f59a2fb30266e088 extends Template
{
    const NAME = 'base.html';

    public function __construct($loader, $helpers = array())
    {
        parent::__construct($loader, $helpers);
        $this->blocks = array(
            'seo' => array($this, 'block_seo'),
            'css_head' => array($this, 'block_css_head'),
            'js_head' => array($this, 'block_js_head'),
            'loader' => array($this, 'block_loader'),
            'content' => array($this, 'block_content'),
            'body' => array($this, 'block_body'),
            'js' => array($this, 'block_js'),
        );
    }

    public function display($context = array(), $blocks = array(), $macros = array(), $imports = array())
    {
        /* line 1 -> 27 */
        echo '<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    ';
        /* line 5 -> 33 */
        $this->displayBlock('seo', $context, $blocks, $macros, $imports);
        /* line 5 -> 35 */
        echo '

    ';
        /* line 7 -> 39 */
        if ((!((array_key_exists('debug', $context) ? $context['debug'] : null)))) {
            /* line 7 -> 41 */
            echo '
        <script src="https://cdn.ravenjs.com/3.14.2/raven.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript">Raven.config(\'https://ba5e4b4320ef4c1088b2a5d4182dc8ea@sentry.studio107.ru/6\').install()</script>
    ';
        }
        /* line 10 -> 47 */
        echo '
    <meta name=\'yandex-verification\' content=\'4280b4fe8b365ca5\'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="fragment" content="!">
    <meta name="theme-color" content="#7f4135">
    <meta name="copyright" content="Кожаная одежка">

    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.png" type="image/png">

    <link href=\'/static/dist/css/loader.css\' rel=\'stylesheet\' type=\'text/css\'>
    <link href=\'/static/dist/css/';
        /* line 29 -> 68 */
        echo $this->helper('escape', $this->helper('get_mode'));
        /* line 29 -> 70 */
        echo '.css\' rel=\'stylesheet\' type=\'text/css\'>
    <link href=\'http://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic\' rel=\'stylesheet\'
          type=\'text/css\'>

    ';
        /* line 33 -> 76 */
        if ($this->helper('param', 'core.core.logo')) {
            /* line 33 -> 78 */
            echo '
        <style>
            #header .header-container .logo-container #logo {
                background-image: url(/media/';
            /* line 36 -> 83 */
            echo $this->helper('escape', $this->helper('param', 'core.core.logo'));
            /* line 36 -> 85 */
            echo ');
            }
        </style>
    ';
        }
        /* line 39 -> 91 */
        echo '

    ';
        /* line 41 -> 95 */
        $this->displayBlock('css_head', $context, $blocks, $macros, $imports);
        /* line 42 -> 97 */
        echo '

    <script type="text/javascript">
        window.showWarnings = false;
        window.globalError = false;
        window.SHOW_CITY_IN_CATALOG = false;
        window.odometerOptions = { auto: false };

        ';
        if (!isset($context['s'])) $context['s'] = array();

        /* line 50 -> 109 */
        $this->setAttr($context['s'], array(), (((array_key_exists('site', $context) ? $context['site'] : null)) ? ((array_key_exists('site', $context) ? $context['site'] : null)) : ($this->helper('get_site', 1))));
        /* line 50 -> 111 */
        echo '
        window.DEBUG = ';
        /* line 51 -> 114 */
        echo $this->helper('escape', (((array_key_exists('DEBUG', $context) ? $context['DEBUG'] : null)) ? ('true') : ('false')));
        /* line 51 -> 116 */
        echo ';
        window.CURRENCY_LIST = ';
        /* line 52 -> 119 */
        echo $this->helper('get_currency_list');
        /* line 52 -> 121 */
        echo ';
        window.CURRENCY = \'';
        /* line 53 -> 124 */
        echo $this->helper('get_currency');
        /* line 53 -> 126 */
        echo '\';
        window.CART_IDS = ';
        /* line 54 -> 129 */
        echo $this->helper('get_cart_ids');
        /* line 54 -> 131 */
        echo ';
        window.CART_COUNT = ';
        /* line 55 -> 134 */
        echo $this->helper('get_cart_count');
        /* line 55 -> 136 */
        echo ';
        window.SITE = ';
        /* line 56 -> 139 */
        echo $this->getAttr((array_key_exists('s', $context) ? $context['s'] : null), 'toJson', array());
        /* line 56 -> 141 */
        echo ';
        window.CURRENT_CITY_RAW = ';
        /* line 57 -> 144 */
        echo $this->getAttr($this->getAttr((array_key_exists('s', $context) ? $context['s'] : null), 'city', false), 'toJson', array());
        /* line 57 -> 146 */
        echo ';
        window.CURRENT_CITY = ';
        /* line 58 -> 149 */
        echo $this->helper('escape', $this->getAttr($this->getAttr((array_key_exists('s', $context) ? $context['s'] : null), 'city', false), 'id', false));
        /* line 58 -> 151 */
        echo ';
        window.CURRENT_CITY2 = \'';
        /* line 59 -> 154 */
        echo $this->helper('escape', $this->getAttr($this->getAttr((array_key_exists('s', $context) ? $context['s'] : null), 'city', false), 'name2', false));
        /* line 59 -> 156 */
        echo '\';
        window.CURRENT_REGION = ';
        /* line 60 -> 159 */
        echo $this->helper('escape', $this->getAttr($this->getAttr($this->getAttr((array_key_exists('s', $context) ? $context['s'] : null), 'city', false), 'region', false), 'id', false));
        /* line 60 -> 161 */
        echo ';
        window.BANNERS = ';
        /* line 61 -> 164 */
        echo $this->helper('get_banners');
        /* line 61 -> 166 */
        echo ';
        window.MENU = {
            \'footer-service-menu\': ';
        /* line 63 -> 170 */
        echo $this->helper('get_menu_json', 'footer-service-menu');
        /* line 63 -> 172 */
        echo ',
            \'footer-about-menu\': ';
        /* line 64 -> 175 */
        echo $this->helper('get_menu_json', 'footer-about-menu');
        /* line 64 -> 177 */
        echo ',
            \'header-menu\': ';
        /* line 65 -> 180 */
        echo $this->helper('get_menu_json', 'header-menu');
        /* line 65 -> 182 */
        echo '
        };
        window.TREE = ';
        /* line 67 -> 186 */
        echo $this->helper('catalog_tree');
        /* line 67 -> 188 */
        echo ';
        window.CATEGORY_IDS = ';
        /* line 68 -> 191 */
        echo $this->helper('catalog_ids');
        /* line 68 -> 193 */
        echo ';
        window.PRODUCTS_H = ';
        /* line 69 -> 196 */
        echo $this->helper('get_products_hash');
        /* line 69 -> 198 */
        echo ';
        window.SEO_BLOCK_COLLAPSED = false;
        window.BASE_DOMAIN = \'';
        /* line 71 -> 202 */
        echo $this->helper('get_base_domain');
        /* line 71 -> 204 */
        echo '\';
    </script>

    ';
        /* line 74 -> 209 */
        $this->displayBlock('js_head', $context, $blocks, $macros, $imports);
        /* line 75 -> 211 */
        echo '
</head>
<body>
';
        /* line 78 -> 216 */
        $this->displayBlock('body', $context, $blocks, $macros, $imports);
        /* line 164 -> 218 */
        echo '

';
        /* line 166 -> 222 */
        $this->displayBlock('js', $context, $blocks, $macros, $imports);
        /* line 168 -> 224 */
        echo '

';
        /* line 170 -> 228 */
        $this->loadInclude('_statistics.html')->display($context);
        /* line 170 -> 230 */
        echo '
</body>
</html>
';
    }

    /* line 5 -> 237 */
    public function block_seo($context, $blocks = array(), $macros = array(), $imports = array())
    {
        /* line 5 -> 240 */
        echo $this->helper('escape', $this->helper('meta', (array_key_exists('this', $context) ? $context['this'] : null)));
    }

    /* line 41 -> 244 */
    public function block_css_head($context, $blocks = array(), $macros = array(), $imports = array())
    {
        /* line 41 -> 247 */
        echo '
    ';
    }

    /* line 74 -> 252 */
    public function block_js_head($context, $blocks = array(), $macros = array(), $imports = array())
    {
        /* line 74 -> 255 */
        echo '
    ';
    }

    /* line 108 -> 260 */
    public function block_loader($context, $blocks = array(), $macros = array(), $imports = array())
    {
        /* line 108 -> 263 */
        echo '
                    <div class="loader product-page-container">
                        <div class="loader">
                            <div class="ui segment">
                                <div class="ui active loader"></div>
                                <p></p>
                            </div>
                        </div>
                    </div>
                ';
    }

    /* line 134 -> 276 */
    public function block_content($context, $blocks = array(), $macros = array(), $imports = array())
    {
    }

    /* line 78 -> 281 */
    public function block_body($context, $blocks = array(), $macros = array(), $imports = array())
    {
        /* line 78 -> 284 */
        echo '
    <div id="app">
        <div id="page-loader" class="page-load">
            <div class="loading-logo animated zoomIn">
                ';
        /* line 82 -> 290 */
        $this->loadInclude('logo.svg')->display($context);
        /* line 82 -> 292 */
        echo '
            </div>
            <div class="loading l-page animated zoomIn">
                <i></i>
            </div>
        </div>
        <div id="wrapper">
            <div id="container">
                <div id="header">
                    <div class="header-container">
                        <div class="row">
                            <div class="columns large-3 text-left left-side"></div>
                            <div class="columns large-4 logo-container">
                                <h1 id="logo">
                                    <a class="" href="/">
                                        Кожаная одежда
                                    </a>
                                </h1>
                            </div>
                            <div class="columns large-3 text-right right-side"></div>
                        </div>
                    </div>
                    <div class="menu-container">
                        <div class="menu-image"></div>
                    </div>
                </div>
                ';
        /* line 108 -> 320 */
        $this->displayBlock('loader', $context, $blocks, $macros, $imports);
        /* line 117 -> 322 */
        echo '

                <div class="seo">
                    <div class="row">
                        <div class="columns large-10">
                            ';
        if (!isset($context['seo'])) $context['seo'] = array();

        /* line 122 -> 331 */
        $this->setAttr($context['seo'], array(), $this->helper('get_seo_text', $this->getAttr((array_key_exists('request', $context) ? $context['request'] : null), 'path', false)));
        /* line 122 -> 333 */
        echo '
                            ';
        /* line 123 -> 336 */
        $this->pushContext($context, 'forloop');
        $this->pushContext($context, 'loop');
        $this->pushContext($context, 's');
        foreach (($context['forloop'] = $context['loop'] = $this->iterate($context, (array_key_exists('seo', $context) ? $context['seo'] : null))) as $context['s']) {
            /* line 123 -> 341 */
            echo '
                                <h2>';
            /* line 124 -> 344 */
            echo $this->helper('escape', $this->helper('post_process', $this->getAttr((array_key_exists('s', $context) ? $context['s'] : null), 'name', false)));
            /* line 124 -> 346 */
            echo '</h2>
                                <p>';
            /* line 125 -> 349 */
            echo $this->helper('post_process', $this->getAttr((array_key_exists('s', $context) ? $context['s'] : null), 'text', false));
            /* line 125 -> 351 */
            echo '</p>
                            ';
        }
        $this->popContext($context, 'forloop');
        $this->popContext($context, 'loop');
        $this->popContext($context, 's');
        /* line 126 -> 358 */
        echo '
                        </div>
                    </div>
                </div>

                <div class="content">
                    <div class="row">
                        <div class="columns large-10">
                            ';
        /* line 134 -> 368 */
        $this->displayBlock('content', $context, $blocks, $macros, $imports);
        /* line 134 -> 370 */
        echo '
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="footer">
            <div class="footer-container">
                <div class="footer-menu-container">

                </div>
                <div class="row owner-container">
                    <div class="columns large-5">
                        <p class="owner">
                            © 2003-2016
                            Сеть магазинов «Кожаная одежда»
                        </p>
                    </div>
                    <div class="columns large-5">
                        <p class="author">
                            Разработка сайта:
                            <a href="http://studio107.ru" target="_blank">Студия 107</a>
                            © 2016
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
';
    }

    /* line 166 -> 404 */
    public function block_js($context, $blocks = array(), $macros = array(), $imports = array())
    {
        /* line 166 -> 407 */
        echo '
    <script src="/static/dist/js/';
        /* line 167 -> 410 */
        echo $this->helper('escape', $this->helper('get_mode'));
        /* line 167 -> 412 */
        echo '.bundle.js"></script>
';
    }

    protected static $lines = array(27=>1,33=>5,35=>5,39=>7,41=>7,47=>10,68=>29,70=>29,76=>33,78=>33,83=>36,85=>36,91=>39,95=>41,97=>42,109=>50,111=>50,114=>51,116=>51,119=>52,121=>52,124=>53,126=>53,129=>54,131=>54,134=>55,136=>55,139=>56,141=>56,144=>57,146=>57,149=>58,151=>58,154=>59,156=>59,159=>60,161=>60,164=>61,166=>61,170=>63,172=>63,175=>64,177=>64,180=>65,182=>65,186=>67,188=>67,191=>68,193=>68,196=>69,198=>69,202=>71,204=>71,209=>74,211=>75,216=>78,218=>164,222=>166,224=>168,228=>170,230=>170,240=>5,247=>41,255=>74,263=>108,284=>78,290=>82,292=>82,320=>108,322=>117,331=>122,333=>122,336=>123,341=>123,344=>124,346=>124,349=>125,351=>125,358=>126,368=>134,370=>134,407=>166,410=>167,412=>167,);
}
// end of base.html
