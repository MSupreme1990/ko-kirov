<?php
// app.html 2018-10-22 13:31:17 GMT

use \Mindy\Template\Template;

class __MindyTemplate_5dd562ffdcfe83ac0e37874acee45e89 extends Template
{
    const NAME = 'app.html';

    public function __construct($loader, $helpers = array())
    {
        parent::__construct($loader, $helpers);
    }

    public function display($context = array(), $blocks = array(), $macros = array(), $imports = array())
    {
        /* line 1 -> 18 */
        $this->parent = $this->loadExtends('base.html');
        if (isset($this->parent)) {
            return $this->parent->display($context, $blocks + $this->blocks, $macros + $this->macros, $imports + $this->imports);
        }
        /* line 1 -> 23 */
        echo '
';
    }

    protected static $lines = array(18=>1,23=>1,);
}
// end of app.html
