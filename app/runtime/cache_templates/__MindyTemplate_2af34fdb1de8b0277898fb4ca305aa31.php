<?php
// meta/meta_helper.html 2018-10-22 13:31:17 GMT

use \Mindy\Template\Template;

class __MindyTemplate_2af34fdb1de8b0277898fb4ca305aa31 extends Template
{
    const NAME = 'meta/meta_helper.html';

    public function __construct($loader, $helpers = array())
    {
        parent::__construct($loader, $helpers);
    }

    public function display($context = array(), $blocks = array(), $macros = array(), $imports = array())
    {
        if (!isset($context['hasTitle'])) $context['hasTitle'] = array();

        /* line 1 -> 20 */
        $this->setAttr($context['hasTitle'], array(), false);
        /* line 1 -> 22 */
        echo '
<title>';
        /* line 3 -> 25 */
        if ($this->getAttr((array_key_exists('this', $context) ? $context['this'] : null), 'title', false)) {
            if (!isset($context['hasTitle'])) $context['hasTitle'] = array();

            /* line 4 -> 29 */
            $this->setAttr($context['hasTitle'], array(), true);
            /* line 5 -> 31 */
            $this->pushContext($context, 'forloop');
            $this->pushContext($context, 'loop');
            $this->pushContext($context, 'title');
            foreach (($context['forloop'] = $context['loop'] = $this->iterate($context, $this->getAttr((array_key_exists('this', $context) ? $context['this'] : null), 'title', false))) as $context['title']) {
                /* line 6 -> 36 */
                echo $this->helper('escape', (array_key_exists('title', $context) ? $context['title'] : null));
                /* line 7 -> 38 */
                if ((!($this->getAttr((array_key_exists('loop', $context) ? $context['loop'] : null), 'last', false)))) {
                    /* line 7 -> 40 */
                    echo ' - ';
                }
            }
            $this->popContext($context, 'forloop');
            $this->popContext($context, 'loop');
            $this->popContext($context, 'title');
        } elseif ((array_key_exists('title', $context) ? $context['title'] : null)) {
            if (!isset($context['hasTitle'])) $context['hasTitle'] = array();

            /* line 10 -> 50 */
            $this->setAttr($context['hasTitle'], array(), true);
            /* line 11 -> 52 */
            echo $this->helper('escape', (array_key_exists('title', $context) ? $context['title'] : null));
        }
        /* line 14 -> 55 */
        if ((array_key_exists('hasTitle', $context) ? $context['hasTitle'] : null)) {
            /* line 15 -> 57 */
            echo ' - ';
        }
        /* line 18 -> 60 */
        if ((array_key_exists('site', $context) ? $context['site'] : null)) {
            /* line 19 -> 62 */
            echo $this->helper('escape', (array_key_exists('site', $context) ? $context['site'] : null));
        } else {
            /* line 21 -> 65 */
            echo $this->helper('escape', $this->helper('param', 'core.core.sitename'));
        }
        /* line 23 -> 68 */
        echo '</title>
';
        /* line 24 -> 71 */
        if ((array_key_exists('keywords', $context) ? $context['keywords'] : null)) {
            /* line 24 -> 73 */
            echo '    <meta name="keywords" content="';
            /* line 25 -> 75 */
            echo $this->helper('escape', (array_key_exists('keywords', $context) ? $context['keywords'] : null));
            /* line 25 -> 77 */
            echo '">
';
        }
        /* line 27 -> 81 */
        if ((array_key_exists('description', $context) ? $context['description'] : null)) {
            /* line 27 -> 83 */
            echo '    <meta name="description" content="';
            /* line 28 -> 85 */
            echo $this->helper('escape', (array_key_exists('description', $context) ? $context['description'] : null));
            /* line 28 -> 87 */
            echo '">
';
        }
        /* line 30 -> 91 */
        if ((array_key_exists('canonical', $context) ? $context['canonical'] : null)) {
            /* line 30 -> 93 */
            echo '    <link rel="canonical" href="';
            /* line 31 -> 95 */
            echo $this->helper('escape', (array_key_exists('canonical', $context) ? $context['canonical'] : null));
            /* line 31 -> 97 */
            echo '" />
';
        }
    }

    protected static $lines = array(20=>1,22=>1,25=>3,29=>4,31=>5,36=>6,38=>7,40=>7,50=>10,52=>11,55=>14,57=>15,60=>18,62=>19,65=>21,68=>23,71=>24,73=>24,75=>25,77=>25,81=>27,83=>27,85=>28,87=>28,91=>30,93=>30,95=>31,97=>31,);
}
// end of meta/meta_helper.html
