<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Mail\Admin;

use Mindy\Orm\Model;
use Modules\Admin\Components\ModelAdmin;
use Modules\Mail\Forms\MailForm;
use Modules\Mail\Models\Mail;

class MailAdmin extends ModelAdmin
{
    public function getSearchFields()
    {
        return ['subject', 'email'];
    }

    /**
     * @param Model $model
     *
     * @return \Mindy\Orm\QuerySet
     */
    public function getQuerySet(Model $model)
    {
        return parent::getQuerySet($model)->order(['-id']);
    }

    public function getColumns()
    {
        return [
            'email',
            'subject',
            'created_at',
            'readed_at',
        ];
    }

    public function getCanCreate()
    {
        return false;
    }

    /**
     * @return \Mindy\Orm\Model
     */
    public function getModel()
    {
        return new Mail();
    }

    public function getCreateForm()
    {
        return MailForm::className();
    }
}
