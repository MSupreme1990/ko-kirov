<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Mail\Admin;

use Modules\Admin\Components\ModelAdmin;
use Modules\Mail\Forms\QueueForm;
use Modules\Mail\MailModule;
use Modules\Mail\Models\Queue;

class QueueAdmin extends ModelAdmin
{
    public function verboseNames()
    {
        return [
            'count' => MailModule::t('Count'),
        ];
    }

    public function getCreateForm()
    {
        return QueueForm::className();
    }

    public function getColumns()
    {
        return ['name', 'subject', 'created_at', 'user', 'count', 'is_complete', 'started_at', 'stopped_at'];
    }

    public function getModel()
    {
        return new Queue();
    }
}
