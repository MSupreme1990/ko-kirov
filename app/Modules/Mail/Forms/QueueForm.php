<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Mail\Forms;

use Mindy\Form\Fields\UEditorField;
use Mindy\Form\ModelForm;

class QueueForm extends ModelForm
{
    public function getFields()
    {
        return [
            'message' => [
                'class' => UEditorField::className(),
            ],
        ];
    }
}
