<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Mail\Helper;

use Exception;
use Mindy\Helper\Alias;

class MailHelper
{
    public static function convertToBase64($path)
    {
        $path = Alias::get('www').'/'.ltrim($path, '/');
        if (!is_file($path)) {
            throw new Exception('File not found');
        }
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);

        return 'data:image/'.$type.';base64,'.base64_encode($data);
    }
}
