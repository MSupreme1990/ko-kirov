<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    '/unsubscribe/{email}/{token}' => [
        'name' => 'unsubscribe',
        'callback' => '\Modules\Mail\Controllers\SubscribeController:unsubscribe',
    ],
    '/subscribe/' => [
        'name' => 'subscribe',
        'callback' => '\Modules\Mail\Controllers\SubscribeController:subscribe',
    ],
    '/checker/{uniqueId:c}' => [
        'name' => 'checker',
        'callback' => '\Modules\Mail\Controllers\MailCheckerController:index',
    ],
];
