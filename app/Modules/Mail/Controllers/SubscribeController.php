<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Mail\Controllers;

use Mindy\Base\Mindy;
use Modules\Core\Controllers\CoreController;
use Modules\Mail\MailModule;
use Modules\Mail\Models\Subscribe;

class SubscribeController extends CoreController
{
    public function actionSubscribe()
    {
        if (isset($_POST['email']) && !empty($_POST['email'])) {
            $email = $_POST['email'];

            $model = Subscribe::objects()->filter(['email' => $email])->get();
            if ($model === null) {
                $model = new Subscribe(['email' => $email]);

                if ($model->isValid()) {
                    $model->save();
                }
                if ($this->request->getIsAjax()) {
                    echo $this->json([
                        'status' => true,
                        'message' => [
                            'title' => MailModule::t('You are successfully subscribed'),
                        ],
                    ]);
                    Mindy::app()->end();
                }
                $this->request->flash->warning(MailModule::t('You are successfully subscribed'));

                Mindy::app()->mail->fromCode('mail.subscribe', Mindy::app()->managers, [
                    'email' => $email,
                ]);

                if ($url = $this->getNextUrl()) {
                    $this->redirect($url);
                } else {
                    echo $this->render('mail/subscribe_success.html');
                }
            } else {
                if ($this->request->getIsAjax()) {
                    echo $this->json([
                        'status' => true,
                        'message' => [
                            'title' => MailModule::t('You are already subscribed'),
                        ],
                    ]);
                    Mindy::app()->end();
                }

                $this->request->flash->warning(MailModule::t('You are already subscribed'));

                if ($url = $this->getNextUrl()) {
                    $this->redirect($url);
                } else {
                    echo $this->render('mail/subscribe_failed.html');
                }
            }
        } else {
            $this->request->flash->warning(MailModule::t('Incorrect email address'));
            $this->request->redirect(($url = $this->getNextUrl()) ? $url : '/');
        }
    }

    public function actionUnsubscribe($email, $token)
    {
        $model = Subscribe::objects()->filter([
            'email' => $email,
            'token' => $token,
        ])->get();

        if ($model === null) {
            $this->error(404);
        }

        echo $this->render('mail/unsubscribe_success.html');
    }
}
