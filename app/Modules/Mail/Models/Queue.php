<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Mail\Models;

use Mindy\Base\Mindy;
use Mindy\Orm\Fields\BooleanField;
use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\DateTimeField;
use Mindy\Orm\Fields\ForeignField;
use Mindy\Orm\Fields\ManyToManyField;
use Mindy\Orm\Fields\TextField;
use Mindy\Orm\Model;
use Modules\Mail\MailModule;
use Modules\User\Models\User;

/**
 * Class Queue
 *
 * @method static \Modules\Mail\Models\QueueManager objects($instance = null)
 */
class Queue extends Model
{
    public static function getFields()
    {
        return [
            'name' => [
                'class' => CharField::className(),
                'verboseName' => MailModule::t('Name'),
            ],
            'user' => [
                'class' => ForeignField::className(),
                'modelClass' => User::className(),
                'verboseName' => MailModule::t('User'),
                'editable' => false,
            ],
            'subject' => [
                'class' => CharField::className(),
                'verboseName' => MailModule::t('Subject'),
            ],
            'message' => [
                'class' => TextField::className(),
                'verboseName' => MailModule::t('Message'),
            ],
            'template' => [
                'class' => CharField::className(),
                'verboseName' => MailModule::t('Template'),
                'default' => 'mail/message',
            ],
            'subscribers' => [
                'class' => ManyToManyField::className(),
                'verboseName' => MailModule::t('Subscribers'),
                'modelClass' => Subscribe::className(),
            ],
            'created_at' => [
                'class' => DateTimeField::className(),
                'autoNowAdd' => true,
                'editable' => false,
                'verboseName' => MailModule::t('Created at'),
            ],
            'started_at' => [
                'class' => DateTimeField::className(),
                'editable' => false,
                'verboseName' => MailModule::t('Started at'),
                'null' => true,
            ],
            'stopped_at' => [
                'class' => DateTimeField::className(),
                'editable' => false,
                'verboseName' => MailModule::t('Stopped at'),
                'null' => true,
            ],
            'is_running' => [
                'class' => BooleanField::className(),
                'verboseName' => MailModule::t('Is running'),
                'editable' => false,
            ],
            'is_complete' => [
                'class' => BooleanField::className(),
                'verboseName' => MailModule::t('Is complete'),
                'editable' => false,
            ],
            'data' => [
                'class' => TextField::className(),
                'verboseName' => MailModule::t('Data'),
                'editable' => false,
                'null' => true,
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->name;
    }

    public static function objectsManager($instance = null)
    {
        $className = get_called_class();

        return new QueueManager($instance ? $instance : new $className());
    }

    public function beforeSave($owner, $isNew)
    {
        if ($isNew) {
            $owner->user = Mindy::app()->getUser();
        }
    }

    public function beforeDelete($owner)
    {
        Mail::objects()->filter(['queue' => $owner])->delete();
    }

    public function getCount()
    {
        return Mail::objects()->filter(['is_sended' => false, 'queue' => $this])->count();
    }
}
