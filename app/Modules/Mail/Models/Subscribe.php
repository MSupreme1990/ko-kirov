<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Mail\Models;

use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\EmailField;
use Mindy\Orm\Model;
use Modules\Mail\MailModule;

class Subscribe extends Model
{
    public static function getFields()
    {
        return [
            'email' => [
                'class' => EmailField::className(),
                'verboseName' => MailModule::t('Email'),
                'unique' => true,
            ],
            'token' => [
                'class' => CharField::className(),
                'verboseName' => MailModule::t('Token'),
                'editable' => false,
                'null' => true,
                'length' => 10,
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->email;
    }

    /**
     * @param $owner Model
     * @param $isNew
     */
    public function beforeSave($owner, $isNew)
    {
        if ($isNew) {
            $this->token = substr(md5(microtime()), 0, 10);
        }
    }
}
