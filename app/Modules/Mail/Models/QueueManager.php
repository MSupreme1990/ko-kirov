<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Mail\Models;

use Mindy\Orm\Manager;

class QueueManager extends Manager
{
    public function incomplete()
    {
        return $this->filter(['is_complete' => false]);
    }
}
