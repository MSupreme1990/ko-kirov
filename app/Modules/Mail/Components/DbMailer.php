<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Mail\Components;

use Mindy\Base\Mindy;
use Mindy\Exception\HttpException;
use Mindy\Helper\Console;
use Mindy\Mail\Mailer;
use Modules\Mail\Models\Mail;
use Modules\Mail\Models\MailTemplate;

class DbMailer extends Mailer
{
    public $debug = false;

    public $checkerDomain = '';

    /**
     * @var bool enable reading email checker
     */
    public $checker = true;

    public function fromCode($code, $receiver, $data = [], $attachments = [])
    {
        $uniq = uniqid();
        $template = $this->loadTemplateModel($code);

        $data = $this->dataAdditive($data);
        $subject = $this->renderString($template->subject, $data);
        $message = $this->renderString($template->template, $data);

        $checker = '';
        if ($this->checker) {
            $url = $this->checkerDomain.Mindy::app()->urlManager->reverse('mail:checker', ['uniqueId' => $uniq]);
            $checker = strtr("<img style='width: 1px !important; height: 1px !important;' src='{url}'>", [
                '{url}' => $url,
            ]);
        }

        $text = $this->renderTemplate('mail/message.txt', [
            'content' => $message,
            'subject' => $subject,
        ]);
        $html = $this->renderTemplate('mail/message.html', [
            'content' => $message.$checker,
            'subject' => $subject,
        ]);

        /** @var \Mindy\Mail\MessageInterface $msg */
        $msg = $this->createMessage();
        $msg->setHtmlBody($html);
        if (isset($text)) {
            $msg->setTextBody($text);
        } elseif (isset($html)) {
            if (preg_match('|<body[^>]*>(.*?)</body>|is', $html, $match)) {
                $html = $match[1];
            }
            $html = preg_replace('|<style[^>]*>(.*?)</style>|is', '', $html);
            $msg->setTextBody(strip_tags($html));
        }

        $msg->setTo($receiver);
        $msg->setFrom(((!empty($this->defaultFrom)) ? [$this->defaultFrom] : Mindy::app()->managers));
        $msg->setSubject($subject);
        if (!empty($attachments)) {
            if (!is_array($attachments)) {
                $attachments = [$attachments];
            }
            foreach ($attachments as $file) {
                $msg->attach($file);
            }
        }

        $receivers = [];
        if (is_array($receiver)) {
            foreach ($receiver as $r) {
                $receivers[] = $r;
            }
        } else {
            $receivers[] = $receiver;
        }

        $module = Mindy::app()->getModule('Mail');
        $result = $module->delayedSend ? true : $msg->send();
        if ($result) {
            $model = new Mail([
                'email' => implode(', ', $receivers),
                'subject' => $subject,
                'message_txt' => $text,
                'message_html' => $html,
                'unique_id' => $uniq,
                'is_sended' => true,
            ]);
            $model->save();

            return [$subject, $message];
        }

        return false;
    }

    /**
     * @return string|null
     */
    public function getDomain()
    {
        if ($this->domain) {
            return $this->domain;
        } elseif (Console::isCli() === false) {
            return Mindy::app()->request->http->getHostInfo();
        }

        return null;
    }

    /**
     * @param $code
     *
     * @throws HttpException
     *
     * @return bool|\Mindy\Orm\Orm|null
     */
    protected function loadTemplateModel($code)
    {
        $maildb = MailTemplate::objects()->filter(['code' => $code])->get();
        if ($maildb === null) {
            throw new HttpException(500, "Mail template with code $code do not exists");
        }

        return $maildb;
    }

    protected function dataAdditive($data)
    {
        $t = '';

        if (!empty($_COOKIE)) {
            foreach ($_COOKIE as $k => $v) {
                if (is_string($v)) {
                    if (strtolower($v) != 'null' && !empty($v)) {
                        if (strpos(strtolower($k), 'utm_') === 0 || in_array(strtolower($k), ['gclid', 'yclid'])) {
                            $k_title = ucwords(strtolower(str_replace('_', ' ', $k)));
                            $t .= "<b>{$k_title}:</b> ".$v.'<br/>';
                        }
                    }
                }
            }
        }
        if (!empty($t)) {
            $t = '<hr/> '.$t;
        } else {
            $t = '<hr/> <b>UTM метки не определены.</b>';
        }

        return array_merge($data, ['utm' => $t]);
    }
}
