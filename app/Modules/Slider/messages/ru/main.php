<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return  [
  'Name' => 'Название',
  'Content' => 'Текст',
  'Url' => 'Ссылка',
  'Image' => 'Изображение',
  'Position' => 'Позиция',
  'Is published' => 'Опубликовано',
  'Slide {url} was created' => '',
  'Slide {url} was updated' => '',
  'Slide {url} was deleted' => '',
  'Slides' => 'Слайды',
  'Slider' => 'Слайдер / Ротатор',
  'Create slide' => 'Создать слайд',
  'Update slide: {name}' => 'Редактирование сайда: {name}',
];
