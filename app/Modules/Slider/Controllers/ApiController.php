<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Slider\Controllers;

use Mindy\Base\Mindy;
use Modules\Core\Controllers\ApiBaseController;
use Modules\Slider\Models\Slide;

class ApiController extends ApiBaseController
{
    public function actionIndex()
    {
        $models = Slide::objects()->all();
        $data = [];
        foreach ($models as $model) {
            $data[] = $model->toArray();
        }
        echo $this->json($data);
        Mindy::app()->end();
    }
}
