<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Slider\Admin;

use Modules\Admin\Components\ModelAdmin;
use Modules\Slider\Forms\SlideForm;
use Modules\Slider\Models\Slide;

class SlideAdmin extends ModelAdmin
{
    public $sortingColumn = 'position';

    public function getColumns()
    {
        return ['name', 'url', 'is_published'];
    }

    /**
     * @return \Mindy\Orm\Model
     */
    public function getModel()
    {
        return new Slide();
    }

    public function getCreateForm()
    {
        return SlideForm::className();
    }
}
