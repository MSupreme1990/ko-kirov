<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Slider\Forms;

use Mindy\Form\ModelForm;
use Modules\Slider\Models\Slide;

class SlideForm extends ModelForm
{
    public $exclude = ['position'];

    public function getModel()
    {
        return new Slide();
    }
}
