<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Slider\Helper;

use Mindy\Utils\RenderTrait;
use Modules\Slider\Models\Slide;

class SliderHelper
{
    use RenderTrait;

    public static function render($template = 'slider/slider.html')
    {
        echo self::renderTemplate($template, [
            'models' => Slide::objects()->filter(['is_published' => true])->order(['position'])->all(),
        ]);
    }
}
