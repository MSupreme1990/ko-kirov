<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Slider;

use Mindy\Base\Mindy;
use Mindy\Base\Module;

class SliderModule extends Module
{
    /**
     * @var array
     */
    public $imageSizes = [
        'thumb' => [978, 260],
    ];

    public function getVersion()
    {
        return '1.0';
    }

    public static function preConfigure()
    {
        $tpl = Mindy::app()->template;
        $tpl->addHelper('slider', ['\Modules\Slider\Helper\SliderHelper', 'render']);
    }

    public function getMenu()
    {
        return [
            'name' => $this->getName(),
            'items' => [
                [
                    'name' => self::t('Slides'),
                    'adminClass' => 'SlideAdmin',
                ],
            ],
        ];
    }
}
