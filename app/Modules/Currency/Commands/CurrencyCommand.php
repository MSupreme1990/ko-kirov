<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Currency\Commands;

use Mindy\Console\ConsoleCommand;
use Modules\Currency\Helpers\CbrfHelper;
use Modules\Currency\Models\Currency;

class CurrencyCommand extends ConsoleCommand
{
    public function actionIndex()
    {
        $currency = [
            'USD',
            'EUR',
            'KZT',
            'UAH',
            'BYR',
        ];

        $cbr = new CbrfHelper();
        if ($cbr->load()) {
            $data = [];
            foreach ($currency as $c) {
                $data[strtoupper($c)] = $cbr->get(strtoupper($c));
            }
            (new Currency([
                'data' => $data,
            ]))->save();
        }
    }
}
