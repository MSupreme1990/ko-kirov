<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Currency;

use Mindy\Base\Mindy;
use Mindy\Base\Module;
use Mindy\Helper\Json;

class CurrencyModule extends Module
{
    public static function preConfigure()
    {
        $tpl = Mindy::app()->template;
        $tpl->addHelper('get_currency_list', function () {
            return Json::encode([
                'RUB' => 'Рубль',
                'USD' => 'Доллар',
                'EUR' => 'Евро',
                'KZT' => 'Тенге',
                'UAH' => 'Украинская гривна',
                'BYR' => 'Белорусский рубль',
            ]);
        });
    }
}
