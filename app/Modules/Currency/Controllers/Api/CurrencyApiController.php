<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Currency\Controllers\Api;

use Modules\Core\Controllers\Controller;

class CurrencyApiController extends Controller
{
    public function actionList()
    {
        echo $this->json([
            'RUB' => 'Рубль',
            'USD' => 'Доллар',
            'EUR' => 'Евро',
            'KZT' => 'Тенге',
            'UAH' => 'Украинская гривна',
            'BYR' => 'Белорусский рубль',
        ]);
    }
}
