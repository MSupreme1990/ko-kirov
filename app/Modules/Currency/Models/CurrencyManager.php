<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Currency\Models;

use Mindy\Orm\Manager;

class CurrencyManager extends Manager
{
    public function latest()
    {
        return $this->order(['-id'])->limit(1)->offset(0);
    }
}
