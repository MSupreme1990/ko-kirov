<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Currency\Models;

use Mindy\Orm\Fields\DateTimeField;
use Mindy\Orm\Fields\JsonField;
use Mindy\Orm\Model;
use Modules\Currency\CurrencyModule;

class Currency extends Model
{
    public static function getFields()
    {
        return [
            'created_at' => [
                'class' => DateTimeField::class,
                'verboseName' => CurrencyModule::t('Created at'),
                'autoNowAdd' => true,
            ],
            'data' => [
                'class' => JsonField::class,
                'null' => false,
                'verboseName' => CurrencyModule::t('Data'),
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->created_at;
    }

    public static function objectsManager($instance = null)
    {
        $className = get_called_class();

        return new CurrencyManager($instance ? $instance : new $className());
    }
}
