<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Geo\Models;

use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\ForeignField;
use Mindy\Orm\Fields\HasManyField;
use Mindy\Orm\Model;
use Modules\Geo\GeoModule;

class Region extends Model
{
    public static function getFields()
    {
        return [
            'name' => [
                'class' => CharField::className(),
                'verboseName' => GeoModule::t('Name'),
            ],
            'country' => [
                'class' => ForeignField::className(),
                'modelClass' => Country::className(),
                'verboseName' => GeoModule::t('Country'),
            ],
            'cities' => [
                'class' => HasManyField::className(),
                'modelClass' => City::className(),
                'verboseName' => GeoModule::t('Cities'),
                'editable' => false,
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->name;
    }

    public function toArray()
    {
        return array_merge(parent::toArray(), [
            'cities' => $this->cities->asArray()->all(),
        ]);
    }
}
