<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Geo\Models;

use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Model;
use Modules\Geo\GeoModule;

class Country extends Model
{
    public static function getFields()
    {
        return [
            'name' => [
                'class' => CharField::className(),
                'verboseName' => GeoModule::t('Name'),
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->name;
    }
}
