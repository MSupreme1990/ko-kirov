<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Geo\Models;

use Mindy\Orm\Fields\BooleanField;
use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\ForeignField;
use Mindy\Orm\Model;
use Modules\Geo\GeoModule;

class City extends Model
{
    public static function getFields()
    {
        return [
            'name' => [
                'class' => CharField::className(),
                'verboseName' => GeoModule::t('Name'),
            ],
            'name2' => [
                'class' => CharField::className(),
                'verboseName' => GeoModule::t('Name 2'),
            ],
            'country' => [
                'class' => ForeignField::className(),
                'modelClass' => Country::className(),
                'verboseName' => GeoModule::t('Country'),
            ],
            'region' => [
                'class' => ForeignField::className(),
                'modelClass' => Region::className(),
                'verboseName' => GeoModule::t('Region'),
            ],
            'is_popular' => [
                'class' => BooleanField::className(),
                'verboseName' => GeoModule::t('Is popular'),
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->name;
    }
}
