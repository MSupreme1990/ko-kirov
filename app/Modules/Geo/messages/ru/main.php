<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return  [
  'Country' => 'Страна',
  'Region' => 'Регион',
  'City' => 'Город',
  'Name' => 'Название',
  'Is popular' => 'Популярное',
  'City {url} was created' => '',
  'City {url} was updated' => '',
  'City {url} was deleted' => '',
  'Country {url} was created' => '',
  'Country {url} was updated' => '',
  'Country {url} was deleted' => '',
  'Cities' => 'Города',
  'Region {url} was created' => '',
  'Region {url} was updated' => '',
  'Region {url} was deleted' => '',
  'Geo' => 'Гео',
];
