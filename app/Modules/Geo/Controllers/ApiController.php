<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Geo\Controllers;

use Mindy\Base\Mindy;
use Modules\Core\Controllers\ApiBaseController;
use Modules\Geo\Models\Region;
use Modules\Sites\Models\Site;

/**
 * All rights reserved.
 *
 * @author Falaleev Maxim
 * @email max@studio107.ru
 *
 * @version 1.0
 * @company Studio107
 * @site http://studio107.ru
 * @date 03/04/15 15:54
 */
class ApiController extends ApiBaseController
{
    public function actionIndex()
    {
        $regions = [];
        $models = Region::objects()->all();
        foreach ($models as $region) {
            $regions[] = $region->toArray();
        }
        $site = Mindy::app()->getModule('Sites')->getSite();
        if ($site === null) {
            $site = Site::objects()->asArray()->get(['pk' => 1]);
        }
        echo $this->json([
            'site' => $site,
            'regions' => $regions,
        ]);
        Mindy::app()->end();
    }
}
