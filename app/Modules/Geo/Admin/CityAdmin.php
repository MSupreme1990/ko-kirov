<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Geo\Admin;

use Modules\Admin\Components\ModelAdmin;
use Modules\Geo\Models\City;

class CityAdmin extends ModelAdmin
{
    public function getColumns()
    {
        return ['name', 'country', 'region', 'is_popular'];
    }

    public function getSearchFields()
    {
        return ['name'];
    }

    /**
     * @return \Mindy\Orm\Model
     */
    public function getModel()
    {
        return new City();
    }
}
