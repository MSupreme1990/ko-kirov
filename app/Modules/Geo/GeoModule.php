<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Geo;

use Mindy\Base\Module;

class GeoModule extends Module
{
    public function getMenu()
    {
        return [
            'name' => $this->getName(),
            'items' => [
                [
                    'name' => self::t('Country'),
                    'adminClass' => 'CountryAdmin',
                ],
                [
                    'name' => self::t('Region'),
                    'adminClass' => 'RegionAdmin',
                ],
                [
                    'name' => self::t('City'),
                    'adminClass' => 'CityAdmin',
                ],
            ],
        ];
    }
}
