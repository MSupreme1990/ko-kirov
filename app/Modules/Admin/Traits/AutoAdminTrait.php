<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Admin\Traits;

use DirectoryIterator;

trait AutoAdminTrait
{
    /**
     * @return array
     */
    public function getMenu()
    {
        /* @var $this \Mindy\Base\BaseModule */
        $adminDir = $this->getBasePath().'/Admin';
        $items = [];
        if (is_dir($adminDir)) {
            foreach (new DirectoryIterator($adminDir) as $fileInfo) {
                /* @var $fileInfo \DirectoryIterator */
                if ($fileInfo->isDot()) {
                    continue;
                }
                $className = str_replace('.php', '', $fileInfo->getFilename());
                $class = strtr('Modules\\{module}\\Admin\\{class}', [
                    '{module}' => $this->getId(),
                    '{class}' => $className,
                ]);
                /* @var $admin \Modules\Admin\Components\ModelAdmin|\Modules\Admin\Components\NestedAdmin */
                $admin = new $class();

                $items[] = [
                    'name' => $admin->getVerboseName(),
                    'adminClass' => $className,
                ];
            }
        }

        return $items ? [
            'name' => $this->getName(),
            'items' => $items,
        ] : [];
    }
}
