<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    'Module' => 'Модуль',
    'Remove' => 'Удалить',
    'Last changes' => 'Последние изменения',
    'Lately, nothing interesting happened.' => 'За последнее время ничего интересного не произошло.',
    'Search' => 'Поиск',
    'Create' => 'Создать',
    'Choose action' => 'Выберите действие',
    'Submit' => 'Выбрать',
    'Save' => 'Сохранить',
    'Save and continue' => 'Сохранить и продолжить',
    'Save and create' => 'Сохранить и создать',
    'Information about' => 'Информация:',
    'Print' => 'Печать',
    'Number / identifier' => 'Номер / идентификатор',
    'Yes' => 'Да',
    'No' => 'Нет',
    'Update' => 'Редактировать',
    'View at site' => 'Посмотреть на сайте',
    'Do you really want to delete this item?' => 'Вы действительно хотите удалить данный объект?',
    'Delete' => 'Удалить',
    'Open site' => 'Открыть сайт',
    'Settings' => 'Настройки',
    'Hide / show sidebar' => 'Скрыть / показать панель',
    'Hide sidebar' => 'Скрыть / показать панель',
    'User actions' => 'Действия пользователей',
    'Actions' => 'Действия',
    'Home' => 'Главная',
    'Admin' => 'Администрирование',
    'Users online' => 'Пользователей онлайн',
    'Version' => 'Версия',
    'License key' => 'Лицензионный ключ',
    'Free version' => 'Бесплатная версия',
    'Current version' => 'Текущая версия',
    'Available version' => 'Доступная версия',
    'Information about available version unavailable. Please try again later.' => 'Информация о доступной версии недоступна. Пожалуйста, попробуйте позже.',
    'News' => 'Новости',
    'All news' => 'Все новости',
    'Go' => 'Go',
    'Dashboard' => 'Рабочий стол',
    'User actions log' => 'Действия пользователей',
    '{model} [[{url}|{name}]] was updated' => 'Обновлено: {model} [[{url}|{name}]]',
    '{model} [[{url}|{name}]] was created' => 'Создано: {model} [[{url}|{name}]]',
    '{model} [[{url}|{name}]] was deleted' => 'Удалено: {model} [[{url}|{name}]]',
];
