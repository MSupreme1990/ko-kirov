<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Admin\Dashboard;

use Modules\Admin\Components\Dashboard;
use Modules\Core\Models\UserLog;
use Modules\Core\Tables\UserLogTable;

class UserLogDashboard extends Dashboard
{
    public function getData()
    {
        $qs = UserLog::objects()->order(['-created_at']);
        $table = new UserLogTable($qs);

        return [
            'table' => $table,
        ];
    }

    public function getTemplate()
    {
        return 'admin/dashboard/user_log.html';
    }
}
