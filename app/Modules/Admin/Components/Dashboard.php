<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Admin\Components;

use Mindy\Utils\RenderTrait;

abstract class Dashboard
{
    use RenderTrait;

    abstract public function getTemplate();

    public function __toString()
    {
        return (string) $this->renderTemplate($this->getTemplate(), $this->getData());
    }

    public function getData()
    {
        return [
        ];
    }

    public function render()
    {
        echo $this->renderTemplate($this->getTemplate(), $this->getData());
    }
}
