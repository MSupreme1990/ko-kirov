<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Admin;

use Mindy\Base\Module;
use Mindy\Helper\Alias;

class AdminModule extends Module
{
    protected $dashboards = [];

    public function getDashboardClasses()
    {
        if (empty($this->dashboards)) {
            $path = Alias::get('application.config.dashboard').'.php';
            if (is_file($path)) {
                $this->dashboards = include_once $path;
            }
        }

        return $this->dashboards;
    }
}
