<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Meta\Middleware;

use Mindy\Base\Mindy;
use Mindy\Http\Request;
use Mindy\Middleware\Middleware;
use Modules\Meta\Models\Meta;

class MetaMiddleware extends Middleware
{
    public function processRequest(Request $request)
    {
        if ($meta = Meta::objects()->filter(['url' => $request->http->requestUri])->limit(1)->get()) {
            $metaInfo = [
                'title' => $meta->title,
                'keywords' => $meta->keywords,
                'description' => $meta->description,
                'canonical' => $meta->getCanonicalUrl(),
            ];

            $controller = Mindy::app()->controller;

            foreach ($metaInfo as $key => $value) {
                $controller->set[ucfirst($key)] = $value;
            }
        }
    }
}
