<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Meta\Admin;

use Mindy\Base\Mindy;
use Modules\Admin\Components\ModelAdmin;
use Modules\Meta\Forms\MetaForm;
use Modules\Meta\MetaModule;
use Modules\Meta\Models\Meta;

class MetaAdmin extends ModelAdmin
{
    public function getColumns()
    {
        $columns = ['title', 'url'];
        if (Mindy::app()->getModule('Meta')->onSite) {
            $columns[] = 'site';
        }

        return $columns;
    }

    public function getSearchFields()
    {
        return ['title', 'url', 'canonical'];
    }

    public function getModel()
    {
        return new Meta();
    }

    public function getCreateForm()
    {
        return MetaForm::className();
    }

    public function getVerboseName()
    {
        return MetaModule::t('Meta information');
    }

    public function getVerboseNamePlural()
    {
        return MetaModule::t('Meta information');
    }
}
