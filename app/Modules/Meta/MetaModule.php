<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Meta;

use Mindy\Base\Mindy;
use Mindy\Base\Module;

class MetaModule extends Module
{
    public $onSite;

    public function init()
    {
        if (is_null($this->onSite)) {
            $this->onSite = Mindy::app()->hasModule('Sites');
        }
    }

    public static function preConfigure()
    {
        $tpl = Mindy::app()->template;
        $tpl->addHelper('meta', ['\Modules\Meta\Helpers\MetaHelper', 'getMeta']);
    }

    public function getMenu()
    {
        return [
            'name' => $this->getName(),
            'items' => [
                [
                    'name' => $this->getName(),
                    'adminClass' => 'MetaAdmin',
                ],
            ],
        ];
    }
}
