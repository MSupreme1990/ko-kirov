<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Meta\Forms;

use Mindy\Form\Fields\CharField;
use Mindy\Form\Fields\CheckboxField;
use Mindy\Form\Fields\TextAreaField;
use Mindy\Form\Fields\TextField;
use Mindy\Form\ModelForm;
use Modules\Meta\MetaModule;
use Modules\Meta\Models\Meta;

class MetaForm extends ModelForm
{
    public $exclude = ['is_custom'];

    public function getFields()
    {
        return [
            'is_custom' => [
                'class' => CheckboxField::className(),
                'label' => MetaModule::t('Is custom'),
            ],
            'title' => [
                'class' => TextField::className(),
                'label' => MetaModule::t('Title'),
                'hint' => 'Лимиты для Google 220 символов. Лимиты для Yandex 250 символов. Используйте большее кол-во символов на ваш страх и риск. Так же обратите внимание, что в данных полях не допускаются переносы строк и спец. символы',
            ],
            'canonical' => [
                'class' => CharField::className(),
                'label' => MetaModule::t('Canonical'),
            ],
            'description' => [
                'class' => TextAreaField::className(),
                'label' => MetaModule::t('Description'),
                'hint' => 'Лимиты для Google 200 символов. Лимиты для Yandex 200 символов. Используйте большее кол-во символов на ваш страх и риск. Так же обратите внимание, что в данных полях не допускаются переносы строк и спец. символы',
            ],
            'keywords' => [
                'class' => TextAreaField::className(),
                'label' => MetaModule::t('Keywords'),
                'hint' => 'Лимиты для Google 200 символов. Лимиты для Yandex 200 символов. Используйте большее кол-во символов на ваш страх и риск. Так же обратите внимание, что в данных полях не допускаются переносы строк и спец. символы',
            ],
        ];
    }

    public function getModel()
    {
        return new Meta();
    }
}
