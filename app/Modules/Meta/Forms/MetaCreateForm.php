<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Meta\Forms;

use Mindy\Form\Fields\CharField;
use Mindy\Form\Fields\CheckboxField;
use Mindy\Form\Fields\TextAreaField;
use Mindy\Form\Fields\TextField;
use Mindy\Form\ModelForm;
use Modules\Meta\MetaModule;
use Modules\Meta\Models\Meta;

class MetaCreateForm extends ModelForm
{
    public function getFields()
    {
        return [
            'is_custom' => [
                'class' => CheckboxField::className(),
                'label' => MetaModule::t('Is custom'),
            ],
            'title' => [
                'class' => TextField::className(),
                'label' => MetaModule::t('Title'),
            ],
            'canonical' => [
                'class' => CharField::className(),
                'label' => MetaModule::t('Canonical'),
            ],
            'description' => [
                'class' => TextAreaField::className(),
                'label' => MetaModule::t('Description'),
            ],
            'keywords' => [
                'class' => TextAreaField::className(),
                'label' => MetaModule::t('Keywords'),
            ],
        ];
    }

    public function getModel()
    {
        return new Meta();
    }
}
