<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return  [
  'Meta information' => 'Метаинформация',
  'Is custom' => 'Указать вручную',
  'Title' => 'Заголовок',
  'Description' => 'Описание',
  'Keywords' => 'Ключевые слова',
  'Meta' => 'Метаинформация',
  'If "Set manually" field was not set, data will be generated automatically' => 'Если поле "Указать вручную" не указано, то данные будут сгенерированы автоматически',
  'Url' => 'Ссылка',
  'Meta {url} was created' => 'Метаинформация {url} была добавлена',
  'Meta {url} was updated' => 'Метаинформация {url} была обновлена',
  'Meta {url} was deleted' => 'Метаинформация {url} была удалена',
  'meta information' => 'мета информация',
  'Meta custom' => 'Указать вручную',
  'Content type' => 'Content type',
  'Object identificator' => 'ID объекта',
  'Create time' => 'Время создания',
  'Update time' => 'Время обновления',
  'Add meta information' => 'Добавить метаинформацию',
  'Update meta information' => 'Редактировать метаинформацию',
];
