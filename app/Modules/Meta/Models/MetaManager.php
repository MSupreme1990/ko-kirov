<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Meta\Models;

use Mindy\Orm\Manager;
use Modules\Sites\Traits\SiteTrait;

class MetaManager extends Manager
{
    use SiteTrait;

    public function currentSite()
    {
        $this->filter(['site' => $this->getCurrentSite()]);

        return $this;
    }
}
