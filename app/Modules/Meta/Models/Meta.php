<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Meta\Models;

use Mindy\Base\Mindy;
use Mindy\Orm\Fields\BooleanField;
use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\ForeignField;
use Mindy\Orm\Fields\TextField;
use Mindy\Orm\Model;
use Modules\Meta\MetaModule;
use Modules\Sites\SitesModule;

class Meta extends Model
{
    public static function getFields()
    {
        $fields = [
            'is_custom' => [
                'class' => BooleanField::className(),
                'verboseName' => MetaModule::t('Is custom'),
                'helpText' => MetaModule::t('If "Set manually" field was not set, data will be generated automatically'),
            ],
            'title' => [
                'class' => TextField::className(),
                'length' => 200,
                'verboseName' => MetaModule::t('Title'),
                'helpText' => 'Лимиты для Google 220 символов. Лимиты для Yandex 250 символов. Используйте большее кол-во символов на ваш страх и риск. Так же обратите внимание, что в данных полях не допускаются переносы строк и спец. символы',
            ],
            'keywords' => [
                'class' => TextField::className(),
                'length' => 200,
                'verboseName' => MetaModule::t('Keywords'),
                'helpText' => 'Лимиты для Google 200 символов. Лимиты для Yandex 200 символов. Используйте большее кол-во символов на ваш страх и риск. Так же обратите внимание, что в данных полях не допускаются переносы строк и спец. символы',
            ],
            'description' => [
                'class' => TextField::className(),
                'length' => 200,
                'verboseName' => MetaModule::t('Description'),
                'helpText' => 'Лимиты для Google 200 символов. Лимиты для Yandex 200 символов. Используйте большее кол-во символов на ваш страх и риск. Так же обратите внимание, что в данных полях не допускаются переносы строк и спец. символы',
            ],
            'url' => [
                'class' => CharField::className(),
                'verboseName' => MetaModule::t('Url'),
                'null' => true,
            ],
            'canonical' => [
                'class' => CharField::className(),
                'verboseName' => MetaModule::t('Canonical'),
                'null' => true,
            ],
        ];

        if (Mindy::app()->hasModule('Sites')) {
            $fields['site'] = [
                'class' => ForeignField::className(),
                'modelClass' => Mindy::app()->getModule('Sites')->modelClass,
                'verboseName' => SitesModule::t('Site'),
//                'required' => true,
                'editable' => false,
                'null' => true,
            ];
        }

        return $fields;
    }

    public function beforeSave($owner, $isNew)
    {
        $sitesModule = Mindy::app()->getModule('Sites');
        if (($isNew || empty($owner->site)) && $sitesModule) {
            $owner->site = $sitesModule->getSite();
        }
    }

    public static function objectsManager($instance = null)
    {
        $className = get_called_class();

        return new MetaManager($instance ? $instance : new $className());
    }

    public function getCanonicalUrl()
    {
        if (!empty($this->canonical)) {
            return $this->canonical;
        }

        return $this->url;
    }
}
