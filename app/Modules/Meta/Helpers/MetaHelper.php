<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Meta\Helpers;

use Mindy\Base\Mindy;
use Mindy\Utils\RenderTrait;
use Modules\Core\Controllers\Controller;
use Modules\Meta\Models\Meta;

class MetaHelper
{
    use RenderTrait;

    public static function getMeta(Controller $controller)
    {
        $uri = Mindy::app()->request->http->requestUri;
        if (($pos = strpos($uri, '?')) !== false) {
            $uri = substr($uri, 0, $pos);
        }

        $qs = Meta::objects()->filter(['url' => $uri]);

        /*
        if (Mindy::app()->getModule('Meta')->onSite) {
            $qs = $qs->currentSite();
        }
        */

        $meta = $qs->limit(1)->get();
//        d($meta);
        if ($meta) {
            echo self::renderTemplate('meta/meta_helper.html', [
                'this' => $controller,
                'title' => self::postProcess($meta->title),
                'description' => self::postProcess($meta->description),
                'keywords' => self::postProcess($meta->keywords),
                'canonical' => self::postProcess($meta->getCanonicalUrl()),
                'site' => Mindy::app()->getModule('Sites')->getSite(),
            ]);
        } else {
            echo self::renderTemplate('meta/meta_helper.html', [
                'this' => $controller,
                'site' => Mindy::app()->getModule('Sites')->getSite(),
            ]);
        }
    }

    public static function postProcess($text)
    {
        $site = Mindy::app()->getModule('Sites')->getSite();
        if ($site) {
            $city = $site->city;

            return strtr($text, [
                '{{ geo.name }}' => $city->name,
                '{{ geo.name2 }}' => $city->name2,
            ]);
        }

        return $text;
    }
}
