<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Sitemap;

use Mindy\Base\Module;
use Mindy\Helper\Alias;

class SitemapModule extends Module
{
    public $enableHtmlVersion = false;

    public $sitemaps = [];

    public function getSitemaps()
    {
        if (empty($this->sitemaps)) {
            $path = Alias::get('application.config.sitemaps').'.php';
            if (is_file($path)) {
                $this->sitemaps = include_once $path;
            }
        }

        return $this->sitemaps;
    }

    public function getVersion()
    {
        return '1.0';
    }
}
