<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Sitemap\Controllers;

use Mindy\Base\Mindy;
use Modules\Core\Controllers\CoreController;
use Modules\Sitemap\Components\SitemapIndex;
use Modules\Sitemap\SitemapModule;

class SitemapController extends CoreController
{
    public function actionIndex()
    {
        $sitemapIndex = new SitemapIndex([
            'sitemaps' => Mindy::app()->getModule('sitemap')->getSitemaps(),
        ]);
        echo $sitemapIndex->render();
    }

    public function actionView($name)
    {
        $sitemap = $this->getSitemap($name);
        if ($sitemap === null) {
            $this->error(404);
        }
        echo $sitemap->render();
    }

    protected function getSitemap($name)
    {
        $sitemaps = Mindy::app()->getModule('sitemap')->getSitemaps();

        return isset($sitemaps[$name]) ? $sitemaps[$name] : null;
    }

    public function actionHtml()
    {
        if (!$this->getModule()->enableHtmlVersion) {
            $this->error(404);
        }
        $this->addTitle(SitemapModule::t('Sitemap'));
        $this->addBreadcrumb(SitemapModule::t('Sitemap'));
        echo $this->render('sitemap/index.html', [
            'sitemaps' => Mindy::app()->getModule('sitemap')->getSitemaps(),
        ]);
    }
}
