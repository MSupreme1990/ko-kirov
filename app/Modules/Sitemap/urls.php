<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    '/sitemap.xml' => [
        'name' => 'xml',
        'callback' => '\Modules\Sitemap\Controllers\SitemapController:index',
    ],
    '/sitemap-{name:[A-Za-z0-9-]+}.xml' => [
        'name' => 'view',
        'callback' => '\Modules\Sitemap\Controllers\SitemapController:view',
    ],
    '/sitemap-html' => [
        'name' => 'html',
        'callback' => '\Modules\Sitemap\Controllers\SitemapController:html',
    ],
];
