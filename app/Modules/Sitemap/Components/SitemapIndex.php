<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Sitemap\Components;

use Mindy\Base\Mindy;
use Mindy\Helper\Traits\Accessors;
use Mindy\Helper\Traits\Configurator;
use Mindy\Helper\Xml;

class SitemapIndex implements ISitemapIndex
{
    use Accessors, Configurator;

    public $sitemaps = [];

    public function getSitemaps()
    {
        $data = [
            '@attributes' => [
                'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
                'xmlns' => 'http://www.sitemaps.org/schemas/sitemap/0.9',
                'xsi:schemaLocation' => 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd',
            ],
        ];
        foreach ($this->sitemaps as $name => $sitemap) {
            $data[] = [
                'sitemap' => [
                    'loc' => Mindy::app()->request->http->getHostInfo().'/sitemap-'.ltrim($name, '/').'.xml',
                    'lastmod' => date(DATE_W3C),
                ],
            ];
        }

        return Xml::encode('sitemapindex', $data, MINDY_DEBUG);
    }

    public function render($header = true)
    {
        if ($header) {
            header('Content-Type: text/xml');
        }

        return $this->getSitemaps();
    }
}
