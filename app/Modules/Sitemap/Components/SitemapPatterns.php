<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Sitemap\Components;

use Mindy\Base\Mindy;
use Mindy\Router\CustomPatterns;

class SitemapPatterns extends CustomPatterns
{
    public function getSitemaps()
    {
        return Mindy::app()->getModule('sitemap')->getSitemaps();
    }

    /**
     * @return array
     */
    public function getPatterns()
    {
        $patterns = [];
        $sitemaps = $this->getSitemaps();
        foreach ($sitemaps as $url => $sitemap) {
            $patterns[] = [
                'path' => array_shift($url),
                'name_prefix' => !empty($this->namespace) ? $this->namespace.'.' : '',
                'values' => [
                    'controller' => '\Modules\Sitemap\Controllers\SitemapController',
                    'action' => 'view',
                ],
            ];
        }

        return $patterns;
    }
}
