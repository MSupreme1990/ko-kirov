<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\User\Forms;

use Mindy\Form\ModelForm;
use Modules\User\Models\User;
use Modules\User\UserModule;

/**
 * Class UserForm
 */
class UserForm extends ModelForm
{
    public function getFieldsets()
    {
        return [
            UserModule::t('Main information') => ['username', 'email', 'is_staff', 'is_superuser', 'is_active'],
            UserModule::t('Extra information') => ['groups', 'permissions'],
        ];
    }

    public function getModel()
    {
        return new User();
    }
}
