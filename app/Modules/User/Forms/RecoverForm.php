<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\User\Forms;

use Mindy\Base\Mindy;
use Mindy\Form\Fields\CharField;
use Mindy\Form\Form;
use Modules\User\Models\User;
use Modules\User\UserModule;

/**
 * Class RecoverForm
 */
class RecoverForm extends Form
{
    public function getFields()
    {
        return [
            'username_or_email' => [
                'class' => CharField::className(),
                'label' => UserModule::t('Email'),
            ],
        ];
    }

    public function send()
    {
        $model = User::objects()->filter(
            [strpos($this->username_or_email->getValue(), '@') ? 'email' : 'username' => $this->username_or_email->getValue()]
        )->get();

        if ($model && $model->objects()->changeActivationKey()) {
            $app = Mindy::app();
            $recoverUrl = $app->urlManager->reverse('user.recover_activate', [
                'key' => $model->activation_key,
            ]);

            return $app->mail->fromCode('user.recover', $model->email, [
                'data' => $model,
                'username' => $model->username,
                'site' => $app->getModule('Sites')->getSite(),
                'activation_url' => $app->request->http->absoluteUrl($recoverUrl),
            ]);
        }

        return false;
    }
}
