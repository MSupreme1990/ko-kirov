<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\User\Forms;

use Mindy\Form\Fields\Select2Field;
use Mindy\Form\ModelForm;
use Modules\User\Models\Group;
use Modules\User\UserModule;

/**
 * Class GroupForm
 */
class GroupForm extends ModelForm
{
    public function getFieldsets()
    {
        return [
            UserModule::t('Main information') => [
                'name', 'description',
            ],
            UserModule::t('Settings') => [
                'is_visible', 'is_locked',
            ],
            UserModule::t('Permissions') => [
                'permissions',
            ],
        ];
    }

    public function getFields()
    {
        return [
            'permissions' => [
                'class' => Select2Field::className(),
            ],
        ];
    }

    public function getModel()
    {
        return new Group();
    }
}
