<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\User\PasswordHasher;

use Mindy\Base\Mindy;

class BitrixPasswordHasher implements IPasswordHasher
{
    /**
     * @return string random
     */
    public function generateSalt()
    {
        return Mindy::app()->getSecurityManager()->generateRandomString(8, true);
    }

    /**
     * @param $password string
     *
     * @return string
     */
    public function hashPassword($password)
    {
        $salt = $this->generateSalt();

        return $salt.md5($salt.$password);
    }

    /**
     * @param $password string
     * @param $hash string
     *
     * @return string
     */
    public function verifyPassword($password, $hash)
    {
        $salt = substr($hash, 0, 8);

        return $hash == $salt.md5($salt.$password);
    }
}
