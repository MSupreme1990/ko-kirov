<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\User\PasswordHasher;

use Mindy\Helper\Password;

class MindyPasswordHasher implements IPasswordHasher
{
    /**
     * @return string random
     */
    public function generateSalt()
    {
        return Password::generateSalt();
    }

    /**
     * @param $password string
     *
     * @return string
     */
    public function hashPassword($password)
    {
        return Password::hashPassword($password);
    }

    /**
     * @param string $password
     * @param string $hash
     *
     * @return bool|string
     */
    public function verifyPassword($password, $hash)
    {
        return Password::verifyPassword($password, $hash);
    }
}
