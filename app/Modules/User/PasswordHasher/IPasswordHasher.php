<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\User\PasswordHasher;

/**
 * Interface IHashStrategy
 */
interface IPasswordHasher
{
    /**
     * @return string random
     */
    public function generateSalt();

    /**
     * @param $password string
     *
     * @return string
     */
    public function hashPassword($password);

    /**
     * @param $password string
     * @param $hash string
     *
     * @return string
     */
    public function verifyPassword($password, $hash);
}
