<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\User\Components;

use Mindy\Base\Mindy;
use Modules\Core\Models\UserLog;

/**
 * Class UserActionsTrait
 */
trait UserActionsTrait
{
    public static function recordAction($message, $module)
    {
        if (MINDY_TEST === false) {
            $app = Mindy::app();
            UserLog::objects()->create([
                'message' => $message,
                'module' => $module,
                'ip' => $app->getUser()->getIp(),
                'user' => $app->getUser()->getIsGuest() ? null : $app->getUser(),
            ]);
        }
    }
}
