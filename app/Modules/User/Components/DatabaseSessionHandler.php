<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\User\Components;

use Exception;
use Mindy\Base\Mindy;
use Mindy\Helper\Traits\Configurator;
use Modules\User\Models\Session;
use SessionHandler;

/**
 * Class DatabaseSessionHandler
 */
class DatabaseSessionHandler extends SessionHandler
{
    use Configurator;

    public $key;

    public $encrypt = false;

    public function init()
    {
        $this->key = Mindy::getVersion();
    }

    /**
     * @return bool|void
     */
    public function close()
    {
        return true;
    }

    /**
     * Session open handler.
     * Do not call this method directly.
     *
     * @param string $save_path  session save path
     * @param string $session_id session name
     *
     * @return bool whether session is opened successfully
     */
    public function open($save_path, $session_id)
    {
        return true;
    }

    /**
     * Session read handler.
     * Do not call this method directly.
     *
     * @param string $session_id session ID
     *
     * @return string the session data
     */
    public function read($session_id)
    {
        $session = Session::objects()->filter([
            'expire__gte' => time(),
            'id' => $session_id,
        ])->limit(1)->get();

        $data = $session === null ? '' : $session->data;

        if ($this->encrypt) {
            $data = mcrypt_decrypt(MCRYPT_3DES, $this->key, $data, MCRYPT_MODE_ECB);
        }

        return $data;
    }

    /**
     * Session write handler.
     * Do not call this method directly.
     *
     * @param string $session_id   session ID
     * @param string $session_data session data
     *
     * @throws Exception
     *
     * @return bool whether session write is successful
     */
    public function write($session_id, $session_data)
    {
        // exception must be caught in session write handler
        // http://us.php.net/manual/en/function.session-set-save-handler.php
        $expire = time() + (int) ini_get('session.gc_maxlifetime');

        if ($this->encrypt) {
            $session_data = mcrypt_encrypt(MCRYPT_3DES, $this->key, $session_data, MCRYPT_MODE_ECB);
        }

        $session = Session::objects()->limit(1)->get(['id' => $session_id]);
        if ($session === null) {
            $session = new Session([
                'id' => $session_id,
                'data' => $session_data,
                'expire' => $expire,
            ]);
            if ($session->save() === false) {
                throw new Exception("Can't create session");
            }
        } else {
            Session::objects()->filter(['id' => $session_id])->update([
                'data' => $session_data,
                'expire' => $expire,
            ]);
        }

        return true;
    }

    /**
     * Session destroy handler.
     * Do not call this method directly.
     *
     * @param string $session_id session ID
     *
     * @return bool whether session is destroyed successfully
     */
    public function destroy($session_id)
    {
        Session::objects()->filter(['id' => $session_id])->delete();

        return true;
    }

    /**
     * Session GC (garbage collection) handler.
     * Do not call this method directly.
     *
     * @param int $maxlifetime the number of seconds after which data will be seen as 'garbage' and cleaned up
     *
     * @return bool whether session is GCed successfully
     */
    public function gc($maxlifetime)
    {
        Session::objects()->filter(['expire__lt' => time()])->delete();

        return true;
    }
}
