<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\User\Models;

use Mindy\Orm\Fields\HasManyField;

/**
 * Class User
 *
 * @method static \Modules\User\Models\UserManager objects($instance = null)
 */
class User extends UserBase
{
    public static function getFields()
    {
        return array_merge(parent::getFields(), [
            'keys' => [
                'class' => HasManyField::className(),
                'modelClass' => Key::className(),
                'editable' => false,
            ],
        ]);
    }

    public function getAbsoluteUrl()
    {
        return $this->reverse('user:view', ['username' => $this->username]);
    }
}
