<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\User\Models;

use Mindy\Orm\Fields\ForeignField;
use Mindy\Orm\Fields\IntField;
use Mindy\Orm\Model;
use Modules\User\Components\Permissions;
use Modules\User\UserModule;

/**
 * Class PermissionObjectThrough
 */
class PermissionObjectThrough extends Model
{
    public static function getFields()
    {
        return [
            'owner_id' => [
                'class' => IntField::className(),
                'verboseName' => UserModule::t('Owner'),
            ],
            'type' => [
                'class' => IntField::className(),
                'choices' => [
                    Permissions::TYPE_USER,
                    Permissions::TYPE_GROUP,
                ],
                'verboseName' => UserModule::t('Type'),
            ],
            'permission' => [
                'class' => ForeignField::className(),
                'modelClass' => PermissionObject::className(),
                'verboseName' => UserModule::t('Permission'),
            ],
        ];
    }
}
