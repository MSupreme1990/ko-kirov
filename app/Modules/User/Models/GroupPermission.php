<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\User\Models;

use Mindy\Orm\Fields\ForeignField;
use Mindy\Orm\Model;
use Modules\User\UserModule;

/**
 * Class GroupPermission
 */
class GroupPermission extends Model
{
    public static function getFields()
    {
        return [
            'user_group' => [
                'class' => ForeignField::className(),
                'modelClass' => Group::className(),
                'verboseName' => UserModule::t('Group'),
            ],
            'permission' => [
                'class' => ForeignField::className(),
                'modelClass' => Permission::className(),
                'verboseName' => UserModule::t('Permission'),
            ],
        ];
    }
}
