<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\User\Models;

use Mindy\Orm\Fields\ForeignField;
use Mindy\Orm\Model;
use Modules\User\UserModule;

/**
 * Class UserPermission
 */
class UserPermission extends Model
{
    public static function getFields()
    {
        return [
            'user' => [
                'class' => ForeignField::className(),
                'modelClass' => User::className(),
                'verboseName' => UserModule::t('User'),
            ],
            'permission' => [
                'class' => ForeignField::className(),
                'modelClass' => Permission::className(),
                'verboseName' => UserModule::t('Permission'),
            ],
        ];
    }
}
