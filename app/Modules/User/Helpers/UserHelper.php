<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\User\Helpers;

use Mindy\Utils\RenderTrait;
use Modules\User\Forms\LoginForm;

class UserHelper
{
    use RenderTrait;

    public static function render($request, $template = 'user/_login.html')
    {
        return self::renderTemplate($template, [
            'form' => new LoginForm(),
            'request' => $request,
        ]);
    }
}
