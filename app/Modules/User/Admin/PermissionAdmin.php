<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\User\Admin;

use Modules\Admin\Components\ModelAdmin;
use Modules\User\Forms\PermissionForm;
use Modules\User\Models\Permission;

/**
 * Class PermissionAdmin
 */
class PermissionAdmin extends ModelAdmin
{
    public function getColumns()
    {
        return [
            'code',
            'name',
        ];
    }

    public function getCreateForm()
    {
        return PermissionForm::className();
    }

    public function getModel()
    {
        return new Permission();
    }
}
