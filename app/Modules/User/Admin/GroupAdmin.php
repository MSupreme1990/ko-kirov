<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\User\Admin;

use Modules\Admin\Components\ModelAdmin;
use Modules\User\Forms\GroupForm;
use Modules\User\Models\Group;

/**
 * Class GroupAdmin
 */
class GroupAdmin extends ModelAdmin
{
    public function getColumns()
    {
        return ['id', 'name', 'is_locked', 'is_visible'];
    }

    public function getCreateForm()
    {
        return GroupForm::className();
    }

    public function getModel()
    {
        return new Group();
    }
}
