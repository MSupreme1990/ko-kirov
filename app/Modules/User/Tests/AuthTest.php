<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\User\Tests;

use Modules\User\Models\User;

class AuthTest extends UserBaseTest
{
    public function tearDown()
    {
        parent::tearDown();
        $this->app->auth->logout();
    }

    public function testLoginAndLogout()
    {
        /** @var \Modules\User\Components\Auth $auth */
        $auth = $this->app->auth;
        $username = 'foo';
        $password = 'bar';
        $user = User::objects()->createUser($username, $password, 'admin@admin.com');

        $this->assertTrue($auth->getIsGuest());
        $this->assertTrue($auth->login($user));
        $this->assertFalse($auth->getIsGuest());

        $auth->logout();
        $this->assertTrue($auth->getIsGuest());
    }
}
