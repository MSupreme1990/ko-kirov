<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\User\Commands;

use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\DateField;
use Mindy\Orm\Fields\EmailField;
use Mindy\Orm\Fields\TextField;
use Mindy\Orm\Model;

class BitrixUser extends Model
{
    public static function getFields()
    {
        return [
            'LOGIN' => [
                'class' => CharField::className(),
            ],
            'PASSWORD' => [
                'class' => CharField::className(),
            ],
            'CHECKWORD' => [
                'class' => CharField::className(),
            ],
            'NAME' => [
                'class' => CharField::className(),
            ],
            'LAST_NAME' => [
                'class' => CharField::className(),
            ],
            'SECOND_NAME' => [
                'class' => CharField::className(),
            ],
            'EMAIL' => [
                'class' => EmailField::className(),
            ],
            'EXTERNAL_AUTH_ID' => [
                'class' => CharField::className(),
            ],
            'XML_ID' => [
                'class' => CharField::className(),
            ],
            'PERSONAL_COUNTRY' => [
                'class' => CharField::className(),
            ],
            'PERSONAL_PHONE' => [
                'class' => CharField::className(),
            ],
            'PERSONAL_MOBILE' => [
                'class' => CharField::className(),
            ],
            'PERSONAL_CITY' => [
                'class' => CharField::className(),
            ],
            'ACTIVE' => [
                'class' => CharField::className(),
            ],
            'PERSONAL_BIRTHDAY' => [
                'class' => DateField::className(),
            ],
            'PERSONAL_NOTES' => [
                'class' => TextField::className(),
            ],
            'PERSONAL_STREET' => [
                'class' => TextField::className(),
            ],
        ];
    }

    public static function tableName()
    {
        return 'b_user';
    }

    public function getIsActive()
    {
        return $this->ACTIVE == 'Y';
    }
}
