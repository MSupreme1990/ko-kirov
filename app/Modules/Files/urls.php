<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    '/' => [
        'name' => 'index',
        'callback' => '\Modules\Files\Controllers\FilesController:index',
    ],
    '/api/' => [
        'name' => 'api',
        'callback' => '\Modules\Files\Controllers\FilesController:api',
    ],
    '/upload/' => [
        'name' => 'upload',
        'callback' => '\Modules\Files\Controllers\FilesController:upload',
    ],
    /*
     * Actions for FilesField
     */
    '/files_upload/' => [
        'name' => 'files_upload',
        'callback' => '\Modules\Files\Controllers\UploadController:upload',
    ],
    '/files_sort/' => [
        'name' => 'files_sort',
        'callback' => '\Modules\Files\Controllers\UploadController:sort',
    ],
    '/files_delete/' => [
        'name' => 'files_delete',
        'callback' => '\Modules\Files\Controllers\UploadController:delete',
    ],
];
