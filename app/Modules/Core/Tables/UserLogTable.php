<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Core\Tables;

use Mindy\Table\Columns\RawColumn;
use Mindy\Table\Table;
use Modules\Core\CoreModule;

class UserLogTable extends Table
{
    public function getColumns()
    {
        return [
            'created_at' => [
                'class' => RawColumn::className(),
                'title' => CoreModule::t('Created at'),
            ],
            'message' => [
                'class' => RawColumn::className(),
                'title' => CoreModule::t('Message'),
            ],
            'ip' => [
                'class' => RawColumn::className(),
                'title' => CoreModule::t('Ip'),
            ],
        ];
    }

    public function render()
    {
        return strtr($this->template, [
            '{html}' => $this->getHtmlAttributes(),
            '{caption}' => $this->renderCaption(),
            '{header}' => $this->renderHeader(),
            '{footer}' => $this->renderFooter(),
            '{body}' => $this->renderBody(),
            '{pager}' => $this->getPager()->render('admin/admin/_pager.html'),
        ]);
    }
}
