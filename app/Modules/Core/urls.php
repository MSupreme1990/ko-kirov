<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    '/modules/' => [
        'name' => 'module_list',
        'callback' => '\Modules\Core\Controllers\ModulesController:index',
    ],
    '/modules/install/{name:\w+}' => [
        'name' => 'module_install',
        'callback' => '\Modules\Core\Controllers\ModulesController:install',
    ],
    '/modules/update/{name:\w+}/{version}' => [
        'name' => 'module_update',
        'callback' => '\Modules\Core\Controllers\ModulesController:update',
    ],
    '/modules/{name:\w+}' => [
        'name' => 'module_view',
        'callback' => '\Modules\Core\Controllers\ModulesController:view',
    ],
    '/settings' => [
        'name' => 'settings',
        'callback' => '\Modules\Core\Controllers\SettingsController:index',
    ],
    '/help/online' => [
        'name' => 'help-online',
        'callback' => '\Modules\Core\Controllers\HelpController:index',
    ],
];
