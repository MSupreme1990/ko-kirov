<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Core\Commands;

use Exception;
use Mindy\Console\ConsoleCommand;

class TreeCommand extends ConsoleCommand
{
    public function actionRecover($className)
    {
        if (!class_exists($className)) {
            throw new Exception('Unknown class');
        }

        $i = 0;
        $skip = [];
        while ($className::objects()->filter(['lft__isnull' => true])->count() != 0) {
            ++$i;
            $fixed = 0;
            echo 'Iteration: '.$i.PHP_EOL;
            $models = $className::objects()
                ->exclude(['pk__in' => $skip])
                ->filter(['lft__isnull' => true])
                ->order(['parent_id'])->all();
            foreach ($models as $model) {
                $model->lft = $model->rgt = $model->level = $model->root = null;
                if ($model->saveRebuild()) {
                    $skip[] = $model->pk;
                    ++$fixed;
                }
                echo '.';
            }
            echo PHP_EOL;
            echo 'Fixed: '.$fixed.PHP_EOL;
        }
    }
}
