<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Core\Models;

use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\EmailField;
use Mindy\Orm\Fields\ImageField;
use Modules\Core\CoreModule;

class CoreSettings extends SettingsModel
{
    public function __toString()
    {
        return (string) $this->t('Core settings');
    }

    public static function getFields()
    {
        return [
            'logo' => [
                'class' => ImageField::className(),
                'null' => true,
                'verboseName' => CoreModule::t('Logo', [], 'settings'),
            ],
            'sitename' => [
                'class' => CharField::className(),
                'null' => true,
                'verboseName' => CoreModule::t('Sitename', [], 'settings'),
            ],
            'email_owner' => [
                'class' => EmailField::className(),
                'null' => true,
                'verboseName' => CoreModule::t('Email site owner', [], 'settings'),
            ],
        ];
    }
}
