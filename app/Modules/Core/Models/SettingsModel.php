<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Core\Models;

use Mindy\Orm\Manager;
use Mindy\Orm\Model;

abstract class SettingsModel extends Model
{
    /**
     * @param bool $asArray
     *
     * @return \Mindy\Orm\Orm
     */
    public static function getInstance($asArray = false)
    {
        $className = get_called_class();
        $manager = new Manager(new $className());
        list($instance, $created) = $manager->asArray($asArray)->getOrCreate(['id' => 1]);

        return $instance;
    }

    public static function t($str, $params = [], $dic = 'settings')
    {
        return self::getModule()->t($str, $params, $dic);
    }
}
