<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Core\Models;

use Mindy\Orm\Manager;

class MigrationManager extends Manager
{
    public function last()
    {
        $this->limit(1)->offset(0)->order(['-timestamp']);

        return $this;
    }
}
