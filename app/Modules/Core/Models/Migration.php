<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Core\Models;

use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\IntField;
use Mindy\Orm\Model;

/**
 * Class Core
 *
 * @method static \Modules\Core\Models\MigrationManager objects($instance = null)
 */
class Migration extends Model
{
    public static function getFields()
    {
        return [
            'module' => [
                'class' => CharField::className(),
                'editable' => false,
            ],
            'model' => [
                'class' => CharField::className(),
                'editable' => false,
            ],
            'timestamp' => [
                'class' => IntField::className(),
                'editable' => false,
            ],
        ];
    }

    public static function objectsManager($instance = null)
    {
        $className = get_called_class();

        return new MigrationManager($instance ? $instance : new $className());
    }
}
