<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return  [
  'The requested page does not exist.' => 'Страница не найдена',
  'Invalid request. Please do not repeat this request again.' => 'Некорректный запрос. Пожалуйста, не повторяйте этот запрос снова',
  'You are not authorized to perform this action.' => 'Вы не авторизованы для выполнения этого действия',
  'Error' => 'Ошибка',
  'Forbidden' => 'Доступ ограничен',
];
