<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return  [
  'Today' => 'Сегодня',
  'Yesterday' => 'Вчера',
  'minutes1' => 'минуту',
  'minutes4' => 'минуты',
  'minutes5' => 'минут',
  'Just now' => 'Только что',
  '{minutes} {ending} ago' => '{minutes} {ending} назад',
];
