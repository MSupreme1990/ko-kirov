<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return  [
  'Sitename' => 'Название сайта',
  'Email site owner' => 'Email владельца сайта',
  'Core settings' => 'Основные настройки',
];
