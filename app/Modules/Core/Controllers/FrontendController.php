<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Core\Controllers;

/**
 * FrontendController class file.
 *
 * @deprecated use \Mindy\Base\CoreController
 *
 * @author Falaleev Maxim <max@studio107.com>
 *
 * @see http://studio107.ru/
 *
 * @copyright Copyright &copy; 2010-2012 Studio107
 * @license http://www.cms107.com/license/
 *
 * @since 1.1.1
 *
 * @version 1.0
 */
class FrontendController extends CoreController
{
}
