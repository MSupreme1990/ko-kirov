<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Core\Controllers;

use Modules\Core\CoreModule;

class HelpController extends BackendController
{
    public function actionIndex()
    {
        $this->addBreadcrumb(CoreModule::t('Help'));
        $this->addTitle(CoreModule::t('Help'));
        echo $this->render('core/help.html');
    }
}
