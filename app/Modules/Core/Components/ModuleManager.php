<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Core\Components;

use Mindy\Base\Mindy;
use Mindy\Helper\Alias;
use Mindy\Helper\FileHelper;

class ModuleManager
{
    /**
     * @param $name
     *
     * @return mixed
     */
    public static function install($name)
    {
        $core = Mindy::app()->getModule('Core');

        $hasModule = Mindy::app()->getModule($name);
        if ($hasModule) {
            return false;
        }
        $version = $core->update->install($name);
        $module = Mindy::app()->getModule($name);
        if ($version && $module) {
            $module->install();
        }

        return $version;
    }

    /**
     * @param $name
     * @param null $updateToVersion
     *
     * @return mixed
     */
    public static function update($name, $updateToVersion = null)
    {
        $core = Mindy::app()->getModule('Core');
        $module = Mindy::app()->getModule($name);
        $currentVersion = $module->getVersion();
        $version = $core->update->update($name, $currentVersion, $updateToVersion);
        if ($version && $module) {
            $module->update();
        }

        return $version;
    }

    /**
     * @param $name
     *
     * @return mixed
     */
    public static function delete($name)
    {
        $module = Mindy::app()->getModule($name);
        if ($module) {
            $module->delete();
            $path = $module->getModulePath();
        } else {
            $path = Alias::get('Modules.'.$name);
        }
        FileHelper::removeDirectory($path);

        return !is_dir($path);
    }
}
