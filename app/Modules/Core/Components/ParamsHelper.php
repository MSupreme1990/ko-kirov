<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Core\Components;

use Mindy\Base\Mindy;

/**
 * Class ParamsHelper
 */
class ParamsHelper
{
    /**
     * @param $name
     *
     * @return string|null
     */
    public static function get($name, $defaultValue = null)
    {
        $value = Mindy::app()->cache->get($name);
        if (!$value) {
            $value = self::fetchSetting($name);
        }

        return $value ? $value : $defaultValue;
    }

    /**
     * @param $name
     */
    protected static function fetchSetting($name)
    {
        if (substr_count($name, '.') != 2) {
            return null;
        }
        list($module, $model, $param) = explode('.', $name);
        $module = ucfirst($module);
        $model = ucfirst($model);
        /** @var \Modules\Core\Models\SettingsModel $className */
        $className = '\\Modules\\'.$module.'\\Models\\'.$model.'Settings';
        $data = $className::getInstance(true);

        return isset($data[$param]) ? $data[$param] : null;
    }
}
