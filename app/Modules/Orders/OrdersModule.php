<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Orders;

use Mindy\Base\Module;
use Modules\Ko\KoModule;

class OrdersModule extends Module
{
    public static function preConfigure()
    {
    }

    public function getMenu()
    {
        return [
            'name' => $this->getName(),
            'items' => [
                [
                    'name' => KoModule::t('Quick orders'),
                    'adminClass' => 'QuickOrderKoAdmin',
                ],
                [
                    'name' => KoModule::t('Orders'),
                    'adminClass' => 'CartOrderKoAdmin',
                ],
                [
                    'name' => self::t('Custom Orders'),
                    'adminClass' => 'CustomOrderAdmin',
                ],
            ],
        ];
    }
}
