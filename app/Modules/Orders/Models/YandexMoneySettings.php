<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Orders\Models;

use Mindy\Orm\Fields\CharField;
use Modules\Core\Models\SettingsModel;
use Modules\Orders\OrdersModule;

class YandexMoneySettings extends SettingsModel
{
    public function __toString()
    {
        return (string) $this->t('Настройка яндекс кошелька');
    }

    public static function getFields()
    {
        return [
            'shop_id' => [
                'class' => CharField::className(),
                'null' => true,
                'verboseName' => OrdersModule::t('Идентификатор магазина'),
            ],
            'sc_id' => [
                'class' => CharField::className(),
                'null' => true,
                'verboseName' => OrdersModule::t('Идентификатор витрины'),
            ],
        ];
    }
}
