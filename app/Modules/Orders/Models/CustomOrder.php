<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Orders\Models;

use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\DateTimeField;
use Mindy\Orm\Fields\DecimalField;
use Mindy\Orm\Fields\TextField;
use Mindy\Orm\Model;
use Modules\Catalog\CatalogModule;
use Modules\Catalog\Helpers\KeyHelper;
use Modules\Orders\Fields\JsonProductsField;
use Modules\Orders\OrdersModule;

class CustomOrder extends Model
{
    public static function getFields()
    {
        return [
            'code' => [
                'class' => CharField::className(),
                'verboseName' => OrdersModule::t('Code'),
                'null' => true,
            ],
            'name' => [
                'class' => CharField::className(),
                'verboseName' => OrdersModule::t('Name'),
            ],
            'email' => [
                'class' => CharField::className(),
                'verboseName' => OrdersModule::t('Email'),
                'null' => true,
            ],
            'phone' => [
                'class' => CharField::className(),
                'verboseName' => OrdersModule::t('Phone'),
            ],
            'address' => [
                'class' => TextField::className(),
                'verboseName' => OrdersModule::t('Address'),
                'null' => true,
            ],
            'price' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'verboseName' => CatalogModule::t('Price RUB'),
                'null' => true,
            ],
            'products' => [
                'class' => JsonProductsField::className(),
                'verboseName' => OrdersModule::t('Products'),
            ],
            'created_at' => [
                'class' => DateTimeField::className(),
                'autoNowAdd' => true,
                'verboseName' => CatalogModule::t('Created time'),
                'editable' => false,
            ],
            'updated_at' => [
                'class' => DateTimeField::className(),
                'autoNow' => true,
                'verboseName' => CatalogModule::t('Updated time'),
                'editable' => false,
            ],
        ];
    }

    private function calcPrice()
    {
        $summ = 0;
        if (!empty($this->products)) {
            foreach ($this->products as $product) {
                $count = (int) $product['count'];

                if ($count <= 0) {
                    $count = 1;
                    $product['count'] = 1;
                }

                $summ += ($count * (float) $product['price']);
            }

            $this->price = $summ;
        }
    }

    public function onBeforeInsertInternal()
    {
        if (empty($this->code)) {
            $this->code = KeyHelper::genUUID();
        }

        $this->calcPrice();

        parent::onBeforeInsertInternal();
    }

    public function onBeforeUpdateInternal()
    {
        $this->calcPrice();

        parent::onBeforeUpdateInternal();
    }

    public function getAbsoluteUrl()
    {
        return $this->reverse('orders:pay_view', ['code' => $this->code]);
    }
}
