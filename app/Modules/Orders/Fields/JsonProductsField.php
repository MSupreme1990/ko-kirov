<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Orders\Fields;

use Mindy\Orm\Fields\JsonField;
use Modules\Orders\Helpers\ArrayToTextTable;

class JsonProductsField extends JsonField
{
    public function toPlainTexTable()
    {
        $products = $this->getValue();
        $data = [];

        if (!empty($products)) {
            foreach ($products as $product) {
                $data[] = [
                    'Артикул' => $product['code'],
                    'Название' => $product['name'],
                    'Размер' => $product['size'],
                    'Количество' => $product['count'],
                    'Цена за ед. руб.' => $product['price'],
                ];
            }
        }

        $renderer = new ArrayToTextTable($data);
        $renderer->showHeaders(true);

        return $renderer->render(true);
    }

    public function toPlainText()
    {
        $products = $this->getValue();
        $data = '';

        if (!empty($products)) {
            foreach ($products as $product) {
                $size = (empty($product['size'])) ? '' : " ({$product['size']} размер)";
                $data .= <<<TEXT
[{$product['code']}] {$product['name']}{$size}, {$product['count']}шт. - {$product['price']} руб. за ед. \n
TEXT;
            }
        }

        return $data;
    }
}
