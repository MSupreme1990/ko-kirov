<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Orders\Controllers;

use Modules\Catalog\Models\Product;
use Modules\Core\Controllers\CoreController;
use Modules\Orders\Models\CustomOrder;

class CustomOrderController extends CoreController
{
    public function actionPayment($code)
    {
        $model = CustomOrder::objects()->filter(['code' => $code])->get();

        if (!$model) {
            $this->error(404);
        }

        $pId = [];
        foreach ($model->products as $product) {
            $pId[] = $product['id'];
        }

        $products = Product::objects()->filter(['id__in' => $pId])->all();

        echo $this->render('orders/payment.html', [
            'model' => $model,
            'products' => $products,
        ]);
    }
}
