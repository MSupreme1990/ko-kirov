<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Orders\Inputs;

use Mindy\Base\Mindy;
use Mindy\Form\Fields\CharField;

class PaymentLinkField extends CharField
{
    public $template = "<input type='hidden' value='{value}' id='{id}' name='{name}'{html}/>";

    public function render()
    {
        $label = $this->renderLabel();
        $input = $this->renderInput();
        $link = '';
        if (!$this->getForm()->getInstance()->getIsNewRecord()) {
            $baseUrl = Mindy::app()->getBaseUrl(true);
            $link = "<p>{$baseUrl}{$this->getForm()->getInstance()->getAbsoluteUrl()}</p>";
        }
        $hint = $this->hint ? $this->renderHint() : '';
        $errors = $this->renderErrors();

        return implode("\n", [$label, $link, $input, $hint, $errors]);
    }
}
