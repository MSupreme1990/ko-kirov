<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Orders\Inputs;

use Mindy\Form\Fields\Field;
use Mindy\Helper\JavaScript;
use Mindy\Helper\JavaScriptExpression;
use Mindy\Helper\Json;
use Mindy\Locale\Translate;
use Mindy\Utils\RenderTrait;
use Modules\Catalog\Models\Product;

class JsonProductsField extends Field
{
    use RenderTrait;

    private static $_products = null;

    public $options = [];

    public $pageSize = 10;

    public $modelField = 'name';

    public $placeholder = 'Please select value';

    public function init()
    {
        parent::init();

        if (!self::$_products) {
            self::$_products = [];

            foreach (Product::objects()->batch() as $products) {
                foreach ($products as $product) {
                    self::$_products[] = [
                        'id' => $product->id,
                        'text' => "[{$product->code}] {$product->name}",
                        'code' => $product->code,
                        'name' => $product->name,
                        'price' => $product->price_rub,
                    ];
                }
            }
        }
    }

    public function renderInput()
    {
        $options = [
            'width' => 'resolve',
            'allowClear' => true,
            'blurOnChange' => true,
            'openOnEnter' => false,
            'multiple' => false,
            'placeholder' => Translate::getInstance()->t('form', $this->placeholder),
            'minimumInputLength' => 2,
//            'ajax' => [
//                'url' => "",
//                'dataType' => 'json',
//                'quietMillis' => 250,
//                'data' => new JavaScriptExpression('function (term, page) {
//                    return {
//                        select2: term,
//                        page: page,
//                        field: "' . $this->getName() . '",
//                        pageSize: "' . $this->pageSize . '",
//                        modelField: "id"
//                    };
//                }'),
//                'results' => new JavaScriptExpression('function (data, page) {
//                    var more = (page * 30) < data.total_count;
//                    return {
//                        results: data.items,
//                        more: more
//                    };
//                }'),
//            ],
            'escapeMarkup' => new JavaScriptExpression('function (m) {
                return m;
            }'),
        ];

        $data = $this->getValue();
        if ($data && !is_array($data)) {
            $data = Json::decode($data);
        }
        $data = $data ?: [];

        return $this->renderTemplate('inputs/json_products_list_field.html', [
            'id' => $this->getHtmlId(),
            'name' => $this->getHtmlName(),
            'data' => $data,
            'options' => JavaScript::encode($options),
            'products' => JavaScript::encode(self::$_products),
            'encoded' => Json::encode($data),
        ]);
    }
}
