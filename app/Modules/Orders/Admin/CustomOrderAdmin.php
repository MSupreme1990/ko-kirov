<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Orders\Admin;

use Modules\Admin\Components\ModelAdmin;
use Modules\Orders\Forms\CustomOrderForm;
use Modules\Orders\Models\CustomOrder;

class CustomOrderAdmin extends ModelAdmin
{
    public function getColumns()
    {
        return [
            'name',
            'phone',
            'email',
            'price',
        ];
    }

    public function getCreateForm()
    {
        return CustomOrderForm::className();
    }

    /**
     * @return \Mindy\Orm\Model
     */
    public function getModel()
    {
        return new CustomOrder();
    }
}
