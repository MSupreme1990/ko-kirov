<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    'Code' => 'Код',
    'Email' => 'E-mail',
    'Phone' => 'Телефон',
    'Name' => 'Ф.И.О',
    'Price' => 'Сумма',
    'Price RUB' => 'Сумма',
    'Address' => 'Адрес',
    'Products' => 'Товары',

    'Custom Orders' => 'Заказы Yandex',
];
