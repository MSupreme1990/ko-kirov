<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    '/payment/{code:[a-zA-Z0-9]+}' => [
        'name' => 'pay_view',
        'callback' => '\Modules\Orders\Controllers\CustomOrderController:payment',
    ],
];
