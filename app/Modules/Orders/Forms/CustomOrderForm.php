<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Orders\Forms;

use Mindy\Form\Fields\CharField;
use Mindy\Form\ModelForm;
use Modules\Orders\Inputs\JsonProductsField;
use Modules\Orders\Inputs\PaymentLinkField;
use Modules\Orders\Models\CustomOrder;

class CustomOrderForm extends ModelForm
{
    public function getFields()
    {
        return [
            'code' => [
                'class' => PaymentLinkField::className(),
                'label' => 'Ссылка на оплату',
                'html' => [
                    'readonly' => 'readonly',
                ],
            ],
            'price' => [
                'class' => CharField::className(),
                'label' => 'Сумма',
                'html' => [
                    'readonly' => 'readonly',
                ],
            ],
            'products' => [
                'class' => JsonProductsField::className(),
            ],
        ];
    }

    public function getModel()
    {
        return new CustomOrder();
    }
}
