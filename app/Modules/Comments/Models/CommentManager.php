<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Comments\Models;

use Mindy\Orm\TreeManager;

class CommentManager extends TreeManager
{
    public function nospam()
    {
        $this->filter(['is_spam' => false]);

        return $this;
    }

    public function published()
    {
        d(1);
        $this->filter(['is_published' => true]);

        return $this;
    }
}
