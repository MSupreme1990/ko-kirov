<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Comments\Components\Akismet;

/**
 * Used internally by the Akismet class and to mock the Akismet anti spam service in
 * the unit tests.
 *
 * N.B. It is not necessary to implement this class to use the Akismet class.
 *
 * @name    AkismetRequestSender
 *
 * @version    0.5
 *
 * @author    Alex Potsides
 *
 * @see    http://www.achingbrain.net/
 */
interface AkismetRequestSender
{
    /**
     *  Sends the data to the remote host.
     *
     * @param string $host           the host to send/receive data
     * @param int    $port           the port on the remote host
     * @param string $request        the data to send
     * @param int    $responseLength The amount of data to read.  Defaults to 1160 bytes.
     *
     * @throws An exception is thrown if a connection cannot be made to the remote host
     * @returns    The server response
     */
    public function send($host, $port, $request, $responseLength = 1160);
}
