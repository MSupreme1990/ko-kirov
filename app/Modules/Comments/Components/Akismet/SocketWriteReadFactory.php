<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Comments\Components\Akismet;

/**
 * Used internally by the Akismet class and to mock the Akismet anti spam service in
 * the unit tests.
 *
 * N.B. It is not necessary to call this class directly to use the Akismet class.
 *
 * @name    SocketWriteReadFactory
 *
 * @version    0.5
 *
 * @author    Alex Potsides
 *
 * @see    http://www.achingbrain.net/
 */
class SocketWriteReadFactory implements AkismetRequestFactory
{
    public function createRequestSender()
    {
        return new SocketWriteRead();
    }
}
