<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Comments\Components\Akismet;

/**
 * Used internally by the Akismet class and to mock the Akismet anti spam service in
 * the unit tests.
 *
 * N.B. It is not necessary to implement this class to use the Akismet class.
 *
 * @name    AkismetRequestFactory
 *
 * @version    0.5
 *
 * @author    Alex Potsides
 *
 * @see    http://www.achingbrain.net/
 */
interface AkismetRequestFactory
{
    public function createRequestSender();
}
