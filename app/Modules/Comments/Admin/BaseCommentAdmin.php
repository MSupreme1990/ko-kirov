<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Comments\Admin;

use Modules\Admin\Components\ModelAdmin;

abstract class BaseCommentAdmin extends ModelAdmin
{
    public function getColumns()
    {
        return [
            'username',
            'email',
            'user',
            'created_at',
            'published_at',
            'is_spam',
            'is_published',
        ];
    }

    public function getVerboseName()
    {
        return $this->getModule()->t('comment');
    }

    public function getVerboseNamePlural()
    {
        return $this->getModule()->t('comments');
    }
}
