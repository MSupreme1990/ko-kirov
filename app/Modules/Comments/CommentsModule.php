<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Comments;

use Mindy\Base\Mindy;
use Mindy\Base\Module;

class CommentsModule extends Module
{
    /**
     * @var array akisment config
     */
    public $akisment = [];
    /**
     * @var bool
     */
    public $premoderate = true;
    /**
     * @var string
     */
    public $recaptchaPublicKey;
    /**
     * @var string
     */
    public $recaptchaSecretKey;

    public static function preConfigure()
    {
        $tpl = Mindy::app()->template;
        $tpl->addHelper('render_comments', ['\Modules\Comments\Helper\CommentHelper', 'render_comments']);
    }
}
