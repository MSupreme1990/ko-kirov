<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return  [
  'The comment is successfully added' => 'Комментарий успешно добавлен',
  'Your comment will appear on the website after being moderated' => 'Ваш комментарий появится на сайте после проверки модератором',
  'Username' => 'Имя',
  'Email' => 'Электронная почта',
  'User' => 'Пользователь',
  'Is spam' => 'Спам',
  'Is published' => 'Опубликовано',
  'Comment' => 'Комментарий',
  'Created at' => 'Время создания',
  'Updated at' => 'Время обновления',
  'Published at' => 'Время публикации',
  'Send comment' => 'Отправить комментарий',
  'Comments' => 'Комментарии',
];
