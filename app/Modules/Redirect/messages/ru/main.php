<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return  [
  'From url' => 'Откуда (url)',
  'To url' => 'Куда (url)',
  'Type' => 'Тип перенаправления',
  'Redirect {url} was created' => 'Перенаправление {url} было добавлено',
  'Redirect {url} was updated' => 'Перенаправление {url} было обновлено',
  'Redirect {url} was deleted' => 'Перенаправление {url} было удалено',
  'Redirect' => 'Перенаправление',
  'Redirects' => 'Перенаправления',
  'Create redirect' => 'Создать перенаправление',
  'Update redirect: {name}' => 'Редактирование перенаправления: {name}',
];
