<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Redirect\Middleware;

use Mindy\Http\Request;
use Mindy\Middleware\Middleware;
use Modules\Redirect\Models\Redirect;

/**
 * All rights reserved.
 *
 * @author Falaleev Maxim
 * @email max@studio107.ru
 *
 * @version 1.0
 * @company Studio107
 * @site http://studio107.ru
 * @date 14/04/14.04.2014 15:20
 */
class RedirectMiddleware extends Middleware
{
    public function processRequest(Request $request)
    {
        $pathInfo = $request->http->getPathInfo();
        if (!empty($pathInfo)) {
            $model = Redirect::objects()->filter([
                'from_url' => '/'.ltrim($pathInfo, '/'),
            ])->get();
            if ($model === null) {
                $model = Redirect::objects()->filter([
                    'from_url' => '/'.ltrim(strtok($pathInfo, '?'), '/').'*',
                ])->get();
                if ($model === null) {
                    return false;
                }
                $request->redirect($model->to_url, null, $model->type);
            } else {
                $request->redirect($model->to_url, null, $model->type);
            }
        }

        return false;
    }
}
