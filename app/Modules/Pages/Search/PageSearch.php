<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Pages\Search;

use Modules\Pages\Models\Page;
use Modules\Search\Components\SearchIndex;

class PageSearch extends SearchIndex
{
    public $fields = [
        'name',
        'slug',
        'content_short',
        'content',
        'published_at',
    ];

    /**
     * @return array
     */
    public function getParams()
    {
        return [
            '_all' => ['analyzer' => 'my_analyzer'],
        ];
    }

    public function getModel()
    {
        return new Page();
    }

    public function getQuerySet()
    {
        return $this->getModel()->objects()->published();
    }
}
