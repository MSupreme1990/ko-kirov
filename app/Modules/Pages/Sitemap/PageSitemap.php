<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Pages\Sitemap;

use Mindy\Base\Mindy;
use Modules\Sitemap\Components\Sitemap;

/**
 * Class PageSitemap
 */
class PageSitemap extends Sitemap
{
    public function getModelClass()
    {
        return Mindy::app()->getModule('Pages')->pagesModel;
    }

    public function getLastMod($data)
    {
        if (isset($data['updated_at'])) {
            $date = $data['updated_at'];
        } else {
            $date = $data['created_at'];
        }

        return $this->formatLastMod($date);
    }

    public function getQuerySet()
    {
        $qs = parent::getQuerySet();

        return $qs->order(['root', 'lft']);
    }

    public function getLoc($data)
    {
        $url = $data['url'];

        return $data['is_index'] ? $this->reverse('page:view', ['/']) : $this->reverse('page:view', [$url]);
    }
}
