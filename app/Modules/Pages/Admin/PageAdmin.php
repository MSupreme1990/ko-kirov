<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Pages\Admin;

use Mindy\Base\Mindy;
use Modules\Admin\Components\NestedAdmin;
use Modules\Pages\PagesModule;

/**
 * Class PageAdmin
 */
class PageAdmin extends NestedAdmin
{
    public $linkColumn = 'name';

    public function getColumns()
    {
        return ['name'];
    }

    public function getSearchFields()
    {
        return ['name', 'url'];
    }

    public function getCreateForm()
    {
        return Mindy::app()->getModule('Pages')->pagesForm;
    }

    public function getModel()
    {
        $modelClass = Mindy::app()->getModule('Pages')->pagesModel;

        return new $modelClass();
    }

    public function getNames($model = null)
    {
        return [
            PagesModule::t('Pages'),
            PagesModule::t('Create page'),
            PagesModule::t('Update page'),
        ];
    }

    public function getActions()
    {
        return array_merge(parent::getActions(), [
            'publish' => PagesModule::t('Publish'),
            'unpublish' => PagesModule::t('Unpublish'),
        ]);
    }

    public function unpublish(array $data = [])
    {
        $modelClass = Mindy::app()->getModule('Pages')->pagesModel;
        $modelClass::objects()->filter(['pk' => $data['models']])->update(['is_published' => false]);

        $this->redirect('admin.list', [
            'module' => $this->getModel()->getModuleName(),
            'adminClass' => $this->classNameShort(),
        ]);
    }

    public function publish(array $data = [])
    {
        $modelClass = Mindy::app()->getModule('Pages')->pagesModel;
        $modelClass::objects()->filter(['pk' => $data['models']])->update(['is_published' => true]);

        $this->redirect('admin.list', [
            'module' => $this->getModel()->getModuleName(),
            'adminClass' => $this->classNameShort(),
        ]);
    }
}
