<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Pages\Admin;

use Modules\Comments\Admin\BaseCommentAdmin;
use Modules\Pages\Models\Comment;

/**
 * Class CommentAdmin
 */
class CommentAdmin extends BaseCommentAdmin
{
    public function getColumns()
    {
        return ['id', 'username', 'email', 'created_at', 'is_published'];
    }

    /**
     * @return \Mindy\Orm\Model
     */
    public function getModel()
    {
        return new Comment();
    }
}
