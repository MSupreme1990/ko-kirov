<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Pages\Admin;

use Mindy\Base\Mindy;
use Modules\Admin\Components\ModelAdmin;
use Modules\Pages\PagesModule;

/**
 * Class BlockAdmin
 */
class BlockAdmin extends ModelAdmin
{
    public function getColumns()
    {
        return ['slug', 'name'];
    }

    public function getCreateForm()
    {
        return Mindy::app()->getModule('Pages')->blockForm;
    }

    public function getModel()
    {
        $modelClass = Mindy::app()->getModule('Pages')->blockModel;

        return new $modelClass();
    }

    public function getVerboseName()
    {
        return PagesModule::t('text block');
    }

    public function getVerboseNamePlural()
    {
        return PagesModule::t('text blocks');
    }
}
