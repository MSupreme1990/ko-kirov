<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Pages\Forms;

use Mindy\Base\Mindy;
use Mindy\Form\Fields\WysiwygField;
use Mindy\Form\ModelForm;

/**
 * Class BlockForm
 */
class BlockForm extends ModelForm
{
    public function getFields()
    {
        return [
            'content' => [
                'class' => WysiwygField::className(),
            ],
        ];
    }

    public function getModel()
    {
        $cls = Mindy::app()->getModule('Pages')->blockModel;

        return new $cls();
    }
}
