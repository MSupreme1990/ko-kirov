<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    '/{url:.*}/c/send' => [
        'name' => 'comment_send',
        'callback' => '\Modules\Pages\Controllers\CommentController:save',
    ],
    '/{url:.*}/c/' => [
        'name' => 'comment_list',
        'callback' => '\Modules\Pages\Controllers\CommentController:view',
    ],
    '/{url:.*}' => [
        'name' => 'view',
        'callback' => '\Modules\Pages\Controllers\PageController:view',
    ],
];
