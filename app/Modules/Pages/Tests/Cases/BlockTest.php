<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Pages\Tests;

use Mindy\Tests\DatabaseTestCase;
use Modules\Pages\Components\BlockHelper;
use Modules\Pages\Models\Block;
use Modules\User\Models\User;

class BlockTest extends DatabaseTestCase
{
    protected function getModels()
    {
        return [new Block(), new User()];
    }

    public function testBlock()
    {
        $this->assertEquals('{{%pages_block}}', Block::tableName());

        $model = new Block([
            'name' => 'bar',
            'slug' => 'foo',
            'content' => '123',
        ]);
        $this->assertTrue($model->save());
        $out = BlockHelper::render('foo');
        $this->assertEquals('123', $out);
    }
}
