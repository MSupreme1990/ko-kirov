<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Pages\Controllers;

use Modules\Core\Controllers\ApiBaseController;
use Modules\Pages\Models\Page;

class ApiController extends ApiBaseController
{
    public function actionView($url)
    {
        $model = Page::objects()->get([
            'url' => $url,
        ]);

        if ($model === null) {
            echo $this->json([
                'error' => true,
                'message' => 'Page not found',
            ]);
            $this->end();
        }

        $childrenModels = [];
        $models = $model->objects()->children()->all();
        foreach ($models as $children) {
            $childrenModels[] = $children->toArray();
        }

        $page = array_merge($model->toArray(), [
            'children' => $childrenModels,
        ]);

        echo $this->json([
            'status' => true,
            'page' => $page,
        ]);
    }
}
