<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Pages\Models;

use Mindy\Orm\Fields\TreeForeignField;
use Modules\Comments\Models\BaseComment;
use Modules\Pages\PagesModule;

/**
 * Class Comment
 *
 * @method static \Modules\Comments\Models\CommentManager objects($instance = null)
 */
class Comment extends BaseComment
{
    public static function getFields()
    {
        return array_merge(parent::getFields(), [
            'page' => [
                'class' => TreeForeignField::className(),
                'modelClass' => Page::className(),
                'verboseName' => PagesModule::t('Page'),
            ],
        ]);
    }

    /**
     * @return BaseComment
     */
    public function getRelation()
    {
        return $this->page;
    }
}
