<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Pages\Components;

use Mindy\Base\Mindy;

/**
 * All rights reserved.
 *
 * @author Falaleev Maxim
 * @email max@studio107.ru
 *
 * @version 1.0
 * @company Studio107
 * @site http://studio107.ru
 * @date 14/05/14.05.2014 16:34
 */

/**
 * Class BlockHelper
 */
class PagesHelper
{
    public static function getPages($parentId, $limit = 10, $offset = 0, $order = [])
    {
        $modelClass = Mindy::app()->getModule('Pages')->pagesModel;

        return $modelClass::objects()->filter([
            'parent_id' => $parentId,
        ])->limit($limit)->offset($offset)->order($order)->all();
    }
}
