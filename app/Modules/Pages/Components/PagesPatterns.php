<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Pages\Components;

use Mindy\Router\CustomPatterns;

/**
 * @DEPRECATED
 * Class PagesPatterns
 */
class PagesPatterns extends CustomPatterns
{
    public function getPatterns()
    {
        $namespace = !empty($this->namespace) ? $this->namespace.'.' : '';
        $patterns = [];
        $callback = ['\Modules\Pages\Controllers\PageController', 'view'];

//        $urls = Pages::objects()->valuesList(['url'], true);
//        foreach ($urls as $url) {
//            $patterns['/' . $url] = [
//                'callback' => $callback
//            ];
//        }

        $patterns['/{url:[^?]+}'] = [
            'name' => $namespace.'view',
            'callback' => $callback,
        ];
        $patterns['/'] = [
            'name' => $namespace.'index',
            'callback' => $callback,
        ];

        return $patterns;
    }
}
