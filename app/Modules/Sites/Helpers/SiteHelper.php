<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Sites\Helpers;

use Mindy\Base\Mindy;
use Modules\Sites\Models\Site;

class SiteHelper
{
    /**
     * @param bool $currentFirst
     *
     * @return Site[]
     */
    public static function getSites($currentFirst = false)
    {
        $modelClass = Mindy::app()->getModule('Sites')->modelClass;
        $currentSite = Mindy::app()->getModule('Sites')->getSite();
        $sites = $modelClass::objects()->all();
        if ($currentFirst && $currentSite) {
            usort($sites, function ($siteA, $siteB) {
                $current = Mindy::app()->getModule('Sites')->getSite();
                if ($siteA->id == $current->id) {
                    return 0;
                }

                return 1;
            });
        }

        return $sites;
    }

    /**
     * @return string
     */
    public static function getSelect()
    {
        $request = Mindy::app()->request;
        $sites = self::getSites();
        if (count($sites) > 1) {
            $url = $request->getPath();
            $schema = $request->http->getSchema();
            $options = '';
            foreach ($sites as $site) {
                $options .= strtr('<option value="{value}"{selected}>{name}</option>', [
                    '{value}' => $schema.'://'.$site->domain.$url,
                    '{name}' => $site->name,
                    '{selected}' => $site->domain == $request->getHost() ? ' selected' : '',
                ]);
            }
            $js = 'window.location=this.value;';

            return "<select onchange='$js'>$options</select>";
        }

        return '';
    }
}
