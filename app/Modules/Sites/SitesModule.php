<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Sites;

use Mindy\Base\Module;
use Modules\Sites\Models\Site;

class SitesModule extends Module
{
    public $modelClass = 'Modules\Sites\Models\Site';
    public $formClass = 'Modules\Sites\Forms\SiteForm';

    /**
     * @var \Modules\Sites\Models\Site
     */
    private $_site;

    public function setSite(Site $model)
    {
        $this->_site = $model;
    }

    public function getSite()
    {
        return $this->_site;
    }

    public function getMenu()
    {
        return [
            'name' => $this->getName(),
            'items' => [
                [
                    'name' => self::t('Sites'),
                    'adminClass' => 'SiteAdmin',
                ],
            ],
        ];
    }
}
