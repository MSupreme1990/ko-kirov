<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return  [
  'Site' => 'Сайт',
  'site' => 'сайт',
  'sites' => 'сайты',
  'Domain' => 'Домен',
  'Name' => 'Название',
  'City' => 'Город',
  'Phones' => 'Телефоны',
  'Main phone' => 'Основной телефон',
  'Opt phone' => 'Оптовый телефон',
  'Email' => 'Электронная почта',
  'Latitude' => 'Широта',
  'Longitude' => 'Долгота',
  'Site {url} was created' => 'Сайт {url} был добавлен',
  'Site {url} was updated' => 'Сайт {url} был обновлен',
  'Site {url} was deleted' => 'Сайт {url} был удален',
  'Sites' => 'Сайты',
  'Theme' => 'Тема',
];
