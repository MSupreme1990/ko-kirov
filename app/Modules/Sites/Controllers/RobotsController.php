<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Sites\Controllers;

use Modules\Core\Controllers\CoreController;

class RobotsController extends CoreController
{
    public function actionIndex()
    {
        header('Content-Type: text/plain');
        $model = $this->getModule()->getSite();
        if ($this->getRequest()->getHost() == 'ko-kirov.ru') {
            echo $this->render('sites/robots.txt', [
                'model' => $model,
            ]);
        } else {
            echo empty($model->robots) ? $this->render('sites/robots.txt', [
                'model' => $model,
            ]) : $model->robots;
        }
    }
}
