<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Sites\Traits;

use Mindy\Base\Mindy;

trait SiteTrait
{
    public function getCurrentSiteId()
    {
        return $this->getCurrentSite()->pk;
    }

    public function getCurrentSite()
    {
        return Mindy::app()->getModule('Sites')->getSite();
    }
}
