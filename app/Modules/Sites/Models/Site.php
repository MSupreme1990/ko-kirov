<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Sites\Models;

use Mindy\Base\Mindy;
use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\EmailField;
use Mindy\Orm\Fields\FloatField;
use Mindy\Orm\Fields\ForeignField;
use Mindy\Orm\Fields\IntField;
use Mindy\Orm\Fields\TextField;
use Mindy\Orm\Model;
use Modules\Geo\Models\City;
use Modules\Sites\SitesModule;

class Site extends Model
{
    public static function getFields()
    {
        return [
            'domain' => [
                'class' => CharField::className(),
                'verboseName' => SitesModule::t('Domain'),
            ],
            'name' => [
                'class' => CharField::className(),
                'verboseName' => SitesModule::t('Name'),
            ],
            'city' => [
                'class' => ForeignField::className(),
                'modelClass' => City::className(),
                'verboseName' => SitesModule::t('City'),
                'null' => true,
            ],
            'robots' => [
                'class' => TextField::className(),
                'verboseName' => 'robots.txt',
                'null' => true,
            ],
            'phones' => [
                'class' => TextField::className(),
                'verboseName' => SitesModule::t('Phones'),
            ],
            'phone_main' => [
                'class' => CharField::className(),
                'verboseName' => SitesModule::t('Main phone'),
            ],
            'phone_opt' => [
                'class' => CharField::className(),
                'verboseName' => SitesModule::t('Opt phone'),
            ],
            'email' => [
                'class' => EmailField::className(),
                'verboseName' => SitesModule::t('Email'),
            ],
            'lat' => [
                'class' => FloatField::className(),
                'verboseName' => SitesModule::t('Latitude'),
                'null' => true,
            ],
            'lng' => [
                'class' => FloatField::className(),
                'verboseName' => SitesModule::t('Longitude'),
                'null' => true,
            ],
            'zoom' => [
                'class' => IntField::className(),
                'verboseName' => SitesModule::t('Zoom (0-20)'),
                'null' => true,
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->name;
    }

    public function getAbsoluteUrl()
    {
        return Mindy::app()->request->http->getSchema().'://'.$this->domain;
    }
}
