<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Sites\Middleware;

use Mindy\Base\Mindy;
use Mindy\Helper\Console;
use Mindy\Http\Request;
use Mindy\Middleware\Middleware;

class CurrentSiteMiddleware extends Middleware
{
    public function processRequest(Request $request)
    {
        if (!Console::isCli()) {
            $modelClass = Mindy::app()->getModule('Sites')->modelClass;
            $model = $modelClass::objects()->filter([
                'domain' => $this->decode($request->http->getHost()),
            ])->get();
            if ($model !== null) {
                Mindy::app()->getModule('Sites')->setSite($model);
            }
        }
    }

    public function decode($value)
    {
        if (function_exists('idn_to_utf8')) {
            return idn_to_utf8($value);
        } elseif (class_exists('\True\Punycode')) {
            $pc = new \True\Punycode(Mindy::app()->locale['charset']);

            return $pc->decode($value);
        }
        Mindy::app()->logger->error('CurrentSiteMiddleware required php intl or \\True\\Punycode packages');

        return $value;
    }
}
