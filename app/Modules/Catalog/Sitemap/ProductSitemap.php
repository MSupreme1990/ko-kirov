<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Sitemap;

use Modules\Catalog\Models\Product;
use Modules\Sitemap\Components\Sitemap;

class ProductSitemap extends Sitemap
{
    /**
     * @return string model class name
     */
    public function getModelClass()
    {
        return Product::className();
    }

    public function getLastMod($data)
    {
        if (isset($data['updated_at'])) {
            $date = $data['updated_at'];
        } else {
            $date = $data['created_at'];
        }

        return $this->formatLastMod($date);
    }

    public function getLoc($data)
    {
        return $this->reverse('ko:product_detail', ['pk' => $data['id']]);
    }
}
