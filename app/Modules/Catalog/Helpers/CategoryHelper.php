<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Helpers;

use Mindy\Utils\RenderTrait;
use Modules\Catalog\Models\Category;

class CategoryHelper
{
    use RenderTrait;

    public static function render($template = 'catalog/_categories.html')
    {
        echo self::renderTemplate($template, [
            'models' => Category::tree()->roots()->all(),
        ]);
    }
}
