<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Helpers;

use Mindy\Base\Mindy;

class ImagePatchHelper
{
    private static $_obj = null;
    private static $_sizes = null;
    private static $_base = null;

    public static function getPatches($field, $object, $origin, $fullpatch = false)
    {
        if (empty(self::$_obj) || get_class($object) != get_class(self::$_obj)) {
            self::$_sizes = array_keys($object::getFields()[$field]['sizes']);
            self::$_obj = $object;
        }

        $patch = explode('/', $origin);
        $file = end($patch);
        unset($patch[count($patch) - 1]);
        $patch = implode('/', $patch);

        if ($fullpatch && empty(self::$_base)) {
            self::$_base = Mindy::app()->storage->baseUrl;
        }
        if ($fullpatch) {
            $patch = self::$_base.$patch;
        }
        $sizesPatch = ['original' => $patch.'/'.$file];

        foreach (self::$_sizes as $size) {
            $sizesPatch[$size] = $patch.'/'.$size.'_'.$file;
        }

        return $sizesPatch;
    }
}
