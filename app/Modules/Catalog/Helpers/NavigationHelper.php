<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Helpers;

use Mindy\Base\Mindy;
use Mindy\Helper\Json;
use Mindy\Utils\RenderTrait;
use Modules\Catalog\CatalogModule;
use Modules\Catalog\Models\Category;

class NavigationHelper
{
    use RenderTrait;

    public static function render($current_url = null)
    {
        return self::renderTemplate('catalog/_navigation.html', [
            'models' => Category::objects()->asTree()->all(),
            'current_url' => $current_url,
        ]);
    }

    public static function tree()
    {
        $result = [];
        $roots = Category::objects()
            ->select(['id', 'root'])
            ->roots()
            ->order(['root'])
            ->cache(20)
            ->all();

        foreach ($roots as $root) {
            $childRaw = $root->objects()
                ->select(['id', 'parent_id'])
                ->filter(['root' => $root->root, 'parent_id__isnull' => 0])
                ->order(['lft'])
                ->cache(20)
                ->asArray()
                ->all();

            $items = [];
            foreach ($childRaw as $child) {
                $items[$child['id']] = $child;
            }

            foreach ($items as $id => $model) {
                if (isset($items[$model['parent_id']])) {
                    if (!isset($items[$model['parent_id']]['items'])) {
                        $items[$model['parent_id']]['items'] = [];
                    }
                    $items[$model['parent_id']]['items'][] = $model;

                    unset($items[$id]);
                }
            }

            $tItems = [];
            foreach ($items as $item) {
                $tItems[] = $item;
            }

            $result[] = [
                'category' => [
                    'id' => $root->id,
                    'parent_id' => null,
                ],
                'items' => $tItems,
            ];
        }

        return Json::encode($result);
    }

    public static function urls()
    {
        $data = [];
        $models = Category::objects()->order(['root'])->cache(10)->all();
        foreach ($models as $model) {
            $data[substr($model->getAbsoluteUrl(), 1)] = $model->toArray();
        }

        return Json::encode($data);
    }

    public static function ids()
    {
        $pH = CatalogModule::get_products_hash();
        $cache_key = 'cats_ids_all_'.$pH['hash'];

        if ($data = Mindy::app()->cache->get($cache_key)) {
            return $data;
        }

        $data = [];
        $models = Category::objects()
            ->select(['id', 'parent_id', 'root', 'url', 'name', 'min_size', 'max_size', 'woman'])
            ->order(['root'])
            ->cache(20)
            ->asArray()
            ->all();
        foreach ($models as $model) {
            $data[$model['id']] = [
                'category' => $model,
                'products' => null,
                'products_loaded' => false,
            ];
        }
        $data = Json::encode($data);

        Mindy::app()->cache->set($cache_key, $data);

        return $data;
    }
}
