<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Mindy\Router\Patterns;

return [
    '/core' => new Patterns('Modules.Admin.urls', 'admin'),
    '/c' => new Patterns('Modules.Catalog.urls', 'catalog'),
];
