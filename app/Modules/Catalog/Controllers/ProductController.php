<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Controllers;

use Mindy\Base\Mindy;
use Modules\Catalog\Models\Product;
use Modules\Core\Controllers\CoreController;

class ProductController extends CoreController
{
    public function actionView($url)
    {
        $qs = Product::objects()->filter([
            'url' => $url,
        ]);

        if (Mindy::app()->getUser()->is_superuser === false) {
            $qs->filter(['is_published' => true]);
        }

        $model = $qs->get();
        if ($model === null) {
            $this->error(404);
        }

        $r = $this->getRequest();
        $city = $r->cookies->get('city');
        if ($city && $r->getDomain() != MAIN_DOMAIN) {
            $r->redirect(MAIN_DOMAIN.$model->getAbsoluteUrl());
        }

        echo $this->render('catalog/product_detail.html', [
            'model' => $model,
            'images' => $model->images->all(),
            'categories' => $model->categories->all(),
        ]);
    }
}
