<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Controllers;

use DateTime;
use Mindy\Base\Mindy;
use Mindy\Helper\Json;
use Mindy\Query\ConnectionManager;
use Modules\Catalog\CatalogModule;
use Modules\Catalog\Helpers\ImagePatchHelper;
use Modules\Catalog\Models\Category;
use Modules\Catalog\Models\Image;
use Modules\Catalog\Models\Product;
use Modules\Catalog\Models\ProductThrough;
use Modules\Core\Controllers\ApiBaseController;

class Api_v11_Controller extends ApiBaseController
{
    protected $cache_delay = 604800; //(60*60*24*7);

    public function actionGet($url)
    {
        $pH = CatalogModule::get_products_hash();
        $cache_key = 'category_'.$url.$pH['hash'];

        if ($out = Mindy::app()->cache->get($cache_key)) {
            echo $this->json($out);
            Mindy::app()->end();
        }

        $model = Category::objects()->get(['url' => $url]);
        if ($model === null) {
            $this->error(404);
        }

        $products = ProductThrough::objects()
            ->filter(['category_id' => $model->id])
            ->order(['position'])
            ->select(['product_id'])
            ->valuesList(['product_id'], true);

        $out = [
            'products' => $products,
            'category' => $model->toArray(),
        ];
        Mindy::app()->cache->add($cache_key, $out, $this->cache_delay);
        echo $this->json($out);
    }

    public function actionGetProductImages($id)
    {
        $objImage = new Image();
        $rawImages = Image::objects()
            ->select('catalog_image_1.id, image')
            ->filter([
                'product__is_published' => true,
                'product_id' => $id,
            ])
            ->order(['-is_main'])
            ->asArray()
            ->all();

        $images = [];
        foreach ($rawImages as $rImage) {
            $img = $rImage;
            $img['image'] = ImagePatchHelper::getPatches('image', $objImage, $rImage['image'], true);
            $images[] = $img;
        }

        echo $this->json($images);
    }

    public function actionAllProduct()
    {
        $pH = CatalogModule::get_products_hash();
        $cache_key = 'products_all_'.$pH['hash'];

        if ($mobile = Mindy::app()->request->cookies->get('MOBILE', false)) {
            $cache_key .= '_mobile';
        }

        if ($out = Mindy::app()->cache->get($cache_key)) {
            echo $out;
            Mindy::app()->end();
        }

        $chunk = null;
        $limit = null;

        $db = ConnectionManager::getDb();

        $models = [];

        if ($chunk && $limit) {
            $rawModels = Product::objects()
                ->filter(['is_published' => true])
                ->with(['discount'])
                ->offset($chunk * $limit)
                ->limit($limit)
                ->group(['id'])
                ->asArray()
                ->all();
        } else {
            $rawModels = Product::objects()
                ->filter(['is_published' => true])
                ->with(['discount'])
                ->group(['id'])
                ->asArray()
                ->all();
        }

        $rawImages = Image::objects()
            ->select('image, product_id, is_main, count(*) as count')
            ->filter(['product__is_published' => true])
            ->order(['is_main'])
            ->group(['product_id'])
            ->asArray()
            ->all();

        $rawSizes = $db->createCommand('
        SELECT t.product_id as id, group_concat(cz.value) as sizes
        FROM catalog_product_catalog_size t
        LEFT OUTER JOIN catalog_size cz ON t.size_id = cz.id
        GROUP BY  t.product_id
        ', [])->queryAll(\PDO::FETCH_ASSOC);

        $objImage = new Image();
        foreach ($rawModels as $rModel) {
            $image = null;
            $images = [];
            $tImages = [];

            if (!$rModel['discount']['id']) {
                $rModel['discount'] = null;
            }

            foreach ($rawImages as $rImage) {
                if ($rModel['id'] == $rImage['product_id']) {
                    $img = $rImage;
                    $img['image'] = ImagePatchHelper::getPatches('image', $objImage, $rImage['image'], true);

                    $images[] = $img;
                    if ($img['is_main']) {
                        $image = $img;
                    }
                } else {
                    $tImages[] = $rImage;
                }
            }

            $rawImages = $tImages;

            if (empty($image) && !empty($images)) {
                $image = $images[0];
            }

            $rModel['sizes'] = [];
            $tSizes = [];
            foreach ($rawSizes as $rSize) {
                if ($rModel['id'] == $rSize['id']) {
                    $rModel['sizes'] = explode(',', $rSize['sizes']);
                } else {
                    $tSizes[] = $rSize;
                }
            }
            $rawSizes = $tSizes;
            $updated = (($rModel['updated_at']) ? $rModel['updated_at'] : $rModel['created_at']);
            $updated = new DateTime($updated);
            $updated = $updated->getTimestamp();

            if ($mobile && !empty($images)) {
                $images = [$images[0]];
            }

            $images_count = (!empty($images)) ? $images[0]['count'] : 0;
            $images_loaded = ($images_count > 0) ? false : true;

            unset($image['count']);
            unset($image['is_main']);
            unset($image['product_id']);

            $models[$rModel['id']] = array_merge($rModel, [
                    'image' => $image,
                    'images' => $images,
                    'images_count' => $images_count,
                    'images_loaded' => $images_loaded,
                    'updated' => $updated,
                    'related' => [],
                ]
            );
        }

        $out = base64_encode(gzcompress(Json::encode($models), 9));
        Mindy::app()->cache->add($cache_key, $out, $this->cache_delay);
        echo $out;
    }

    public function actionProductRelated($url)
    {
        $data = Mindy::app()->cache->get('api_product_related_'.$url);
        if (!$data) {
            $qs = Product::objects()->filter([
                'url' => $url,
            ]);

            if (Mindy::app()->getUser()->is_superuser === false) {
                $qs->filter(['is_published' => true]);
            }
            $model = $qs->get();
            if ($model === null) {
                echo $this->json([
                    'status' => false,
                    'error' => true,
                ]);
                $this->end();
            }

            $filter = [];
            if ($model->category_id) {
                $filter['categories__pk'] = $model->category_id;
            } else {
                $filter['categories__pk__in'] = $model->categories->valuesList(['id'], true);
            }

            $relatedModels = $model
                ->related
                ->limit(20)
                ->valuesList(['id'], true);
            $newModels = Product::objects()
                ->filter($filter)
                ->limit(20 - count($relatedModels))
                ->order(['?'])
                ->valuesList(['id'], true);

            $data = $relatedModels + $newModels;

            Mindy::app()->cache->set('api_product_related_'.$url, $data, $this->cache_delay);
        }
        echo $this->json([
            'related' => $data,
            'status' => true,
            'error' => false,
        ]);
        Mindy::app()->end();
    }

    public function actionSearch()
    {
        $q = isset($_GET['q']) ? $_GET['q'] : '';
        if (empty($q)) {
            echo $this->json([
                'q_error' => true,
                'products' => [],
                'total' => 0,
            ]);
            $this->end();
        }

        $q = str_replace('№', '', $q);
        $q = trim($q);

        $qs = Product::objects()
            ->filter([
                'name__contains' => $q,
            ])
            ->orFilter([
                'code__contains' => $q,
            ]);

        $products = $qs->valuesList(['id'], true);
        $total = count($products);

        $data = [
            'q_error' => false,
            'products' => $products,
            'total' => $total,
        ];

        echo $this->json($data);
        Mindy::app()->end();
    }
}
