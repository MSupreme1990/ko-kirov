<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Controllers;

use Mindy\Base\Mindy;
use Mindy\Pagination\Pagination;
use Modules\Catalog\CatalogModule;
use Modules\Catalog\Models\Category;
use Modules\Core\Controllers\CoreController;

class CategoryController extends CoreController
{
    public function actionIndex()
    {
        $this->setBreadcrumbs([
            ['name' => CatalogModule::t('Shop'), 'url' => Mindy::app()->urlManager->reverse('ko:index')],
        ]);

        if (!($content = Mindy::app()->cache->get('catalog/category_list/index'))) {
            $qs = Category::tree()->roots();
            $pager = new Pagination($qs);
            $content = $this->render('catalog/category_list_blank.html', [
                'models' => $pager->paginate(),
                'pager' => $pager,
            ]);

            Mindy::app()->cache->set('catalog/category_list/index', $content, 604800);
        }

        echo $this->render('catalog/category_list_content.html', [
            'content' => $content,
        ]);
    }

    public function actionList($url)
    {
        if (substr($url, -1) == '/') { // TODO: рекламщики мудаки урлы расставили как мудилы
            $this->request->redirect('/'.substr($url, 0, strlen($url) - 1), null, 301);
        }

        $category = Category::objects()->filter(['url' => $url])->get();
        if ($category === null) {
            $this->error(404);
        }

        $r = $this->getRequest();
        $city = $r->cookies->get('city');
        if ($city !== null) {
            $newDomain = 'http://'.$city;
            if ($r->domain != $newDomain) {
                $r->redirect($newDomain.$category->getAbsoluteUrl());
            }
        }

        if (!($content = Mindy::app()->cache->get($url))) {
            $parents = $category->objects()->ancestors()->order(['lft'])->all();
            $parents[] = $category;
            $breadcrumbs = [
                ['name' => CatalogModule::t('Shop'), 'url' => Mindy::app()->urlManager->reverse('ko:index')],
            ];
            foreach ($parents as $parent) {
                $breadcrumbs[] = [
                    'url' => $parent->getAbsoluteUrl(),
                    'name' => (string) $parent,
                ];
            }
            $this->setBreadcrumbs($breadcrumbs);

            Mindy::app()->session->add('category', $category);

            $pager = new Pagination($category->production);
            $content = $this->render('catalog/category_detail_blank.html', [
                'categories' => $category->objects()->children()->all(),
                'category' => $category,
                'productions' => $pager->paginate(),
                'pager' => $pager,
            ]);

            Mindy::app()->cache->set($url, $content, 604800);
        }

        echo $this->render('catalog/category_detail_content.html', [
            'content' => $content,
        ]);
    }

    public function actionRecent()
    {
        echo $this->render('ko/index.html');
    }
}
