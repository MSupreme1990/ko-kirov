<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Controllers;

use Mindy\Base\Mindy;
use Mindy\Query\ConnectionManager;
use Modules\Catalog\Models\Category;
use Modules\Catalog\Models\Product;
use Modules\Core\Controllers\ApiBaseController;

class ApiController extends ApiBaseController
{
    public $allowedOrder = [
        'position_asc',
        'position_desc',
        'price_asc',
        'price_desc',
        'update_asc',
        'update_desc',
        'discount_asc',
        'discount_desc',
    ];

    public function actionTree()
    {
        $models = [];
        $roots = Category::objects()->roots()->order(['root'])->all();
        foreach ($roots as $root) {
            $children = $root->objects()->filter([
                'root' => $root->root,
            ])->asTree()->all();

            $rootRaw = array_shift($children);
            $models[] = [
                'root' => $root->toArray(),
                'models' => $rootRaw['items'],
            ];
        }

        echo $this->json($models);
        Mindy::app()->end();
    }

    public function imageUrl($url, $prefix)
    {
        $path = dirname($url);
        $name = basename($url);

        return $path.'/'.$prefix.'_'.$name;
    }

    protected function filterQs($qs, $params)
    {
        $order = false;
        foreach ($params as $key => $value) {
            if ($key == 'sizes') {
                $qs->filter(['sizes__value__range' => $value]);
            } elseif (in_array($value, $this->allowedOrder)) {
                $order = true;
                $qs->order([$value]);
            }
            continue;
        }

        if ($order === false) {
            $qs->order(['product_through__position']);
        }
    }

    protected function getProducts($qs)
    {
        $products = [];
        foreach ($qs->all() as $product) {
            $images = [];
            foreach ($product->images->all() as $image) {
                $images[] = $image->toArray();
            }
            $products[] = array_merge($product->toArray(), [
                'image' => $product->image->toArray(),
                'images' => $images,
                'sizes' => $product->sizes->valuesList(['value'], true),
                'discount' => $product->discount ? $product->discount->toArray() : [],
            ]);
        }

        return $products;
    }

    public function actionCategory($url)
    {
        $cache = Mindy::app()->cache;
        $cacheKey = 'category_array_'.$url;

        $data = $cache->get($cacheKey);
        if (!$data) {
            $model = Category::objects()->asArray()->get(['url' => $url]);
            if ($model === null) {
                $this->error(404);
            }

            $data = [
                'category' => $model,
            ];
            $cache->set($cacheKey, $data, 3600);
        }

        echo $this->json($data);
        Mindy::app()->end();
    }

    public function actionGet($url)
    {
        if (isset($_GET['pageSize']) && !empty($_GET['pageSize'])) {
            $pageSize = (int) $_GET['pageSize'];
        } else {
            $pageSize = 100;
        }

        $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        if (empty($page)) {
            $page = 1;
        }

        $r = $this->getRequest();
        $sort = $r->get->get('sort', []);
        $sizes = $r->get->get('sizes', []);
        $model = Category::objects()->get(['url' => $url]);
        if ($model === null) {
            $this->error(404);
        }

        $qs = $model->production->paginate($page, $pageSize)->getQuerySet();

        /**
         * Raw sql because mysql 5.1 can't run query with ORDER and GROUP correct.
         */
        $db = ConnectionManager::getDb();

        $orderBy = [];
        if (in_array($sort, $this->allowedOrder)) {
            list($field, $direction) = explode('_', $sort);

            if ($field == 'discount') {
                $field .= '_id';
            }

            if ($field == 'update') {
                $field = 'created_at';
            }

            $direction = in_array(strtoupper($direction), ['ASC', 'DESC']) ? strtoupper($direction) : 'DESC';

            if ($field == 'price') {
                $orderBy[] = "`products`.`price_old_rub` {$direction}";
                $orderBy[] = "`products`.`price_rub` {$direction}";
            } else {
                $orderBy[] = "`products`.`{$field}` {$direction}";
            }
        } else {
            $orderBy[] = '`products`.`position` ASC';
        }

        $cache = Mindy::app()->cache;
        $cacheRawKey = serialize([
            'url' => $url,
            'order' => $orderBy,
            'sort' => $sort,
            'page' => $page,
            'pageSize' => $pageSize,
        ]);
        $cacheKey = md5($cacheRawKey);
        $data = $cache->get($cacheKey);
        if ($data === false) {
            $where = '';
            if (Mindy::app()->user->is_superuser == false) {
                $where = 'and `catalog_product_2`.`is_published` = 1';
            }

            $orders = implode(', ', $orderBy);
            if (!empty($sizes) && count($sizes) == 2) {
                list($minSize, $maxSize) = $sizes;

                $sql = "SELECT `products`.* FROM (
SELECT `catalog_category_catalog_product_1`.`position`, `catalog_product_2`.*
    FROM `catalog_product` `catalog_product_2`
    JOIN `catalog_category_catalog_product` `catalog_category_catalog_product_1` ON `catalog_category_catalog_product_1`.`product_id`=`catalog_product_2`.`id`
    LEFT OUTER JOIN `catalog_product_catalog_size` `catalog_product_catalog_size_4` ON `catalog_product_2`.`id` = `catalog_product_catalog_size_4`.`product_id`
    LEFT OUTER JOIN `catalog_size` `catalog_size_5` ON `catalog_product_catalog_size_4`.`size_id` = `catalog_size_5`.`id`
    WHERE `catalog_category_catalog_product_1`.`category_id`=:category_id AND `catalog_size_5`.`value` BETWEEN :min_size AND :max_size
    {$where}
    ORDER BY `catalog_category_catalog_product_1`.`position`
) AS `products`
GROUP BY `products`.`id`
ORDER BY {$orders}
LIMIT {$qs->limit}
OFFSET {$qs->offset}";

                $objects = $db->createCommand($sql, [
                    ':category_id' => $model->pk,
                    ':min_size' => $minSize,
                    ':max_size' => $maxSize,
                ])->queryAll(\PDO::FETCH_ASSOC);

                $totalSql = "SELECT COUNT(`products`.`id`) AS total FROM (
    SELECT `catalog_product_2`.`id`
    FROM `catalog_product` `catalog_product_2`
    JOIN `catalog_category_catalog_product` `catalog_category_catalog_product_1` ON `catalog_category_catalog_product_1`.`product_id`=`catalog_product_2`.`id`
    LEFT OUTER JOIN `catalog_category_catalog_product` `catalog_category_catalog_product_3` ON `catalog_product_2`.`id` = `catalog_category_catalog_product_3`.`product_id`
    LEFT OUTER JOIN `catalog_product_catalog_size` `catalog_product_catalog_size_4` ON `catalog_product_2`.`id` = `catalog_product_catalog_size_4`.`product_id`
    LEFT OUTER JOIN `catalog_size` `catalog_size_5` ON `catalog_product_catalog_size_4`.`size_id` = `catalog_size_5`.`id`
    WHERE `catalog_category_catalog_product_1`.`category_id`=:category_id AND `catalog_size_5`.`value` BETWEEN :min_size AND :max_size
    {$where}
    GROUP BY `catalog_product_2`.`id`
) AS `products`";

                $total = $db->createCommand($totalSql, [
                    ':category_id' => $model->pk,
                    ':min_size' => $minSize,
                    ':max_size' => $maxSize,
                ])->queryScalar();
            } else {
                $sql = "SELECT `products`.* FROM (
    SELECT `catalog_category_catalog_product_1`.`position`, `catalog_product_2`.*
    FROM `catalog_product` `catalog_product_2`
    JOIN `catalog_category_catalog_product` `catalog_category_catalog_product_1` ON `catalog_category_catalog_product_1`.`product_id`=`catalog_product_2`.`id`
    WHERE `catalog_category_catalog_product_1`.`category_id`=:category_id
    {$where}
    ORDER BY `catalog_category_catalog_product_1`.`position`
) AS `products`
GROUP BY `products`.`id`
ORDER BY {$orders}
LIMIT {$qs->limit}
OFFSET {$qs->offset}";

                $objects = $db->createCommand($sql, [
                    ':category_id' => $model->pk,
                ])->queryAll(\PDO::FETCH_ASSOC);

                $totalSql = "SELECT COUNT(`products`.`id`) AS total FROM (
    SELECT `catalog_product_2`.`id`
    FROM `catalog_product` `catalog_product_2`
    JOIN `catalog_category_catalog_product` `catalog_category_catalog_product_1` ON `catalog_category_catalog_product_1`.`product_id`=`catalog_product_2`.`id`
    LEFT OUTER JOIN `catalog_category_catalog_product` `catalog_category_catalog_product_3` ON `catalog_product_2`.`id` = `catalog_category_catalog_product_3`.`product_id`
    WHERE `catalog_category_catalog_product_1`.`category_id`=:category_id
    {$where}
    GROUP BY `catalog_product_2`.`id`
) AS `products`";

                $total = $db->createCommand($totalSql, [
                    ':category_id' => $model->pk,
                ])->queryScalar();
            }

            $products = [];
            foreach ($objects as $obj) {
                $product = Product::create($obj);
                $images = [];
                foreach ($product->images->all() as $image) {
                    $images[] = $image->toArray();
                }
                $products[] = array_merge($product->toArray(), [
                    'position' => $obj['position'],
                    'image' => $product->image ? $product->image->toArray() : [],
                    'images' => $images,
                    'sizes' => $product->sizes->valuesList(['value'], true),
                    'discount' => $product->discount ? $product->discount->toArray() : [],
                ]);
            }

            $data = [
                'products' => $products,
                'category' => $model->toArray(),
                'total' => $total,
            ];

            // seconds
            $oneHour = 3600;

            $cache->set($cacheKey, $data, $oneHour);
        }

        echo $this->json($data);
        Mindy::app()->end();
    }

    public function actionSearch()
    {
        $q = isset($_GET['q']) ? $_GET['q'] : '';
        if (empty($q)) {
            echo $this->json([
                'q_error' => true,
                'products' => [],
                'total' => 0,
            ]);
            $this->end();
        }

        $pageSize = isset($_GET['pageSize']) ? (int) $_GET['pageSize'] : 100;
        if (empty($pageSize)) {
            $pageSize = 1;
        }

        $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        if (empty($page)) {
            $page = 1;
        }

        $q = str_replace('№', '', $q);
        $q = trim($q);

        $qs = Product::objects()
            ->filter([
                'name__contains' => $q,
            ])
            ->orFilter([
                'code__contains' => $q,
            ]);
        $total = $qs->count();
        $objects = $qs->paginate($page, $pageSize)->all();

        $products = [];
        foreach ($objects as $product) {
            $images = [];
            foreach ($product->images->all() as $image) {
                $images[] = $image->toArray();
            }
            $products[] = array_merge($product->toArray(), [
                'position' => $product->position,
                'image' => $product->image ? $product->image->toArray() : [],
                'images' => $images,
                'sizes' => $product->sizes->valuesList(['value'], true),
                'discount' => $product->discount ? $product->discount->toArray() : [],
            ]);
        }

        $data = [
            'q_error' => false,
            'products' => $products,
            'total' => $total,
        ];

        echo $this->json($data);
        Mindy::app()->end();
    }

    public function actionSearchCategory()
    {
        $q = isset($_GET['q']) ? $_GET['q'] : '';
        if (empty($q)) {
            echo $this->json([
                'models' => [],
            ]);
            $this->end();
        }

        $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        if (empty($page)) {
            $page = 1; // TODO: ?
        }

        $models = Category::objects()
                    ->asArray()
                    ->filter(['name__contains' => $q])
                    ->limit(10)
                    ->all();

        echo $this->json([
            'models' => $models,
        ]);
        Mindy::app()->end();
    }

    public function actionGetWithCleanOrm($url)
    {
        $pageSize = 100;
        $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;

        $r = $this->getRequest();
        $params = $r->get->get('sort', []);
        $model = Category::objects()->get(['url' => $url]);
        if ($model === null) {
            $this->error(404);
        }

        $qsTotal = $model->production;
        $this->filterQs($qsTotal, $params);
        $total = $qsTotal->count();

        $qs = $model->production->paginate($page, $pageSize);
        $this->filterQs($qs, $params);
        $products = $this->getProducts($qs);

        $data = [
            'category' => $model->toArray(),
            'products' => $products,
            'total' => $total,
        ];

        echo $this->json($data);
        Mindy::app()->end();
    }

    public function actionProductGet($url)
    {
        $data = Mindy::app()->cache->get('api_product_'.$url);
        if (!$data) {
            $qs = Product::objects()->filter([
                'url' => $url,
            ]);

            if (Mindy::app()->getUser()->is_superuser === false) {
                $qs->filter(['is_published' => true]);
            }
            $model = $qs->get();
            if ($model === null) {
                echo $this->json([
                    'status' => false,
                    'error' => true,
                ]);
                $this->end();
            }

            /*
            9.15. Блок «Вместе с этой моделью смотрят» содержит ротатор товаров.
            Единовременно в ротаторе отображается 5 товаров, при нажатии на элемент
            управления ротатором отображается еще 5 товаров, причем в данном блоке
            может быть не больше 20 (двадцать) товаров. Администратор сайта имеет
            возможность указать товары для данного блока из админ.панели сайта.
            Если товары не указаны или указано в недостаточном количестве, то в
            блоке выводятся случайные товары текущей категории товара.
            */
            $related = [];
            $filter = [];
            if ($model->category_id) {
                $filter['categories__pk'] = $model->category_id;
            } else {
                $filter['categories__pk__in'] = $model->categories->valuesList(['id'], true);
            }

            $relatedModels = $model->related->limit(20)->all();
            $newModels = Product::objects()
                ->filter($filter)
                ->limit(20 - count($relatedModels))
                ->order(['?'])
                ->all();
            $mergedModels = $relatedModels + $newModels;
            foreach ($mergedModels as $r) {
                $relatedImages = [];
                foreach ($r->images->all() as $image) {
                    $relatedImages[] = $image->toArray();
                }

                $related[] = array_merge($r->toArray(), [
                    'images' => $relatedImages,
                    'image' => $r->image ? $r->image->toArray() : [],
                ]);
            }

            $images = [];
            foreach ($model->images->all() as $image) {
                $images[] = $image->toArray();
            }

            $data = [
                'model' => array_merge($model->toArray(), [
                    'images' => $images,
                    'image' => $model->image->toArray(),
                    'sizes' => $model->sizes->valuesList(['value'], true),
                    'discount' => $model->discount ? $model->discount->toArray() : [],
                ]),
                'related' => $related,
            ];
            Mindy::app()->cache->set('product_'.$url, $data);
        }
        echo $this->json($data);
        Mindy::app()->end();
    }

    public function actionIndex()
    {
        $data = [
            'new_models' => $this->fetchNew(),
            'popular' => $this->fetchPopular(),
            'categories' => $this->fetchCategories(),
        ];
        echo $this->json($data);
        Mindy::app()->end();
    }

    protected function fetchNew()
    {
        $models = Product::objects()
            ->filter([
                'is_new' => true,
            ])
            ->all();

        $products = [];
        foreach ($models as $product) {
            $images = [];
            foreach ($product->images->all() as $image) {
                $images[] = $image->toArray();
            }
            $products[] = array_merge($product->toArray(), [
                'image' => count($images) > 0 ? $images[0] : [],
                'images' => $images,
                'sizes' => $product->sizes->valuesList(['value'], true),
            ]);
        }

        return $products;
    }

    protected function fetchPopular()
    {
        $models = Product::objects()
            ->filter([
                'is_popular' => true,
            ])
            ->all();

        $products = [];
        foreach ($models as $product) {
            $images = [];
            foreach ($product->images->all() as $image) {
                $images[] = $image->toArray();
            }
            $products[] = array_merge($product->toArray(), [
                'image' => count($images) > 0 ? $images[0] : [],
                'images' => $images,
                'sizes' => $product->sizes->valuesList(['value'], true),
            ]);
        }

        return $products;
    }

    protected function fetchCategories()
    {
        $models = Category::objects()
            ->filter(['level' => 2])
            ->limit(15)
            ->offset(0)
            ->order(['root', 'lft'])
            ->exclude(['pk__in' => [3, 14]])
            ->all();

        $categories = [];
        foreach ($models as $product) {
            $categories[] = array_merge($product->toArray(), [
                'image' => $product->image->toArray(),
                'image_hover' => $product->image_hover->toArray(),
            ]);
        }

        return $categories;
    }

    public function actionCategories()
    {
        echo $this->json($this->fetchCategories());
        Mindy::app()->end();
    }

    public function actionNew()
    {
        echo $this->json($this->fetchNew());
        Mindy::app()->end();
    }

    public function actionPopular()
    {
        echo $this->json($this->fetchPopular());
        Mindy::app()->end();
    }
}
