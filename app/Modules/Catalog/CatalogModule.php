<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog;

use Mindy\Base\Mindy;
use Mindy\Base\Module;
use Mindy\Helper\Json;
use Modules\Catalog\Helpers\KeyHelper;
use Modules\Catalog\Models\Category;
use Modules\Catalog\Models\Product;

class CatalogModule extends Module
{
    /*
     * cashe keys:
     * -products_hash
     */

    public static function get_products_hash()
    {
        if ($out = Mindy::app()->cache->get('products_hash')) {
            return $out;
        }
        $out = [
            'hash' => KeyHelper::genUUID(),
            'count' => Product::objects()->filter(['is_published' => true])->count(),
        ];

        Mindy::app()->cache->add('products_hash', $out);

        return $out;
    }

    public static function preConfigure()
    {
        $tpl = Mindy::app()->template;
        $tpl->addHelper('catalog_navigation', ['Modules\Catalog\Helpers\NavigationHelper', 'render']);
        $tpl->addHelper('catalog_tree', ['Modules\Catalog\Helpers\NavigationHelper', 'tree']);
        $tpl->addHelper('catalog_urls', ['Modules\Catalog\Helpers\NavigationHelper', 'urls']);
        $tpl->addHelper('catalog_ids', ['Modules\Catalog\Helpers\NavigationHelper', 'ids']);
        $tpl->addHelper('get_products_hash', function () {
            $out = self::get_products_hash();

            return Json::encode($out);
        });
        $tpl->addHelper('product_replace_name', function ($name) {
            return str_ireplace($name, ['SEXI', 'VISKI', 'ВИСКИ', 'СЕКСИ'], '');
        });
        $tpl->addHelper('get_products', function () {
            if ($out = Mindy::app()->cache->get('raw_json')) {
                return $out;
            }
            $json = [];

            $cats = Category::objects()->filter(['level' => 2])->all();
            foreach ($cats as $model) {
                $categories = $model->objects()->children()->all();
                $tmp = [];
                $products = [];
                foreach ($categories as $category) {
                    $qs = $category->production->paginate(1);
                    foreach ($qs->all() as $product) {
                        if (isset($tmp[$product->pk])) {
                            continue;
                        }
                        $tmp[$product->pk] = true;

                        $images = [];
                        foreach ($product->images->all() as $image) {
                            $images[] = $image->toArray();
                        }
                        $products[] = array_merge($product->toArray(), [
                                'image' => $product->image->toArray(),
                                'images' => $images,
                            ]);
                    }
                }

                $data = [
                        'category' => $model->toArray(),
                        'products' => $products,
                    ];
                $json[$model->pk] = $data;

                $out = Json::encode($json);
                Mindy::app()->cache->set('raw_json', $out);
            }

            return $out;
        });
    }

    public function getMenu()
    {
        return [
            'name' => $this->getName(),
            'items' => [
                [
                    'name' => self::t('Categories'),
                    'adminClass' => 'CategoryAdmin',
                ],
                [
                    'name' => self::t('Productions'),
                    'adminClass' => 'ProductAdmin',
                ],
                [
                    'name' => self::t('Product through'),
                    'adminClass' => 'ProductThroughAdmin',
                ],
                [
                    'name' => self::t('Images'),
                    'adminClass' => 'ImageAdmin',
                ],
                [
                    'name' => self::t('Discounts'),
                    'adminClass' => 'DiscountAdmin',
                ],
                [
                    'name' => self::t('Sizes'),
                    'adminClass' => 'SizeAdmin',
                ],
            ],
        ];
    }
}
