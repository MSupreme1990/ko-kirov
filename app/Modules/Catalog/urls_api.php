<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    '/get/{url:.*}' => [
        'name' => 'get',
        'callback' => '\Modules\Catalog\Controllers\ApiController:get',
    ],
    '/get-category/{url:.*}' => [
        'name' => 'category',
        'callback' => '\Modules\Catalog\Controllers\ApiController:category',
    ],
    '/product/get/{url:.*}' => [
        'name' => 'get',
        'callback' => '\Modules\Catalog\Controllers\ApiController:productGet',
    ],
    '/search' => [
        'name' => 'search',
        'callback' => '\Modules\Catalog\Controllers\ApiController:search',
    ],
    '/search-category' => [
        'name' => 'search-category',
        'callback' => '\Modules\Catalog\Controllers\ApiController:searchCategory',
    ],
    '/tree' => [
        'name' => 'tree',
        'callback' => '\Modules\Catalog\Controllers\ApiController:tree',
    ],
    '/index' => [
        'name' => 'index',
        'callback' => '\Modules\Catalog\Controllers\ApiController:index',
    ],
    '/new' => [
        'name' => 'new',
        'callback' => '\Modules\Catalog\Controllers\ApiController:new',
    ],
    '/popular' => [
        'name' => 'popular',
        'callback' => '\Modules\Catalog\Controllers\ApiController:popular',
    ],
    '/categories' => [
        'name' => 'categories',
        'callback' => '\Modules\Catalog\Controllers\ApiController:categories',
    ],

    '/v1-1/all-products' => [
        'name' => 'allProduct',
        'callback' => '\Modules\Catalog\Controllers\Api_v11_Controller:allProduct',
    ],
    '/v1-1/product/related/{url:.*}' => [
        'name' => 'ProductRecent',
        'callback' => '\Modules\Catalog\Controllers\Api_v11_Controller:productRelated',
    ],
    '/v1-1/category/get/{url:.*}' => [
        'name' => 'get',
        'callback' => '\Modules\Catalog\Controllers\Api_v11_Controller:get',
    ],
    '/v1-1/product/images/{id:i}' => [
        'name' => 'ProductRecent',
        'callback' => '\Modules\Catalog\Controllers\Api_v11_Controller:getProductImages',
    ],
    '/v1-1/search' => [
        'name' => 'search',
        'callback' => '\Modules\Catalog\Controllers\Api_v11_Controller:search',
    ],
];
