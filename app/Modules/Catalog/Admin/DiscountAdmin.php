<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Admin;

use Modules\Admin\Components\ModelAdmin;
use Modules\Catalog\Models\Discount;

class DiscountAdmin extends ModelAdmin
{
    /**
     * @return \Mindy\Orm\Model
     */
    public function getModel()
    {
        return new Discount();
    }

    public function getColumns()
    {
        return ['name'];
    }
}
