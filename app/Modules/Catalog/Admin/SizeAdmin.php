<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Admin;

use Mindy\Orm\Model;
use Modules\Admin\Components\ModelAdmin;
use Modules\Catalog\Models\Size;

class SizeAdmin extends ModelAdmin
{
    public function getColumns()
    {
        return ['value'];
    }

    /**
     * @param Model $model
     *
     * @return QuerySet
     */
    public function getQuerySet(Model $model)
    {
        return parent::getQuerySet($model)->order(['value']);
    }

    /**
     * @return \Mindy\Orm\Model
     */
    public function getModel()
    {
        return new Size();
    }
}
