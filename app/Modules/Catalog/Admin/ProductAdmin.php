<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Admin;

use Modules\Admin\Components\ModelAdmin;
use Modules\Catalog\Forms\ProductForm;
use Modules\Catalog\Models\Product;

class ProductAdmin extends ModelAdmin
{
    public $pageSize = 100;

    public function getSearchFields()
    {
        return ['name', 'code', 'url'];
    }

    public function getCreateForm()
    {
        return ProductForm::className();
    }

    /**
     * @return \Mindy\Orm\Model
     */
    public function getModel()
    {
        return new Product();
    }

    public function getColumns()
    {
        return ['name', 'code', 'price_rub', 'price_old_rub'];
    }
}
