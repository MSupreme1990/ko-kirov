<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Admin;

use Modules\Admin\Components\NestedAdmin;
use Modules\Catalog\Forms\CategoryForm;
use Modules\Catalog\Models\Category;

class CategoryAdmin extends NestedAdmin
{
    public $linkColumn = 'name';

    /**
     * @return \Mindy\Orm\Model
     */
    public function getModel()
    {
        return new Category();
    }

    public function getCreateForm()
    {
        return CategoryForm::className();
    }

    public function getColumns()
    {
        return ['name'];
    }

    public function getSearchFields()
    {
        return ['name', 'url'];
    }
}
