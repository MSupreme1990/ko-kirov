<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Admin;

use Mindy\Base\Mindy;
use Mindy\Exception\Exception;
use Mindy\Orm\Model;
use Modules\Admin\Components\ModelAdmin;
use Modules\Catalog\Forms\ProductThroughFilterForm;
use Modules\Catalog\Models\Category;
use Modules\Catalog\Models\ProductThrough;

class ProductThroughAdmin extends ModelAdmin
{
    public $category = null;

    public $showPkColumn = false;

    public $pageSize = 800;

    public function getCanCreate()
    {
        return false;
    }

    public function getQuerySet(Model $model)
    {
        $qs = parent::getQuerySet($model);
        $qs->order(['position']);

        return $qs;
    }

    public function orderColumns()
    {
        return [
            'product__code' => 'product__code',
            'product__price_rub' => 'product__price_rub',
            'product__price_old_rub' => 'product__price_old_rub',
        ];
    }

    /**
     * Verbose names for custom columns
     *
     * @return array
     */
    public function verboseNames()
    {
        return [
            'product__code' => 'Артикул',
            'product__price_rub' => 'Цена',
            'product__price_old_rub' => 'Старая цена',
        ];
    }

    public function init()
    {
        if (isset($_GET['ProductThroughFilterForm'])) {
            $this->sortingColumn = 'position';
            $this->category = Category::objects()->get([
                'pk' => $_GET['ProductThroughFilterForm']['category'],
            ]);
        }
    }

    /**
     * @return string|null
     */
    public function getFilterForm()
    {
        return ProductThroughFilterForm::className();
    }

    public function getColumns()
    {
        return ['position', 'product', 'product__code', 'product__price_rub', 'product__price_old_rub'];
    }

    /**
     * @return \Mindy\Orm\Model
     */
    public function getModel()
    {
        return new ProductThrough();
    }

    public function sorting(array $data = [])
    {
        /* @var $qs \Mindy\Orm\QuerySet */
        $modelClass = $this->getModel();
        if (isset($data['models'])) {
            $models = $data['models'];
        } else {
            throw new Exception('Failed to receive models');
        }
        /**
         * Pager-independent sorting
         */
        $oldPositions = [];
        foreach ($models as $pk) {
            $oldPositions[] = $modelClass::objects()->get(['pk' => $pk])->{$this->sortingColumn};
        }
        asort($oldPositions);
        foreach ($models as $i => $pk) {
            $newPosition = $i + 1;
            $modelClass::objects()->filter(['pk' => $pk])->update([$this->sortingColumn => $newPosition]);
        }
        if (Mindy::app()->request->getIsAjax()) {
            Mindy::app()->controller->json(['success' => true]);
        } else {
            $this->redirect('admin:list', ['module' => $this->getModel()->getModuleName(), 'adminClass' => $this->classNameShort()]);
        }
    }
}
