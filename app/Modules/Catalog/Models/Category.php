<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Models;

use Mindy\Orm\Fields\BooleanField;
use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\DateTimeField;
use Mindy\Orm\Fields\DecimalField;
use Mindy\Orm\Fields\ImageField;
use Mindy\Orm\Fields\IntField;
use Mindy\Orm\Fields\SlugField;
use Mindy\Orm\Fields\TextField;
use Mindy\Orm\TreeModel;
use Modules\Catalog\CatalogModule;
use Modules\Ko\Fields\ManyToManySortField;
use Modules\Pages\PagesModule;

class Category extends TreeModel
{
    public $metaConfig = [
        'title' => 'name',
        'keywords' => 'description',
        'description' => 'description',
    ];

    public static function getFields()
    {
        return array_merge([
            'is_index' => [
                'class' => BooleanField::className(),
                'verboseName' => PagesModule::t('Is index'),
                'default' => false,
            ],
            'name' => [
                'class' => CharField::className(),
                'length' => 255,
                'verboseName' => CatalogModule::t('Name'),
            ],
            'url' => [
                'class' => SlugField::className(),
                'length' => 255,
                'unique' => true,
                'verboseName' => CatalogModule::t('Url'),
            ],

            'price_rub' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'verboseName' => CatalogModule::t('Price RUB'),
            ],
            'price_usd' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'verboseName' => CatalogModule::t('Price USD'),
                'null' => true,
            ],
            'price_eur' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'verboseName' => CatalogModule::t('Price EUR'),
                'null' => true,
            ],
            'price_kzt' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'verboseName' => CatalogModule::t('Price KZT'),
                'null' => true,
            ],
            'price_uah' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'verboseName' => CatalogModule::t('Price UAH'),
                'null' => true,
            ],
            'price_byr' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'verboseName' => CatalogModule::t('Price BYR'),
                'null' => true,
            ],

            'description' => [
                'class' => TextField::className(),
                'null' => true,
                'verboseName' => CatalogModule::t('Description'),
            ],
            'production' => [
                'class' => ManyToManySortField::className(),
                'modelClass' => Product::className(),
                'through' => ProductThrough::className(),
                'relatedName' => 'category',
                'verboseName' => CatalogModule::t('Productions'),
                'editable' => false,
                'order' => 'position',
            ],
            'woman' => [
                'class' => BooleanField::className(),
                'verboseName' => CatalogModule::t('Category for woman'),
            ],
            'min_size' => [
                'class' => IntField::className(),
                'verboseName' => CatalogModule::t('Minimal size'),
                'default' => 42,
            ],
            'max_size' => [
                'class' => IntField::className(),
                'verboseName' => CatalogModule::t('Maximum size'),
                'default' => 64,
            ],
            'image' => [
                'class' => ImageField::className(),
                'sizes' => [
                    'thumb' => [
                        233, 317,
                    ],
                    'thumb_slide' => [
                        62, 84,
                    ],
                    'resize' => [
                        372, 520,
                    ],
                    'detail' => [
                        504,
                    ],
                    'big' => [
                        978,
                    ],
                    'quick_look_slide' => [
                        90, 121,
                    ],
                    'detail_slide' => [
                        74, 97,
                    ],
                ],
                'verboseName' => CatalogModule::t('Image'),
            ],
            'image_hover' => [
                'class' => ImageField::className(),
                'sizes' => [
                    'thumb' => [
                        233, 317,
                    ],
                    'thumb_slide' => [
                        62, 84,
                    ],
                    'resize' => [
                        372, 520,
                    ],
                    'detail' => [
                        504,
                    ],
                    'big' => [
                        978,
                    ],
                    'quick_look_slide' => [
                        90, 121,
                    ],
                    'detail_slide' => [
                        74, 97,
                    ],
                ],
                'verboseName' => CatalogModule::t('Image'),
            ],
            'created_at' => [
                'class' => DateTimeField::className(),
                'autoNowAdd' => true,
                'verboseName' => CatalogModule::t('Created at'),
                'editable' => false,
            ],
            'updated_at' => [
                'class' => DateTimeField::className(),
                'autoNow' => true,
                'verboseName' => CatalogModule::t('Updated at'),
                'editable' => false,
            ],
        ], parent::getFields());
    }

    public function __toString()
    {
        return (string) $this->name;
    }

    public function getAbsoluteUrl()
    {
        return $this->reverse('ko:list', ['url' => $this->url]);
    }

    public function getAdminNames($instance = null)
    {
        $names = parent::getAdminNames($instance);
        $names[0] = $this->getModule()->t('Categories');

        return $names;
    }
}
