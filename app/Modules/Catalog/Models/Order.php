<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Models;

use Mindy\Orm\Fields\BooleanField;
use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\DateTimeField;
use Mindy\Orm\Fields\DecimalField;
use Mindy\Orm\Fields\ForeignField;
use Mindy\Orm\Fields\TextField;
use Mindy\Orm\Model;
use Modules\Catalog\CatalogModule;
use Modules\User\Models\User;

class Order extends Model
{
    public static function getFields()
    {
        return [
            'created_at' => [
                'class' => DateTimeField::className(),
                'autoNowAdd' => true,
                'verboseName' => CatalogModule::t('Created time'),
            ],
            'updated_at' => [
                'class' => DateTimeField::className(),
                'autoNow' => true,
                'verboseName' => CatalogModule::t('Updated time'),
            ],
            'user' => [
                'class' => ForeignField::className(),
                'modelClass' => User::className(),
                'null' => true,
                'verboseName' => CatalogModule::t('User'),
            ],
            'name' => [
                'class' => CharField::className(),
                'required' => true,
                'verboseName' => CatalogModule::t('Name'),
            ],
            'order' => [
                'class' => TextField::className(),
                'null' => false,
                'verboseName' => CatalogModule::t('Order'),
            ],
            'price' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'verboseName' => CatalogModule::t('Price'),
            ],
            'is_complete' => [
                'class' => BooleanField::className(),
                'default' => false,
                'verboseName' => CatalogModule::t('Is complete'),
            ],
        ];
    }
}
