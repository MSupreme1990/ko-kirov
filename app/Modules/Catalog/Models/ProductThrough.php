<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Models;

use Mindy\Orm\Fields\ForeignField;
use Mindy\Orm\Fields\IntField;
use Mindy\Orm\Model;
use Modules\Catalog\CatalogModule;

class ProductThrough extends Model
{
    public static function getFields()
    {
        return [
            'product' => [
                'class' => ForeignField::className(),
                'modelClass' => Product::className(),
                'verboseName' => CatalogModule::t('Product'),
            ],
            'category' => [
                'class' => ForeignField::className(),
                'modelClass' => Category::className(),
                'verboseName' => CatalogModule::t('Category'),
            ],
            'position' => [
                'class' => IntField::className(),
                'verboseName' => CatalogModule::t('Position'),
                'default' => 0,
                'null' => false,
            ],
        ];
    }

    public static function tableName()
    {
        return 'catalog_category_catalog_product';
    }
}
