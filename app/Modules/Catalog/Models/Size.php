<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Models;

use Mindy\Orm\Fields\IntField;
use Mindy\Orm\Model;
use Modules\Catalog\CatalogModule;

class Size extends Model
{
    public static function getFields()
    {
        return [
            'value' => [
                'class' => IntField::className(),
                'verboseName' => CatalogModule::t('Size'),
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->value;
    }
}
