<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Models;

use Mindy\Orm\Fields\BooleanField;
use Mindy\Orm\Fields\ForeignField;
use Mindy\Orm\Fields\ImageField;
use Mindy\Orm\Model;
use Modules\Catalog\CatalogModule;
use Modules\Core\Fields\Orm\PositionField;

class Image extends Model
{
    public static function getFields()
    {
        return [
            'image' => [
                'class' => ImageField::className(),
                'sizes' => [
                    'thumb' => [
                        233, 317,
                    ],
                    'thumb_slide' => [
                        62, 84,
                    ],
                    'resize' => [
                        372, 520,
                    ],
                    'quick_look_slide' => [
                        90, 121,
                    ],
                    'quick_look_slide_another' => [
                        107, 145,
                    ],
//
//                    'watermark_thumb' => [
//                        233, 317,
//                        'options' => [
//                            'watermark' => ''
//                        ]
//                    ],
//                    'watermark_thumb_slide' => [
//                        62, 84,
//                        'options' => [
//                            'watermark' => ''
//                        ]
//                    ],
//                    'watermark_resize' => [
//                        372, 520,
//                        'options' => [
//                            'watermark' => ''
//                        ]
//                    ],
//                    'watermark_quick_look_slide' => [
//                        90, 121,
//                        'options' => [
//                            'watermark' => ''
//                        ]
//                    ],
//                    'watermark_quick_look_slide_another' => [
//                        107, 145,
//                        'options' => [
//                            'watermark' => ''
//                        ]
//                    ],
                ],
                'verboseName' => CatalogModule::t('Image'),
            ],
            'product' => [
                'class' => ForeignField::className(),
                'modelClass' => Product::className(),
                'relatedName' => 'images',
                'verboseName' => CatalogModule::t('Product'),
            ],
            'position' => [
                'class' => PositionField::className(),
                'default' => 0,
                'verboseName' => CatalogModule::t('Position'),
            ],
            'is_main' => [
                'class' => BooleanField::className(),
                'verboseName' => CatalogModule::t('Is main'),
            ],
        ];
    }

    /**
     * @param $owner Model
     * @param $isNew
     */
    public function beforeSave($owner, $isNew)
    {
        if ($this->is_main) {
            $qs = $this->objects()->filter(['product' => $this->product]);
            if ($isNew == false) {
                $qs->exclude(['pk' => $this->pk]);
            }
            $qs->update(['is_main' => false]);
        }
    }
}
