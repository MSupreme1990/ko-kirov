<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Models;

use Mindy\Base\Mindy;
use Mindy\Orm\Fields\BooleanField;
use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\DateTimeField;
use Mindy\Orm\Fields\DecimalField;
use Mindy\Orm\Fields\ForeignField;
use Mindy\Orm\Fields\HasManyField;
use Mindy\Orm\Fields\IntField;
use Mindy\Orm\Fields\ManyToManyField;
use Mindy\Orm\Fields\SlugField;
use Mindy\Orm\Fields\TextField;
use Mindy\Orm\Model;
use Modules\Cart\Interfaces\ICartItem;
use Modules\Catalog\CatalogModule;
use Modules\Catalog\Forms\OrderForm;
use Modules\Catalog\Helpers\ImagePatchHelper;
use Modules\Ko\Fields\ManyToManySortField;

class Product extends Model implements ICartItem
{
    public $position;

    public $metaConfig = [
        'title' => 'name',
        'keywords' => 'content',
        'description' => 'content',
    ];

    public static function getFields()
    {
        return [
            'name' => [
                'class' => CharField::className(),
                'verboseName' => CatalogModule::t('Name'),
            ],
            'url' => [
                'class' => SlugField::className(),
                'length' => 255,
                'null' => false,
                'verboseName' => CatalogModule::t('Url'),
                'unique' => true,
            ],
            'price_source' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'null' => true,
            ],
            'price_discount' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'null' => true,
            ],
            'price_rub' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'verboseName' => CatalogModule::t('Price RUB'),
                'null' => true,
            ],
            'price_usd' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'verboseName' => CatalogModule::t('Price USD'),
                'null' => true,
            ],
            'price_eur' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'verboseName' => CatalogModule::t('Price EUR'),
                'null' => true,
            ],
            'price_kzt' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'verboseName' => CatalogModule::t('Price KZT'),
                'null' => true,
            ],
            'price_uah' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'verboseName' => CatalogModule::t('Price UAH'),
                'null' => true,
            ],
            'price_byr' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'verboseName' => CatalogModule::t('Price BYR'),
                'null' => true,
            ],

            'price_old_rub' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'verboseName' => CatalogModule::t('Old price RUB'),
                'null' => true,
            ],
            'price_old_usd' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'verboseName' => CatalogModule::t('Old price USD'),
                'null' => true,
            ],
            'price_old_eur' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'verboseName' => CatalogModule::t('Old price EUR'),
                'null' => true,
            ],
            'price_old_kzt' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'verboseName' => CatalogModule::t('Old price KZT'),
                'null' => true,
            ],
            'price_old_uah' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'verboseName' => CatalogModule::t('Old price UAH'),
                'null' => true,
            ],
            'price_old_byr' => [
                'class' => DecimalField::className(),
                'precision' => 10,
                'scale' => 2,
                'verboseName' => CatalogModule::t('Old price BYR'),
                'null' => true,
            ],

            'discount_percentage' => [
                'class' => IntField::className(),
                'null' => true,
                'verboseName' => CatalogModule::t('Discount percentage'),
            ],
            'woman' => [
                'class' => BooleanField::className(),
                'verboseName' => CatalogModule::t('For woman'),
                'default' => true,
            ],
            'min_size' => [
                'class' => IntField::className(),
                'verboseName' => CatalogModule::t('Minimal size'),
                'null' => true,
            ],
            'max_size' => [
                'class' => IntField::className(),
                'verboseName' => CatalogModule::t('Maximum size'),
                'null' => true,
            ],
            'code' => [
                'class' => CharField::className(),
                'null' => true,
                'verboseName' => CatalogModule::t('Code'),
            ],
            'content_short' => [
                'class' => TextField::className(),
                'null' => true,
                'verboseName' => CatalogModule::t('Short content'),
            ],
            'content' => [
                'class' => TextField::className(),
                'null' => true,
                'verboseName' => CatalogModule::t('Content'),
            ],
            'hits' => [
                'class' => IntField::className(),
                'editable' => false,
                'verboseName' => CatalogModule::t('Hits'),
                'default' => 0,
            ],
            'created_at' => [
                'class' => DateTimeField::className(),
                'autoNowAdd' => true,
                'verboseName' => CatalogModule::t('Created time'),
                'editable' => false,
            ],
            'updated_at' => [
                'class' => DateTimeField::className(),
                'autoNow' => true,
                'verboseName' => CatalogModule::t('Updated time'),
                'editable' => false,
            ],
            'is_published' => [
                'class' => BooleanField::className(),
                'default' => false,
                'verboseName' => CatalogModule::t('Is published'),
            ],
            'is_new' => [
                'class' => BooleanField::className(),
                'default' => false,
                'verboseName' => CatalogModule::t('Is new'),
            ],
            'is_popular' => [
                'class' => BooleanField::className(),
                'default' => false,
                'verboseName' => CatalogModule::t('Is popular'),
            ],
            'images' => [
                'class' => HasManyField::className(),
                'modelClass' => Image::className(),
                'verboseName' => CatalogModule::t('Name'),
                'to' => 'product_id',
                'editable' => false,
            ],
            'discount' => [
                'class' => ForeignField::className(),
                'modelClass' => Discount::className(),
                'verboseName' => CatalogModule::t('Discount'),
                'null' => true,
            ],
            'category' => [
                'class' => ForeignField::className(),
                'modelClass' => Category::className(),
                'verboseName' => CatalogModule::t('Default category'),
                'null' => true,
            ],
            'categories' => [
                'class' => ManyToManySortField::className(),
                'modelClass' => Category::className(),
                'through' => ProductThrough::className(),
                'verboseName' => CatalogModule::t('Categories'),
                'order' => 'position',
            ],
            'product_through' => [
                'class' => HasManyField::className(),
                'modelClass' => ProductThrough::className(),
                'editable' => false,
                'from' => 'id',
                'to' => 'product_id',
            ],
            'sizes' => [
                'class' => ManyToManyField::className(),
                'modelClass' => Size::className(),
                'verboseName' => CatalogModule::t('Sizes'),
            ],
            'related' => [
                'class' => ManyToManyField::className(),
                'verboseName' => CatalogModule::t('Related'),
                'modelClass' => self::className(),
            ],
        ];
    }

    public function beforeSave($owner, $isNew)
    {
        $owner->price_source = empty($owner->price_rub) ? $owner->price_old_rub : $owner->price_rub;
        if (false === empty($owner->price_old_rub)) {
            $owner->price_discount = $owner->price_old_rub - $owner->price_rub;
        }
    }

    public static function objectsManager($instance = null)
    {
        $className = get_called_class();

        return new ProductManager($instance ? $instance : new $className());
    }

    public function __toString()
    {
        return (string) $this->name;
    }

    public function getAbsoluteUrl()
    {
        return $this->reverse('ko:product_detail', ['url' => $this->url]);
    }

    public function order(OrderForm $form)
    {
        if ($data = $form->send()) {
            $app = Mindy::app();
            list($subject, $message) = $data;
            $order = new Order([
                'price' => $this->price,
                'user' => $app->user,
                'name' => $subject,
                'order' => $message,
            ]);

            return $order->save();
        }

        return false;
    }

    public function getImage()
    {
        $main = $this->images->filter(['is_main' => true])->get();
        if ($main) {
            return $main;
        }

        return $this->images->limit(1)->offset(0)->get();
    }

    /**
     * @return mixed unique product identification
     */
    public function getUniqueId()
    {
        return $this->pk;
    }

    /**
     * @return int|float
     */
    public function getPrice()
    {
        $symbol = strtolower(Mindy::app()->getModule('Ko')->getCurrency());
        $fieldName = 'price_'.$symbol;
        $fieldOldName = 'price_old_'.$symbol;

        return empty((int) $this->{$fieldName}) ? (float) $this->{$fieldOldName} : (float) $this->{$fieldName};
    }

    /**
     * @param $quantity int
     * @param $type mixed
     * @param $data array|null
     *
     * @return int|float return total product price based on quantity and weight type
     */
    public function recalculate($quantity, $type, $data)
    {
        return $this->getPrice() * $quantity;
    }

    public function afterSave($owner, $isNew)
    {
        $pH = CatalogModule::get_products_hash();
        $cache_key = 'products_all_'.$pH['hash'];
        Mindy::app()->cache->delete($cache_key);
        $cache_key .= '_mobile';
        Mindy::app()->cache->delete($cache_key);
        Mindy::app()->cache->delete('products_hash');

        parent::afterSave($owner, $isNew);
    }

    public function toArray()
    {
        $objImage = new Image();
        $rawImages = Image::objects()
            ->select('catalog_image_1.id, image')
            ->filter([
                'product__is_published' => true,
                'product_id' => $this->id,
            ])
            ->order(['-is_main'])
            ->asArray()
            ->all();

        $images = [];
        foreach ($rawImages as $rImage) {
            $images[] = ImagePatchHelper::getPatches('image', $objImage, $rImage['image'], true);
        }

        return array_merge(parent::toArray(), [
            'discount' => $this->discount ? $this->discount->toArray() : [],
            'category' => $this->category ? $this->category->toArray() : [],
            'images' => $images,
            'sizes' => $this->sizes->valuesList(['value'], true),
        ]);
    }
}
