<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Forms;

use Mindy\Form\Fields\DropDownField;
use Mindy\Form\ModelForm;
use Modules\Catalog\Models\Category;
use Modules\Catalog\Models\ProductThrough;

class ProductThroughFilterForm extends ModelForm
{
    public $exclude = ['product', 'position'];

    public function getFields()
    {
        $choices = function () {
            $list = ['' => ''];

            $qs = Category::objects()->order(['root', 'lft']);
            $parents = $qs->all();
            foreach ($parents as $model) {
                $level = $model->level ? $model->level - 1 : $model->level;
                $list[$model->pk] = $level ? str_repeat('..', $level).' '.$model->name : $model->name;
            }

            return $list;
        };

        return [
            'category' => [
                'class' => DropDownField::className(),
                'choices' => $choices,
            ],
        ];
    }

    public function getModel()
    {
        return new ProductThrough();
    }

    public function getQsFilter()
    {
        if ($categories = $this->category->getValue()) {
            $categories = is_array($categories) ? $categories : [$categories];

            return [
                'category__in' => $categories,
            ];
        }

        return [];
    }
}
