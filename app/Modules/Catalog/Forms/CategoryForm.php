<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Forms;

use Mindy\Form\ModelForm;
use Modules\Catalog\Models\Category;
use Modules\Meta\Forms\MetaInlineForm;

class CategoryForm extends ModelForm
{
    public function getModel()
    {
        return new Category();
    }

    public function getInlines()
    {
        return [
            ['meta' => MetaInlineForm::className()],
        ];
    }
}
