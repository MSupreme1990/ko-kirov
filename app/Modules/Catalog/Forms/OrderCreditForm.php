<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Forms;

use Mindy\Form\Fields\CharField;
use Mindy\Form\Fields\EmailField;
use Mindy\Form\Fields\LicenseField;
use Modules\Catalog\CatalogModule;

class OrderCreditForm extends OrderForm
{
    public $templateCode = 'catalog.order_credit';

    public function getFields()
    {
        return [
            'name' => [
                'class' => CharField::className(),
                'required' => true,
                'label' => CatalogModule::t('Name'),
            ],
            'phone' => [
                'class' => CharField::className(),
                'required' => true,
                'label' => CatalogModule::t('Phone'),
                'html' => [
                    'class' => 'phone',
                ],
            ],
            'city' => [
                'class' => EmailField::className(),
                'required' => true,
                'label' => CatalogModule::t('City'),
            ],
            'is_accepted' => [
                'class' => LicenseField::className(),
                'label' => CatalogModule::t('I agree to the processing of personal data'),
                'htmlLabel' => [
                    'data-url' => '#license',
                    'data-width' => 650,
                    'class' => 'mmodal',
                ],
            ],
        ];
    }
}
