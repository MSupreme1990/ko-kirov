<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Catalog\Forms;

use Mindy\Form\Fields\DropDownField;
use Mindy\Form\Fields\Select2Field;
use Mindy\Form\Fields\UEditorField;
use Mindy\Form\ModelForm;
use Modules\Catalog\CatalogModule;
use Modules\Catalog\Models\Category;
use Modules\Catalog\Models\Product;
use Modules\Files\Fields\FilesField;
use Modules\Meta\Forms\MetaInlineForm;

class ProductForm extends ModelForm
{
    public function getFieldsets()
    {
        return [
            CatalogModule::t('Main information') => [
                'name',
                'price_rub',
                'price_old_rub',
                'content',
                'content_short',
                'is_published',
                'code',
                'discount',
                'min_size',
                'max_size',
                'sizes',
                'discount_percentage',
                'is_new',
                'is_popular',
                'is_published',
                'url',
                'woman',
            ],
            CatalogModule::t('Related products') => [
                'related',
            ],
            CatalogModule::t('Category settings') => [
                'category',
                'categories',
            ],
            CatalogModule::t('Images') => [
                'images',
            ],
        ];
    }

    public function getFields()
    {
        return [
            'related' => Select2Field::className(),
            'category' => [
                'class' => DropDownField::className(),
                'choices' => function () {
                    $list = ['' => ''];
                    $qs = Category::objects()->order(['root', 'lft']);
                    $parents = $qs->all();
                    foreach ($parents as $model) {
                        $level = $model->level ? $model->level - 1 : $model->level;
                        $list[$model->pk] = $level ? str_repeat('..', $level).' '.$model->name : $model->name;
                    }

                    return $list;
                },
                'label' => CatalogModule::t('Default category'),
            ],
            'categories' => [
                'class' => DropDownField::className(),
                'choices' => function () {
                    $list = [];
                    $qs = Category::objects()->order(['root', 'lft']);
                    $parents = $qs->all();
                    foreach ($parents as $model) {
                        $level = $model->level ? $model->level - 1 : $model->level;
                        $list[$model->pk] = $level ? str_repeat('..', $level).' '.$model->name : $model->name;
                    }

                    return $list;
                },
                'multiple' => true,
                'label' => CatalogModule::t('Categories'),
            ],
            'images' => [
                'class' => FilesField::className(),
                'relatedFileField' => 'image',
            ],
            'content' => [
                'class' => UEditorField::className(),
            ],
        ];
    }

    public function getModel()
    {
        return new Product();
    }

    public function getInlines()
    {
        return [
            ['meta' => MetaInlineForm::className()],
        ];
    }

//    public function getInlines()
//    {
//        return [
//            ['product' => ImageInlineForm::className()]
//        ];
//    }
}
