<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    '/' => [
        'name' => 'list',
        'callback' => '\Modules\Cart\Controllers\CartController:list',
    ],
    '/delete/{key}' => [
        'name' => 'delete',
        'callback' => '\Modules\Cart\Controllers\CartController:delete',
    ],
    '/quantity/{key}-{quantity:\d+}' => [
        'name' => 'quantity',
        'callback' => '\Modules\Cart\Controllers\CartController:quantity',
    ],
    '/quantity/{key}-inc' => [
        'name' => 'quantity_increase',
        'callback' => '\Modules\Cart\Controllers\CartController:increase',
    ],
    '/quantity/{key}-dec' => [
        'name' => 'quantity_decrease',
        'callback' => '\Modules\Cart\Controllers\CartController:decrease',
    ],
];
