<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Cart;

use Mindy\Base\Mindy;
use Mindy\Base\Module;

class CartModule extends Module
{
    /**
     * @var
     */
    public $listRoute;
    /**
     * @var array
     */
    public $cartConfig = [
        'class' => '\Modules\Cart\Components\Cart',
    ];

    public static function preConfigure()
    {
        $tpl = Mindy::app()->template;
        $tpl->addHelper('cart', function () {
            return Mindy::app()->getModule('Cart')->getCart();
        });
    }

    public function init()
    {
        $this->setComponent('cart', $this->cartConfig);
    }

    /**
     * @return \Modules\Cart\Components\Cart
     */
    public function getCart()
    {
        return $this->getComponent('cart');
    }
}
