<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Cart\Tests;

use Mindy\Helper\Traits\Accessors;
use Mindy\Helper\Traits\Configurator;
use Modules\Cart\Interfaces\ICartItem;

class Product implements ICartItem
{
    use Accessors, Configurator;

    public $id;

    public $price;

    public function __toString()
    {
        return (string) $this->id;
    }

    /**
     * @return mixed unique product identification
     */
    public function getUniqueId()
    {
        return $this->id;
    }

    /**
     * @return int|float
     */
    public function getPrice()
    {
        return $this->price;
    }
}
