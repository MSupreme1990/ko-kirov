<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    'Delete' => 'Удалить',
    'Price' => 'Цена',
    'Attributes' => 'Аттрибуты',
    'Quantity' => 'Количество',
    'Name' => 'Название',
    'Actions' => 'Действия',
    'Cart' => 'Корзина',
    'Position sucessfully removed' => 'Позиция удалена',
    'Position sucessfully added' => 'Позиция добавлена',
    'Quantity updated' => 'Количество обновлено',
    'Product added' => 'Продукт добавлен',
    'Position updated' => 'Позиция обновлена',
];
