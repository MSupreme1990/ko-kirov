<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Cart\Components;

/**
 * Interface IDiscount
 */
interface IDiscount
{
    /**
     * Apply discount to CartItem position. If new prices is equal old price - return old price.
     *
     * @param Cart     $cart
     * @param CartItem $item
     *
     * @return int|float new price with discount
     */
    public function applyDiscount(Cart $cart, CartItem $item);
}
