<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Menu\Models;

use Mindy\Orm\Fields\CharField;
use Mindy\Orm\TreeModel;
use Modules\Menu\MenuModule;

/**
 * Class Menu
 *
 * @method static \Mindy\Orm\TreeManager tree($instance = null)
 * @method static \Mindy\Orm\Manager objects($instance = null)
 */
class Menu extends TreeModel
{
    public static function getFields()
    {
        return array_merge(parent::getFields(), [
            'slug' => [
                'class' => CharField::className(),
                'verboseName' => MenuModule::t('Slug'),
                'null' => true,
            ],
            'name' => [
                'class' => CharField::className(),
                'verboseName' => MenuModule::t('Name'),
            ],
            'url' => [
                'class' => CharField::className(),
                'verboseName' => MenuModule::t('Url'),
            ],
        ]);
    }

    public function __toString()
    {
        return (string) $this->name;
    }
}
