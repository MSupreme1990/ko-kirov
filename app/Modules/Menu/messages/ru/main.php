<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return  [
  'menu' => 'Меню',
  'Parent' => 'Родитель',
  'Slug' => 'Ключ',
  'Name' => 'Наименование',
  'Url' => 'Ссылка',
  'Menu {url} was created' => 'Меню {url} было добавлено',
  'Menu {url} was updated' => 'Меню {url} было обновлено',
  'Menu {url} was deleted' => 'Меню {url} было удалено',
  'Position' => 'Порядок',
  'Menu' => 'Меню',
  'Menu items' => 'Пункты меню',
  'Create menu' => 'Создать меню',
  'Update menu: {name}' => 'Редактирование меню: {name}',
];
