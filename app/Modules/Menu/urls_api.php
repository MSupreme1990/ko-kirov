<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    '/list' => [
        'name' => 'list',
        'callback' => '\Modules\Menu\Controllers\ApiController:list',
    ],
    '/{slug:c}' => [
        'name' => 'get',
        'callback' => '\Modules\Menu\Controllers\ApiController:get',
    ],
];
