<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Menu\Controllers;

use Mindy\Base\Mindy;
use Modules\Core\Controllers\ApiBaseController;
use Modules\Menu\Helpers\MenuHelper;
use Modules\Menu\Models\Menu;

class ApiController extends ApiBaseController
{
    public function actionGet($slug)
    {
        $data = MenuHelper::getNestedMenu($slug);
        echo $this->json($data ? $data : []);
        Mindy::app()->end();
    }

    public function actionList()
    {
        $data = [];
        foreach (Menu::objects()->filter(['level' => 1])->all() as $menu) {
            $data[$menu->slug] = MenuHelper::getNestedMenu($menu->slug);
        }
        echo $this->json($data);
        Mindy::app()->end();
    }
}
