<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Menu;

use Mindy\Base\Mindy;
use Mindy\Base\Module;

class MenuModule extends Module
{
    public $defaultController = 'menu';

    public static function preConfigure()
    {
        $tpl = Mindy::app()->template;
        $tpl->addHelper('get_menu', ['\Modules\Menu\Helpers\MenuHelper', 'renderMenu']);
    }

    public function getMenu()
    {
        return [
            'name' => $this->getName(),
            'items' => [
                [
                    'name' => $this->getName(),
                    'adminClass' => 'MenuAdmin',
                ],
            ],
        ];
    }
}
