<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Menu\Helpers;

use Mindy\Base\Mindy;
use Mindy\Utils\RenderTrait;
use Modules\Menu\Models\Menu;

class MenuHelper
{
    use RenderTrait;

    public static function renderMenu($slug, $template = 'menu/menu.html')
    {
        $menu = Menu::objects()->get(['slug' => $slug]);

        return $menu ? self::renderTemplate($template, [
            'items' => $menu->tree()->descendants()->asTree()->all(),
        ]) : '';
    }

    /**
     * @param $slug
     *
     * @return array|bool
     */
    public static function getNestedMenu($slug)
    {
        $model = Menu::objects()->get([
            'slug' => $slug,
        ]);
        if ($model === null) {
            return false;
        }

        return iterator_to_array(self::getItems($model));
    }

    protected static function getItems(Menu $model)
    {
        $items = $model->tree()->descendants()->asTree()->all();

        return self::iterateItems($items);
    }

    protected static function iterateItems(array $items)
    {
        foreach ($items as $item) {
            $item['react_route'] = self::getReactRoute('GET', $item['url']);
            if (isset($item['items']) && !empty($item['items'])) {
                $old = $item['items'];
                $item['items'] = [];
                foreach (self::iterateItems($old) as $t) {
                    $item['items'][] = $t;
                }
            }
            yield $item;
        }
    }

    protected static function getReactRoute($httpMethod, $uri)
    {
        $router = Mindy::app()->urlManager;
        $uri = strtok($uri, '?');
        $uri = ltrim($uri, '/');
        $data = $router->dispatchRoute($httpMethod, $uri);
        if ($data === false) {
            if ($router->trailingSlash && substr($uri, -1) !== '/') {
                $data = $router->dispatchRoute($httpMethod, $uri.'/');
                if ($data === false) {
                    return false;
                }

                return self::formatReactRoute($data);
            }

            return false;
        }

        return self::formatReactRoute($data);
    }

    protected static function formatReactRoute(array $data)
    {
        list($routeData, $paramsData) = $data;
        $tmp = explode('\\', array_shift($routeData));
        $reactRoute = $tmp[2].ucfirst(array_shift($routeData));

        return [
            'route' => $reactRoute,
            'data' => $paramsData,
        ];
    }
}
