<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Translate;

use Mindy\Base\Module;

class TranslateModule extends Module
{
    public function getVersion()
    {
        return '0.2';
    }

    public function getMenu()
    {
        return [
            'name' => self::t('Translate'),
            'items' => [
                [
                    'name' => self::t('Translate'),
                    'url' => 'translate:index',
                    'icon' => 'icon-translate',
                ],
            ],
        ];
    }
}
