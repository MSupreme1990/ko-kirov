<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Translate\Tables;

use Mindy\Base\Mindy;
use Mindy\Table\Columns\LinkColumn;
use Mindy\Table\Table;
use Modules\Translate\TranslateModule;

class LanguageTable extends Table
{
    public function getColumns()
    {
        $url = Mindy::app()->urlManager;

        return [
            [
                'class' => LinkColumn::className(),
                'route' => function ($record) use ($url) {
                    return $url->reverse('translate:language', ['language' => $record]);
                },
                'title' => TranslateModule::t('Language'),
            ],
        ];
    }
}
