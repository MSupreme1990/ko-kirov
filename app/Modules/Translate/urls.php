<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    '/' => [
        'name' => 'index',
        'callback' => '\Modules\Translate\Controllers\TranslateController:index',
    ],
    '/process' => [
        'name' => 'process',
        'callback' => '\Modules\Translate\Controllers\TranslateController:process',
    ],
    '/lang/{lang:\w+}' => [
        'name' => 'language',
        'callback' => '\Modules\Translate\Controllers\TranslateController:language',
    ],
];
