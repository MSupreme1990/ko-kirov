<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Translate\Commands;

use Mindy\Console\ConsoleCommand;
use Modules\Translate\Helpers\TranslateHelper;

class TranslateCommand extends ConsoleCommand
{
    public function actionCollect()
    {
        $helper = new TranslateHelper();
        $helper->collect();
    }
}
