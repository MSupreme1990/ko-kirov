<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Middleware;

use Mindy\Http\Request;
use Mindy\Middleware\Middleware;

class UtmMiddleware extends Middleware
{
    /**
     * Event owner RenderTrait
     *
     * @param \Mindy\Http\Request $request
     */
    public function processResponse(Request $request)
    {
        if (in_array(http_response_code(), [301, 302, 303])) {
            $params = [];
            if (!empty($request->get)) {
                foreach ($request->get as $k => $v) {
                    if (is_string($v)) {
                        if (strtolower($v) != 'null' && !empty($v)) {
                            if ((strpos(strtolower($k), 'utm_') === 0) || in_array(strtolower($k), ['gclid', 'yclid'])) {
                                $params[$k] = $v;
                            }
                        }
                    }
                }
            }

            if (!empty($params)) {
                $headers = headers_list();
                $headers_arr = [];
                foreach ($headers as $header) {
                    list($key, $value) = explode(':', $header, 2);
                    $headers_arr[trim($key)] = trim($value);
                }

                $path = $headers_arr['Location'];
                $path .= ((strpos($path, '?') === false) ? '?' : '&');
                $path .= http_build_query($params);

                header('Location: '.$path, true);
            }
        }
    }
}
