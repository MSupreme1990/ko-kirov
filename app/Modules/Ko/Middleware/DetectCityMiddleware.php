<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Middleware;

use Mindy\Base\Mindy;
use Mindy\Helper\Console;
use Mindy\Http\Cookie;
use Mindy\Http\Request;
use Mindy\Middleware\Middleware;
use Modules\Ko\Vendor\SxGeo;
use Modules\Sites\Models\Site;

class DetectCityMiddleware extends Middleware
{
    private $_sxgeo = null;

    private $EUR = [
        'AT', 'BE', 'BG',
        'HU', 'GB', 'GR', 'DE', 'DK',
        'IT', 'IE', 'ES', 'CY', 'LU',
        'LV', 'LT', 'MT', 'NL', 'PT',
        'PL', 'RO', 'SI', 'SK', 'FR',
        'FI', 'HR', 'CZ', 'SE ', 'EE',
    ];

    private $USD = [
    ];

    protected function detectCurrency($country)
    {
        // temporary? disabled
        return 'RUB';

        if ($country == 'RU') {
            $currency = 'RUB';
        } elseif ($country == 'BY') {
            $currency = 'BYR';
        } elseif ($country == 'KZ') {
            $currency = 'KZT';
        } elseif ($country == 'UA') {
            $currency = 'UAH';
        } elseif (in_array($country, $this->EUR)) {
            $currency = 'EUR';
        } elseif (in_array($country, $this->USD)) {
            $currency = 'USD';
        } else {
            $currency = 'USD';
        }

        return $currency;
    }

    public function processRequest(Request $request)
    {
        if (!Console::isCli()) {
            $ip = $_SERVER['REMOTE_ADDR'];
            $country = $this->getSxGeo()->getCountry($ip);
            if ($country) {
                $currency = $this->detectCurrency($country);
            } else {
                $currency = 'RUB';
            }
            Mindy::app()->getModule('Ko')->setCurrency($currency);

            $cookie = $request->cookies;
            $storedCity = $cookie->get('city');
            if ($request->getDomain() == MAIN_DOMAIN && empty($storedCity->value)) {
                $geo = $this->getSxGeo()->getCityFull($ip);
                if (isset($geo['city']) && !empty($geo['city'])) {
                    $city = $geo['city'];
                    $model = Site::objects()->filter(['city__name' => $city['name_ru']])->limit(1)->get();

                    if ($model !== null && $model->domain != str_replace('http://', '', MAIN_DOMAIN)) {
                        $cookie->add('city', new Cookie('city', $model->domain, [
                            'domain' => '.'.str_replace('http://', '', MAIN_DOMAIN),
                        ]));
                    }
                }
            } elseif ($request->getDomain() != MAIN_DOMAIN) {
                $model = Site::objects()
                    ->filter([
                        'domain' => str_replace('http://', '', $request->getDomain()),
                    ])->get();
                if ($model !== null) {
                    $cookie->add('city', new Cookie('city', $model->domain, [
                        'domain' => '.'.str_replace('http://', '', MAIN_DOMAIN),
                    ]));
                }
            }
        }
    }

    protected function getSxGeo()
    {
        if ($this->_sxgeo === null) {
            $this->_sxgeo = new SxGeo(__DIR__.'/../data/SxGeoCity.dat', SXGEO_BATCH | SXGEO_MEMORY);
        }

        return $this->_sxgeo;
    }
}
