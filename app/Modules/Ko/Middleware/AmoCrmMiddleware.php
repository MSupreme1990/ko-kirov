<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Middleware;

use Mindy\Http\Request;
use Mindy\Middleware\Middleware;

class AmoCrmMiddleware extends Middleware
{
    protected $amoCookieVars = [
        'utm_source',
        'utm_campaign',
        'utm_content',
        'utm_medium',
        'utm_term',
        'gclid',
        'yclid',
    ];

    /**
     * Event owner RenderTrait
     *
     * @param \Mindy\Http\Request $request
     */
    public function processRequest(Request $request)
    {
        foreach ($this->amoCookieVars as $var) {
            $value = $request->get->get($var, false);
            if ($value !== false) {
                $request->cookies->add($var, $value);
            }
        }
    }
}
