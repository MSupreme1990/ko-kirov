<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    '/save-phone' => [
        'name' => 'savePhone',
        'callback' => '\Modules\Ko\Controllers\ApiController:savePhone',
    ],
    '/gift' => [
        'name' => 'gift',
        'callback' => '\Modules\Ko\Controllers\ApiController:gift',
    ],
    '/gift_extra' => [
        'name' => 'gift_extra',
        'callback' => '\Modules\Ko\Controllers\ApiController:giftExtra',
    ],
    '/gift_sales' => [
        'name' => 'gift_sales',
        'callback' => '\Modules\Ko\Controllers\ApiController:giftSales',
    ],
    '/gift_sales_extra' => [
        'name' => 'gift_sales_extra',
        'callback' => '\Modules\Ko\Controllers\ApiController:giftSalesExtra',
    ],
    '/sales' => [
        'name' => 'sales',
        'callback' => '\Modules\Ko\Controllers\ApiController:sales',
    ],
    '/question' => [
        'name' => 'question',
        'callback' => '\Modules\Ko\Controllers\ApiController:question',
    ],
    '/question_extra' => [
        'name' => 'question_extra',
        'callback' => '\Modules\Ko\Controllers\ApiController:questionExtra',
    ],
    '/reviews' => [
        'name' => 'reviews',
        'callback' => '\Modules\Ko\Controllers\ApiController:reviews',
    ],
    '/reviews_create' => [
        'name' => 'reviews_create',
        'callback' => '\Modules\Ko\Controllers\ApiController:reviewsCreate',
    ],
    '/reviews_expert' => [
        'name' => 'reviews_expert',
        'callback' => '\Modules\Ko\Controllers\ApiController:reviewsExpert',
    ],
    '/video_reviews' => [
        'name' => 'video_reviews',
        'callback' => '\Modules\Ko\Controllers\ApiController:videoReviews',
    ],
    '/quick_order' => [
        'name' => 'quick_order',
        'callback' => '\Modules\Ko\Controllers\ApiController:quickOrder',
    ],
    '/credit_order' => [
        'name' => 'credit_order',
        'callback' => '\Modules\Ko\Controllers\ApiController:creditOrder',
    ],
    '/paylate/create' => [
        'name' => 'paylate_create',
        'callback' => '\Modules\Ko\Controllers\Api\PaylateApiController:create',
    ],
    '/paylate/status' => [
        'name' => 'paylate_status',
        'callback' => '\Modules\Ko\Controllers\Api\PaylateApiController:status',
    ],
    '/credit_possible' => [
        'name' => 'credit_possible',
        'callback' => '\Modules\Ko\Controllers\ApiController:creditPossible',
    ],
    '/detect_size' => [
        'name' => 'detect_size',
        'callback' => '\Modules\Ko\Controllers\ApiController:detectSize',
    ],
    '/geo' => [
        'name' => 'geo',
        'callback' => '\Modules\Ko\Controllers\ApiController:geo',
    ],
    '/request-call' => [
        'name' => 'request_call',
        'callback' => '\Modules\Ko\Controllers\ApiController:requestCall',
    ],
    '/price-delivery' => [
        'name' => 'price_delivery',
        'callback' => '\Modules\Ko\Controllers\ApiController:priceDelivery',
    ],
    '/seo' => [
        'name' => 'seo',
        'callback' => '\Modules\Ko\Controllers\ApiController:seo',
    ],
    '/feedback' => [
        'name' => 'feedback',
        'callback' => '\Modules\Ko\Controllers\ApiController:feedback',
    ],
    '/salesback' => [
        'name' => 'salesback',
        'callback' => '\Modules\Ko\Controllers\ApiController:salesback',
    ],
    '/banner' => [
        'name' => 'banner',
        'callback' => '\Modules\Ko\Controllers\ApiController:banner',
    ],
    '/cart/' => [
        'name' => 'cart',
        'callback' => '\Modules\Ko\Controllers\CartController:list',
    ],
    '/cart/add/{uniqueId}/{quantity:i}' => [
        'name' => 'cart_add',
        'callback' => '\Modules\Ko\Controllers\CartController:add',
    ],
    '/cart/update/{key}/{uniqueId}/{quantity:i}' => [
        'name' => 'cart_update',
        'callback' => '\Modules\Ko\Controllers\CartController:update',
    ],
    '/cart/quantity/{key}/{quantity:i}' => [
        'name' => 'cart_quantity',
        'callback' => '\Modules\Ko\Controllers\CartController:quantity',
    ],
    '/cart/delete/{key}' => [
        'name' => 'cart_delete',
        'callback' => '\Modules\Ko\Controllers\CartController:delete',
    ],
    '/cart/list' => [
        'name' => 'cart_list',
        'callback' => '\Modules\Ko\Controllers\CartController:list',
    ],
    '/cart/list/json' => [
        'name' => 'cart_list_json',
        'callback' => '\Modules\Ko\Controllers\CartController:jsonList',
    ],
    '/cart-order' => [
        'name' => 'order',
        'callback' => '\Modules\Ko\Controllers\ApiController:cartOrder',
    ],
    '/cart-order-quick' => [
        'name' => 'order_quick',
        'callback' => '\Modules\Ko\Controllers\ApiController:cartOrderQuick',
    ],
    '/coupon/{code:.*}' => [
        'name' => 'search',
        'callback' => '\Modules\Ko\Controllers\ApiController:coupon',
    ],
    '/get_coupon' => [
        'name' => 'search',
        'callback' => '\Modules\Ko\Controllers\ApiController:getcoupon',
    ],
    '/send-to-email' => [
        'name' => 'recent_send_to_email',
        'callback' => '\Modules\Ko\Controllers\ApiController:recentsendtoemail',
    ],

    '/get/seo/{url:.*}' => [
        'name' => 'seo',
        'callback' => '\Modules\Ko\Controllers\ApiController:seo',
    ],

    '/v1-2/settings/{type:.*}' => [
        'name' => 'settings',
        'callback' => '\Modules\Ko\Controllers\Api_v12_Controller:settings',
    ],

    '/product/view' => [
        'callback' => '\Modules\Ko\Controllers\Api\ProductApiController:view',
    ],
    '/product/list' => [
        'callback' => '\Modules\Ko\Controllers\Api\ProductApiController:list',
    ],
    '/product/related' => [
        'callback' => '\Modules\Ko\Controllers\Api\ProductApiController:related',
    ],
    '/product/autocomplete' => [
        'callback' => '\Modules\Ko\Controllers\Api\ProductApiController:autocomplete',
    ],
    '/product/search' => [
        'callback' => '\Modules\Ko\Controllers\Api\ProductApiController:search',
    ],
    '/product/popular' => [
        'callback' => '\Modules\Ko\Controllers\Api\ProductApiController:popular',
    ],
    '/product/new' => [
        'callback' => '\Modules\Ko\Controllers\Api\ProductApiController:new',
    ],
    '/category/list' => [
        'callback' => '\Modules\Ko\Controllers\Api\CategoryApiController:list',
    ],
    '/banner/list' => [
        'callback' => '\Modules\Ko\Controllers\Api\BannerApiController:list',
    ],
];
