<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Commands;

use Mindy\Console\ConsoleCommand;
use Modules\Catalog\Models\Category;
use Modules\Catalog\Models\Product;
use Modules\Currency\Models\Currency;

class SyncCurrencyCommand extends ConsoleCommand
{
    public function actionIndex()
    {
        $currency = Currency::objects()->latest()->get();
        if ($currency) {
            $data = $currency->data;
            foreach (Category::objects()->all() as $model) {
                $model->price_usd = $this->calculateCurrency($model->price_rub, $data['USD']);
                $model->price_eur = $this->calculateCurrency($model->price_rub, $data['EUR']);
                $model->price_kzt = $this->calculateCurrency($model->price_rub, $data['KZT']);
                $model->price_uah = $this->calculateCurrency($model->price_rub, $data['UAH']);
                $model->price_byr = $this->calculateCurrency($model->price_rub, $data['BYR']);

                $model->save([
                    'price_usd',
                    'price_eur',
                    'price_kzt',
                    'price_uah',
                    'price_byr',
                ]);
            }

            foreach (Product::objects()->batch(100) as $models) {
                foreach ($models as $model) {
                    $model->price_usd = $this->calculateCurrency($model->price_rub, $data['USD']);
                    $model->price_eur = $this->calculateCurrency($model->price_rub, $data['EUR']);
                    $model->price_kzt = $this->calculateCurrency($model->price_rub, $data['KZT']);
                    $model->price_uah = $this->calculateCurrency($model->price_rub, $data['UAH']);
                    $model->price_byr = $this->calculateCurrency($model->price_rub, $data['BYR']);

                    $model->price_old_usd = $this->calculateCurrency($model->price_old_rub, $data['USD']);
                    $model->price_old_eur = $this->calculateCurrency($model->price_old_rub, $data['EUR']);
                    $model->price_old_kzt = $this->calculateCurrency($model->price_old_rub, $data['KZT']);
                    $model->price_old_uah = $this->calculateCurrency($model->price_old_rub, $data['UAH']);
                    $model->price_old_byr = $this->calculateCurrency($model->price_old_rub, $data['BYR']);

                    $model->save([
                        'price_usd',
                        'price_eur',
                        'price_kzt',
                        'price_uah',
                        'price_byr',

                        'price_old_usd',
                        'price_old_eur',
                        'price_old_kzt',
                        'price_old_uah',
                        'price_old_byr',
                    ]);
                }
            }
        }
    }

    protected function calculateCurrency($nominal, $new)
    {
        return $nominal / $new;
    }
}
