<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Commands;

use Mindy\Console\ConsoleCommand;
use Modules\Ko\Traits\AmoCrm;

class AmoCrmCommand extends ConsoleCommand
{
    use AmoCrm;

    public function actionIndex()
    {
        $email = 'test@studio107.ru';
        $phone = 666;

        $data = [
            'Телефон' => $phone,
        ];
        $text = '';
        foreach ($data as $key => $value) {
            $text = strtr("0: 1\n", [$key, $value]);
        }
        $this->amoCRM_process('test', $phone, $email, $text);
    }
}
