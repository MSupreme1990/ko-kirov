<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Commands;

use Imagine\Exception\RuntimeException;
use Mindy\Console\ConsoleCommand;
use Mindy\Helper\Console;
use Mindy\Query\ConnectionManager;
use Mindy\Storage\Files\LocalFile;
use Modules\Catalog\Models\Category;
use Modules\Catalog\Models\Image;
use Modules\Catalog\Models\Product;

class KoMigrateCommand extends ConsoleCommand
{
    public function actionForceResize()
    {
        $total = Image::objects()->count();
        echo 'Total: '.$total.PHP_EOL;

        // start the timer
        $starttime = microtime(true);
        // Call microtime once (we want to calculate time wasted by microtime 2 calls)
        microtime(true);
        $images = Image::objects()->order(['pk'])->all();
        foreach ($images as $i => $img) {
            echo $total - ($i + 1).' ('.$img->pk.')'.PHP_EOL;
            $field = $img->image;
            $absPath = $field->getStorage()->path($field->getValue());
            $image = $field->getImagine()->open($absPath);
            $field->processSource($image, true);
            if ($i % 10 === 0) {
                echo 'Cleanup'.PHP_EOL;
                system('rm -rf /private/var/tmp/*');
            }
        }

        // stop the timer and return the result
        $endtime = microtime(true);

        $duration = $endtime - $starttime;
        $hours = (int) ($duration / 60 / 60);
        $minutes = (int) ($duration / 60) - $hours * 60;
        $seconds = (int) $duration - $hours * 60 * 60 - $minutes * 60;

        echo $seconds.PHP_EOL;
    }

    public function actionCreateUrls()
    {
        $womansCatList = [2];
        $mansCatList = [1];

        $mansCatList += Category::objects()->get(['pk' => 1])->objects()->descendants()->valuesList(['pk'], true);
        $mansCatList = array_unique($mansCatList);

        $womansCatList += Category::objects()->get(['pk' => 2])->objects()->descendants()->valuesList(['pk'], true);
        $womansCatList = array_unique($womansCatList);

        $womans = Product::objects()->filter(['categories__pk__in' => $womansCatList])->all();
        $mans = Product::objects()->filter(['categories__pk__in' => $mansCatList])->all();

        $womanSizes = [7, 6, 5, 4, 3, 2, 1, 15];
        $manSizes = [3, 4, 5, 6, 7, 8, 9, 10];

        $db = ConnectionManager::getDb();

        foreach ($womans as $item) {
            foreach ($womanSizes as $size) {
                $db->createCommand()->insert('catalog_product_catalog_size', [
                    'product_id' => $item->id,
                    'size_id' => $size,
                ])->execute();
            }
        }

        foreach ($mans as $item) {
            foreach ($manSizes as $size) {
                $db->createCommand()->insert('catalog_product_catalog_size', [
                    'product_id' => $item->id,
                    'size_id' => $size,
                ])->execute();
            }
        }
    }

    public function actionImages()
    {
        $basePath = '/Volumes/mac/Users/max/projects/ko2/www/media/Catalog/Image/2015-04-08/';

        $db = ConnectionManager::getDb();
        $items = $db->createCommand('SELECT * FROM catalog_product_migrate')->queryAll(\PDO::FETCH_OBJ);
        foreach ($items as $item) {
            $images = array_unique(array_filter([
                $item->image,
                $item->image_1,
                $item->image_2,
                $item->image_3,
            ]));

            foreach ($images as $image) {
                $imagePath = strtr('/Volumes/Windows 7/public/modules/shop/production/{id}/original/{image}', [
                    '{id}' => $item->id,
                    '{image}' => $image,
                ]);

                $basename = basename($image);
                $ext = strrchr($basename, '.');
                $newName = md5(str_replace($ext, '', $basename)).'_0'.$ext;
                $newPath = $basePath.$newName;
                if (file_exists($newPath)) {
                    copy($imagePath, $newPath);
                } else {
                    echo 'Skip: '.$imagePath.PHP_EOL;
                }
            }
        }
    }

    public function actionIndex()
    {
        echo 'Truncate objects'.PHP_EOL;
        Image::objects()->truncate();
        Product::objects()->truncate();

        echo 'Start migrate'.PHP_EOL;
        $db = ConnectionManager::getDb();
        $items = $db->createCommand('SELECT * FROM catalog_product_migrate')->queryAll(\PDO::FETCH_OBJ);
        foreach ($items as $item) {
            $images = array_unique(array_filter([
                $item->image,
                $item->image_1,
                $item->image_2,
                $item->image_3,
            ]));

            preg_match('/№([0-9]+)/', $item->name, $matches);
            $code = count($matches) > 0 ? $matches[1] : null;
            $price = $item->price_new ? $item->price_new : $item->price;
            $model = new Product([
                'id' => $item->id,
                'name' => $item->name,
                'content' => $item->description,
                'price' => $price ? $price : 0,
                'price_old' => $item->price_new ? $item->price : null,
                'code' => $code,
            ]);
            if ($model->isValid()) {
                $saved = $model->save();
                if ($saved) {
                    echo $model->id.' saved'.PHP_EOL;
                } else {
                    echo $model->id.' save failed'.PHP_EOL;
                }
            } else {
                echo Console::color('Skip model. isValid() == false: '.$model->id, null, Console::BACKGROUND_RED).PHP_EOL;
                echo print_r($model->getErrors(), true).PHP_EOL.PHP_EOL;
            }

            echo $item->id.PHP_EOL;
            foreach ($images as $i => $image) {
                // $imageUrl = strtr("http://ko-kirov.ru/public/modules/shop/production/{id}/original/{image}", [
                //     "{id}" => $item->id,
                //     "{image}" => $image
                // ]);

                $imagePath = strtr('/Volumes/Windows 7/public/modules/shop/production/{id}/original/{image}', [
                    '{id}' => $item->id,
                    '{image}' => $image,
                ]);
                try {
                    Image::objects()->create([
                        'product_id' => $item->id,
                        'image' => new LocalFile($imagePath),
                        'is_main' => $i == 0,
                    ]);
                    echo $image.' saved'.PHP_EOL;
                } catch (RuntimeException $e) {
                    echo 'Skip image: '.$imagePath.PHP_EOL;
                    continue;
                }
            }
        }
    }
}
