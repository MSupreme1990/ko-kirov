<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko;

use Mindy\Base\Mindy;
use Mindy\Base\Module;
use Mindy\Helper\Json;
use Modules\Ko\Models\Banner;
use Modules\Ko\Models\SeoText;
use Modules\Menu\Helpers\MenuHelper;
use Modules\Sites\Models\Site;

class KoModule extends Module
{
    private $_currency = 'RUB';

    public static function preConfigure()
    {
        $tpl = Mindy::app()->template;
        $tpl->addHelper('get_site', function ($pk = null) {
            if ($pk) {
                return Site::objects()->filter(['pk' => $pk])->get();
            }

            $site = Mindy::app()->getModule('Sites')->getSite();
            if ($site) {
                return $site;
            }

            return Site::objects()->filter(['pk' => 1])->get();
        });
        $tpl->addHelper('get_seo_text', function ($url) {
            $site = Mindy::app()->getModule('Sites')->getSite();

            return SeoText::objects()->filter([
                'site' => $site,
                'url' => $url,
            ])->all();
        });
        $tpl->addHelper('get_banners', function () {
            $models = Banner::objects()->all();
            $data = [];
            foreach ($models as $model) {
                $data[$model->url] = $model->toArray();
            }

            return Json::encode($data);
        });
        $tpl->addHelper('get_cart_count', function () {
            /** @var \Modules\Cart\CartModule $module */
            $module = Mindy::app()->getModule('Cart');

            return count($module->getCart()->getItems());
        });
        $tpl->addHelper('get_cart_ids', function () {
            /** @var \Modules\Cart\CartModule $module */
            $module = Mindy::app()->getModule('Cart');
            $ids = [];
            foreach ($module->getCart()->getItems() as $item) {
                $ids[] = $item->getObject()->id;
            }

            return Json::encode($ids);
        });
        $tpl->addHelper('get_seotexts', function () {
            $site = Mindy::app()->getModule('Sites')->getSite();
            $models = SeoText::objects()
                ->filter(['site_id__isnull' => true])
                ->orFilter(['site' => $site])
                ->group(['url'])
                ->all();
            $data = [];
            foreach ($models as $model) {
                $data[] = $model->toArray();
            }

            return Json::encode($data);
        });
        $tpl->addHelper('get_menu_json', function ($slug) {
            $data = Mindy::app()->cache->get('api_menu_'.$slug);
            if (!$data) {
                $data = MenuHelper::getNestedMenu($slug);
                Mindy::app()->cache->set('api_menu_'.$slug, $data, 120);
            }

            return Json::encode($data);
        });
        $tpl->addHelper('get_mode', function () {
            if (isset($_COOKIE['MOBILE']) && $_COOKIE['MOBILE'] == 'false') {
                return 'desktop';
            }

            return (new \Mobile_Detect())->isMobile() ? 'mobile' : 'desktop';
        });
        $tpl->addHelper('post_process', function ($text) {
            $site = Mindy::app()->getModule('Sites')->getSite();

            if ($site) {
                $city = $site->city;

                return strtr($text, [
                    '{{ geo.name }}' => $city->name,
                    '{{ geo.name2 }}' => $city->name2,
                ]);
            }

            return strtr($text, [
                '{{ geo.name }}' => '',
                '{{ geo.name2 }}' => '',
            ]);
        });
        $tpl->addHelper('get_currency', function () {
            return Mindy::app()->getModule('Ko')->getCurrency();
        });
        $tpl->addHelper('get_base_domain', function () {
            $a = Mindy::app()->request->session->getCookieParams();

            return isset($a['domain']) ? $a['domain'] : '';
        });
    }

    public function setCurrency($currency)
    {
        $this->_currency = $currency;
    }

    public function getCurrency()
    {
        return $this->_currency;
    }

    public function getMenu()
    {
        return [
            'name' => $this->getName(),
            'items' => [
                [
                    'name' => self::t('Reviews'),
                    'adminClass' => 'ReviewAdmin',
                ],
                [
                    'name' => self::t('Video reviews'),
                    'adminClass' => 'VideoReviewAdmin',
                ],
                [
                    'name' => self::t('Gifts'),
                    'adminClass' => 'GiftAdmin',
                ],
                [
                    'name' => self::t('Phones'),
                    'adminClass' => 'PhoneAdmin',
                ],
                [
                    'name' => self::t('Credit orders'),
                    'adminClass' => 'CreditOrderAdmin',
                ],
                [
                    'name' => self::t('Price delivery'),
                    'adminClass' => 'PriceDeliveryAdmin',
                ],
                [
                    'name' => self::t('Questions'),
                    'adminClass' => 'QuestionAdmin',
                ],
                [
                    'name' => self::t('Call requests'),
                    'adminClass' => 'RequestCallAdmin',
                ],
                [
                    'name' => self::t('Seo texts'),
                    'adminClass' => 'SeoTextAdmin',
                ],
                [
                    'name' => self::t('Detect size'),
                    'adminClass' => 'DetectSizeAdmin',
                ],
                [
                    'name' => self::t('Possible credit'),
                    'adminClass' => 'CreditPossibleAdmin',
                ],
                [
                    'name' => self::t('Feedback'),
                    'adminClass' => 'FeedbackAdmin',
                ],
                [
                    'name' => self::t('Banners'),
                    'adminClass' => 'BannerAdmin',
                ],
                [
                    'name' => self::t('Sales'),
                    'adminClass' => 'SalesAdmin',
                ],
                [
                    'name' => self::t('Sales back'),
                    'adminClass' => 'SalesBackAdmin',
                ],
                [
                    'name' => self::t('Gift sales'),
                    'adminClass' => 'GiftSalesAdmin',
                ],
            ],
        ];
    }

    public function getOrder($orderId, array $items, array $details)
    {
        /* Тестовый - 1-178YO4Z */
        $partnerId = 'a06b000001vJaD4AAK';

        return [
            'items' => $items,
            'details' => $details,
            'partnerId' => $partnerId,
            'partnerOrderId' => $orderId,
            'partnerName' => 'Кожанная одежда',
            'deliveryType' => '',
        ];
    }

    public function getSecretPhrase()
    {
        /* Тестовая 321ewq */
        return 'ko-kirov-secret-vJaD42a5';
    }
}
