<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Forms;

use Modules\Ko\Models\Sales;
use Modules\Ko\Traits\AmoCrm;

class SalesForm extends AmoCrmSendForm
{
    use AmoCrm;

    public $email_template = 'ko.sales';

    public function getModel()
    {
        return new Sales();
    }
}
