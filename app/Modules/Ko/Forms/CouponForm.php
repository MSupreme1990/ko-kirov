<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Forms;

use Modules\Ko\Models\Coupon;

class CouponForm extends GiftForm
{
    public function getModel()
    {
        return new Coupon();
    }

    public function send()
    {
        if (!MINDY_DEBUG) {
            $data = [
                'Имя' => $this->name->getValue(),
                'Телефон' => $this->phone->getValue(),
            ];

            $text = '';
            foreach ($data as $key => $value) {
                $text = strtr("0: 1\n", [$key, $value]);
            }

            $this->sendCrm("Пользователь оформил купон.\n".$text);
        }
    }
}
