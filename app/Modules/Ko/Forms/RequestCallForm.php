<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Forms;

use Modules\Ko\Models\RequestCall;

class RequestCallForm extends AmoCrmSendForm
{
    public $email_template = 'ko.request_call';

    public function getModel()
    {
        return new RequestCall();
    }
}
