<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Forms;

use Mindy\Base\Mindy;
use Mindy\Form\ModelForm;
use Modules\Ko\Traits\AmoCrm;

class AmoCrmSendForm extends ModelForm
{
    use AmoCrm;

    public $email_template = '';

    public function sendEmail()
    {
        return Mindy::app()->mail->fromCode($this->email_template, Mindy::app()->managers, [
            'model' => $this->getInstance(),
        ]);
    }

    public function send()
    {
        list($title, $data) = $this->sendEmail();

        if (!MINDY_DEBUG) {
            $this->sendCrm($title."\n".$data);
        }
    }

    public function sendCrm($text = '')
    {
        $model = $this->getInstance();

        $name = ((isset($model->name)) ? $this->name->getValue() : '');
        $phone = ((isset($model->phone)) ? $this->phone->getValue() : '');
        $email = ((isset($model->email)) ? $this->email->getValue() : '');

        $this->amoCRM_process($name, $phone, $email, $text);
    }
}
