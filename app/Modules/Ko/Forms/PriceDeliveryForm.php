<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Forms;

use Modules\Ko\Models\PriceDelivery;

class PriceDeliveryForm extends AmoCrmSendForm
{
    public $email_template = 'ko.price_delivery_form';

    public function getModel()
    {
        return new PriceDelivery();
    }
}
