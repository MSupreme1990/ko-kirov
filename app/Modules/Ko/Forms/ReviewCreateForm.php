<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Forms;

use Mindy\Base\Mindy;
use Mindy\Form\ModelForm;
use Modules\Ko\Models\Review;

class ReviewCreateForm extends ModelForm
{
    public $exclude = ['author'];

    public function getModel()
    {
        return new Review();
    }

    public function send()
    {
        return Mindy::app()->mail->fromCode('ko.review_create_form', Mindy::app()->managers, [
            'model' => $this->getInstance(),
        ]);
    }
}
