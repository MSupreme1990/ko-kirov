<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Forms;

use Mindy\Form\Fields\UEditorField;
use Mindy\Form\ModelForm;
use Modules\Files\Fields\FilesField;
use Modules\Ko\KoModule;
use Modules\Ko\Models\Review;

class ReviewForm extends ModelForm
{
    //    public function getFieldsets()
//    {
//        return [
//            KoModule::t('Main information') => [
//                'name', 'author', 'content', 'video', 'is_published', 'is_expert'
//            ],
//            KoModule::t('Images') => [
//                'images'
//            ]
//        ];
//    }

    public function getFields()
    {
        return [
            'content' => [
                'class' => UEditorField::className(),
            ],
            'images' => [
                'class' => FilesField::className(),
                'relatedFileField' => 'image',
            ],
        ];
    }

    public function getModel()
    {
        return new Review();
    }
}
