<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Forms;

use Mindy\Form\Fields\CharField;
use Mindy\Validation\MinLengthValidator;
use Modules\Ko\Models\PayLate;
use Modules\Ko\Traits\AmoCrm;

class PayLateForm extends AmoCrmSendForm
{
    use AmoCrm;

    public $email_template = 'ko.paylate';

    public function getFields()
    {
        return [
            'phone' => [
                'class' => CharField::class,
                'validators' => [
                    new MinLengthValidator(9),
                ],
            ],
        ];
    }

    public function getModel()
    {
        return new PayLate();
    }
}
