<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Forms;

use Modules\Ko\Models\Order;

class OrderForm extends AmoCrmSendForm
{
    public $email_template = 'ko.cart_order';

    public function getModel()
    {
        return new Order();
    }
}
