<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Forms;

use Mindy\Form\Fields\HiddenField;
use Modules\Ko\Models\CreditOrder;

class CreditOrderForm extends AmoCrmSendForm
{
    public $email_template = 'ko.credit_order_Form';

    public function getFields()
    {
        return [
            'product' => [
                'class' => HiddenField::className(),
            ],
        ];
    }

    public function getModel()
    {
        return new CreditOrder();
    }
}
