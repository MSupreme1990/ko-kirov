<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Fields;

use Mindy\Orm\Fields\ManyToManyField;
use Mindy\Orm\ManyToManyManager;
use Mindy\Query\ConnectionManager;

class ManyToManySortField extends ManyToManyField
{
    public $order;

    /**
     * @return \Mindy\Orm\ManyToManyManager QuerySet of related objects
     */
    public function getManager()
    {
        $manager = new ManyToManyManager($this->getRelatedModel(), [
            'modelColumn' => $this->getRelatedModelColumn(),
            'primaryModelColumn' => $this->getModelColumn(),
            'primaryModel' => $this->getModel(),
            'relatedTable' => $this->getTableName(),
            'extra' => $this->extra,
            'through' => $this->through,
        ]);
        $manager->getQuerySet();

        if ($this->order) {
            $db = ConnectionManager::getDb();
            $manager->order([
                $manager->relatedTableAlias.'.'.$db->schema->quoteColumnName($this->order),
            ]);
        }

        return $manager;
    }
}
