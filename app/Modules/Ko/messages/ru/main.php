<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    'Plain order' => 'Товары заказа',
    'Orders' => 'Заказы (корзина)',
    'Address' => 'Адрес',
    'Total price' => 'Итоговая сумма',
    'Main information' => 'Основная информация',
    'Images' => 'Изображения',
    'Reviews' => 'Отзывы',
    'Video reviews' => 'Видео отзывы',
    'Gifts' => 'Подарки',
    'Phones' => 'Телефоны',
    'Credit orders' => 'Кредитные заявки',
    'Price delivery' => 'Стоимость доставки',
    'Questions' => 'Вопросы',
    'Quick orders' => 'Быстрые заказы',
    'Call requests' => 'Запросы перезвонить',
    'Name' => 'Имя',
    'Phone' => 'Телефон',
    'Product' => 'Продукт',
    'Size' => 'Размер',
    'CreditOrder {url} was created' => 'Кредитная заявка {url} была создана',
    'CreditOrder {url} was updated' => 'Кредитная заявка {url} была обновлена',
    'CreditOrder {url} was deleted' => 'Кредитная заявка {url} была Удалена',
    'Email' => 'Электронная почта',
    'Question' => 'Вопрос',
    'Gift {url} was created' => 'Заявка на подарок {url} была создана',
    'Gift {url} was updated' => 'Заявка на подарок {url} была обновлена',
    'Gift {url} was deleted' => 'Заявка на подарок {url} была удалена',
    'Phone {url} was created' => 'Телефон {url} был создан',
    'Phone {url} was updated' => 'Телефон {url} был обновлен',
    'Phone {url} was deleted' => 'Телефон {url} был удален',
    'City' => 'Город',
    'PriceDelivery {url} was created' => '',
    'PriceDelivery {url} was updated' => '',
    'PriceDelivery {url} was deleted' => '',
    'Question {url} was created' => '',
    'Question {url} was updated' => '',
    'Question {url} was deleted' => '',
    'QuickOrder {url} was created' => '',
    'QuickOrder {url} was updated' => '',
    'QuickOrder {url} was deleted' => '',
    'RequestCall {url} was created' => '',
    'RequestCall {url} was updated' => '',
    'RequestCall {url} was deleted' => '',
    'Author' => 'Автор',
    'Content' => 'Содержимое',
    'Video' => 'Видео',
    'Is published' => 'Опубликовано',
    'Created at' => 'Дата создания',
    'Updated at' => 'Дата изменения',
    'Delivery type' => 'Тип доставки',
    'Comment' => 'Комментарий',
    'Is expert' => 'Мнение эксперта',
    'Review {url} was created' => '',
    'Review {url} was updated' => '',
    'Review {url} was deleted' => '',
    'Image' => 'Изображение',
    'Review' => 'Отзыв',
    'Position' => 'Позиция',
    'Is main' => 'Главная',
    'ReviewImage {url} was created' => '',
    'ReviewImage {url} was updated' => '',
    'ReviewImage {url} was deleted' => '',
    'VideoReview {url} was created' => '',
    'VideoReview {url} was updated' => '',
    'VideoReview {url} was deleted' => '',
    'Ko' => 'Кожанная одежда',
    'Create video review' => 'Добавление видео',
    'Update video review: {name}' => 'Редактирование видео',
    'Create review' => 'Создание отзыва',
    'Update review: {name}' => 'Редактирование отзыва: {name}',
    'Seo texts' => 'Сео тексты',
    'Detect size' => 'Определить размер',
    'Possible credit' => 'Возможность кредитования',
    'Feedback' => 'Обратная связь',
    'Banners' => 'Рекламные блоки',
    'Sales' => 'Оптовые продажи',
    'Gift sales' => 'Подарки (опт)',
    'Gift saless' => 'Подарки (опт)',

    'Sales back' => 'Опт. продажи (Мод)',
];
