<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Models;

use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Model;
use Modules\Ko\KoModule;

class PriceDelivery extends Model
{
    public static function getFields()
    {
        return [
            'phone' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Phone'),
                'required' => true,
            ],
            'city' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('City'),
                'null' => true,
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->phone;
    }
}
