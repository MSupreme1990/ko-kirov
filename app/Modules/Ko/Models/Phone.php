<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Models;

use Mindy\Base\Mindy;
use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Model;
use Modules\Ko\KoModule;

class Phone extends Model
{
    public static function getFields()
    {
        return [
            'phone' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Phone'),
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->phone;
    }

    public function send()
    {
        return Mindy::app()->mail->fromCode('ko.send-phone', Mindy::app()->managers, [
            'model' => $this,
        ]);
    }
}
