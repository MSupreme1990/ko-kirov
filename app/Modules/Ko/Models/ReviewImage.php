<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Models;

use Mindy\Orm\Fields\BooleanField;
use Mindy\Orm\Fields\ForeignField;
use Mindy\Orm\Fields\ImageField;
use Mindy\Orm\Model;
use Modules\Core\Fields\Orm\PositionField;
use Modules\Ko\KoModule;

class ReviewImage extends Model
{
    public static function getFields()
    {
        return [
            'image' => [
                'class' => ImageField::className(),
                'sizes' => [
                    'thumb' => [
                        150, 176,
                    ],
                    'thumb_slide' => [
                        112, 85,
                    ],
                    'resize' => [
                        978,
                    ],
                ],
                'verboseName' => KoModule::t('Image'),
            ],
            'review' => [
                'class' => ForeignField::className(),
                'modelClass' => Review::className(),
                'relatedName' => 'images',
                'verboseName' => KoModule::t('Review'),
            ],
            'position' => [
                'class' => PositionField::className(),
                'default' => 0,
                'verboseName' => KoModule::t('Position'),
            ],
            'is_main' => [
                'class' => BooleanField::className(),
                'verboseName' => KoModule::t('Is main'),
            ],
        ];
    }

    /**
     * @param $owner Model
     * @param $isNew
     */
    public function beforeSave($owner, $isNew)
    {
        if ($this->is_main) {
            $qs = $this->objects()->filter(['review' => $this->review]);
            if ($isNew == false) {
                $qs->exclude(['pk' => $this->pk]);
            }
            $qs->update(['is_main' => false]);
        }
    }
}
