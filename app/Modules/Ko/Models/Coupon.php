<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Models;

use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\DateTimeField;
use Mindy\Orm\Fields\TextField;
use Mindy\Orm\Model;
use Modules\Ko\KoModule;

class Coupon extends Model
{
    public static function getFields()
    {
        return [
            'name' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Name'),
                'required' => true,
            ],
            'phone' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Phone'),
                'required' => true,
//                'unique' => true
            ],
            'phone_raw' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Phone'),
                'required' => true,
            ],
            'code' => [
                'class' => TextField::className(),
                'verboseName' => KoModule::t('Code'),
                'null' => true,
            ],
            'created_at' => [
                'class' => DateTimeField::className(),
                'autoNowAdd' => true,
                'verboseName' => KoModule::t('Code'),
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->code;
    }

    public static function clearPhone($phone)
    {
        $phone = preg_replace('/[^0-9]/', '', $phone); // отсекаем все символы кроме цифр
        return substr($phone, 1); // отсекаем первый символ (код страны)
    }

    public function beforeValidate($owner)
    {
        if ($owner->getIsNewRecord()) {
            if (!empty($owner->phone)) {
                $owner->phone_raw = $owner->phone;
//                $owner->phone = self::clearPhone($owner->phone);
            }
        }
    }

    public function beforeSave($owner, $isNew)
    {
        if ($isNew) {
            $owner->code = sprintf('%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff)
            );
            $owner->phone_raw = $owner->phone;
            $owner->phone = self::clearPhone($owner->phone);
        }
    }
}
