<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Models;

use Mindy\Orm\Fields\BooleanField;
use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\DecimalField;
use Mindy\Orm\Fields\EmailField;
use Mindy\Orm\Fields\TextField;
use Mindy\Orm\Model;

class PayLate extends Model
{
    public static function getFields()
    {
        return [
            'name' => [
                'class' => CharField::class,
            ],
            'phone' => [
                'class' => CharField::class,
            ],
            'email' => [
                'class' => EmailField::class,
                'null' => true,
            ],
            'price' => [
                'class' => DecimalField::class,
                'precision' => 10,
                'scale' => 2,
            ],
            'is_paid' => [
                'class' => BooleanField::class,
                'default' => false,
            ],
            'is_cancel' => [
                'class' => BooleanField::class,
                'default' => false,
            ],
            'products' => [
                'class' => TextField::class,
            ],
        ];
    }
}
