<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Models;

use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\ForeignField;
use Mindy\Orm\Fields\TextField;
use Mindy\Orm\Model;
use Modules\Ko\KoModule;
use Modules\Sites\Models\Site;

class SeoText extends Model
{
    public static function getFields()
    {
        return [
            'name' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Name'),
            ],
            'text' => [
                'class' => TextField::className(),
                'verboseName' => KoModule::t('Text'),
            ],
            'url' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Url'),
            ],
            'site' => [
                'class' => ForeignField::className(),
                'verboseName' => KoModule::t('Site'),
                'modelClass' => Site::className(),
                'null' => true,
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->name;
    }
}
