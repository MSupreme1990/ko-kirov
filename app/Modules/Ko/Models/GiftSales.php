<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Models;

use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\EmailField;
use Mindy\Orm\Fields\TextField;
use Mindy\Orm\Model;
use Modules\Ko\KoModule;

/**
 * Class GiftSales
 *
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $question
 * @property string $referer
 */
class GiftSales extends Model
{
    public static function getFields()
    {
        return [
            'name' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Name'),
                'required' => false,
                'null' => true,
            ],
            'phone' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Phone'),
                'required' => true,
            ],
            'email' => [
                'class' => EmailField::className(),
                'verboseName' => KoModule::t('Email'),
                'required' => false,
                'null' => true,
            ],
            'question' => [
                'class' => TextField::className(),
                'verboseName' => KoModule::t('Question'),
                'null' => true,
            ],
            'referer' => [
                'class' => CharField::className(),
                'null' => true,
                'editable' => false,
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->name;
    }
}
