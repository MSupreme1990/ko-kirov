<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Models;

use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Model;
use Modules\Ko\KoModule;

class DetectSize extends Model
{
    public static function getFields()
    {
        return [
            'name' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Name'),
                'required' => false,
                'null' => true,
            ],
            'phone' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Phone'),
                'required' => true,
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->name;
    }
}
