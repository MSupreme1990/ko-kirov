<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Models;

use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\DateTimeField;
use Mindy\Orm\Fields\DecimalField;
use Mindy\Orm\Fields\TextField;
use Mindy\Orm\Model;
use Modules\Ko\KoModule;

class Order extends Model
{
    public static function getFields()
    {
        return [
            'name' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Name'),
                'null' => true,
            ],
            'phone' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Phone'),
            ],
            'email' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Email'),
                'null' => true,
            ],
            'comment' => [
                'class' => TextField::className(),
                'verboseName' => KoModule::t('Comment'),
                'null' => true,
            ],
            'address' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Address'),
                'null' => true,
            ],
            'type' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Delivery type'),
                'null' => true,
            ],
            'total' => [
                'class' => DecimalField::className(),
                'verboseName' => KoModule::t('Total price'),
                'precision' => 10,
                'scale' => 2,
            ],
            'plain' => [
                'class' => TextField::className(),
                'verboseName' => KoModule::t('Plain order'),
            ],
            'created_at' => [
                'class' => DateTimeField::className(),
                'verboseName' => KoModule::t('Created at'),
                'autoNowAdd' => true,
            ],
            'updated_at' => [
                'class' => DateTimeField::className(),
                'verboseName' => KoModule::t('Updated at'),
                'autoNow' => true,
                'null' => true,
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->pk;
    }
}
