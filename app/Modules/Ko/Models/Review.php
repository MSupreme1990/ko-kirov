<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Models;

use Mindy\Orm\Fields\BooleanField;
use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\DateTimeField;
use Mindy\Orm\Fields\EmailField;
use Mindy\Orm\Fields\HasManyField;
use Mindy\Orm\Fields\IntField;
use Mindy\Orm\Fields\TextField;
use Mindy\Orm\Model;
use Modules\Ko\KoModule;

class Review extends Model
{
    public static function getFields()
    {
        return [
            'name' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Name'),
            ],
            'phone' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Phone'),
            ],
            'email' => [
                'class' => EmailField::className(),
                'verboseName' => KoModule::t('Email'),
            ],
            'author' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Author'),
            ],
            'content' => [
                'class' => TextField::className(),
                'verboseName' => KoModule::t('Content'),
            ],
            'video' => [
                'class' => CharField::className(),
                'null' => true,
                'verboseName' => KoModule::t('Video'),
            ],
            'is_published' => [
                'class' => BooleanField::className(),
                'verboseName' => KoModule::t('Is published'),
            ],
            'images' => [
                'class' => HasManyField::className(),
                'modelClass' => ReviewImage::className(),
                'verboseName' => KoModule::t('Name'),
                'to' => 'review_id',
                'editable' => false,
            ],
            'created_at' => [
                'class' => DateTimeField::className(),
                'autoNowAdd' => true,
                'verboseName' => KoModule::t('Created at'),
                'editable' => true,
            ],
            'is_expert' => [
                'class' => BooleanField::className(),
                'verboseName' => KoModule::t('Is expert'),
            ],
            'position' => [
                'class' => IntField::className(),
                'null' => true,
                'verboseName' => KoModule::t('Position'),
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->name;
    }

    public static function objectsManager($instance = null)
    {
        $className = get_called_class();

        return new ReviewManager($instance ? $instance : new $className());
    }
}
