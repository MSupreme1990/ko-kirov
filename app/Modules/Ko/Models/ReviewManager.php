<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Models;

use Mindy\Base\Mindy;
use Mindy\Orm\Manager;

class ReviewManager extends Manager
{
    public function getQuerySet()
    {
        $qs = parent::getQuerySet();
        if (Mindy::app()->user->is_superuser == false) {
            $qs->filter(['is_published' => true]);
        }

        return $qs;
    }
}
