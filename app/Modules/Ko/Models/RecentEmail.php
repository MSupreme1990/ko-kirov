<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Models;

use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\TextField;
use Mindy\Orm\Model;
use Modules\Ko\KoModule;

class RecentEmail extends Model
{
    public static function getFields()
    {
        return [
            'email' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Email'),
                'null' => false,
            ],
            'plaint' => [
                'class' => TextField::className(),
                'verboseName' => KoModule::t('Phone'),
                'null' => true,
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->email;
    }
}
