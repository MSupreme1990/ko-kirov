<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Models;

use Mindy\Orm\Fields\BooleanField;
use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\ImageField;
use Mindy\Orm\Model;
use Modules\Ko\KoModule;

class Banner extends Model
{
    public static function getFields()
    {
        return [
            'name' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Name'),
            ],
            'url' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Url'),
            ],
            'target_url' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Target url'),
            ],
            'image' => [
                'class' => ImageField::className(),
                'verboseName' => KoModule::t('Image'),
            ],
            'is_published' => [
                'class' => BooleanField::className(),
                'verboseName' => KoModule::t('Is published'),
                'default' => false,
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->name;
    }
}
