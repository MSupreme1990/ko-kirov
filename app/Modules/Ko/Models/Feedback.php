<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Models;

use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\TextField;
use Mindy\Orm\Model;
use Modules\Ko\KoModule;

/**
 * Class Feedback
 *
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $comment
 */
class Feedback extends Model
{
    public static function getFields()
    {
        return [
            'name' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Name'),
                'null' => true,
            ],
            'phone' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Phone'),
                'required' => true,
            ],
            'email' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Email'),
                'null' => true,
            ],
            'comment' => [
                'class' => TextField::className(),
                'verboseName' => KoModule::t('Phone'),
                'null' => true,
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->name;
    }
}
