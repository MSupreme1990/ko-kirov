<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Models;

use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Model;
use Modules\Core\Fields\Orm\PositionField;
use Modules\Ko\KoModule;

class VideoReview extends Model
{
    public static function getFields()
    {
        return [
            'video' => [
                'class' => CharField::className(),
                'verboseName' => KoModule::t('Video'),
            ],
            'position' => [
                'class' => PositionField::className(),
                'verboseName' => KoModule::t('Position'),
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->video;
    }
}
