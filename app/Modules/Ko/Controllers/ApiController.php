<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Controllers;

use Mindy\Base\Mindy;
use Modules\Catalog\Models\Product;
use Modules\Core\Controllers\ApiBaseController;
use Modules\Geo\Models\City;
use Modules\Geo\Models\Region;
use Modules\Ko\Forms\CouponForm;
use Modules\Ko\Forms\CreditOrderForm;
use Modules\Ko\Forms\CreditPossibleForm;
use Modules\Ko\Forms\DetectSizeForm;
use Modules\Ko\Forms\FeedbackForm;
use Modules\Ko\Forms\GiftForm;
use Modules\Ko\Forms\GiftSalesForm;
use Modules\Ko\Forms\OrderForm;
use Modules\Ko\Forms\OrderQuickForm;
use Modules\Ko\Forms\PriceDeliveryForm;
use Modules\Ko\Forms\QuestionForm;
use Modules\Ko\Forms\QuickOrderForm;
use Modules\Ko\Forms\RecentEmailForm;
use Modules\Ko\Forms\RequestCallForm;
use Modules\Ko\Forms\ReviewCreateForm;
use Modules\Ko\Forms\SalesBackForm;
use Modules\Ko\Forms\SalesForm;
use Modules\Ko\Helpers\CartHelper;
use Modules\Ko\Models\Banner;
use Modules\Ko\Models\Coupon;
use Modules\Ko\Models\CreditOrder;
use Modules\Ko\Models\Feedback;
use Modules\Ko\Models\Gift;
use Modules\Ko\Models\GiftSales;
use Modules\Ko\Models\Phone;
use Modules\Ko\Models\Question;
use Modules\Ko\Models\QuickOrder;
use Modules\Ko\Models\RecentEmail;
use Modules\Ko\Models\Review;
use Modules\Ko\Models\SalesBack;
use Modules\Ko\Models\SeoText;
use Modules\Ko\Models\VideoReview;
use Modules\Sites\Models\Site;
use Orion\Component\Client\Client;
use Orion\Component\Client\Crm\Contact;
use Orion\Component\Client\Crm\Lead;

class ApiController extends ApiBaseController
{
    public function actionRecentSendToEmail()
    {
        $form = new RecentEmailForm();

        $r = $this->getRequest();
        if ($r->getIsPost() && $form->populate($_POST)->isValid()) {
            if (!empty($_POST['products'])) {
                $objects = Product::objects()->filter(['id__in' => explode(',', $_POST['products'])])->all();

                if ($objects) {
                    $model = new RecentEmail();
                    $model->email = $form->email->getValue();
                    $model->plaint = $this->renderTemplate('ko/to_email_plain.html', ['items' => $objects]);
                    $model->save();

                    Mindy::app()->mail->fromCode($form->email_template, $model->email, [
                        'logoPath' => '/logo.png',
                        'model' => $model,
                        'items' => $objects,
                    ]);

                    Mindy::app()->mail->fromCode('ko.recent_email_form_manager', Mindy::app()->managers, [
                        'logoPath' => '/logo.png',
                        'model' => $model,
                        'items' => $objects,
                    ]);
                }
            }

            echo $this->json([
                'success' => true,
                'message' => 'Спасибо! Заявка успешно отправлена.',
            ]);

            $form->sendCrm();
            Mindy::app()->end();
        } else {
            echo $this->json([
                'errors' => $form->getJsonErrors(),
            ]);
            Mindy::app()->end();
        }
    }

    public function actionSavePhone()
    {
        if (
            $this->getRequest()->getIsPost() &&
            isset($_POST['value'])
        ) {
            /** @var Phone $model */
            list($model, $new) = Phone::objects()->getOrCreate(['phone' => $_POST['value']]);

            echo $this->json([
                'status' => true,
            ]);

            $value = str_replace([' ', '(', ')', '_', '-'], '', $_POST['value']);
            if (mb_strlen($value, 'UTF-8') >= 9 && $new) {
                $model->send();
            }
        } else {
            $this->error(404);
        }
    }

    public function actionGiftExtra()
    {
        if (!isset($_POST['GiftForm']['id'])) {
            $this->error(404);
        }

        $id = $_POST['GiftForm']['id'];
        $instance = Gift::objects()->filter([
            'pk' => $id,
        ])->get();
        if ($instance === null) {
            $this->error(404);
        }

        $form = new GiftForm([
            'instance' => $instance,
        ]);

        $r = $this->getRequest();
        if ($r->getIsPost() && $form->populate($_POST)->isValid() && $form->save()) {
            $phone = Coupon::clearPhone($form->phone);
            $coup = Coupon::objects()->filter(['phone' => $phone])->get();
            if (!$coup) {
                $coup = new Coupon();
                $coup->phone = $form->instance->phone;
                $coup->name = $form->instance->name;

                $coup->save();
            }

            echo $this->json([
                'success' => true,
                'message' => 'Спасибо! Заявка успешно отправлена.',
                'code' => $coup->code,
            ]);

            $form->send();
            Mindy::app()->end();
        } else {
            echo $this->json([
                'errors' => $form->getJsonErrors(),
            ]);
            Mindy::app()->end();
        }
    }

    public function actionGift()
    {
        $form = new GiftForm();

        $r = $this->getRequest();
        if ($r->getIsPost() && $form->populate($_POST)->isValid()) {
            $form->setModelAttributes([
                'referer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
            ]);
            $form->save();

            try {
                $coup = $form->getInstance();

                /** @var Client $orion */
                $orion = Mindy::app()->orion;

                $contact = new Contact();
                $contact->setName($coup->name);
                $contact->setPhone($coup->phone);
                if (($result = $orion->send($contact)) && $result->isSuccess()) {
                    $contactId = $result->getId();

                    $lead = new Lead();
                    $lead->setName('Заявка на купон');
                    $lead->setContactId($contactId);
                    $lead->setSource($_SERVER['SERVER_NAME']);

                    $orion->send($lead);
                }
            } catch (\Exception $e) {
            }

            echo $this->json([
                'pk' => $form->getInstance()->pk,
                'success' => true,
                'message' => 'Заявка на подарок успешно отправлена. Для того чтобы распечатать купон на скидку, введите следующие данные:',
            ]);

            $form->send();
            Mindy::app()->end();
        } else {
            echo $this->json([
                'errors' => $form->getJsonErrors(),
            ]);
            Mindy::app()->end();
        }
    }

    public function actionGiftSalesExtra()
    {
        if (!isset($_POST['GiftSalesForm']['id'])) {
            $this->error(404);
        }

        $id = $_POST['GiftSalesForm']['id'];
        $instance = GiftSales::objects()->filter([
            'pk' => $id,
        ])->get();
        if ($instance === null) {
            $this->error(404);
        }

        $form = new GiftSalesForm([
            'instance' => $instance,
        ]);

        $r = $this->getRequest();
        if ($r->getIsPost() && $form->populate($_POST)->isValid() && $form->save()) {
            echo $this->json([
                'success' => true,
                'message' => 'Спасибо! Заявка успешно отправлена.',
            ]);

            $form->send();
            Mindy::app()->end();
        } else {
            echo $this->json([
                'errors' => $form->getJsonErrors(),
            ]);
            Mindy::app()->end();
        }
    }

    public function actionGiftSales()
    {
        $form = new GiftSalesForm();

        $r = $this->getRequest();
        if ($r->getIsPost() && $form->populate($_POST)->isValid()) {
            $form->setModelAttributes([
                'referer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
            ]);
            $form->save();

            try {
                /** @var GiftSales $instance */
                $instance = $form->getInstance();

                /** @var Client $orion */
                $orion = Mindy::app()->orion;

                $contact = new Contact();
                $contact->setName($instance->name);
                $contact->setPhone($instance->phone);
                $contact->setEmail($instance->email);
                if (($result = $orion->send($contact)) && $result->isSuccess()) {
                    $contactId = $result->getId();

                    $lead = new Lead();
                    $lead->setName('Доступ к оптовым ценам + Бонус');
                    $lead->setContactId($contactId);
                    $lead->setSource($_SERVER['SERVER_NAME']);
                    $lead->setDescription($instance->question);

                    $orion->send($lead);
                }
            } catch (\Exception $e) {
            }

            echo $this->json([
                'pk' => $form->getInstance()->pk,
                'success' => true,
                'message' => 'Заявка на подарок успешно отправлена. В ближайшее время Вам перезвонит менеджер и расскажет, как забрать подарок. А пока для уточнения введите следующие данные:',
            ]);

            $form->send();
            Mindy::app()->end();
        } else {
            echo $this->json([
                'errors' => $form->getJsonErrors(),
            ]);
            Mindy::app()->end();
        }
    }

    public function actionFeedback()
    {
        $form = new FeedbackForm();

        $r = $this->getRequest();
        if ($r->getIsPost() && $form->populate($_POST)->isValid()) {
            $form->setModelAttributes([
                'referer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
            ]);
            $form->save();

            try {
                /** @var Feedback $instance */
                $instance = $form->getInstance();

                /** @var Client $orion */
                $orion = Mindy::app()->orion;

                $contact = new Contact();
                $contact->setName($instance->name);
                $contact->setPhone($instance->phone);
                $contact->setEmail($instance->email);
                if (($result = $orion->send($contact)) && $result->isSuccess()) {
                    $contactId = $result->getId();

                    $lead = new Lead();
                    $lead->setName('Форма обратной связи');
                    $lead->setContactId($contactId);
                    $lead->setSource($_SERVER['SERVER_NAME']);
                    $lead->setDescription($instance->comment);

                    $orion->send($lead);
                }
            } catch (\Exception $e) {
            }

            echo $this->json([
                'pk' => $form->getInstance()->pk,
                'success' => true,
                'message' => 'Спасибо! Заявка успешно отправлена.',
            ]);

            $form->send();
            Mindy::app()->end();
        } else {
            echo $this->json([
                'errors' => $form->getJsonErrors(),
            ]);
            Mindy::app()->end();
        }
    }

    public function actionSalesBack()
    {
        $form = new SalesBackForm();

        $r = $this->getRequest();
        if ($r->getIsPost() && $form->populate($_POST)->isValid()) {
            $form->setModelAttributes([
                'referer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
            ]);
            $form->save();

            try {
                /** @var SalesBack $instance */
                $instance = $form->getInstance();

                /** @var Client $orion */
                $orion = Mindy::app()->orion;

                $contact = new Contact();
                $contact->setName($instance->name);
                $contact->setPhone($instance->phone);
                $contact->setEmail($instance->email);
                if (($result = $orion->send($contact)) && $result->isSuccess()) {
                    $contactId = $result->getId();

                    $lead = new Lead();
                    $lead->setName('Заявка на продажи');
                    $lead->setContactId($contactId);
                    $lead->setSource($_SERVER['SERVER_NAME']);
                    $lead->setDescription($instance->comment);

                    $orion->send($lead);
                }
            } catch (\Exception $e) {
            }

            echo $this->json([
                'pk' => $form->getInstance()->pk,
                'success' => true,
                'message' => 'Спасибо! Заявка успешно отправлена.',
            ]);

            $form->send();
            Mindy::app()->end();
        } else {
            echo $this->json([
                'errors' => $form->getJsonErrors(),
            ]);
            Mindy::app()->end();
        }
    }

    public function actionBanner($url)
    {
        $model = Banner::objects()->filter([
            'url' => $url,
            'is_published' => true,
        ])->get();
        echo $this->json($model ? $model->toArray() : []);
        Mindy::app()->end();
    }

    public function actionCreditPossible()
    {
        $form = new CreditPossibleForm();

        $r = $this->getRequest();
        if ($r->getIsPost() && $form->populate($_POST)->isValid()) {
            $form->setModelAttributes([
                'referer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
            ]);
            $form->save();

            $form->send();

            echo $this->json([
                'success' => true,
                'message' => 'Спасибо! Заявка успешно отправлена.',
            ]);

            $form->send();
            Mindy::app()->end();
        } else {
            echo $this->json([
                'errors' => $form->getJsonErrors(),
            ]);
            Mindy::app()->end();
        }
    }

    public function actionSales()
    {
        $form = new SalesForm();

        $r = $this->getRequest();
        if ($r->getIsPost() && $form->populate($_POST)->isValid()) {
            $form->setModelAttributes([
                'referer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
            ]);
            $form->save();

            $form->send();

            echo $this->json([
                'success' => true,
                'message' => 'Спасибо! Заявка успешно отправлена.',
            ]);
            Mindy::app()->end();
        } else {
            echo $this->json([
                'errors' => $form->getJsonErrors(),
            ]);
            Mindy::app()->end();
        }
    }

    public function actionDetectSize()
    {
        $form = new DetectSizeForm();

        $r = $this->getRequest();
        if ($r->getIsPost() && $form->populate($_POST)->isValid()) {
            $form->setModelAttributes([
                'referer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
            ]);
            $form->save();

            $form->send();

            echo $this->json([
                'success' => true,
                'message' => 'Спасибо! Заявка успешно отправлена.',
            ]);

            $form->send();
            Mindy::app()->end();
        } else {
            echo $this->json([
                'errors' => $form->getJsonErrors(),
            ]);
            Mindy::app()->end();
        }
    }

    public function actionQuestion()
    {
        $form = new QuestionForm();

        $r = $this->getRequest();
        if ($r->getIsPost() && $form->populate($_POST)->isValid()) {
            $form->setModelAttributes([
                'referer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
            ]);
            $form->save();

            $form->send();

            echo $this->json([
                'pk' => $form->getInstance()->pk,
                'success' => true,
                'message' => 'Спасибо! Заявка успешно отправлена.',
            ]);

            $form->send();
            Mindy::app()->end();
        } else {
            echo $this->json([
                'errors' => $form->getJsonErrors(),
            ]);
            Mindy::app()->end();
        }
    }

    public function actionQuestionExtra()
    {
        if (!isset($_POST['QuestionForm']['id'])) {
            $this->error(404);
        }

        $id = $_POST['QuestionForm']['id'];
        $instance = Question::objects()->filter([
            'pk' => $id,
        ])->get();
        if ($instance === null) {
            $this->error(404);
        }

        $form = new QuestionForm([
            'instance' => $instance,
        ]);

        $r = $this->getRequest();
        if ($r->getIsPost() && $form->populate($_POST)->isValid() && $form->save()) {
            echo $this->json([
                'success' => true,
                'message' => 'Спасибо! Заявка успешно отправлена.',
            ]);

            $form->send();
            Mindy::app()->end();
        } else {
            echo $this->json([
                'errors' => $form->getJsonErrors(),
            ]);
            Mindy::app()->end();
        }
    }

    public function actionReviewsCreate()
    {
        $form = new ReviewCreateForm();

        $r = $this->getRequest();
        if ($r->getIsPost() && $form->populate($_POST)->isValid() && $form->save()) {
            echo $this->json([
                'success' => true,
                'message' => 'Благодарим Вас за оставленный отзыв! <br/>
<span class="text">
Отправьте, пожалуйста, свою фотографию для размещения на сайте (желательно в меховом изделии) по адресу
<br/>email: <a href="mailto:aamt@mail.ru">aamt@mail.ru</a>
</span>',
            ]);

            $form->send();
            Mindy::app()->end();
        } else {
            echo $this->json([
                'errors' => $form->getJsonErrors(),
            ]);
            Mindy::app()->end();
        }
    }

    public function actionReviews()
    {
        $data = Mindy::app()->cache->get('api_reviews_all');
        if (!$data) {
            $video = VideoReview::objects()->order(['position'])->asArray()->all();

            $qs = Review::objects()
                ->filter([
                    'is_expert' => false,
                    // Issue #5076
                    'is_published' => true,
                ])
                ->order(['position']);

            if (!isset($_GET['unlimit'])) {
                $qs->limit(10)->offset(0);
            }

            $reviews = [];
            foreach ($qs->all() as $product) {
                $images = [];
                $mainImage = null;
                foreach ($product->images->all() as $image) {
                    if ($image->is_main) {
                        $mainImage = $image;
                    }
                    $images[] = $image->toArray();
                }
                if ($mainImage === null && count($images) > 0) {
                    $mainImage = $images[0];
                }
                $reviews[] = array_merge($product->toArray(), [
                    'image' => $mainImage,
                    'images' => $images,
                ]);
            }
            $data = [
                'reviews' => $reviews,
                'video' => $video,
            ];
            Mindy::app()->cache->set('api_reviews_all', $data);
        }

        echo $this->json($data);
        Mindy::app()->end();
    }

    public function actionReviewsExpert()
    {
        $data = Mindy::app()->cache->get('api_reviews_expert');
        if (!$data) {
            $qs = Review::objects()->filter([
                'is_expert' => true,
            ])->order(['?'])->limit(1);
            $data = [];
            foreach ($qs->all() as $product) {
                $images = [];
                $mainImage = null;
                foreach ($product->images->all() as $image) {
                    if ($image->is_main) {
                        $mainImage = $image;
                    }
                    $images[] = $image->toArray();
                }
                if ($mainImage === null && count($images) > 0) {
                    $mainImage = $images[0];
                }
                $data[] = array_merge($product->toArray(), [
                    'image' => $mainImage->toArray(),
                    'images' => $images,
                ]);
            }
            Mindy::app()->cache->set('api_reviews_expert', $data);
        }

        echo $this->json($data);
        Mindy::app()->end();
    }

    public function actionVideoReviews()
    {
        $isFull = isset($_GET['full']);
        $data = Mindy::app()->cache->get('api_video_reviews_'.($isFull ? 'full' : 'random'));
        if (!$data) {
            $qs = VideoReview::objects()->order(['position'])->asArray();
            if (!$isFull) {
                $qs->limit(3);
            }
            $data = $qs->all();
            Mindy::app()->cache->set('api_video_reviews', $data);
        }
        echo $this->json($data);
        Mindy::app()->end();
    }

    public function actionQuickOrder()
    {
        $hasInstance = false;
        if (
            isset($_POST['QuickOrderForm']) &&
            isset($_POST['QuickOrderForm']['id'])
        ) {
            $instance = QuickOrder::objects()->get([
                'pk' => $_POST['QuickOrderForm']['id'],
            ]);
            $data = [
                'instance' => $instance,
            ];
            $hasInstance = true;
        } else {
            $data = [];
        }
        $form = new QuickOrderForm($data);

        $r = $this->getRequest();
        if ($r->getIsPost() && $form->populate($_POST)->isValid() && $form->save()) {
            echo $this->json([
                'pk' => $form->getInstance()->pk,
                'success' => true,
                'message' => 'Спасибо! Заявка успешно отправлена.',
                'complete' => $hasInstance,
            ]);

            $form->send();
            Mindy::app()->end();
        } else {
            echo $this->json([
                'errors' => $form->getJsonErrors(),
            ]);
            Mindy::app()->end();
        }
    }

    public function actionCreditOrder()
    {
        $hasInstance = false;
        if (
            isset($_POST['CreditOrderForm']) &&
            isset($_POST['CreditOrderForm']['id'])
        ) {
            $instance = CreditOrder::objects()->get([
                'pk' => $_POST['CreditOrderForm']['id'],
            ]);
            $data = [
                'instance' => $instance,
            ];
            $hasInstance = true;
        } else {
            $data = [];
        }
        $form = new CreditOrderForm($data);

        $r = $this->getRequest();
        if ($r->getIsPost() && $form->populate($_POST)->isValid() && $form->save()) {
            echo $this->json(array_merge([
                'pk' => $form->getInstance()->pk,
                'success' => true,
                'message' => 'Спасибо! Заявка успешно отправлена.',
                'complete' => $hasInstance,
            ], $this->signCredit($form->getInstance())));

            $form->send();
            Mindy::app()->end();
        } else {
            echo $this->json([
                'errors' => $form->getJsonErrors(),
            ]);
            Mindy::app()->end();
        }
    }

    private function signCredit(CreditOrder $order)
    {
        $model = $order->product;
        $module = Mindy::app()->getModule('Ko');
        $price = $model->price == '0.00' ? $model->price_old : $model->price;
        $items = [
            [
                'title' => $model->name,
                'category' => '',
                'qty' => 1,
                'price' => $price,
            ],
        ];

        $firstname = $lastname = $middlename = '';
        $nameArr = explode(' ', $order->name);
        if (count($nameArr) == 3) {
            list($lastname, $firstname, $middlename) = $nameArr;
        } elseif (count($nameArr) == 2) {
            list($lastname, $firstname) = $nameArr;
        } else {
            $firstname = array_shift($nameArr);
        }
        $details = [
            'firstname' => $firstname,
            // 'lastname' => $lastname,
            // 'middlename' => $middlename,
            'email' => $order->email,
            'cellphone' => $order->phone,
        ];

        // print_r($details);
        // die();

        $order = $module->getOrder($order->pk.rand().time(), $items, $details);
        $base64 = base64_encode(json_encode($order));
        /* Получение подписи сообщения */
        $sig = $this->signMessage($base64, $module->getSecretPhrase());

        return [
            'base64' => $base64,
            'sig' => $sig,
            'price' => $price,
        ];
    }

    private function signMessage($message, $secretPhrase)
    {
        $message = $message.$secretPhrase;
        $result = md5($message).sha1($message);
        // 1102? Are you fucking kidding me?
        for ($i = 0; $i < 1102; ++$i) {
            $result = md5($result);
        }

        return $result;
    }

    public function actionGeo()
    {
        $siteModels = Site::objects()->all();
        $sites = [];
        foreach ($siteModels as $site) {
            $sites[$site->city_id] = $site->toArray();
        }

        $pkList = [];
        foreach ($siteModels as $site) {
            $pkList[] = $site->city_id;
        }
        $popular = City::objects()->with(['region'])->filter(['is_popular' => true, 'pk__in' => $pkList])->asArray()->all();

        $regions = [];
        $regionModels = Region::objects()->all();
        $cities = [];
        foreach ($regionModels as $region) {
            $regions = $region->toArray();
            $cities[] = array_merge($region->toArray(), [
                'cities' => $region->cities->with(['region'])->filter(['pk__in' => $pkList])->asArray()->all(),
            ]);
        }

        $site = Mindy::app()->getModule('Sites')->getSite();
        if ($site === null) {
            $site = Site::objects()->get(['pk' => 1]);
        }

        $data = [
            'cities' => $cities,
            'regions' => $regions,
            'site' => $site->toArray(),
            'city' => $site->city->toArray(),
            'popular' => $popular,
            'sites' => $sites,
        ];

        echo $this->json($data);
        Mindy::app()->end();
    }

    public function actionRequestCall()
    {
        $form = new RequestCallForm();

        $r = $this->getRequest();
        if ($r->getIsPost() && $form->populate($_POST)->isValid()) {
            $form->setModelAttributes([
                'referer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
            ]);
            $form->save();

            echo $this->json([
                'success' => true,
                'message' => 'Спасибо! Заявка успешно отправлена.',
            ]);

            $form->send();
            Mindy::app()->end();
        } else {
            echo $this->json([
                'errors' => $form->getJsonErrors(),
            ]);
            Mindy::app()->end();
        }
    }

    public function actionPriceDelivery()
    {
        $form = new PriceDeliveryForm();

        $r = $this->getRequest();

        if ($r->getIsPost() && $form->populate($_POST)->isValid()) {
            $form->setModelAttributes([
                'referer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
            ]);
            $form->save();

            $form->send();

            echo $this->json([
                'success' => true,
                'message' => 'Спасибо! Заявка успешно отправлена.',
            ]);

            $form->save();
            Mindy::app()->end();
        } else {
            echo $this->json([
                'errors' => $form->getJsonErrors(),
            ]);
            Mindy::app()->end();
        }
    }

    public function actionSeo()
    {
        $site = Mindy::app()->getModule('Sites')->getSite();

        $models = SeoText::objects()->filter([
            'site' => $site,
            'url' => isset($_GET['url']) ? $_GET['url'] : '/',
        ])->asArray()->all();

        echo $this->json($models);
        Mindy::app()->end();
    }

    public function actionCartOrder()
    {
        if ($this->getRequest()->getIsPost() === false) {
            $this->error(404);
        }

        $form = new OrderForm();
        $form->setAttributes($_POST);

        $data = CartHelper::getArrayCart();
        $form->setAttributes([
            'plain' => $this->renderTemplate('ko/order_plain.html', $data),
            'total' => $data['total'],
        ]);
        if ($form->isValid() && $form->save()) {
            /** @var \Modules\Cart\Components\Cart $cart */
            $cart = Mindy::app()->getModule('Cart')->cart;

            // https://gitlab.studio107.ru/ko/ko-kirov/issues/63
            // $cart->clear();

            echo $this->json([
                'status' => true,
            ]);

            $form->send();
            $this->end();
        } else {
            echo $this->json([
                'status' => false,
            ]);
            $this->end();
        }
    }

    public function actionCartOrderQuick()
    {
        if ($this->getRequest()->getIsPost() === false) {
            $this->error(404);
        }

        $form = new OrderQuickForm();
        $form->populate($_POST);

        $data = CartHelper::getArrayCart();
        $form->setAttributes([
            'plain' => $this->renderTemplate('ko/order_plain.html', $data),
            'total' => $data['total'],
        ]);
        if ($form->isValid() && $form->save()) {
            /** @var \Modules\Cart\Components\Cart $cart */
            $cart = Mindy::app()->getModule('Cart')->cart;
            $cart->clear();

            echo $this->json([
                'status' => true,
            ]);

            $form->send();
            $this->end();
        } else {
            echo $this->json([
                'errors' => $form->getJsonErrors(),
                'status' => false,
            ]);
            $this->end();
        }
    }

    public function actionCoupon($code)
    {
        $pDays = '+7 day';

        $c = Coupon::objects()->get(['code' => $code]);
        if ($c) {
            $time = strtotime($c->created_at);

            if (time() < strtotime($pDays, $time)) {
                echo $this->json([
                    'name' => $c->name,
                    'phone' => $c->phone_raw,
                    'create_at' => date('d.m.Y', $time),
                    'valid_until' => date('d.m.Y', strtotime($pDays, $time)),
                    'code' => $code,
                    'error' => false,
                ]);
                Mindy::app()->end();
            }
        }

        echo $this->json([
            'error' => true,
        ]);
    }

    public function actionGetCoupon()
    {
        //TODO исправить, не отправляется письмо
        $form = new CouponForm();
        $r = $this->getRequest();

        if ($r->getIsPost()) {
            $form->populate($_POST);

            $coup = null;

            if ($phone = Coupon::clearPhone($form->phone->value)) {
                $coup = Coupon::objects()->filter(['phone' => $phone])->get();
            }

            if (!$coup) {
                $form->phone_raw->value = $form->phone->value;

                if ($form->isValid()) {
                    $form->setModelAttributes([
                        'referer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
                    ]);
                    $form->save();

                    $form->send();

                    $coup = $form->getInstance();
                } else {
                    echo $this->json([
                        'errors' => $form->getJsonErrors(),
                    ]);
                    Mindy::app()->end();
                }
            }

            echo $this->json([
                'success' => true,
                'message' => 'Спасибо! Заявка успешно отправлена.',
                'code' => $coup->code,
            ]);
            Mindy::app()->end();
        }
    }
}
