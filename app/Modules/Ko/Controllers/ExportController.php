<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Controllers;

use Modules\Catalog\Models\Category;
use Modules\Catalog\Models\Image;
use Modules\Catalog\Models\Product;
use Modules\Core\Controllers\CoreController;

class ExportController extends CoreController
{
    public function actionYandex()
    {
        $this->inner('export/yandex.xml');
    }

    public function actionGoogle()
    {
        $this->inner('export/google.xml');
    }

    public function inner($template)
    {
        $products = Product::objects()->asArray()->all();
        $imagesRaw = Image::objects()->group(['product_id'])->valuesList(['product_id', 'image']);
        $images = [];
        foreach ($imagesRaw as $image) {
            $images[$image['product_id']] = $image['image'];
        }
        $categories = Category::objects()->asArray();
        header('Content-Type: text/xml');
        echo $this->render($template, [
            'products' => $products,
            'images' => $images,
            'categories' => $categories,
        ]);
    }
}
