<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Controllers;

use Mindy\Base\Mindy;
use Mindy\Pagination\Pagination;
use Modules\Catalog\CatalogModule;
use Modules\Catalog\Models\Category;
use Modules\Core\Controllers\CoreController;

class KoController extends CoreController
{
    public function actionContacts()
    {
        //        $r = $this->getRequest();
//        $city = $r->cookies->get('city');

//        if ($city !== null) {
//            if ($r->domain != MAIN_DOMAIN) {
//                $r->redirect(MAIN_DOMAIN . '/contacts');
//            }
//        }

        echo $this->render('ko/contacts.html');
    }

    public function actionList($url)
    {
        $category = Category::objects()->get(['url' => $url]);
        if ($category === null) {
            $this->error(404);
        }

        $r = $this->getRequest();
        $city = $r->session->get('city');
        if ($city !== null) {
            $newDomain = 'http://'.$city;
            if ($r->domain != $newDomain) {
                $r->redirect($newDomain.$category->getAbsoluteUrl());
            }
        }

        $parents = $category->objects()->ancestors()->order(['lft'])->all();
        $parents[] = $category;
        $breadcrumbs = [
            ['name' => CatalogModule::t('Shop'), 'url' => Mindy::app()->urlManager->reverse('ko:index')],
        ];
        foreach ($parents as $parent) {
            $breadcrumbs[] = [
                'url' => $parent->getAbsoluteUrl(),
                'name' => (string) $parent,
            ];
        }
        $this->setBreadcrumbs($breadcrumbs);

        Mindy::app()->session->add('category', $category);

        $pager = new Pagination($category->production);
        echo $this->render('catalog/category_detail.html', [
            'categories' => $category->objects()->children()->all(),
            'category' => $category,
            'productions' => $pager->paginate(),
            'pager' => $pager,
        ]);
    }

    public function actionIndex()
    {
        $r = $this->getRequest();
        $cookies = $r->cookies;
        $city = $cookies->get('city');
        if ($city !== null && $r->domain != MAIN_DOMAIN) {
            if ($model = Category::objects()->filter(['is_index' => true])->limit(1)->get()) {
                $r->redirect($r->domain.'/'.$model->url);
            }
        } else {
            echo $this->render('ko/index.html');
        }
    }

    public function actionReviews()
    {
        echo $this->render('ko/reviews.html');
    }

    public function actionSales()
    {
        echo $this->render('ko/sales.html');
    }

    public function actionDummy()
    {
        echo $this->render('ko/sales.html');
    }

    public function actionGift()
    {
        echo $this->render('ko/gift.html');
    }
}
