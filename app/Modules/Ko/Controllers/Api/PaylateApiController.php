<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Controllers\Api;

use Mindy\Base\Mindy;
use Modules\Core\Controllers\Controller;
use Modules\Ko\Forms\PayLateForm;
use Modules\Ko\Models\PayLate;

class PaylateApiController extends Controller
{
    public function actionCreate()
    {
        $request = $this->getRequest();
        if (false === $request->getIsPost()) {
            echo $this->json(['status' => false]);
            Mindy::app()->end();
        } else {
            $form = new PayLateForm();
            $data = $request->post->all();
            if (isset($data['phone'])) {
                $data['phone'] = str_replace([' ', '_', '-', '(', ')', '+'], '', $data['phone']);
            }

            $price = 0;
            $items = json_decode($data['products'], true);
            foreach ($items as $item) {
                $price += $item['price'] * $item['quantity'];
            }
            $data['price'] = $price;

            $form->setAttributes($data);
            if ($form->isValid()) {
                if (false === $form->save()) {
                    echo $this->json(['status' => false]);
                    Mindy::app()->end();
                }

                $form->send();

                Mindy::app()->getModule('Cart')->getComponent('cart')->clear();

                echo $this->json([
                    'status' => true,
                    'token' => $this->generateToken($form->getInstance()->id),
                    'id' => $form->getInstance()->id,
                ]);
            } else {
                echo $this->json([
                    'status' => false,
                    'errors' => $form->getErrors(),
                ]);
                Mindy::app()->end();
            }
        }
    }

    protected function generateToken($orderId)
    {
        $login = 'ko-kirov';
        $password = 'N345KyU';

        return md5($login.md5($password).$orderId);
    }

    public function actionStatus()
    {
        $request = $this->getRequest();
        $orderId = $request->post->get('order_id');

        $token = $this->generateToken($orderId);
        if (($token != $request->post->get('token'))) {
            echo "RESULT:-1\nDESCR:ошибка определения источника запроса";
            Mindy::app()->end();
        }

        $order = PayLate::objects()->get(['id' => $orderId]);
        if (null === $order) {
            echo "RESULT:-1\nDESCR:некорректный номер заказа";
            Mindy::app()->end();
        }

        if ($order->price != $request->post->get('sum')) {
            echo "RESULT:-1\nDESCR:несовпадение сумм";
            Mindy::app()->end();
        }

        $state = $request->post->get('state');
        if ($state == 1) {
            $order->is_paid = true;
            $order->save();
            echo "RESULT:1\nDESCR:статус оплатил";
            Mindy::app()->end();
        } elseif ($state == -1) {
            $order->is_cancel = true;
            $order->save();
            echo "RESULT:1\nDESCR:отказ принят";
            Mindy::app()->end();
        } elseif ($state == 0) {
            echo "RESULT:1\nDESCR:актуален";
            Mindy::app()->end();
        } else {
            echo "RESULT:1\nDESCR:неизвестное состояние";
            Mindy::app()->end();
        }
    }
}
