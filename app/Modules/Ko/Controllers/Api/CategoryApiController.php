<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Controllers\Api;

use Modules\Catalog\Models\Category;
use Modules\Core\Controllers\Controller;

class CategoryApiController extends Controller
{
    public function actionList()
    {
        $objects = Category::objects()->order(['lft'])->asTree()->all();

        echo $this->json([
            'objects' => $objects,
        ]);
    }
}
