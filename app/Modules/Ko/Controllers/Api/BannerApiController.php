<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Controllers\Api;

use Modules\Core\Controllers\Controller;
use Modules\Ko\Models\Banner;

class BannerApiController extends Controller
{
    public function actionList()
    {
        $objects = Banner::objects()->asArray()->all();

        echo $this->json([
            'status' => true,
            'objects' => $objects,
        ]);
    }
}
