<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Controllers\Api;

use Mindy\Base\Mindy;
use Mindy\Orm\Q\OrQ;
use Mindy\Pagination\Pagination;
use Modules\Catalog\Models\Category;
use Modules\Catalog\Models\Product;
use Modules\Core\Controllers\Controller;

class ProductApiController extends Controller
{
    protected $cacheTime = 604800; //(60*60*24*7);

    public function actionView()
    {
        $url = $this->getRequest()->get->get('url');
        if (empty($url)) {
            echo $this->json(['status' => false, 'error' => true, 'message' => 'Missing url']);
            Mindy::app()->end();
        }

        $product = Product::objects()->get(['url' => $url]);
        if ($product === null) {
            echo $this->json(['status' => false, 'error' => true, 'message' => 'Missing product']);
            Mindy::app()->end();
        }

        echo $this->json([
            'status' => true,
            'product' => $product->toArray(),
        ]);
    }

    public function actionList()
    {
        $url = $this->getRequest()->get->get('url');
        if (empty($url)) {
            echo $this->json(['status' => false, 'error' => true, 'message' => 'Missing url']);
            Mindy::app()->end();
        }

        $category = Category::objects()->get(['url' => $url]);
        if ($category === null) {
            echo $this->json(['status' => false, 'error' => true, 'message' => 'Missing category']);
            Mindy::app()->end();
        }

        $qs = Product::objects()->filter(['is_published' => true, 'product_through__category_id' => $category->id]);

        if (
            ($minSize = (int) $this->getRequest()->get->get('min_size')) > 0 &&
            ($maxSize = (int) $this->getRequest()->get->get('max_size')) > 0
        ) {
            // $qs->filter(['min_size__gte' => $minSize, 'max_size__lte' => $maxSize]);
            $qs->filter([
                'sizes__value__range' => [$minSize, $maxSize],
            ]);
        }

        $allowedOrders = [
            'price', '-price',
            'update', '-update',
            'popular', '-popular',
            'discount_percentage', '-discount_percentage',
        ];
        if (
            ($order = $this->getRequest()->get->get('order')) &&
            in_array($order, $allowedOrders)
        ) {
            switch ($order) {
                case 'price':
                    $order = '-price_source';
                    break;
                case '-price':
                    $order = 'price_source';
                    break;

                case 'discount_percentage':
                    $order = '-price_discount';
                    break;
                case '-discount_percentage':
                    $order = 'price_discount';
                    break;

                case 'update':
                    $order = '-id';
                    break;
                case '-update':
                    $order = 'id';
                    break;

                case '-popular':
                    $order = '-is_popular';
                    break;
                case 'popular':
                    $order = 'is_popular';
                    break;
            }

            $qs->order([
                $order,
            ]);
        } else {
            $qs->order(['product_through__position']);
        }

        $pager = new Pagination($qs, [
            'pageKey' => 'page',
            'pageSize' => 100,
            'pageSizeKey' => 'page_size',
        ]);
        $models = $pager->paginate();

        $objects = [];
        foreach ($models as $model) {
            $objects[] = $model->toArray();
        }

        $breadcrumbs = $category->objects()->ancestors()->valuesList(['name', 'url']);

        echo $this->json([
            'breadcrumbs' => $breadcrumbs,
            'category' => $category->toArray(),
            'objects' => $objects,
            'status' => true,
            'meta' => $pager->toJson()['meta'],
        ]);
    }

    public function actionSearch()
    {
        $q = $this->getRequest()->get->get('q');
        if (empty($q)) {
            echo $this->json([
                'status' => true,
                'objects' => [],
            ]);
            Mindy::app()->end();
        }

        $categories = Category::objects()->filter([
            'name__icontains' => $q,
        ]);
        $ids = [];
        foreach ($categories as $category) {
            $ids = array_merge($ids, $category->production->valuesList(['id'], true));
        }

        $qs = Product::objects();
        if (empty($ids)) {
            $qs->filter([
                new OrQ([
                    ['name__icontains' => $q],
                    ['code__icontains' => $q],
                ]),
            ]);
        } else {
            $qs->filter(['id__in' => $ids]);
            $qs->filter([
                new OrQ([
                    ['name__icontains' => $q],
                    ['code__icontains' => $q],
                ]),
            ])->limit(10);
        }

        if (
            ($minSize = (int) $this->getRequest()->get->get('min_size')) > 0 &&
            ($maxSize = (int) $this->getRequest()->get->get('max_size')) > 0
        ) {
            // $qs->filter(['min_size__gte' => $minSize, 'max_size__lte' => $maxSize]);
            $qs->filter([
                'sizes__value__range' => [$minSize, $maxSize],
            ]);
        }

        $allowedOrders = [
            'price', '-price',
            'update', '-update',
            'popular', '-popular',
            'discount', '-discount',
        ];
        if (
            ($order = $this->getRequest()->get->get('order')) &&
            in_array($order, $allowedOrders)
        ) {
            switch ($order) {
                case 'discount':
                    $order .= '_id';
                    break;
                case 'update':
                    $order = 'created_at';
                    break;
                case '-update':
                    $order = '-created_at';
                    break;
                case '-popular':
                    $order = '-is_popular';
                    break;
                case 'popular':
                    $order = 'is_popular';
                    break;
            }

            $qs->order([
                str_replace('price', 'price_rub', $order),
            ]);
        } else {
            $qs->order(['product_through__position']);
        }

        $pager = new Pagination($qs, [
            'pageKey' => 'page',
            'pageSize' => 100,
            'pageSizeKey' => 'page_size',
        ]);
        $models = $pager->paginate();

        $products = [];
        foreach ($models as $model) {
            $products[] = $model->toArray();
        }
        echo $this->json([
            'status' => true,
            'objects' => $products,
            'meta' => $pager->toJson()['meta'],
        ]);
        Mindy::app()->end();
    }

    private function sortArrayByArray(array $array, array $orderArray)
    {
        $ordered = [];
        foreach ($orderArray as $key) {
            if (array_key_exists($key, $array)) {
                $ordered[$key] = $array[$key];
                unset($array[$key]);
            }
        }

        return $ordered + $array;
    }

    public function actionAutocomplete()
    {
        $q = $this->getRequest()->get->get('q');
        if (empty($q)) {
            echo $this->json([
                'status' => true,
                'objects' => [],
            ]);
            Mindy::app()->end();
        }

        $products = [];
        $models = Product::objects()->filter(['name__icontains' => $q])->limit(10);
        foreach ($models as $model) {
            $products[] = $model->toArray();
        }
        echo $this->json([
            'status' => true,
            'objects' => $products,
        ]);
        Mindy::app()->end();
    }

    public function actionRelated()
    {
        $url = $this->getRequest()->get->get('url');
        if (empty($url)) {
            echo $this->json(['status' => false, 'error' => true, 'message' => 'Missing url']);
            Mindy::app()->end();
        }

        $product = Product::objects()->get(['is_published' => true, 'url' => $url]);
        if ($product === null) {
            echo $this->json(['status' => false, 'error' => true, 'message' => 'Missing product']);
            Mindy::app()->end();
        }

        $relatedModels = $product->related->limit(20)->all();

        $qs = Product::objects()
            ->exclude(['id' => $product->id])
            ->limit(20 - count($relatedModels))
            ->filter(['is_published' => true])
            ->order(['?']);
        if ($product->category_id) {
            $qs->filter(['categories__id' => $product->category_id]);
        } else {
            $qs->filter(['categories__pk__in' => $product->categories->valuesList(['id'], true)]);
        }

        $newModels = $qs->all();

        $data = [];
        foreach (array_merge($relatedModels, $newModels) as $model) {
            $data[] = $model->toArray();
        }

        echo $this->json(['objects' => $data, 'status' => true]);
    }

    public function actionNew()
    {
        $models = Product::objects()
            ->filter(['is_new' => true])
            ->all();

        $products = [];
        foreach ($models as $product) {
            $products[] = $product->toArray();
        }

        echo $this->json([
            'objects' => $products,
        ]);
    }

    public function actionPopular()
    {
        $models = Product::objects()
            ->filter(['is_popular' => true])
            ->all();

        $products = [];
        foreach ($models as $product) {
            $products[] = $product->toArray();
        }

        echo $this->json([
            'objects' => $products,
        ]);
    }
}
