<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Controllers;

use Mindy\Base\Mindy;
use Mindy\Helper\Json;
use Modules\Core\Controllers\ApiBaseController;
use Modules\Ko\Models\Banner;
use Modules\Ko\Models\SeoText;

class Api_v11_Controller extends ApiBaseController
{
    protected $cache_delay = 604800; //(60*60*24*7);

    public function actionGetSeo($url)
    {
        $site = Mindy::app()->getModule('Sites')->getSite();
        $models = SeoText::objects()
            ->filter(['url' => '/'.$url, 'site_id__isnull' => true])
            ->orFilter(['url' => '/'.$url, 'site' => $site])
            ->group(['url'])
            ->asArray()
            ->all();

        if (empty($models)) {
            $this->error(404);
        }

        echo $this->json($models);
    }

    public function actionGetBanner($url)
    {
        $model = Banner::objects()
            ->filter(['url' => $url])
            ->limit(1)
            ->get();

        if (!$model) {
            $this->error(404);
        }

        echo $model->toArray();
    }
}
