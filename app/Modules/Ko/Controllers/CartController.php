<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Controllers;

use Mindy\Base\Mindy;
use Modules\Cart\Controllers\CartController as BaseCartController;
use Modules\Catalog\Models\Product;
use Modules\Ko\Helpers\CartHelper;

class CartController extends BaseCartController
{
    protected function addInternal($uniqueId, $quantity, $type)
    {
        $product = Product::objects()->get(['id' => $uniqueId]);
        if ($product && $quantity > 0) {
            $cart = $this->getCart();
            $extra = isset($_POST['extra']) ? $_POST['extra'] : [];
            $cart->add($product, $quantity, $type, $extra);

            return true;
        }

        return false;
    }

    public function actionList()
    {
        header('Content-Type: application/json');
        echo CartHelper::getJsonCart();
        Mindy::app()->end();
    }

    public function actionJsonlist()
    {
        header('Content-Type: application/json');
        echo CartHelper::getJsonCart();
        Mindy::app()->end();
    }

    protected function updateInternal($key, $uniqueId, $quantity, $type)
    {
        $key = (int) $key;
        $product = Product::objects()->get(['id' => $uniqueId]);
        if ($product && $quantity > 0) {
            $cart = $this->getCart();
            $extra = isset($_POST['extra']) ? $_POST['extra'] : [];
            $positionKey = $cart->getPositionByKey($key);
            if ($positionKey) {
                $item = $cart->getStorage()->get($positionKey);
                $item->setData($extra);
                $cart->getStorage()->remove($positionKey);
                $cart->getStorage()->add($positionKey, $item);
            } else {
                return false;
            }

            return true;
        }

        return false;
    }
}
