<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Controllers;

use Mindy\Base\Mindy;
use Modules\Core\Controllers\ApiBaseController;

class Api_v12_Controller extends ApiBaseController
{
    public function actionSettings($type)
    {
        header('Content-Type: application/javascript');

        if ($type == 'desktop') {
            echo $this->render('settings/v1_2/desktop.html', []);
            Mindy::app()->end();
        }

        if ($type == 'mobile') {
            echo $this->render('settings/v1_2/mobile.html', []);
            Mindy::app()->end();
        }

        echo $this->render('settings/dummy.html', []);
    }
}
