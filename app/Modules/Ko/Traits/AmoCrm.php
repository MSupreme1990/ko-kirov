<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Traits;

use DateTime;
use DateTimeZone;
use Exception;
use Mindy\Base\Mindy;
use Mindy\Form\Fields\HiddenField;

trait AmoCrm
{
    //список параметров amoCRM
    //по хорошему их надо в конфг пихнуть.
    public $amoCRM_settings = [
        'responsible_user_id' => 213444, // ID ответственного за созданные контакты пользователя
        'custom_fields' => [ // массив со списком ID пользовательских полей
            'email' => 921072, // email
            'phone' => 921070, // телефон
            'Источник' => 926862,
            'utm_term' => 1280892,
            'utm_campaign' => 1280894,
            'utm_content' => 1280896,
        ],
        'AMOCRM_SUBDOMAIN' => 'kokirov', // аккаунт - поддомен (первое слово в адресе. Перед .amocrm.ru )
        'AMOCRM_MASTER_LOGIN' => 'aamt@mail.ru',  // данные по пользователю с полным доступом e-mail
//        'AMOCRM_MASTER_HASH' => 'ba0b12a9fe06aedc98ad4eefb01506e1', # Ключ для авторизации в API со страницы профиля amocrm
        'AMOCRM_MASTER_HASH' => '0d8db1d2890a58a58736b4a6d71e397f', // Ключ для авторизации в API со страницы профиля amocrm
        'AMOCRM_ACCOUNT_TIMEZONE' => 'Europe/Moscow', // временная зона, указанная в настройках аккаунта
        'cookieName' => null, // имя файла cookie для данного процесса.
        'server_time' => null,
    ];
    public $amoCRM_params = [];

    public function getCrmFields()
    {
        return [
            'utm_term' => [
                'class' => HiddenField::className(),
            ],
            'utm_campaign' => [
                'class' => HiddenField::className(),
            ],
            'utm_source' => [
                'class' => HiddenField::className(),
            ],
            'utm_medium' => [
                'class' => HiddenField::className(),
            ],
        ];
    }

    /**
     * Создаёт контакт в amoCRM.
     *
     * @param $name - имя
     * @param $phone - телефон
     * @param $email - почта
     * @param $text - примечание
     *
     * @return bool - true в случае успеха, иначе false
     */
    public function amoCRM_process($name, $phone, $email = null, $text = null)
    {
        $phone = preg_replace('/[^0-9]/', '', $phone);

        if (!$this->amo_curl_authorize()) {
            return false;
        }
        $contact_id = $this->amoCRM_check($phone, $email);
        if (empty($contact_id)) {
            $contacts['request']['contacts']['add'] = [
                [
                    'name' => ((!empty($name)) ? $name : 'Новый контакт'),
                    'responsible_user_id' => $this->amoCRM_settings['responsible_user_id'],
                    'custom_fields' => [],
                ],
            ];
            if (!empty($email)) {
                $contacts['request']['contacts']['add'][0]['custom_fields'][] = [
                    'id' => $this->getContactCustomFieldId('EMAIL'),
                    'values' => [
                        ['value' => $email, 'enum' => 'PRIV'],
                    ],
                ];
            }
            if (!empty($phone)) {
                $contacts['request']['contacts']['add'][0]['custom_fields'][] = [
                    'id' => $this->getContactCustomFieldId('PHONE'),
                    'values' => [
                        ['value' => $phone, 'enum' => 'OTHER'],
                    ],
                ];
            }

            ///START_UTM
            if (!empty($_COOKIE)) {
                foreach ($_COOKIE as $k => $v) {
                    if (is_string($v)) {
                        if (strtolower($v) != 'null' && !empty($v)) {
                            if (strpos(strtolower($k), 'utm_') === 0 || in_array(strtolower($k), ['gclid', 'yclid'])) {
                                if (in_array(strtolower($k), ['gclid', 'yclid'])) {
                                    $contacts['request']['contacts']['add'][0]['custom_fields'][] = [
                                        'id' => $this->getContactCustomFieldId('Источник'),
                                        'values' => [
                                            [
                                                'value' => ((strtolower($k) == 'yclid') ? 'Яндекс' : 'Гугл'),
                                                'enum' => 'OTHER',
                                            ],
                                        ],
                                    ];
                                } else {
                                    if ($this->getContactCustomFieldId(strtolower($k))) {
                                        $contacts['request']['contacts']['add'][0]['custom_fields'][] = [
                                            'id' => $this->getContactCustomFieldId(strtolower($k)),
                                            'values' => [
                                                [
                                                    'value' => $v,
                                                    'enum' => 'OTHER',
                                                ],
                                            ],
                                        ];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ///END_UTM

            $responce = $this->amo_curl_post('/private/api/v2/json/contacts/set', $contacts);
            if (is_null($responce)) {
                return false;
            }
            $contact_id = $responce['contacts']['add'][0]['id'];
        }

        if (!empty($text)) {
            $text = str_replace(['<br/>', '<br>', '<hr>', '<hr/>'], "\n", $text);
            $text = str_replace(['<strong>', '</strong>', '<b>', '</b>'], '', $text);
            $notes['request']['notes']['add'] = [
                [
                    'element_id' => $contact_id,
                    'element_type' => 1,
                    'note_type' => 4,
                    'text' => $text,
                    'responsible_user_id' => $this->amoCRM_params['responsible_user_id'],
                ],
            ];
            $responce = $this->amo_curl_post('/private/api/v2/json/notes/set', $notes);
            if (is_null($responce)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Проверяет существование конаткта в amoCRM по телефону и почте.
     *
     * @param $phone - телефон
     * @param $email - почта
     *
     * @return - ID контакта либо false
     */
    public function amoCRM_check($phone = null, $email = null)
    {
        if (!empty($email)) {
            $args = ['query' => $email];
            $responce = $this->amo_curl_get('/private/api/v2/json/contacts/list', $args);
            if (isset($responce)) {
                foreach ($responce['contacts'] as $contact) {
                    if (isset($contact['custom_fields'])) {
                        foreach ($contact['custom_fields'] as $custom_field) {
                            if ($custom_field['id'] == $this->getContactCustomFieldId('EMAIL')) {
                                foreach ($custom_field['values'] as $value) {
                                    if ($value['value'] == $email) {
                                        return $contact['id'];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (!empty($phone)) {
            $args = ['query' => $phone];
            $responce = $this->amo_curl_get('/private/api/v2/json/contacts/list', $args);
            if (isset($responce)) {
                foreach ($responce['contacts'] as $contact) {
                    if (isset($contact['custom_fields'])) {
                        foreach ($contact['custom_fields'] as $custom_field) {
                            if ($custom_field['id'] == $this->getContactCustomFieldId('PHONE')) {
                                foreach ($custom_field['values'] as $value) {
                                    if ($value['value'] == $phone) {
                                        return $contact['id'];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Проверяет код HTTP на ошибку. Прерывает выполнение в случае ошибки.
     *
     * @param $code - код ошибки для проверки
     *
     * @throws Exception If код индицирует ошибоку
     */
    public function amo_curl_CheckCurlResponse($code)
    {
        $code = (int) $code;
        $errors = [
            301 => 'Moved permanently',
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable',
        ];
        if ($code != 200 && $code != 204) {
            throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error '.$code, $code);
        }
    }

    /**
     * Пытается авторизироваться в amoCRM.
     *
     * @param array  $user_data - аргументы для передачи POST вместо логина и пароля по умолчанию
     * @param string $subdomain - субдомен (аккаунт amoCRM)
     *
     * @return bool - успех операции. true если авторизация успешна.
     */
    public function amo_curl_authorize($user_data = null, $subdomain = null)
    {
        if (empty($subdomain)) {
            $subdomain = $this->amoCRM_settings['AMOCRM_SUBDOMAIN'];
        }
        if (empty($this->amoCRM_settings['cookieName'])) {
            $dir = opendir(__DIR__);
            while (false !== ($file = readdir($dir))) {
                if (stripos($file, 'amocrm_ckie_') !== false) {
                    unlink(__DIR__.'/'.$file);
                }
            }
            $this->amoCRM_settings['cookieName'] = tempnam(__DIR__, 'amocrm_ckie_');
        }

        if (is_null($user_data)) {
            $user_data = ['USER_LOGIN' => $this->amoCRM_settings['AMOCRM_MASTER_LOGIN'],
                'USER_HASH' => $this->amoCRM_settings['AMOCRM_MASTER_HASH'], ];
        }
        $link = 'https://'.$subdomain.'.amocrm.ru/private/api/auth.php?type=json';
        $curl = curl_init(); //Сохраняем дескриптор сеанса cURL
        //Устанавливаем необходимые опции для сеанса cURL
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($user_data));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, $this->amoCRM_settings['cookieName']); //PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_COOKIEJAR, $this->amoCRM_settings['cookieName']); //PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $out = curl_exec($curl); //Инициируем запрос к API и сохраняем ответ в переменную
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        try {
            $this->amo_curl_CheckCurlResponse($code);
            $this->get_account_current();
        } catch (Exception $E) {
            return false;
        }

        $response = json_decode($out, true);

        //Флаг авторизации доступен в свойстве "auth"
        return isset($response['response']['auth']);
    }

    public function get_account_current()
    {
        $cache = false;

        if (Mindy::app()->cache->exists('amoCrmAccountCurrent')) {
            $cache = Mindy::app()->cache->get('amoCrmAccountCurrent');
        }

        if ($cache) {
            $this->amoCRM_params = $cache;
        } else {
            $this->amoCRM_params = $this->amo_curl_get('/private/api/v2/json/accounts/current');
            Mindy::app()->cache->add('amoCrmAccountCurrent', $this->amoCRM_params);
        }
    }

    public function getContactCustomFieldId($name)
    {
        foreach ($this->amoCRM_params['account']['custom_fields']['contacts'] as $arr) {
            if ($arr['name'] == $name || $arr['code'] == $name) {
                return $arr['id'];
            }
        }

        return null;
    }

    /**
     * Выполняет запрос POST по REST API через CURL и возвращает массив полученных данных.
     *
     * @param string $url       - (!) относительный адрес для запроса
     * @param array  $postData  - данные для отправки POST
     * @param string $subdomain - субдомен (аккаунт amoCRM)
     *
     * @return array массив присланный в ответ на запрос или null, если нет данных. В случае ошибки выполнение прерывается.
     */
    public function amo_curl_post($url, $postData, $subdomain = null)
    {
        if (empty($subdomain)) {
            $subdomain = $this->amoCRM_settings['AMOCRM_SUBDOMAIN'];
        }
        if (empty($this->amoCRM_settings['cookieName'])) {
            $dir = opendir(__DIR__);
            while (false !== ($file = readdir($dir))) {
                if (stripos($file, 'amocrm_ckie_') !== false) {
                    unlink(__DIR__.'/'.$file);
                }
            }
            $this->amoCRM_settings['cookieName'] = tempnam(__DIR__, 'amocrm_ckie_');
        }

        $link = 'https://'.$subdomain.'.amocrm.ru'.$url;
        $curl = curl_init($link); //Сохраняем дескриптор сеанса cURL
        //Устанавливаем необходимые опции для сеанса cURL

        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_COOKIEFILE, $this->amoCRM_settings['cookieName']);
        curl_setopt($curl, CURLOPT_COOKIEJAR, $this->amoCRM_settings['cookieName']);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($postData));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);

        $out = curl_exec($curl); //Инициируем запрос к API и сохраняем ответ в переменную
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        try {
            $this->amo_curl_CheckCurlResponse($code);
        } catch (Exception $E) {
            //            d($postData,$E);
            return null;
        }

        $Response = json_decode($out, true);

        return $Response['response'];
    }

    /**
     * Выполняет запрос GET по REST API через CURL и возвращает массив полученных данных.
     *
     * @param string $url           - (!) относительный адрес для запроса
     * @param array  $args          - данные для отправки GET
     * @param int    $modifiedSince - метка времени для передачи в виде параметра modified-since
     * @param string $subdomain     - субдомен (аккаунт amoCRM)
     *
     * @throws Exception
     *
     * @return array часть ['response'] массива присланного в ответ на запрос или null, если нет данных. В случае ошибки выполнение прерывается.
     */
    public function amo_curl_get($url, $args = null, $modifiedSince = null, $subdomain = null)
    {
        if (empty($subdomain)) {
            $subdomain = $this->amoCRM_settings['AMOCRM_SUBDOMAIN'];
        }
        if (empty($this->amoCRM_settings['cookieName'])) {
            $dir = opendir(__DIR__);
            while (false !== ($file = readdir($dir))) {
                if (stripos($file, 'amocrm_ckie_') !== false) {
                    unlink(__DIR__.'/'.$file);
                }
            }
            $this->amoCRM_settings['cookieName'] = tempnam(__DIR__, 'amocrm_ckie_');
        }

        $curl = curl_init(); //Сохраняем дескриптор сеанса cURL
        //Устанавливаем необходимые опции для сеанса cURL
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_HEADER, false);
        if (!is_null($modifiedSince)) {
            $dateTime = new DateTime(); // время хоста
            $dateTime->setTimestamp($modifiedSince); // время по метке
            $dateTime->setTimezone(new DateTimeZone($this->amoCRM_settings['AMOCRM_ACCOUNT_TIMEZONE']));
            $sDate = $dateTime->format('D, d M Y H:i:s');
            curl_setopt($curl, CURLOPT_HTTPHEADER, ["if-modified-since: $sDate"]);
        }
        curl_setopt($curl, CURLOPT_COOKIEFILE, $this->amoCRM_settings['cookieName']);
        curl_setopt($curl, CURLOPT_COOKIEJAR, $this->amoCRM_settings['cookieName']);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $chunk = [];
        $retry_counter = 3;
        $completion_flag = false;
        if (is_null($args)) {
            $args = [];
        }

        while ($retry_counter && !$completion_flag) {
            $completion_flag = true; // надежда...

            $link = 'https://'.$subdomain.'.amocrm.ru'.$url;

            if (!empty($args)) {
                $link .= '?'.http_build_query($args);
            }

            curl_setopt($curl, CURLOPT_URL, $link);

            $out = curl_exec($curl); //Инициируем запрос к API и сохраняем ответ в переменную
            $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            try {
                $this->amo_curl_CheckCurlResponse($code);
            } catch (Exception $E) {
                $err_mess = 'Ошибка: '.$E->getMessage().PHP_EOL.'Код ошибки: '.$E->getCode();
                $completion_flag = false; // .. надежда не оправдалась
                --$retry_counter;
                if (!$retry_counter && !$completion_flag) {
                    throw $E;
                }
                sleep(1);
                continue; //while ($retry_counter && !$completion_flag)
            }

            $response = json_decode($out, true);
            $chunk = $response['response'];
            if (!empty($chunk['server_time'])) {
                $this->amoCRM_settings['server_time'] = $chunk['server_time'];
            }
        }

        curl_close($curl);
        if (!$completion_flag) {
            throw new Exception('Undescribed error');
        }

        return $chunk;
    }

    /**
     * Массовое получение данных.
     *
     * @param string   $url           - (!) относительный адрес для запроса
     * @param string   $dataType      - имя запрашиваемых данных ('leads', 'contacts'...) для использования многостраничного запроса (limit_rows и limit_offset).
     * @param array    $args          - данные для отправки GET. Если указан аргумент 'limit_rows', то он обрабатывается без ограничения в 500 элементов,  описаного в API.
     * @param int      $modifiedSince - ограничение по дате (по времени amoCRM)
     * @param callable $processor     - callback для обработки единичного элемента, если указан - то в конечный результат включается результат обработки, а не исходный элемент
     * @param string   $subdomain     - иной аккаунт, чем указаный в конфиге
     *
     * @return array - {data => {id => {element}...}, 'count' => TOTAL}, если существует $element['id'], иначе
     *               {data => [{element}], 'count' => TOTAL}
     *               значение TOTAL - подсчитанное число элементов, полученных в ответ из amoCRM - МОЖЕТ БЫТЬ БОЛЬШЕ, чем реальное число элементов в data
     */
    public function amo_curl_get_mass($url, $dataType, $args = null, $modifiedSince = null, $processor = null, $subdomain = null)
    {
        if (empty($subdomain)) {
            $subdomain = $this->amoCRM_settings['AMOCRM_SUBDOMAIN'];
        }
        $args = is_null($args) ? [] : $args;
        $nItemsToGet = $args['limit_rows'];
        $limitRows = $nItemsToGet && $nItemsToGet > 0;
        $originalOffset = isset($args['limit_offset']) ? $args['limit_offset'] : 0;

        $resultData = [];
        $args['limit_offset'] = $originalOffset;
        $actualCounter = 0;

        do {
            $args['limit_rows'] = ($limitRows) ? min($nItemsToGet, 400) : 400;
            $batch = $this->amo_curl_get($url, $args, $modifiedSince, $subdomain);

            foreach ($batch[$dataType] as $element) {
                /** @var callable $processor */
                $processed_element = is_null($processor) ? $element : $processor($element);
                if (isset($element['id'])) {
                    $resultData[$element['id']] = $processed_element;
                } else {
                    $resultData[] = $processed_element;
                }
            }

            $elementsRecieved = count($batch[$dataType]);
            $actualCounter += $elementsRecieved;
            $nItemsToGet -= $elementsRecieved;
            $args['limit_offset'] += 400;
        } while ($elementsRecieved == 400 && !($limitRows && $nItemsToGet <= 0));

        return ['data' => $resultData, 'count' => $actualCounter];
    }
}
