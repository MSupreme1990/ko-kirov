<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    '/reviews' => [
        'name' => 'reviews',
        'callback' => '\Modules\Ko\Controllers\KoController:reviews',
    ],
    '/sales' => [
        'name' => 'sales',
        'callback' => '\Modules\Ko\Controllers\KoController:sales',
    ],
    '/contacts' => [
        'name' => 'contacts',
        'callback' => '\Modules\Ko\Controllers\KoController:contacts',
    ],
    '/' => [
        'name' => 'index',
        'callback' => '\Modules\Ko\Controllers\KoController:index',
    ],
    '/catalog/{url:.*}' => [
        'name' => 'product_detail',
        'callback' => '\Modules\Catalog\Controllers\ProductController:view',
    ],
    '/recent' => [
        'name' => 'recent',
        'callback' => '\Modules\Catalog\Controllers\CategoryController:recent',
    ],
    '/gift' => [
        'name' => 'gift',
        'callback' => '\Modules\Ko\Controllers\KoController:gift',
    ],
    '/cart' => [
        'name' => 'cart',
        'callback' => '\Modules\Ko\Controllers\KoController:dummy',
    ],
    '/search' => [
        'name' => 'search',
        'callback' => '\Modules\Ko\Controllers\KoController:dummy',
    ],
    '/test' => [
        'name' => 'search',
        'callback' => '\Modules\Ko\Controllers\KoController:dummy',
    ],
    '/coupon/{code:.*}' => [
        'name' => 'search',
        'callback' => '\Modules\Ko\Controllers\KoController:dummy',
    ],
    '/get_coupon' => [
        'name' => 'search',
        'callback' => '\Modules\Ko\Controllers\KoController:dummy',
    ],
    '/yandex.xml' => [
        'name' => 'export_yandex',
        'callback' => '\Modules\Ko\Controllers\ExportController:yandex',
    ],
    '/google.xml' => [
        'name' => 'export_google',
        'callback' => '\Modules\Ko\Controllers\ExportController:google',
    ],
    '/{url:.*}' => [
        'name' => 'list',
        'callback' => '\Modules\Catalog\Controllers\CategoryController:list',
    ],
];
