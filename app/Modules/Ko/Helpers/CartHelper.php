<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Helpers;

use Mindy\Base\Mindy;
use Mindy\Helper\Json;
use Modules\Cart\Components\CartItem;
use Modules\Currency\Models\Currency;

class CartHelper
{
    public static function getArrayCart()
    {
        /** @var \Modules\Cart\Components\Cart $cart */
        $cart = Mindy::app()->getModule('Cart')->getComponent('cart');
        $items = $cart->getItems();
        $total = 0;

        $itemsResult = [];
        foreach ($items as $key => $item) {
            /* @var $item CartItem */
            $itemResult = [];

            $object = $item->getObject();
            $images = [];
            $mainImage = null;
            foreach ($object->images->all() as $image) {
                if ($image->is_main) {
                    $mainImage = $image;
                }
                $images[] = $image->toArray();
            }
            $itemResult['object'] = array_merge($object->toArray(), [
                'image' => $mainImage === null && count($images) > 0 ? $images[0] : $mainImage->toArray(),
                'images' => $images,
                'sizes' => $object->sizes->valuesList(['value'], true),
            ]);

            $itemResult['data'] = $item->getData();
            $itemResult['quantity'] = $item->getQuantity();
            $itemResult['type'] = $item->getType();
            $itemResult['price'] = $item->getPrice();
            $itemResult['key'] = $key;

            $itemsResult[] = $itemResult;

            $total += $item->recalculate();
        }

        $symbol = Mindy::app()->request->get->get('symbol', 'RUB');
        $currency = Currency::objects()->latest()->get();

        return [
            'total' => $symbol == 'RUB' ? $total : $total / $currency->data[$symbol],
            'items' => $itemsResult,
        ];
    }

    public static function getJsonCart()
    {
        return Json::encode(self::getArrayCart());
    }
}
