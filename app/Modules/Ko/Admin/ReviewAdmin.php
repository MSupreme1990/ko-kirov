<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Admin;

use Mindy\Orm\Model;
use Modules\Admin\Components\ModelAdmin;
use Modules\Ko\Forms\ReviewForm;
use Modules\Ko\Models\Review;

class ReviewAdmin extends ModelAdmin
{
    public function getQuerySet(Model $model)
    {
        return $model->objects()->getQuerySet()->order(['-created_at']);
    }

    public function getColumns()
    {
        return ['name', 'author', 'created_at', 'position'];
    }

    /**
     * @return string
     */
    public function getCreateForm()
    {
        return ReviewForm::className();
    }

    /**
     * @return \Mindy\Orm\Model
     */
    public function getModel()
    {
        return new Review();
    }
}
