<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Admin;

use Modules\Admin\Components\ModelAdmin;
use Modules\Ko\Models\SalesBack;

class SalesBackAdmin extends ModelAdmin
{
    public function getColumns()
    {
        return ['email', 'phone', 'referer'];
    }

    /**
     * @return \Mindy\Orm\Model
     */
    public function getModel()
    {
        return new SalesBack();
    }
}
