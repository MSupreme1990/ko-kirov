<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Admin;

use Modules\Admin\Components\ModelAdmin;
use Modules\Ko\Models\Order;

class CartOrderAdmin extends ModelAdmin
{
    public function getCanCreate()
    {
        return false;
    }

    public function getActionsList()
    {
        return ['delete', 'info'];
    }

    public function getColumns()
    {
        return ['name', 'phone', 'email', 'address', 'total', 'created_at'];
    }

    /**
     * @return \Mindy\Orm\Model
     */
    public function getModel()
    {
        return new Order();
    }
}
