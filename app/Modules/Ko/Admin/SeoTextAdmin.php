<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Ko\Admin;

use Modules\Admin\Components\ModelAdmin;
use Modules\Ko\Forms\SeoTextForm;
use Modules\Ko\Models\SeoText;

class SeoTextAdmin extends ModelAdmin
{
    public function getColumns()
    {
        return [
            'name', 'url', 'site',
        ];
    }

    public function getCreateForm()
    {
        return SeoTextForm::className();
    }

    /**
     * @return \Mindy\Orm\Model
     */
    public function getModel()
    {
        return new SeoText();
    }
}
