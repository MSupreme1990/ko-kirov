<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Offices\Forms;

use Mindy\Form\ModelForm;
use Modules\Offices\Models\Image;
use Modules\Offices\OfficesModule;

class ImageInlineForm extends ModelForm
{
    public $exclude = ['office'];

    /**
     * @return \Mindy\Orm\Model
     */
    public function getModel()
    {
        return new Image();
    }

    public function getName()
    {
        return OfficesModule::t('Images');
    }
}
