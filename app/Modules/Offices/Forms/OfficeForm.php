<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Offices\Forms;

use Mindy\Base\Mindy;
use Mindy\Form\Fields\CharField;
use Mindy\Form\Fields\MapField;
use Mindy\Form\ModelForm;
use Modules\Files\Fields\FilesField;
use Modules\Offices\OfficesModule;

class OfficeForm extends ModelForm
{
    public function getFieldsets()
    {
        return [
            OfficesModule::t('Main information') => [
                'name', 'phone', 'site',
            ],
            OfficesModule::t('Additional information') => [
                'path', 'worktime',
            ],
            OfficesModule::t('Coordinates') => [
                'address', 'lat', 'lng',
            ],
            OfficesModule::t('Images') => [
                'images',
            ],
        ];
    }

    public function getFields()
    {
        return [
            'address' => [
                'class' => MapField::className(),
                'center' => [58.58735796286441, 49.6560005429687],
                'required' => true,
            ],
            'lat' => [
                'class' => CharField::className(),
                'html' => [
                    'readonly' => true,
                ],
            ],
            'lng' => [
                'class' => CharField::className(),
                'html' => [
                    'readonly' => true,
                ],
            ],
            'images' => [
                'class' => FilesField::className(),
                'relatedFileField' => 'image',
            ],
        ];
    }

    public function getModel()
    {
        $modelClass = Mindy::app()->getModule('Offices')->modelClass;

        return new $modelClass();
    }

//    public function getInlines()
//    {
//        if (Mindy::app()->getModule('Offices')->enableImages && !$this->getInstance()->isNewRecord) {
//            return [
//                ['office' => ImageInlineForm::className()]
//            ];
//        };
//        return [];
//    }
}
