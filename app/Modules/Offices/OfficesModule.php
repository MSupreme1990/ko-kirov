<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Offices;

use Mindy\Base\Mindy;
use Mindy\Base\Module;
use Modules\Offices\Models\Office;

class OfficesModule extends Module
{
    public $modelClass = 'Modules\Offices\Models\Office';
    public $formClass = 'Modules\Offices\Forms\OfficeForm';

    public $enableImages = true;

    public $imageSizes = [
        'thumb' => [150, 230],
        'preview' => [300, 420],
    ];

    public function getMenu()
    {
        return [
            'name' => $this->getName(),
            'items' => [
                [
                    'name' => $this->getName(),
                    'adminClass' => 'OfficeAdmin',
                    'icon' => 'icon-meta',
                ],
            ],
        ];
    }

    public static function preConfigure()
    {
        $tpl = Mindy::app()->template;
        $tpl->addHelper('get_offices', function () {
            return Office::objects()->all();
        });
    }
}
