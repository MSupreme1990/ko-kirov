<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Offices\Sitemap;

use Modules\Offices\OfficesModule;
use Modules\Sitemap\Components\Sitemap;

class OfficesSitemap extends Sitemap
{
    public function getModelClass()
    {
        return null;
    }

    public function getQuerySet()
    {
        return null;
    }

    public function getLoc($data)
    {
        return $data['reversed'];
    }

    public function getExtraItems()
    {
        return [
            [
                'reversed' => $this->reverse('offices.offices'),
                'name' => OfficesModule::t('Contacts'),
            ],
        ];
    }
}
