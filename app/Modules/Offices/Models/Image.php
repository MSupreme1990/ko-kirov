<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Offices\Models;

use Mindy\Base\Mindy;
use Mindy\Orm\Fields\BooleanField;
use Mindy\Orm\Fields\ForeignField;
use Mindy\Orm\Fields\ImageField;
use Mindy\Orm\Model;
use Modules\Core\Fields\Orm\PositionField;
use Modules\Offices\OfficesModule;

class Image extends Model
{
    public static function getFields()
    {
        $sizes = Mindy::app()->getModule('Offices')->imageSizes;
        $modelClass = Mindy::app()->getModule('Offices')->modelClass;

        return [
            'image' => [
                'class' => ImageField::className(),
                'sizes' => $sizes,
                'verboseName' => OfficesModule::t('Image'),
            ],
            'office' => [
                'class' => ForeignField::className(),
                'modelClass' => $modelClass,
                'verboseName' => OfficesModule::t('Office'),
            ],
            'position' => [
                'class' => PositionField::className(),
                'default' => 0,
                'verboseName' => OfficesModule::t('Position'),
            ],
            'is_main' => [
                'class' => BooleanField::className(),
                'verboseName' => OfficesModule::t('Is main'),
            ],
        ];
    }

    /**
     * @param $owner Model
     * @param $isNew
     */
    public function beforeSave($owner, $isNew)
    {
        if ($this->is_main) {
            $qs = $this->objects()->filter(['office' => $this->office]);
            if ($isNew == false) {
                $qs->exclude(['pk' => $this->pk]);
            }
            $qs->update(['is_main' => false]);
        }
    }
}
