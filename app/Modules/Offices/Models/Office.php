<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Offices\Models;

use Mindy\Orm\Fields\CharField;
use Mindy\Orm\Fields\FloatField;
use Mindy\Orm\Fields\ForeignField;
use Mindy\Orm\Fields\HasManyField;
use Mindy\Orm\Fields\TextField;
use Mindy\Orm\Model;
use Modules\Offices\OfficesModule;
use Modules\Sites\Models\Site;

class Office extends Model
{
    public static function getFields()
    {
        return [
            'name' => [
                'class' => CharField::className(),
                'verboseName' => OfficesModule::t('Name'),
                'required' => true,
            ],
            'address' => [
                'class' => CharField::className(),
                'verboseName' => OfficesModule::t('Address'),
                'required' => true,
            ],
            'path' => [
                'class' => TextField::className(),
                'verboseName' => OfficesModule::t('Path'),
                'null' => true,
            ],
            'phone' => [
                'class' => CharField::className(),
                'null' => true,
                'verboseName' => OfficesModule::t('Phone'),
            ],
            'worktime' => [
                'class' => TextField::className(),
                'verboseName' => OfficesModule::t('Work time'),
                'null' => true,
            ],
            'lat' => [
                'class' => FloatField::className(),
                'verboseName' => OfficesModule::t('Latitude'),
                'null' => true,
            ],
            'lng' => [
                'class' => FloatField::className(),
                'verboseName' => OfficesModule::t('Longitude'),
                'null' => true,
            ],
            'images' => [
                'class' => HasManyField::className(),
                'modelClass' => Image::className(),
                'to' => 'office_id',
                'verboseName' => OfficesModule::t('Images'),
            ],
            'site' => [
                'class' => ForeignField::className(),
                'modelClass' => Site::className(),
                'verboseName' => OfficesModule::t('Site'),
            ],
        ];
    }

    public function __toString()
    {
        return (string) $this->name;
    }

    public function getImage()
    {
        return $this->images->limit(1)->offset(0)->get();
    }
}
