<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Offices\Admin;

use Mindy\Base\Mindy;
use Modules\Admin\Components\ModelAdmin;
use Modules\Offices\OfficesModule;

class OfficeAdmin extends ModelAdmin
{
    public function getColumns()
    {
        return ['name', 'address'];
    }

    public function getCreateForm()
    {
        return Mindy::app()->getModule('Offices')->formClass;
    }

    /**
     * @return \Mindy\Orm\Model
     */
    public function getModel()
    {
        $modelClass = Mindy::app()->getModule('Offices')->modelClass;

        return new $modelClass();
    }

    public function getNames($model = null)
    {
        return [
            OfficesModule::t('offices'),
            OfficesModule::t('Create office'),
            OfficesModule::t('Update office'),
        ];
    }
}
