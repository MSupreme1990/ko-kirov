<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Offices\Controllers;

use Mindy\Base\Mindy;
use Modules\Core\Controllers\ApiBaseController;
use Modules\Offices\Models\Office;
use Modules\Sites\Models\Site;

class ApiController extends ApiBaseController
{
    public function actionIndex()
    {
        $siteId = (int) $this->getRequest()->get->get('site_id');
        if (empty($siteId)) {
            /*
            echo $this->json([
                'error' => true,
                'message' => "Site not found"
            ]);
            $this->end();
            */
        }

        $model = Site::objects()->get(['id' => $siteId]);
        if ($model === null) {
            $model = Site::objects()->get(['id' => 1]);
            /*
            echo $this->json([
                'error' => true,
                'message' => "Site not found"
            ]);
            $this->end();
            */
        }

        $siteModels = Office::objects()->filter([
            'site_id' => $model->id,
        ])->all();
        $objects = $this->iterateObjects($siteModels);

        $defaultModels = Office::objects()->filter([
            'site_id' => 1,
        ])->all();
        $defaultObjects = $this->iterateObjects($defaultModels);

        echo $this->json([
            'objects' => $objects,
            'defaultObjects' => $defaultObjects,
        ]);
        Mindy::app()->end();
    }

    protected function iterateObjects(array $models)
    {
        $data = [];
        foreach ($models as $model) {
            $images = [];
            foreach ($model->images->order(['position'])->all() as $image) {
                $images[] = $image->toArray();
            }
            $data[] = array_merge($model->toArray(), [
                'images' => $images,
            ]);
        }

        return $data;
    }
}
