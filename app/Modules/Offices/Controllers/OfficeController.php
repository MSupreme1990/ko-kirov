<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Modules\Offices\Controllers;

use Mindy\Base\Mindy;
use Modules\Core\Controllers\CoreController;

class OfficeController extends CoreController
{
    public function actionIndex()
    {
        $this->setBreadcrumbs([
            ['name' => 'Контакты'],
        ]);
        $modelClass = Mindy::app()->getModule('Offices')->modelClass;
        $models = $modelClass::objects()->all();
        echo $this->render('offices/offices.html', [
            'offices' => $models,
        ]);
    }
}
