<?php

/*
 * This file is part of Mindy Framework.
 * (c) 2017 Maxim Falaleev
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return  [
  'offices' => 'офисы',
  'Create office' => 'Добавить офис',
  'Update office' => 'Редактирование офиса',
  'Images' => 'Изображения / Фото',
  'Main information' => 'Основная информация',
  'Additional information' => 'Дополнительная информация',
  'Coordinates' => 'Координаты',
  'Image' => 'Изображение',
  'Office' => 'Офис',
  'Position' => 'Позиция',
  'Is main' => 'Главная',
  'Image {url} was created' => '',
  'Image {url} was updated' => '',
  'Image {url} was deleted' => '',
  'Name' => 'Наименование',
  'Address' => 'Адрес',
  'Path' => 'Как добраться?',
  'Phone' => 'Телефон',
  'Work time' => 'Рабочее время',
  'Latitude' => 'Широта',
  'Longitude' => 'Долгота',
  'Site' => 'Сайт',
  'Office {url} was created' => '',
  'Office {url} was updated' => '',
  'Office {url} was deleted' => '',
  'Contacts' => 'Контакты',
  'Email' => 'Email',
  'Skype' => 'Skype',
  'office' => 'офис',
  'Offices' => 'Офисы',
];
