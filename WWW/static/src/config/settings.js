const DEBUG = [
    'localhost',
    '192.168.0.108'
].indexOf(window.location.hostname) > -1;
 
export default {
    DEBUG,
    BASE_DOMAIN: 'http://ko-kirov.ru',
    BASE_URL: DEBUG ? '' : '',
    MEDIA_URL: 'http://ko-kirov.ru/media',
    API_VERSION: 1,
    VERBOSE: process.env.NODE_ENV !== 'production'
};
