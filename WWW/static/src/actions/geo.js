import c from '../constants';
import wrapper from 'wrapper/geo';

function receive(data) {
    return {
        type: c.GEO_RECEIVE,
        loading: false,
        received: true,
        ...data
    };
}

function loading(value) {
    return {
        type: c.GEO_LOADING,
        loading: value
    };
}

function shouldFetchList(state) {
    const { loading, received } = state.geo;

    if (loading) {
        return false;
    }

    if (received) {
        return false;
    }

    return true;
}

export function fetchList(callback = () => null) {
    return dispatch => {
        dispatch(loading(true));

        return wrapper.list().then(data => {

            let regions = {},
                current_cities = [];

            data.cities.map(region => {
                if (!current_cities) {
                    current_cities = region.cities;
                }
                regions[region.id] = region.cities;
            });

            dispatch(receive({
                ...data,
                current_cities: current_cities,
                regions: regions
            }));

            callback();
        });
    }
}

export function fetchListIfNeeded(callback = () => null) {
    return (dispatch, getState) => {
        if (shouldFetchList(getState())) {
            dispatch(fetchList(callback));
        }
    }
}

function currentCities(data) {
    return {
        type: c.GEO_CURRENT_CITIES,
        current_cities: data
    }
}

export function setCurrentCities(data) {
    return dispatch => {
        dispatch(currentCities(data));
    }
}
