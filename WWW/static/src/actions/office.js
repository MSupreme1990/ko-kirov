import c from '../constants';
import wrapper from 'wrapper/office';

function receive(data) {
    return {
        type: c.OFFICE_RECEIVE,
        loading: false,
        received: true,
        ...data
    }
}

function loading(value) {
    return {
        type: c.OFFICE_LOADING,
        loading: value
    }
}

function shouldFetchList(state, site_id) {
    const { loading, received } = state.recent;

    if (state.geo.site && site_id != state.geo.site.id) {
        return true;
    }

    if (loading) {
        return false;
    }
    return received ? false : true;
}

export function fetchListIfNeeded(site_id) {
    return (dispatch, getState) => {
        if (shouldFetchList(getState(), site_id)) {
            dispatch(loading(true));
            return wrapper
                .list({ site_id })
                .then(data => dispatch(receive(data)));
        }
    }
}

export function fetchList(site_id) {
    return dispatch => {
        dispatch(loading(true));
        wrapper
            .list({ site_id })
            .then(data => dispatch(receive(data)));
    }
}
