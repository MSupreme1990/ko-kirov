import c from '../constants';
import wrapper from 'wrapper/main';

function receive(data) {
    return {
        type: c.MAIN_RECEIVE,
        loading: false,
        ...data
    }
}

function loading(value) {
    return {
        type: c.MAIN_LOADING,
        loading: value
    }
}

function shouldFetchMain(state) {
    const { new_models, categories, popular, loading } = state.main;

    if (loading) {
        return false;
    }

    return new_models.length === 0 || categories.length === 0 || popular.length === 0;
}

export function fetchMainIfNeeded() {
    return (dispatch, getState) => {
        if (shouldFetchMain(getState())) {
            dispatch(loading(true));
            wrapper.main().then(data => dispatch(receive(data)));
        }
    }
}

export function fetchMain() {
    return dispatch => {
        dispatch(loading(true));
        wrapper.main().then(data => dispatch(receive(data)));
    }
}
