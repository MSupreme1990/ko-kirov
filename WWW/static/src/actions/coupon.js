import c from '../constants';
import wrapper from 'wrapper/coupon';

function viewInvalidate(code) {
    return {
        type: c.COUPON_VIEW_INVALIDATE,
        code
    }
}

function viewReceive(code, data) {
    return {
        type: c.COUPON_VIEW_RECEIVE,
        loading: false,
        error: false,
        code,
        coupon: data
    }
}

function viewLoading(value) {
    return {
        type: c.COUPON_VIEW_LOADING,
        loading: value
    }
}

function viewError(data) {
    return {
        type: c.COUPON_VIEW_ERROR,
        loading: false,
        error: true
    }
}

export function fetchView(code) {
    return dispatch => {
        dispatch(viewLoading(true));

        return wrapper.view(code).then(data => {
            dispatch(viewReceive(code, data))
        });
    };
}

export function shouldFetchView(state, code) {
    const { view } = state.coupon;
    if (code in view.view) {
        return false;
    }
    return view.loading === false;
}


export function fetchViewIfNeeded(code) {
    return (dispatch, getState) => {
        if (shouldFetchView(getState(), code)) {
            return dispatch(fetchView(code));
        }
    }
}
