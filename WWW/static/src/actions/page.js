import c from '../constants';
import wrapper from 'wrapper/page';

function viewInvalidate(url) {
    return {
        type: c.PAGE_VIEW_INVALIDATE,
        url
    }
}

function viewReceive(url, data) {
    return {
        type: c.PAGE_VIEW_RECEIVE,
        loading: false,
        error: false,
        url,
        ...data
    }
}

function viewLoading(value) {
    return {
        type: c.PAGE_VIEW_LOADING,
        loading: value
    }
}

function viewError(data) {
    return {
        type: c.PAGE_VIEW_ERROR,
        loading: false,
        error: true
    }
}

export function fetchView(url) {
    return dispatch => {
        dispatch(viewLoading(true));

        return wrapper.view(url)
            .then(data => dispatch(viewReceive(url, data)));
    };
}

export function shouldFetchView(state, url) {
    const { view } = state.page;
    if (url in view.view) {
        return false;
    }
    return view.loading === false;
}


export function fetchViewIfNeeded(url) {
    return (dispatch, getState) => {
        if (shouldFetchView(getState(), url)) {
            return dispatch(fetchView(url));
        }
    }
}
