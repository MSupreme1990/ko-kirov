import c from '../constants';
import { local } from 'easy-storage';

function addRecent(product) {
    return {
        type: c.RECENT_ADD,
        didInvalidate: true,
        product
    }
}

export function didInvalidate() {
    return (dispatch) => {
        return dispatch(invalidate());
    }
}

function invalidate() {
    return {
        type: c.RECENT_INVALIDATE,
        didInvalidate: true
    }
}

export function add(product) {
    return dispatch => {
        dispatch(addRecent(product));
    }
}