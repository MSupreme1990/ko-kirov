import c from '../constants';
import wrapper from 'wrapper/category';
import { local } from 'easy-storage';

function receive(data) {
    let newData = {
        loading: false,
        received: true,
        error: false,
        ...data
    };
    local.set('category', newData);

    return {
        type: c.CATEGORY_RECEIVE,
        ...newData
    };
}

function loading(value) {
    return {
        type: c.CATEGORY_LOADING,
        loading: value
    };
}

function shouldFetchList(state) {
    const { loading, received } = state.category;
    return !(loading || received);
}

export function fetchList() {
    return dispatch => {
        dispatch(loading(true));
        return wrapper.list().then(data => dispatch(receive(data)));
    }
}

export function fetchListIfNeeded() {
    return (dispatch, getState) => {
        if (shouldFetchList(getState())) {
            return dispatch(fetchList());
        }
    }
}
