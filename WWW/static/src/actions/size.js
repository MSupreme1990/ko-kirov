import c from '../constants';
import { local } from 'easy-storage';

export function setSize(size) {
    return dispatch => {
        local.set('size', {currentSize: size});
        dispatch({
            type: c.SIZE_SET,
            size
        });
    }
}