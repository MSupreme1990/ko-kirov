import c from '../constants';
import wrapper from 'wrapper/menu';

function receive(data) {
    return {
        type: c.MENU_RECEIVE,
        loading: false,
        menu: data,
        received: true,
        error: false
    };
}

function loading(value) {
    return {
        type: c.MENU_LOADING,
        loading: value
    };
}

function shouldFetchList(state) {
    const { loading, received } = state.menu;

    return !(loading || received);


}

export function fetchList() {
    return dispatch => {
        dispatch(loading(true));
        return wrapper.list().then(data => dispatch(receive(data)));
    }
}

export function fetchListIfNeeded() {
    return (dispatch, getState) => {
        if (shouldFetchList(getState())) {
            return dispatch(fetchList());
        }
    }
}
