import c from '../constants';
import { modalWindowLock, modalWindowUnlock } from 'lib/utils';

function modalClose() {
    return {
        type: c.MODAL_CLOSE
    };
}

function modalClear() {
    return {
        type: c.MODAL_CLEAR
    };
}

function modalOpen(component, style) {
    return {
        type: c.MODAL_OPEN,
        component,
        style,
        isVisible: true
    };
}

export function close() {
    return (dispatch, getState) => {
        const { queue } = getState().modal;
        if (queue.length == 0) {
            try {
                modalWindowUnlock();
            } catch(e) {}
        }
        return dispatch(modalClose());
    }
}

export function open(component, style = {}) {
    return (dispatch, getState) => {
        const { queue } = getState().modal;
        if (queue.length == 0) {
            try {
                modalWindowLock();
            } catch(e) {}
        }
        dispatch(modalOpen(component, style));
    }
}

export function clear(component, style = {}) {
    return dispatch => dispatch(modalClear());
}