import wrapper from '../wrapper/reviews';
import c from '../constants';

function receive(data) {
    return {
        type: c.REVIEWS_RECEIVE,
        loading: false,
        objects: data.reviews,
        video: data.video
    }
}

function loading(value) {
    return {
        type: c.REVIEWS_LOADING,
        loading: value
    };
}

export function fetchList(callback = () => null) {
    return dispatch => {
        dispatch(loading(true));
        return wrapper.list().then(data => {
            dispatch(receive(data));
            
            callback(data);
        });
    }
}

export function fetchListIfNeeded(callback = () => null) {
    return (dispatch, getState) => {
        if (shouldFetchList(getState())) {
            return dispatch(fetchList(callback));
        }
    }
}

function shouldFetchList(state) {
    return !state.reviews.loading;
}