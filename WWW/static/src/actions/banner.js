import c from '../constants';
import wrapper from 'wrapper/banner';
import { local } from 'easy-storage';

function receive(data) {
    return {
        type: c.BANNER_RECEIVE,
        loading: false,
        received: true,
        error: false,
        ...data
    };
}

function loading(value) {
    return {
        type: c.BANNER_LOADING,
        loading: value
    };
}

function shouldFetchList(state) {
    const { loading, received } = state.banner;
    return !(loading || received);
}

export function fetchList() {
    return dispatch => {
        dispatch(loading(true));
        return wrapper.list().then(data => {
            dispatch(receive(data));
        });
    }
}

export function fetchListIfNeeded() {
    return (dispatch, getState) => {
        if (shouldFetchList(getState())) {
            return dispatch(fetchList());
        }
    }
}
