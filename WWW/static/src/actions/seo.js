import c from '../constants';
import wrapper from 'wrapper/seo';

function viewReceive(url, data) {
    return {
        type: c.SEO_RECEIVE,
        loading: false,
        error: false,
        url,
        objects: data
    }
}

function loading(value) {
    return {
        type: c.SEO_LOADING,
        loading: value
    };
}

export function fetchView(url) {
    return dispatch => {
        dispatch(loading(true));

        return wrapper.view(url)
            .then(data => dispatch(viewReceive(url, data)));
    };
}

export function shouldFetchView(state, url) {
    const { loading, objects } = state.seo;
    if (url in objects) {
        return false;
    }
    return loading === false;
}


export function fetchViewIfNeeded(url) {
    return (dispatch, getState) => {
        if (shouldFetchView(getState(), url)) {
            return dispatch(fetchView(url));
        }
    }
}
