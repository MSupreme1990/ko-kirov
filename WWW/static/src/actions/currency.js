import c from '../constants';
import wrapper from 'wrapper/currency';
import { local } from 'easy-storage';

function receive(data) {
    let newData = {
        type: c.CURRENCY_RECEIVE,
        loading: false,
        received: true,
        objects: data,
        currency: data['RUB'],
        symbol: 'RUB'
    };
    local.set('currency', newData);
    return newData;
}

function loading(value) {
    return {
        type: c.CURRENCY_LOADING,
        loading: value
    };
}

function shouldFetchList(state) {
    const { loading, received } = state.currency;
    return !(loading || received);

}

export function fetchList() {
    return dispatch => {
        dispatch(loading(true));
        return wrapper.list().then(data => dispatch(receive(data)));
    }
}

export function fetchListIfNeeded() {
    return (dispatch, getState) => {
        if (shouldFetchList(getState())) {
            return dispatch(fetchList());
        }
    }
}

function setCurrentSymbol(symbol) {
    return {
        type: c.CURRENCY_SET,
        symbol: symbol
    }
}

export function setSymbol(symbol) {
    return dispatch => dispatch(setCurrentSymbol(symbol));
}
