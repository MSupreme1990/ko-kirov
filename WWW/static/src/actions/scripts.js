import c from '../constants';
import loader from 'lib/loader';

function script(script, loading) {
    return {
        type: c.SCRIPTS_LOADING,
        script,
        loading
    };
}

export function loadVk(callback = () => null) {
    const name = 'vk';

    return (dispatch, getState) => {
        const loaded = getState().scripts[name];

        if (loaded === false) {
            dispatch(script(name, true));

            if (typeof VK == "undefined") {
                loader.loadFile(document, {
                    src: '//vk.com/js/api/openapi.js?116',
                    tag: "script",
                    type: "text/javascript"
                }, callback);
            }
        } else {
            callback();
        }
    }
}

export function loadYandex(callback = () => null) {
    const name = 'yandex';

    return (dispatch, getState) => {
        const loaded = getState().scripts[name];

        if (loaded === false) {
            dispatch(script(name, true));

            if (typeof ymaps == "undefined") {
                loader.loadFile(document, {
                    src: '//api-maps.yandex.ru/2.1/?lang=ru_RU',
                    tag: "script",
                    type: "text/javascript"
                }, callback);
            }
        } else {
            callback();
        }
    }
}
