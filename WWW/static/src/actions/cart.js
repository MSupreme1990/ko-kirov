import c from '../constants';
import wrapper from 'wrapper/cart';
import $ from 'jquery';

function addLoading(value) {
    return {
        type: c.CARD_ADD_LOADING,
        loading: value
    }
}

function receive(data) {
    return {
        type: c.CART_RECEIVE,
        loading: false,
        total: data.total,
        objects: data.items
    }
}

function shouldFetchList(state) {
    const { received, loading, didInvalidate } = state.cart;

    if (!received) {
        return true;
    }

    if (loading) {
        return false;
    }

    if (!didInvalidate) {
        return false;
    }

    return true;
}

export function fetchListIfNeeded() {
    return (dispatch, getState) => {
        if (shouldFetchList(getState())) {
            dispatch(fetchList());
        }
    }
}

export function fetchList() {
    return dispatch => {
        dispatch(loading(true));
        return wrapper.list().then(data => {
            dispatch(receive(data));
        })
    };
}

function add(id) {
    return {
        type: c.CART_ADD,
        id
    };
}

function invalidate() {
    return {
        type: c.CART_INVALIDATE,
        didInvalidate: true
    }
}

export function addToCart(id, callback = () => null) {
    return (dispatch, getState) => {
        dispatch(addLoading(true));
        dispatch(invalidate());

        const { currentSize } = getState().sizes;

        return wrapper
            .add(id, currentSize)
            .then(data => {
                dispatch(add(id));
                dispatch(fetchList());

                $.mnotify({
                    ...data.message,
                    title: "Товар добавлен в корзину",
                    text: "<a href='/cart'>Перейти в козину →</a>",
                    delay: 5000,
                    // nonblock: {
                    //     nonblock: true,
                    //     nonblock_opacity: 1
                    // }
                });

                if (callback) {
                    callback();
                }
            });
    }
}

function loading(value) {
    return {
        type: c.CART_LOADING,
        loading: value
    }
}

export function changeSize(id, size, index) {
    return dispatch => {
        dispatch(loading(true));
        return wrapper.changeSize(id, size, index).then(data => {
            if (data.status) {
                $.mnotify(data.message);
            } else {
                if (typeof data.error != 'undefined') {
                    $.mnotify(data.error);
                }
            }

            dispatch(fetchList());
        });
    }
}

export function removeByIndex(index) {
    return dispatch => {
        return wrapper.removeByIndex(index).then(data => {
            dispatch(fetchList());
        });
    }
}

export function moveToDefferedByIndex(index) {
    return (dispatch, getState) => {
        const state = getState();
        const { objects } = state.cart;

        let product = objects[index];

        wrapper.removeByIndex(index).then(data => {
            $.mnotify(data.message);
            dispatch(addToDeffered(product));
            dispatch(fetchList());
        });
    }
}

export function removeDefferedByIndex(index) {
    return dispatch => dispatch(removeDeffered(index));
}

function removeDeffered(index) {
    return {
        type: c.CART_DEFFERED_REMOVE,
        index
    };
}

function addToDeffered(product) {
    return {
        type: c.CART_DEFFERED_ADD,
        product
    };
}