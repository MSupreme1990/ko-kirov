import c from '../constants';
import wrapper from 'wrapper/product';
import * as recentActions from 'actions/recent';

function viewInvalidate(url) {
    return {
        type: c.PRODUCT_VIEW_INVALIDATE,
        url
    }
}

function viewReceive(url, data) {
    return {
        type: c.PRODUCT_VIEW_RECEIVE,
        loading: false,
        error: false,
        url,
        ...data
    }
}

function viewLoading(value) {
    return {
        type: c.PRODUCT_VIEW_LOADING,
        loading: value
    }
}

function viewError(data) {
    return {
        type: c.PRODUCT_VIEW_ERROR,
        loading: false,
        error: true
    }
}

export function fetchView(url) {
    return dispatch => {
        dispatch(viewLoading(true));

        return wrapper.view(url)
            .then(data => {
                dispatch(recentActions.add(data.product));
                dispatch(viewReceive(url, data))
            });
    };
}

export function shouldFetchView(state, url) {
    const { view } = state.product;
    if (url in view.view) {
        return false;
    }
    return view.loading === false;
}


export function fetchViewIfNeeded(url) {
    return (dispatch, getState) => {
        if (shouldFetchView(getState(), url)) {
            return dispatch(fetchView(url));
        }
    }
}

function shouldFetchRelated(state, url) {
    const { loading, related } = state.product.related;
    return !(loading || url in related);

}

export function fetchRelatedIfNeeded(url) {
    return (dispatch, getState) => {
        if (shouldFetchRelated(getState(), url)) {
            return dispatch(fetchRelated(url));
        }
    }
}

function loadingRelated(value) {
    return {
        type: c.PRODUCT_RECEIVE_LOADING,
        loading: value
    }
}

function receiveRelated(url, data) {
    return {
        type: c.PRODUCT_RECEIVE_RELATED,
        objects: data.objects,
        loading: false,
        error: false,
        url
    }
}

export function fetchRelated(url) {
    return dispatch => {
        dispatch(loadingRelated(true));
        return wrapper.related(url).then(data => dispatch(receiveRelated(url, data)));
    };
}

export function fetchListIfNeeded(url, filters = {}) {
    return (dispatch, getState) => {
        if (shouldFetchList(getState())) {
            return dispatch(fetchList(url, filters));
        }
    }
}

function shouldFetchList(state) {
    const { loading, received } = state.product;
    if (loading) {
        return false;
    }
    // https://gitlab.studio107.ru/ko/ko-kirov/issues/49
    // if (received) {
    //     return false;
    // }
    return true;
}

function receive(data) {
    return {
        type: c.PRODUCT_RECEIVE,
        loading: false,
        received: true,
        ...data
    }
}

function loading(value) {
    return {
        type: c.PRODUCT_LOADING,
        loading: value
    }
}

export function fetchList(url, filters = {}) {
    return dispatch => {
        dispatch(loading(true));
        return wrapper.list(url, filters).then(data => dispatch(receive(data)));
    }
}

function autocompleteLoading(value) {
    return {
        type: c.PRODUCT_AUTOCOMPLETE_LOADING,
        loading: value
    };
}

function autocompleteReceive(data) {
    return {
        type: c.PRODUCT_AUTOCOMPLETE_RECEIVE,
        loading: false,
        ...data
    }
}

export function autocomplete(q) {
    return dispatch => {
        dispatch(autocompleteLoading(q));
        return wrapper.autocomplete(q).then(data => dispatch(autocompleteReceive(data)));
    }
}

function searchLoading(value) {
    return {
        type: c.PRODUCT_SEARCH_LOADING,
        loading: value
    };
}

function searchReceive(data) {
    return {
        type: c.PRODUCT_SEARCH_RECEIVE,
        loading: false,
        ...data
    }
}

export function search(q, filters = {}) {
    return dispatch => {
        dispatch(searchLoading(q));
        return wrapper.search(q, filters).then(data => dispatch(searchReceive(data)));
    }
}

export function fetchNew() {
    return dispatch => {
        dispatch(newLoading(true));
        return wrapper.new().then(data => dispatch(newReceive(data)));
    }
}

function newReceive(data) {
    return {
        type: c.PRODUCT_NEW_RECEIVE,
        loading: false,
        ...data
    }
}

function newLoading(value) {
    return {
        type: c.PRODUCT_NEW_LOADING,
        loading: value
    }
}

export function fetchPopular() {
    return dispatch => {
        dispatch(popularLoading(true));
        return wrapper.popular().then(data => dispatch(popularReceive(data)));
    }
}

function popularLoading(value) {
    return {
        type: c.PRODUCT_POPULAR_LOADING,
        loading: value
    }
}

function popularReceive(data) {
    return {
        type: c.PRODUCT_POPULAR_RECEIVE,
        loading: false,
        ...data
    }
}
