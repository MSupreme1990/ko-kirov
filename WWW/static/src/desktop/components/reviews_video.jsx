import React from 'react';
import api from 'lib/api';
import ReviewsVideo from 'shared/containers/reviews/reviews_video';

export default class ReviewsVideoComponent extends Component {
    state = {
        data: []
    };
 
    componentWillMount() {
        api.get("/api/ko/video_reviews", {}, data => this.setState({ data }));
    }

    render() {
        const { data } = this.state;

        var addReview = (
            <span>
                <i className="reviews-block-add-icon"/>
                Добавить отзыв
            </span>
        );

        return (
            <div className="reviews-block-container">
                <div className="row">
                    <div className="columns small-8">
                        <ModalLink name={addReview} linkClassName="reviews-block-add" className="padding">
                            1
                        </ModalLink>
                        <h3 className="reviews-block-video-title">Видеоотзывы наших покупателей</h3>
                        <ReviewsVideo reviews={data} count={3} />
                    </div>
                    <div className="columns small-2">
                        <h3>
                            Наши счастливые покупатели<br/>
                            Вконтакте
                        </h3>
                        <VkCommunity />
                    </div>
                </div>
            </div>
        );
    }
}
