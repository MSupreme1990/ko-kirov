import React, { PropTypes, Component } from 'react';
import Header from 'shared/components/header/header'
import HeaderRecent from 'shared/components/header/header_recent'
import Footer from 'shared/components/footer'
import { Ga, Ya, AdWords } from 'shared/components/analytics';
import Modal from 'shared/components/global_modal';
import ScrollToTop from 'react-scroll-up';

export default class App extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    getChildren() {
        return React.cloneElement(this.props.children, {

        });
    }

    render() {
        const { query } = this.props;

        const scrollTopStyle = {
            position: 'fixed',
            zIndex: 1000,
            bottom: 50,
            left: 30,
            right: 'auto',
            cursor: 'pointer',
            transitionDuration: '0.2s',
            transitionTimingFunction: 'linear',
            transitionDelay: '0s'
        };

        return (
            <div id="wrapper">
                <div id="container">
                    <div className="container__inner">
                        {/*<HeaderRecent />*/}
                        <Header q={query.q} />
                        {this.getChildren()}
                    </div>
                </div>

                <Ya id='32044066'/>
                <AdWords id='955403784'/>
                <Modal />
                <ScrollToTop style={scrollTopStyle} showUnder={160}>
                    <span className="totop-arrow">Наверх</span>
                </ScrollToTop>
                <Footer />
            </div>
        );
    }
}
