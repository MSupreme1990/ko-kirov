import React, { Component } from 'react';
import { nl2br } from 'lib/utils';
import { Link } from 'react-easy-router';
import OfficeList from 'shared/components/office_list';
import Geo from 'shared/components/geo';
import Features from 'shared/components/features';
import { SalesBackForm } from 'shared/components/product/categories_sales';
import { openModal } from 'lib/utils';
import Feedback from 'shared/components/feedback';
import Loader from 'shared/components/loader.jsx';
import Page from 'shared/components/page.jsx';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as officeActions from 'actions/office';
import * as pageActions from 'actions/page';
import * as geoActions from 'actions/geo';

const URL = 'rekvizity';

class ContactsHandler extends Component {
    componentWillMount() {
        const { pageActions, geoActions } = this.props;
        geoActions.fetchList();
        pageActions.fetchViewIfNeeded(URL);
    }

    handlePrintClick(e) {
        e.preventDefault();
        window.print();
    }

    renderPage() {
        const { page, loading, error } = this.props;

        if (error) {
            return null;
        }

        if (loading || !page) {
            return <Loader />;
        }

        return <Page h1={false} catalog_link={false} page={page}/>;
    }

    renderHeader() {
        const { city } = this.props.geo;

        let cityName = city.name2 ? city.name2 : city.name;

        if (city.id == 1) {
            return (
                <h1>
                    Магазин в г. <a onClick={openModal.bind(this, <Geo />)}>{cityName}</a>
                </h1>
            );
        } else {
            return (
                <h1>
                    Пункт выдачи в г. <a href="#" onClick={openModal.bind(this, <Geo />)}>{cityName}</a>
                    (для заказа в интернет-магазине)
                </h1>
            );
        }
    }

    render() {
        const {
            geo
        } = this.props;

        return (
            <div>
                <div className="row">
                    <div className="columns medium-12 large-12">
                        <Features />
                    </div>
                </div>
                <div className="row contacts-container">
                    <div className="columns medium-12 large-12">
                        <div className="contacts-links">
                            <a href="#" onClick={openModal.bind(this, <Feedback />)}>Связаться с администрацией</a>
                            <a href="#" onClick={this.handlePrintClick.bind(this)}>Версия для печати</a>
                        </div>
                        {this.renderHeader()}

                        <div className="contacts-intro-container">
                            <div className="contacts-intro">
                                График работы менеджеров интернет магазина «Кожаная одежда» (по МСК): ежедневно с 9:00
                                до 22:00. Заявку на сайте Вы можете оставить круглосуточно, указав удобное для Вас время для подтверждения заказа.
                            </div>
                        </div>
                    </div>
                </div>
                {!geo.loading && <OfficeList site_id={geo.site.id} />}
                {this.renderPage()}
            </div>
        );
    }
}

export default connect((state, props) => {
    const { view, loading, error } = state.page.view;
    return {
        loading,
        error,
        page: view[URL],
        ...state.office,
        geo: state.geo
    };
}, dispatch => {
    return {
        pageActions: bindActionCreators(pageActions, dispatch),
        geoActions: bindActionCreators(geoActions, dispatch),
        officeActions: bindActionCreators(officeActions, dispatch)
    };
})(ContactsHandler);
