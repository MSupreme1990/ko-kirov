import React, { Component } from 'react';
import { Link } from 'react-easy-router';
import { AdWords_send, dataLayer_push } from 'lib/utils';
import QuickOrder from 'shared/components/quick_order';
import Breadcrumbs from 'shared/components/breadcrumbs';
import Loader from 'shared/components/loader.jsx';
import NotFoundPage from 'desktop/pages/404.jsx';
import ProductQuickImageSlider from 'shared/components/product/product_quick_image_slider';
import Features from 'shared/components/features';
import { ModalLink } from 'shared/components/modal';
import DetectSize from 'shared/components/detect_size';
import RelatedSlider from 'shared/components/product/related_slider';
import ProductGift from 'shared/components/product/product_gift';
import ReviewsFull from 'shared/components/reviews_full';
import Best from 'shared/components/best';
import Reviews from 'shared/components/reviews';
import Question from 'shared/components/question';
import Sizes from 'shared/components/sizes';
import RecentShow from 'shared/components/product/recent_show';
import GiftCounter from 'shared/components/gift_counter';
import SeoText from 'shared/components/seo_text';
import { openModal, fixRelativeMediaUrls } from 'lib/utils';
import {
    OriginalSocialLikes,
    SocialLikes
} from 'shared/components/social_likes';
import currency from 'shared/components/currency';
import QuickOrderButton from 'inputs/quick_order_button'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PayLate from 'shared/components/PayLate';
import * as recentActions from 'actions/recent';
import * as productActions from 'actions/product';
import * as cartActions from 'actions/cart';

class CatalogProductHandler extends Component {
    componentWillMount() {
        const { params, productActions } = this.props;
        productActions.fetchViewIfNeeded(params.url);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.params.url != nextProps.params.url) {
            const { productActions } = nextProps;
            productActions.fetchViewIfNeeded(nextProps.params.url);
        }
    }

    handleAddToCart(e) {
        e.preventDefault();
        dataLayer_push({ 'event': 'cartadd', 'type': 'productPage' })
    }

    render() {
        const { symbol, loading, error, product } = this.props;

        if (error) {
            return <NotFoundPage />;
        }

        if (loading || !product) {
            return <Loader />;
        }

        return (
            <div>
                <Breadcrumbs model={product}/>

                <div className="row">
                    <div className="product-detail-full">
                        <div className="columns medium-4">
                            <ProductQuickImageSlider
                                cls="product-full-images-slider"
                                prefix="slickfull"
                                product={product}/>
                            <div className="right">
                                <OriginalSocialLikes />
                            </div>
                            <div className="product-detail-share-container">
                                <p>Рассказать друзьям:</p>
                                <SocialLikes />
                            </div>
                        </div>
                        <div className="columns medium-8">
                            <h1>{product.name}</h1>

                            <p className="produt-detail-full-code">Модель № {product.code}</p>


                            <div className="row">
                                <div className="column small-7">
                                    <h4 className="product-detail-full-price">
                                        {currency.hasOldPrice(symbol, product) ? 'Цена по акции' : 'Цена'}: {currency.format(symbol, product)}
                                        {(() => {
                                            if (currency.hasOldPrice(symbol, product)) {
                                                return (
                                                    <span>
                                                <br/>
                                                <span className="product-detail-full-old-price">
                                                    {currency.format(symbol, product, true)}
                                                </span>
                                            </span>
                                                );
                                            }
                                        })()}
                                    </h4>
                                </div>
                                <div className="column small-5">
                                    <div className="b-input-group b-input-group_end">
                                        <PayLate
                                            style={{ marginRight: '5px',
                                                     width: '180px',
                                                     height: '34px',
                                                     borderRadius: '6px'
                                                   }}
                                            products={
                                                [{
                                                    quantity: 1,
                                                    price: product.price_rub || product.price_old_rub,
                                                    object: product
                                                }]
                                            }/>
                                        <QuickOrderButton
                                            className="gray float-right"
                                            product={product}
                                            onClick={this.handleAddToCart.bind(this)}/>
                                    </div>
                                </div>
                            </div>


                            <Features half="1"/>

                            <div>
                                <div className="product-detail-sizes">
                                    <p>Размеры в наличии:</p>
                                    <Sizes product={product} className="product-detail-sizes-list"/>
                                </div>
                                <p className="detect-size-link">
                                    <a href="#" onClick={openModal.bind(this, <DetectSize />, { padding: 0 })}>
                                        Определить свой размер
                                    </a>
                                </p>
                            </div>
                            <QuickOrder product_id={product.id}/>
                            {product.content ? <div
                                    dangerouslySetInnerHTML={{ __html: fixRelativeMediaUrls(product.content) }}></div> : null}
                        </div>
                    </div>
                    <div className="columns medium-12">
                        <hr className="product-detail-hr"/>
                        <div className="product-list-container slider-also">
                            <RelatedSlider product={product}/>
                        </div>
                    </div>
                </div>

                <ProductGift />
                <ReviewsFull title="1"/>
                <Best />
                <Reviews />
                <Question />
                <RecentShow />
                <GiftCounter />
                <SeoText />
            </div>
        );
    }
}

export default connect((state, props) => {
    const { loading, error, view } = state.product.view;
    return {
        symbol: state.currency.symbol,
        loading,
        error,
        product: view[props.params.url]
    };
}, dispatch => {
    return {
        cartActions: bindActionCreators(cartActions, dispatch),
        productActions: bindActionCreators(productActions, dispatch),
        recentActions: bindActionCreators(recentActions, dispatch)
    };
})(CatalogProductHandler);
