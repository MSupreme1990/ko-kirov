import React, { Component } from 'react';
import LazyLoad from 'react-lazy-load';
import Features from 'shared/components/features';
import Breadcrumbs from 'shared/components/breadcrumbs';
import GiftCounter from 'shared/components/gift_counter';
import Best from 'shared/components/best';
import Question from 'shared/components/question';
import Reviews from 'shared/components/reviews';
import SeoText from 'shared/components/seo_text';
import Page from 'shared/components/page.jsx';
import Loader from 'shared/components/loader.jsx';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as pageActions from 'actions/page';

class PagesViewHandler extends Component {
    componentWillMount() {
        const { actions, params } = this.props;
        actions.fetchViewIfNeeded(params.url);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.params.url != nextProps.params.url) {
            const { actions, params } = nextProps;
            actions.fetchViewIfNeeded(params.url);
        }
    }

    render() {
        const { loading, page } = this.props;

        if (loading || !page) {
            return <Loader />;
        }

        return (
            <div>
                <Breadcrumbs model={{ name: page.name }}/>

                <div className="row">
                    <div className="columns medium-12 large-12">
                        <Features />
                    </div>
                </div>

                <Page page={page}/>

                <LazyLoad height="84px">
                    <GiftCounter />
                </LazyLoad>
                <Best />
                <Question />
                <Reviews />
                <SeoText />
            </div>
        );
    }
}

export default connect((state, props) => {
    const { view, loading, error } = state.page.view;
    return {
        loading,
        error,
        page: view[props.params.url]
    }
}, dispatch => {
    return {
        actions: bindActionCreators(pageActions, dispatch)
    };
})(PagesViewHandler);