import React, { Component } from 'react';
import api from 'lib/api';
import { formValidate } from 'lib/utils';
import LazyLoad from 'react-lazy-load';
import { cookie } from 'easy-storage';
import { findDOMNode } from 'react-dom';
import Features from 'shared/components/features';
import SalesContent from 'shared/containers/sales_content';
import Question from 'shared/components/question';
import Reviews from 'shared/components/reviews';
import SeoText from 'shared/components/seo_text';
import PhoneInput from 'inputs/phone_input';
import BestSales from 'shared/components/best_sales';
import GiftCounterSales from 'shared/components/gift_counter_sales';
import { CategoriesSales } from 'shared/components/product/categories_sales';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as pageActions from 'actions/page';
import * as mainActions from 'actions/main';

class WholesalesHandler extends Component {
    state = {
        message: ''
    };

    componentWillMount() {
        const { mainActions } = this.props;
        mainActions.fetchMainIfNeeded();
    }

    handleSubmit(e) {
        e.preventDefault();
        e.stopPropagation();

        var $form = $(e.target);

        api.post('/api/ko/sales', $form.serialize(), function (data) {
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    message: data['message']
                });
            }
        }.bind(this));
    }

    handleNameChange(e) {
        cookie.set('name', e.target.value);
    }

    render() {
        const { categories } = this.props;

        let name = cookie.get('name');

        return (
            <div>
                <div className="row">
                    <div className="columns medium-12 large-12">
                        <Features />
                    </div>
                </div>

                <div className="page-container">
                    <div className="row">
                        <div className="columns medium-12 large-12">
                            <h1>Элитные меховые изделия оптом от производителя</h1>

                            <div className="row page-extra">
                                <div className="columns medium-8">
                                    <div className="page-extra-container-sales">
                                        <div className="page-extra-sales">
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>
                                <div className="columns medium-4">
                                    <div className="page-extra-sidebar-container">
                                        <div className="page-extra-sidebar sales">
                                            <p className="page-extra-title">Преимущества</p>
                                            <ul className="page-extra-list sales">
                                                <li className="page-extra-list-clothes">
                                                    Опт от 3 изделий
                                                </li>
                                                <li className="page-extra-list-prepare">
                                                    Пошив по индивидуальным заказам любых размеров
                                                </li>
                                                <li className="page-extra-list-5000">
                                                    Покупайте не размерными рядами
                                                </li>
                                                <li className="page-extra-list-300">
                                                    Вся наша продукция промаркирована RFID-чипами
                                                </li>
                                                <li className="page-extra-list-auto">
                                                    Бесплатная упаковка и доставка до терминала транспортной компании
                                                </li>
                                            </ul>
                                            <p className="arrow sales"></p>
                                            <form onSubmit={this.handleSubmit.bind(this)} id="sale-form">
                                                <h1 className={this.state.message == '' ? 'hide' : ''}>{this.state.message}</h1>
                                                <div className={this.state.message == '' ? '' : 'hide'}>
                                                    <div className="row">
                                                        <div className="columns medium-12 large-12">
                                                            <p className="form-title">Получите доступ к оптовым
                                                                ценам</p>
                                                            <p className="align center form-small">
                                                                + подарок — систему «Увеличение прибыли
                                                                магазина меховых изделий в 2,5 раза»
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="columns medium-6">
                                                            <div className="sale-form-name">
                                                                <input type="text" name="SalesForm[name]"
                                                                       onChange={this.handleNameChange.bind(this)}
                                                                       defaultValue={name}
                                                                       placeholder="Введите ваше имя"/>
                                                                <ul className="errors"></ul>
                                                            </div>
                                                        </div>
                                                        <div className="columns medium-6">
                                                            <div className="sale-form-phone">
                                                                <PhoneInput name="SalesForm[phone]"/>
                                                                <ul className="errors"></ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="columns medium-12 large-12">
                                                            <div className="sale-form-submit">
                                                                <input type="submit" value="ПОЛУЧИТЬ"
                                                                       className="button"/>
                                                            </div>
                                                            <div className="b-personal-data">
                                                                <span className="b-personal-data__check"><input type="checkbox" checked="checked"/></span>
                                                                Передавая информацию сайту вы принимаете условия
                                                                <a className="b-personal-data__link"
                                                                   href="/page/private"
                                                                   target="_blank">"политики защиты персональных данных".</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <BestSales />

                <GiftCounterSales />

                <div className="row">
                    <div className="columns medium-12 large-12">
                        <div className="product-list-container">
                            <h3 className="text-center category-list-container-title">Наш ассортимент</h3>
                            <CategoriesSales models={categories}/>
                        </div>
                    </div>
                </div>

                <GiftCounterSales />

                <div className="row">
                    <SalesContent />
                </div>

                <LazyLoad height="84px">
                    <GiftCounterSales />
                </LazyLoad>

                <div className="row">
                    <div className="columns medium-12 large-12 work-container">
                        <h2>С кем мы хотим работать</h2>

                        <ul className="small-block-grid-3 work-list">
                            <li className="work-list-icon-sack">
                                <p className="title">Владельцы розничных магазинов меха и кожи</p>
                                <p>Вы сможете закупать уникальный и разнообразный
                                    ассортимент в одном месте.</p>
                            </li>
                            <li className="work-list-icon-shop">
                                <p className="title">Бизнесы смежных тематик</p>
                                <p>Выездная торговля, магазины одежды, модные бутики,
                                    свадебные салоны.</p>
                            </li>
                            <li className="work-list-icon-people">
                                <p className="title">Совместные закупки</p>
                                <p>Имеем большой опыт работы с организаторами
                                    совместных закупок, бронируем все нужные изделия.</p>
                            </li>
                        </ul>
                    </div>
                </div>

                <div className="how-work-container">
                    <div className="row">
                        <div className="columns medium-12 large-12">
                            <h2>Схема нашей работы</h2>
                            <ul className="medium-block-grid-2 large-block-grid-4 how-work-list">
                                <li className="img1">
                                    <p className="text">
                                        Вы оставляете бесплатную заявку
                                    </p>
                                </li>
                                <li className="img2">
                                    <p className="text">
                                        Мы высылаем Вам на почту оптовый каталог
                                        и полезную маркетинговую информацию
                                    </p>
                                </li>
                                <li className="img3">
                                    <p className="text">
                                        Выделяем Вам персонального менеджера,
                                        который консультирует Вас по всем вопросам
                                    </p>
                                </li>
                                <li className="img4">
                                    <p className="text">
                                        Вы оформляете заказ. Менеджер помогает
                                        Вам сформировать список
                                        необходимого товара.
                                    </p>
                                </li>
                                <li className="img5">
                                    <p className="text">
                                        Выставление счета. Оплата через банк, р/с.
                                    </p>
                                </li>
                                <li className="img6">
                                    <p className="text">
                                        Бесплатная упаковка и доставка до терминала транспортной компании, что позволяет экономить на каждой отправке до 1000 руб.
                                    </p>
                                </li>
                                <li className="img7">
                                    <p className="text">
                                        Доведение Вас до продажи всего
                                        закупленного товара. Получениет прибыли
                                    </p>
                                </li>
                                <li className="img8">
                                    <p className="text">
                                        Повторное обращение. Вы в статусе
                                        «постоянный партнер». Предоставление скидок, бонусов.
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="columns small-12">
                        <div className="conditions-container">
                            <h2>Наши первоначальные условия:</h2>
                            <ul className="small-block-grid-2 money-list">
                                <li>
                                    <p className="count">1</p>
                                    <p className="title">Минимальная партия от 3 изделий.</p>
                                </li>
                                <li>
                                    <p className="count">4</p>
                                    <p className="title">
                                        Доставка от терминала за счёт заказчика, любой транспортной компанией.
                                    </p>
                                </li>
                                <li>
                                    <p className="count">2</p>
                                    <p className="title">
                                        Продажа только с чипами в системе маркировка, предоплата 100% по безналичному расчёту.
                                    </p>
                                </li>
                                <li>
                                    <p className="count">3</p>
                                    <p className="title">
                                        Бесплатная упаковка и доставка до терминала транспортной компании.
                                    </p>
                                </li>
                            </ul>

                            <h2 className="second">Давайте вместе добиваться успеха!</h2>
                            <p className="deco-text">
                                Мы настроены исключительно на долгосрочное сотрудничество, поэтому для нас очень важно,
                                чтобы Вы всегда оставались довольны качеством и сервисом, и заказывали у нас снова и
                                снова.
                            </p>
                        </div>
                    </div>
                </div>

                <Question />
                <Reviews />
                <SeoText />
            </div>
        );
    }
}

export default connect((state, props) => {
    return {
        ...state.main
    }
}, dispatch => {
    return {
        mainActions: bindActionCreators(mainActions, dispatch),
        actions: bindActionCreators(pageActions, dispatch)
    };
})(WholesalesHandler);
