import React, { Component } from 'react';
import { openModal } from 'lib/utils';
import New from 'shared/components/product/new';
import Popular from 'shared/components/product/popular';

export default class TestHandler extends Component {
    render() {
        return (
            <div>
                <New />
                <Popular />
            </div>
        );
    }
}