import React, { Component } from 'react';
import $ from 'jquery';
import api from 'lib/api';
import Loader from 'shared/components/loader';
import { findDOMNode } from 'react-dom';
import { formValidate, fixRelativeMediaUrls } from 'lib/utils';
import Features from 'shared/components/features';
import PhoneInput from 'inputs/phone_input';
import CatalogLink from 'shared/components/catalog_link';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as pageActions from 'actions/page';
import Question from 'shared/components/question';
import Reviews from 'shared/components/reviews';
import SeoText from 'shared/components/seo_text';
import { cookie } from 'easy-storage';

const URL = 'credit';

class CreditHandler extends Component {
    state = {
        message: ''
    };

    componentWillMount() {
        const { actions } = this.props;
        actions.fetchViewIfNeeded(URL);
    }

    handleSubmit(e) {
        e.preventDefault();
        e.stopPropagation();

        var $form = $('#credit-form');

        api.post('/api/ko/credit_possible', $form.serialize(), data => {
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    message: data['message']
                });
            }
        });
    }

    handleNameChange(e) {
        cookie.set('name', e.target.value);
    }

    render() {
        const { page, loading } = this.props;

        if (loading || !page) {
            return <Loader />;
        }

        var name = cookie.get('name');
        return (
            <div className="page-credit">
                <div className="row">
                    <div className="columns medium-12 large-12">
                        <Features />
                    </div>
                </div>

                <div className="page-container">
                    <div className="row">
                        <div className="columns medium-12 large-12">
                            <h1>{page.name}</h1>

                            <div className="row page-extra">
                                <div className="columns medium-8">
                                    <div className="page-extra-container-girl">
                                        <div className="page-extra-girl">
                                            <div className="page-extra-text credit">
                                                <div className="page-extra-top-text">
                                                    Вам понравилась эффектная шубка,<br/>
                                                    Но не рассчитывали на покупку<br/>
                                                    сейчас? Не проблема!
                                                </div>

                                                <div className="page-extra-bottom-text">
                                                    Можно оформить кредит!
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="columns medium-4">
                                    <div className="page-extra-sidebar-container">
                                        <div className="page-extra-sidebar">
                                            <p className="page-extra-title">Достоинства онлайн-кредитования</p>
                                            <ul className="page-extra-list credit">
                                                <li className="page-extra-list-mouse">
                                                <span className="multiline">
                                                    Для оформления покупки и кредита Вам не нужно выходить из дома
                                                </span>
                                                </li>
                                                <li className="page-extra-list-time">
                                                <span className="multiline">
                                                    В 95% случаев решения по онлайн кредиту принимается за 2 минуты
                                                </span>
                                                </li>
                                                <li className="page-extra-list-zero">
                                                <span className="multiline">
                                                    0% по кредиту до даты первого платежа
                                                </span>
                                                </li>
                                                <li className="page-extra-list-calendar">
                                                <span className="multiline">
                                                    Бесплатное досрочное погашение
                                                </span>
                                                </li>
                                            </ul>
                                            <p className="arrow credit"></p>
                                            <form onSubmit={this.handleSubmit.bind(this)} id="credit-form">
                                                <h1 className={this.state.message == '' ? 'hide' : ''}>{this.state.message}</h1>
                                                <div className={this.state.message == '' ? '' : 'hide'}>
                                                    <div className="row">
                                                        <div className="columns medium-12">
                                                            <p className="form-title">
                                                                Рассчитать возможность оформления кредита:
                                                            </p>
                                                        </div>
                                                        <div className="columns medium-6">
                                                            <div className="credit-form-name">
                                                                <input type="text" name="CreditPossibleForm[name]" onChange={this.handleNameChange.bind(this)}
                                                                       defaultValue={name} placeholder="Введите ваше имя" />
                                                                <ul className="errors"></ul>
                                                            </div>
                                                        </div>
                                                        <div className="columns medium-6">
                                                            <div className="credit-form-phone">
                                                                <PhoneInput name="CreditPossibleForm[phone]" />
                                                                <ul className="errors"></ul>
                                                            </div>
                                                        </div>
                                                        <div className="columns medium-12 large-12">
                                                            <div className="credit-form-submit">
                                                                <input type="submit" value="РАССЧИТАТЬ" className="button" />
                                                            </div>
                                                        </div>
                                                        <div className="columns medium-12">
                                                            <div className="b-personal-data">
                                                            <span className="b-personal-data__check">
                                                                <input
                                                                    className="b-personal-data__input" type="checkbox"
                                                                    checked="checked"/>
                                                            </span>
                                                                Нажимая кнопку «Получить консультацию», вы принимаете
                                                                <a className="b-personal-data__link"
                                                                   href="/page/private"
                                                                   target="_blank">"условия обработки персональных
                                                                    данных".</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div dangerouslySetInnerHTML={{__html: fixRelativeMediaUrls(page.content)}}></div>

                            <CatalogLink />
                        </div>
                    </div>
                </div>

                <Question />
                <Reviews />
                <SeoText />
            </div>
        );
    }
}

export default connect((state, props) => {
    const { view, loading, error } = state.page.view;
    return {
        loading,
        error,
        page: view[URL]
    }
}, dispatch => {
    return {
        actions: bindActionCreators(pageActions, dispatch)
    };
})(CreditHandler);
