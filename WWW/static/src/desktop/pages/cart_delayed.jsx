import React, { Component, PropTypes } from 'react';
import * as cartActions from 'actions/cart';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Link } from 'react-easy-router';
import { dataLayer_push } from 'lib/utils';
import Features from 'shared/components/features';
import { GetCouponForm } from 'shared/components/forms';
import { ModalLink, ProductModalLink } from 'shared/components/modal';
import currency from 'shared/components/currency';
import { local } from 'easy-storage';

class CartDelayed extends Component {
    state = {
        loading: false,
        error: null,
        items: [],
        delay_items: local.get('delay_products', []),
        total: 0
    };

    loadCart(symbol = 'RUB') {
        this.setState({
            loading: true
        });

        api.get('/api/ko/cart/list/json', { symbol }, data => {
            data['loading'] = false;
            this.setState(data, ()=> {

                AdWords_send({
                    ecomm_pagetype: 'cart',
                    ecomm_totalvalue: this.state.total
                });
            });
        }, () => {
            this.setState({
                error: true,
                loading: false
            });
        });
    }

    renderDelayList() {
        const { deffered } = this.props;

        let header = deffered.length == 0 ? null : (
            <thead>
            <tr>
                <th className="cart-th-photo">Фото</th>
                <th className="cart-th-name">Наименование</th>
                <th className="cart-th-size">Размер</th>
                <th className="cart-th-price">Цена</th>
                <th className="cart-th-actions">Действия</th>
            </tr>
            </thead>
        );

        return (
            <div>
                <table className="cart-table">
                    {header}
                    <tbody>
                    {this.renderDelayListItems()}
                    </tbody>
                </table>
            </div>
        );
    }

    renderDelayListItems() {
        const { symbol, deffered } = this.props;

        return deffered.length == 0 ? (
            <tr>
                <td colSpan="5" className="cart-td-empty">
                    <p className="text-center">
                        Отложенные товары отсутствуют.
                    </p>

                    <p className="text-center">
                        <Link to="CatalogList" params={{url: 'woman'}}>Перейдите в каталог для покупок</Link>.
                    </p>
                </td>
            </tr>
        ) : deffered.map((item, i) => {
            let model = item.object,
                size = typeof item.data.size != "undefined" ? item.data.size : null;

            let price = currency.format(symbol, model, !currency.hasOldPrice(symbol, model));

            return (
                <tr key={'cart-tr-delay-' + model.id + '_' + i}>
                    <td>
                        <img
                            src={model.image.image.thumb}
                            alt={model.name}/>
                    </td>
                    <td>
                        <p className="cart-item-name">
                            <ProductModalLink
                                model={model}
                                router={this.context.router}
                                className="product-quick-look padding"
                                linkClassName="product-item-detail">

                                {model.name} <span className="cart-item-article">№ {model.code}</span>
                            </ProductModalLink>
                        </p>
                    </td>
                    <td>
                        {size}
                    </td>
                    <td>
                        {price}
                    </td>
                    <td className="cart-td-actions">
                        <p>
                            <a href="#" onClick={this.handleAddToCart.bind(this, model.id)}>Добавить в
                                корзину</a>
                        </p>

                        <p>
                            <a href="#" onClick={this.handleDeleteDelay.bind(this, model.id)}>Удалить</a>
                        </p>
                    </td>
                </tr>
            );
        });
    }

    handleAddToCart(id, e) {
        e.preventDefault();

        const { cartActions } = this.props;
        cartActions.addToCart(id);
    }

    handleDeleteDelay(index, e) {
        e.preventDefault();

        if (confirm('Вы действительно хотите удалить данный товар из отложенных товаров?')) {
            const { cartActions } = this.props;
            cartActions.removeDefferedByIndex(index);
        }
    }

    render() {
        const { deffered } = this.props;

        return (
            <div className="row">
                <div className="columns small-12">
                    <Features />

                    <div className="cart-container">
                        <ul className="cart-tab-list">
                            <li><Link to="Cart">Корзина</Link></li>
                            <li><Link to="CartDelayed" className="active">Отложенные товары ({deffered.length})</Link>
                            </li>
                        </ul>
                        {this.renderDelayList()}
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(state => {
    const { deffered, objects, loading } = state.cart;

    return {
        deffered,
        objects,
        loading,
        symbol: state.currency.symbol
    }
}, dispatch => {
    return {
        cartActions: bindActionCreators(cartActions, dispatch)
    };
})(CartDelayed);