import React from 'react';

import PhoneInput from 'inputs/phone_input';

var ReviewsVideoHandler = React.createClass({
    handleSubmit: function(e) {
        e.preventDefault();

        var $form = $('#review-form');

        api.post('/api/ko/reviews_create', $form.serialize(), function(data) {
            formValidate($form, data['errors']);
            if (data['success']) {
                $form.html('<h4>' + data['message'] + '</h4>');
            }
        }.bind(this));
    },
    render: function() {
        var enablePager = true;
        var tabActive = 2;
        return (
            <div className="reviews-page-container">
                <div className="row">
                    <div className="columns large-10">
                        <Features />
                        <p>&nbsp;</p>
                    </div>
                </div>
                <div className="row">
                    <div className="columns large-8">
                        <ul className="reviews-switch">
                            <li className="active">
                                <Link to="Reviews-video">Видеоотзывы</Link>
                            </li>
                            <li>
                                <Link to="Reviews">Текстовые отзывы</Link>
                            </li>
                        </ul>
                        <h1>Отзывы наших Покупателей</h1>

                        <ReviewsFullVideo />
                    </div>
                    <div className="columns large-2">
                        <div className="review-action">
                            <div className="review-action-img"></div>
                            <p>Оставьте свой отзыв и получите длинные кожаные перчатки в подарок при следующем заказе</p>
                        </div>
                        <form id="review-form" method="post" onSubmit={this.handleSubmit}>
                            <h4>Оставьте свой отзыв</h4>
                            <div className="review-form-row form-row">
                                <input type="text" name="ReviewCreateForm[name]" placeholder="Введите ваше имя" />
                                <ul className="errors"></ul>
                            </div>
                            <div className="review-form-row form-row">
                                <PhoneInput name="ReviewCreateForm[phone]" />
                                <ul className="errors"></ul>
                            </div>
                            <div className="review-form-row form-row">
                                <input type="text" name="ReviewCreateForm[email]" placeholder="Введите ваше e-mail" />
                                <ul className="errors"></ul>
                            </div>
                            <div className="review-form-row form-row">
                                <textarea name="ReviewCreateForm[content]" placeholder="Ваш отзыв"></textarea>
                                <ul className="errors"></ul>
                            </div>
                            <div className="review-form-row form-row">
                                <input type="submit" value="ОСТАВИТЬ ОТЗЫВ" className="button" />
                            </div>
                        </form>
                    </div>
                </div>
                <GiftCounter />
                <Best />
                <Question />
                <ReviewsExpert />
                <SeoText />
            </div>
        );
    }
});

export default ReviewsVideoHandler;
