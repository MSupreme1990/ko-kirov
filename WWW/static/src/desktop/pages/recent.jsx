import React, { Component } from 'react';
import { Link } from 'react-easy-router';
import Products from 'shared/components/product/products';
import Features from 'shared/components/features';
import HowOrder from 'shared/components/how_order';
import Question from 'shared/components/question';
import ShowReviews from 'shared/components/show_reviews'; 
import Reviews from 'shared/components/reviews';
import RecentShow from 'shared/components/product/recent_show';
import GiftCounter from 'shared/components/gift_counter';
import Best from 'shared/components/best';
import SeoText from 'shared/components/seo_text';
import { ModalLink } from 'shared/components/modal';
import Loader from 'shared/components/loader';
import CatalogSidebarMenu from 'shared/components/catalog/sidebar_menu';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as recentActions from 'actions/recent';

class PageRecent extends Component {
    renderModels() {
        const { loading, models } = this.props;

        if (loading) {
            return <Loader/>;
        }

        return <Products data={models} count={4} />;
    }

    render() {
        return (
            <div>
                <div className="row">
                    <div className="columns medium-12 large-12">
                        <Features />
                    </div>
                </div>

                <div className="product-page-container">
                    <div className="row">
                        <div className="columns large-2">
                            <div className="sidebar">
                                <CatalogSidebarMenu/>
                            </div>
                        </div>
                        <div className="columns large-8">
                            <div className="product-list-container">
                                <h1>Недавно вы смотрели</h1>
                                {this.renderModels()}
                            </div>
                        </div>
                    </div>

                    <HowOrder />
                    <Question />
                    <ShowReviews />
                    <Reviews />
                    <RecentShow />
                    <GiftCounter />
                    <Best />
                    <SeoText />
                </div>
            </div>
        );
    }
}

export default connect(state => state.recent, dispatch => {
    return {
        recentActions: bindActionCreators(recentActions, dispatch)
    };
})(PageRecent);