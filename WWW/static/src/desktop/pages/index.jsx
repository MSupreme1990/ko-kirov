import React, { Component } from 'react';
import Features from 'shared/components/features';
import SliderMain from 'shared/components/index/slider_main';
import Fly from 'shared/components/fly';
import Categories from 'shared/components/product/categories';
import GiftCounter from 'shared/components/gift_counter';
import Best from 'shared/components/best';
import Reviews from 'shared/components/reviews';
import SeoText from 'shared/components/seo_text';
import { AdWords_send } from 'lib/utils';
import Popular from 'shared/components/product/popular';
import NewModels from 'shared/components/product/new';

export default class KoIndexHandler extends Component {
    componentWillMount() {
        AdWords_send({ ecomm_pagetype: 'home' });
    }

    render() {
        return (
            <div>
                <div className="row">
                    <div className="columns medium-12">
                        <Features />
                    </div>
                </div>

                <SliderMain />

                <div className="row">
                    <div className="columns medium-12">
                        <Fly />
                        <div className="product-list-container">
                            <h3>Каталог</h3>
                            <Categories />
                        </div>
                        <NewModels />
                        <Popular />
                    </div>
                </div>

                <GiftCounter />
                <Best reviews={true} />
                <Reviews />
                <SeoText />
            </div>
        );
    }
}