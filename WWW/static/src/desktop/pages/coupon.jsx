import React, { Component } from 'react';
import { Link } from 'react-easy-router';
import Map from 'shared/components/map';
import Loader from 'shared/components/loader.jsx';
import * as couponActions from 'actions/coupon';
import * as officeActions from 'actions/office';
import * as geoActions from 'actions/geo';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import settings from 'config/settings';

class CouponHandler extends Component {
    map_params = {
        lat: 58.5979,
        lng: 49.6681,
        controls: []
    };

    componentWillMount() {
        const {
            geo,
            geoActions,
            officeActions,
            couponActions,
            params
        } = this.props;

        couponActions.fetchViewIfNeeded(params.code);
        geoActions.fetchListIfNeeded();
        if (geo) {
            officeActions.fetchListIfNeeded(geo.site.id);
        }
    }

    componentWillReceiveProps(nextProps) {
        const { geo, officeActions } = nextProps;
        if (geo) {
            if (geo.id != this.props.geo.id) {
                officeActions.fetchListIfNeeded(geo.site.id);
            }
        }
    }

    renderContent() {
        const { coupon, error, loading } = this.props.coupon;

        if (error) {
            return (
                <div className="section section-title">
                    <div className="row">
                        <div className="column small-12">
                            <div className="container">
                                <div className="h1">
                                    Сожалеем но такого кода нет, либо истек его срок.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }

        if (loading || !coupon) {
            return <Loader />;
        }

        let map = null;

        const { office } = this.props;
        if (office.loading) {
            map = <Loader />;
        } else {
            map = <Map ref='map' objects={office.defaultObjects} {...this.map_params} />;
        }

        return (
            <div className="row">
                <div className="columns small-2">&nbsp;</div>
                <div className="columns small-8">
                    <div className="section section-title">
                        <div className="row">
                            <div className="columns small-12">
                                <div className="container">
                                    <div className="h1">
                                        Промокод на скидку + подарок
                                    </div>
                                    <div className="text">
                                        1000 рублей — норковые шубы; 500 рублей — дубленки, изделия из кожи
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div className="section section-coupon">
                        <div className="row">
                            <div className="column small-8">
                                <p><span className="bold">Ф.И.О:</span> {coupon.name} </p>
                                <p><span className="bold">Номер телефона:</span> {coupon.phone} </p>
                                <p><span className="bold">Дата выдачи:</span> {coupon.create_at} </p>
                                <p><span className="bold">Действителен до:</span> {coupon.valid_until} </p>

                            </div>
                            <div className="column small-4">
                                <div className="block-code">
                                    <p className="text">Ваш промокод:</p>
                                    <p className="code">
                                        {coupon.code}
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="section section-provision">
                        <div className="row">
                            <div className="columns small-12">
                                <div className="block-provision px13">
                                    <p className="title">Условия использования:</p>
                                    <p>1. Купон суммируется со всеми скидками!</p>
                                    <p>2. Купон дает право его обладателю получить дополнительную скидку в салонах <br/>и
                                        интернет-магазине «Кожаная одежда»</p>
                                    <p>3. Вы можете приобрести товар в рассрочку или кредит на срок до 2-х лет без
                                        переплаты. Для оформления нужен только паспорт, оформление на месте. Товар Вы
                                        забираете сразу.</p>
                                    <p>4. Купон не действует в период проведения специальных акций.</p>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div className="section section-map">
                        <div className="row">
                            <div className="column small-12">
                                <p className="title">Салоны на карте:</p>
                                <p className="addresses">
                                    <span className="address">Октябрьский проспект, 86</span>&ensp;
                                    <span className="address">ТЦ «Прайд», ул. К.Маркса, 129</span>&ensp;
                                    <span className="address">ТЦ «КУБ», ул. Горького 5/1</span>&ensp;
                                </p>

                                <div className="map">
                                    <div className="block-map">
                                        {map}
                                    </div>

                                    <div className="questions">
                                        <div className="row">
                                            <div className="column small-6">
                                                <p className="px15">Есть вопросы ?</p>
                                                <p className="px15">Звоните прямо сейчас!</p>
                                            </div>
                                            <div className="column small-6">
                                                <p className="phone bold">8 (800) 500-14-22</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="section section-footer">
                        <div className="row">
                            <div className="column small-12">
                                <p><span className="bold">Адрес сайта: </span>ko-kirov.ru</p>
                                <p className="px12">Если у Вас нет возможности распечатать купон, сфотографируйте его или
                                    запишите промокод</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="columns small-2">&nbsp;</div>
            </div>
        );
    }

    render() {
        return (
            <div className="coupon-wrapper">
                <div className="section section-logo">
                    <div className="row">
                        <div className="columns small-12">
                            <Link to="KoIndex">
                                <img src={"/logo.svg"} alt="Кожаная одежда"/>
                            </Link>
                        </div>
                    </div>
                </div>

                {this.renderContent()}
            </div>
        );
    }
}

export default connect((state, props) => {
    const { code } = props.params;
    const { loading, view, error } = state.coupon.view;

    return {
        office: state.office,
        geo: state.geo,
        coupon: {
            coupon: view[code],
            loading,
            error
        }
    };
}, dispatch => {
    return {
        officeActions: bindActionCreators(officeActions, dispatch),
        geoActions: bindActionCreators(geoActions, dispatch),
        couponActions: bindActionCreators(couponActions, dispatch)
    };
})(CouponHandler);
