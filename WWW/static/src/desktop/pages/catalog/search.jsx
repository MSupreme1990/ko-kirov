import React, { Component, PropTypes } from 'react';
import { Link } from 'react-easy-router';
import { ModalLink } from 'shared/components/modal';
import Breadcrumbs from 'shared/components/breadcrumbs';
import Banner from 'shared/components/banner';
import Features from 'shared/components/features';
import SeoText from 'shared/components/seo_text';
import SidebarMenu from 'shared/components/catalog/sidebar_menu'
import { AdWords_send } from 'lib/utils';
import { openModal } from 'lib/utils';
import shallowequal from 'shallowequal';

import Base from 'desktop/pages/catalog/base';

import * as productActions from 'actions/product';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class SearchListHandler extends Base {
    static propTypes = {
        filters: PropTypes.object.isRequired,
        productActions: PropTypes.object.isRequired
    };

    getRoute() {
        return 'Search';
    }

    componentWillMount() {
        const { query, filters, productActions } = this.props;
        productActions.search(query.q, filters);

        AdWords_send({
            ecomm_pagetype: 'search'
        });
    }

    componentWillReceiveProps(nextProps) {
        const { query, filters, productActions } = nextProps;
        if (
            query.q != this.props.query.q ||
            shallowequal(this.props.filters, filters) === false
        ) {
            productActions.search(query.q, filters);

            AdWords_send({
                ecomm_pagetype: 'search'
            });
        }
    }

    render() {
        const { query } = this.props;

        return (
            <div>
                <Breadcrumbs model={{name: 'Поиск по запросу: ' + query.q}}/>

                <div className="row">
                    <div className="columns medium-12">
                        <Features />
                    </div>
                </div>

                <div className="product-page-container">
                    <div className="row">
                        <div className="columns medium-3 large-2">
                            <div className="sidebar">
                                <SidebarMenu />
                            </div>
                        </div>
                        <div className="columns medium-9 large-10">
                            <div className="product-list-container">
                                <div className="product-list-container">
                                    <h1>Поиск по запросу: {query.q}</h1>
                                    <Banner url={window.location.pathname}/>
                                    <SeoText />
                                    {this.renderList()}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect((state, props) => {
    const { meta, objects, loading, error } = state.product.search;

    const { query } = props;
    let fields = [
            'page', 'page_size', 'min_size',
            'max_size', 'order', 'q'
        ],
        filters = {};
    fields.map(field => {
        filters[field] = query[field];
    });

    return {
        meta,
        filters,
        objects,
        loading,
        error,
        geo: state.geo
    };
}, dispatch => {
    return {
        productActions: bindActionCreators(productActions, dispatch)
    };
})(SearchListHandler);
