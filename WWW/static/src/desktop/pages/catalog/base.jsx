import $ from 'jquery';
import React, { Component, PropTypes } from 'react';
import { Link } from 'react-easy-router';
import SizeRange from 'shared/components/size_range';
import mobile from 'lib/mobile';
import { ModalLink } from 'shared/components/modal';
import Loader from 'shared/components/loader';
import Breadcrumbs from 'shared/components/breadcrumbs';
import Banner from 'shared/components/banner';
import Features from 'shared/components/features';
import GiftCounter from 'shared/components/gift_counter';
import HowOrder from 'shared/components/how_order';
import Question from 'shared/components/question';
import ShowReviews from 'shared/components/show_reviews';
import Reviews from 'shared/components/reviews';
import RecentShow from 'shared/components/product/recent_show';
import Best from 'shared/components/best';
import SeoText from 'shared/components/seo_text';
import Paginator from 'shared/containers/paginator';
import SidebarMenu from 'shared/components/catalog/sidebar_menu'
import CatalogProductsRender from 'shared/components/catalog/products_render'
import NotFoundPage from 'desktop/pages/404.jsx';
import { AdWords_send } from 'lib/utils';
import { openModal } from 'lib/utils';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

export default class CatalogListHandler extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    state = {
        loading: true,
        // Main area
        category: [],
        // Products
        models: [],
        models_origin: [],
        group: [],
        // Filter params
        sort: {},
        // Error state
        error: false,
        // pagination
        currentPage: 1,
        perPage: (mobile.any() ? 20 : 100),
        numPages: 1
    };

    static propTypes = {
        filters: PropTypes.object.isRequired,
        productActions: PropTypes.object.isRequired
    };

    getRoute() {
        return '???';
    }

    handlePageSize(page_size, e) {
        e.preventDefault();

        const { query, filters } = this.props;
        this.context.router.to(this.getRoute(), this.props.params, {
            ...query,
            ...filters,
            page_size,
            page: 1
        });
    }

    onChangeSize(min_size, max_size) {
        const { params, query } = this.props;

        this.context.router.to(this.getRoute(), params, {
            ...query,
            min_size,
            max_size
        });
    }

    renderFilterProducts(products = []) {
        const { query, meta } = this.props;
        if (typeof meta == 'undefined') {
            return;
        }

        return (
            <div>
                <div className='row'>
                    <div className="columns small-12">
                        <CatalogProductsRender
                            products={products}
                            group={products}/>
                    </div>
                </div>
                <p className="clear"/>
                <div className="margin-top">
                    <div className="row">
                        <div className="columns medium-12 large-9">
                            <p className="filter-title">Навигация:</p>
                            <Paginator
                                page={Number(query.page || 1)}
                                prevText='<'
                                nextText='>'
                                numPages={meta.pages_count}
                                onClick={this.onPaginate.bind(this)}/>
                        </div>
                        <div className="columns medium-12 large-3">
                            {this.renderPageSize()}
                        </div>
                    </div>
                </div>
                <p className="clear"/>
            </div>
        );
    }

    renderPageSize() {
        const { filters } = this.props;

        let steps = [100, 200],
            nodes = steps.map((size, i) => (
                <a key={'page-size' + size} href="#" onClick={this.handlePageSize.bind(this, size)}
                   className={(!filters.page_size && i === 0 || filters.page_size == size) ? 'active' : ''}>{size}</a>
            ));

        return (
            <div className="page-size-container">
                <div className="page-size">
                    <p className="filter-title">Показывать по:</p>
                    <p>{nodes}</p>
                </div>
            </div>
        );
    }

    renderFilterItem(key, name) {
        const { query } = this.props;

        let cls = '';
        if (query.order == key) {
            cls = 'asc';
        } else if (query.order == '-' + key) {
            cls = 'desc';
        }

        return (
            <a href="#" className={cls}
               onClick={this.handlerFilterKey.bind(this, key)}>{name}</a>
        );
    }

    renderFilter() {
        const { query } = this.props;

        let min = Number(query.min_size),
            max = Number(query.max_size);

        return (
            <div className="product-filter-container">
                <div className="row">
                    <div className="columns medium-12 large-6">
                        <p className="filter-title">Сортировать по:</p>
                        <ul className="filter-list">
                            <li>{this.renderFilterItem('is_popular', 'Популярности')}</li>
                            <li>{this.renderFilterItem('price', 'Цене')}</li>
                            <li>{this.renderFilterItem('discount_percentage', 'Скидке')}</li>
                            <li>{this.renderFilterItem('update', 'Обновлению')}</li>
                        </ul>
                    </div>

                    <div className="columns medium-12 large-3">
                        <div className="range-input">
                            <p className="filter-title">Размер:</p>
                            <SizeRange min={min} max={max} onChange={this.onChangeSize.bind(this)}/>
                        </div>
                    </div>

                    <div className="columns medium-12 large-3">
                        {this.renderPageSize()}
                    </div>
                </div>
            </div>
        );
    }

    renderList() {
        const { error, loading, objects, received } = this.props;

        if (loading || received == false) {
            return <Loader />;
        }

        if (error) {
            return <NotFoundPage />;
        }

        let pageModelsStart = this.state.perPage * (this.state.currentPage - 1),
            products = [...objects].slice(pageModelsStart, pageModelsStart + this.state.perPage);

        return (
            <div>
                {this.renderFilter()}
                {this.renderFilterProducts(products)}
            </div>
        );
    }

    render() {
        const { params, category } = this.props;

        const url = params.url;

        return (
            <div>
                <Breadcrumbs model={{ name: category.name }}/>

                <div className="row">
                    <div className="columns medium-12">
                        <Features />
                    </div>
                </div>

                <div className="product-page-container">
                    <div className="row">
                        <div className="columns medium-3 large-2">
                            <div className="sidebar">
                                <SidebarMenu url={url} woman={category.woman}/>
                            </div>
                        </div>
                        <div className="columns medium-9 large-10">
                            <div className="product-list-container">
                                <div className="product-list-container">
                                    <h1>{category.name}</h1>
                                    <Banner url={url}/>
                                    <SeoText />
                                    {this.renderList()}
                                </div>
                            </div>
                        </div>
                    </div>
                    <HowOrder />
                    <Question />
                    <ShowReviews />
                    <Reviews />
                    <RecentShow />
                    <GiftCounter />
                    <Best />
                </div>
            </div>
        );
    }

    handlerFilterKey(key, e) {
        e.preventDefault();

        const { query, params } = this.props;

        this.context.router.to(this.getRoute(), params, {
            ...query,
            order: query.order == key ? '-' + key : key
        });
    }

    onPaginate(e, page) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: 0
        }, 650, () => {
            const { query, filters } = this.props;
            this.context.router.to(this.getRoute(), this.props.params, {
                ...query,
                ...filters,
                page
            });
        });
    }
}