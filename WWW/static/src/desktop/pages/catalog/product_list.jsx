import React, { Component, PropTypes } from 'react';
import { Link } from 'react-easy-router';
import SizeRange from 'shared/components/size_range';
import mobile from 'lib/mobile';
import { ModalLink } from 'shared/components/modal';
import Breadcrumbs from 'shared/components/breadcrumbs';
import Banner from 'shared/components/banner';
import Features from 'shared/components/features';
import SeoText from 'shared/components/seo_text';
import SidebarMenu from 'shared/components/catalog/sidebar_menu'
import NotFoundPage from 'desktop/pages/404.jsx';
import { AdWords_send } from 'lib/utils';
import { openModal } from 'lib/utils';
import shallowequal from 'shallowequal';

import Base from 'desktop/pages/catalog/base';

import * as productActions from 'actions/product';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class CatalogListHandler extends Base {
    state = {
        loading: true,
        // Main area
        category: [],
        // Products
        models: [],
        models_origin: [],
        group: [],
        // Filter params
        sort: {},
        // Error state
        error: false,
        // pagination
        currentPage: 1,
        perPage: (mobile.any() ? 20 : 100),
        numPages: 1
    };

    static propTypes = {
        filters: PropTypes.object.isRequired,
        productActions: PropTypes.object.isRequired
    };

    getRoute() {
        return 'CatalogList';
    }

    componentWillMount() {
        const { params, filters, productActions } = this.props;
        productActions.fetchListIfNeeded(params.url, filters);

        AdWords_send({
            ecomm_pagetype: 'category'
        });
    }

    componentWillReceiveProps(nextProps) {
        const { filters, params, productActions } = nextProps;
        if (
            this.props.params.url != nextProps.params.url ||
            shallowequal(this.props.filters, filters) === false
        ) {
            productActions.fetchList(params.url, filters);

            AdWords_send({
                ecomm_pagetype: 'category'
            });
        }
    }

    getCatalogName() {
        const { received, category, loading, error, geo } = this.props;

        if (loading || received == false) {
            return <span>Загрузка...</span>;
        }

        if (error) {
            return <NotFoundPage />;
        }

        const showCityInName = false;

        return showCityInName ? category.name + ' в ' + geo.city.name2 : category.name;
    }

    renderBreadcrumbs() {
        const { breadcrumbs, loading } = this.props;

        if (loading) {
            return null;
        }

        let nodes = breadcrumbs.map(bc => {
            return {
                name: bc.name,
                route: 'CatalogList',
                params: {url: bc.url}
            }
        });

        nodes.push({name: this.getCatalogName()});

        return <Breadcrumbs breadcrumbs={nodes}/>;
    }

    render() {
        const { params, category } = this.props;

        const url = params.url;

        return (
            <div>
                {this.renderBreadcrumbs()}

                <div className="row">
                    <div className="columns medium-12">
                        <Features />
                    </div>
                </div>

                <div className="product-page-container">
                    <div className="row">
                        <div className="columns medium-3 large-2">
                            <div className="sidebar">
                                <SidebarMenu url={url} woman={category.woman}/>
                            </div>
                        </div>
                        <div className="columns medium-9 large-10">
                            <div className="product-list-container">
                                <h1>{this.getCatalogName()}</h1>
                                <Banner url={url}/>
                                <SeoText />
                                {this.renderList()}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect((state, props) => {
    const { category, meta, objects, loading, error, breadcrumbs } = state.product;

    const { query } = props;
    let fields = [
            'page', 'page_size', 'min_size',
            'max_size', 'order'
        ],
        filters = {};
    fields.map(field => {
        filters[field] = query[field];
    });

    return {
        meta,
        breadcrumbs,
        filters,
        category,
        objects,
        loading,
        error,
        geo: state.geo
    };
}, dispatch => {
    return {
        productActions: bindActionCreators(productActions, dispatch)
    };
})(CatalogListHandler);
