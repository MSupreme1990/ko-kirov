import React from 'react';

import App from 'desktop/app';
import GiftCounter from 'shared/components/gift_counter';
import HowOrder from 'shared/components/how_order';
import Question from 'shared/components/question';
import ShowReviews from 'shared/components/show_reviews';
import Reviews from 'shared/components/reviews';
import RecentShow from 'shared/components/product/recent_show';
import Best from 'shared/components/best';

export default class CatalogApp extends App {
    getChildren() {
        return (
            <div>
                {super.getChildren()}
                <HowOrder />
                <Question />
                <ShowReviews />
                <Reviews />
                <RecentShow />
                <GiftCounter />
                <Best />
            </div>
        )
    }
}