import React from 'react';
import { Link } from 'react-easy-router';

var NotFoundPage = React.createClass({
    render: function() {
        window.globalError = false;
        return (
              <section className="error-page">
                  <section className="error-data">
                      <section className="error-code">
                          404
                      </section>
                      <section className="error-info">
                          <section className="multiline">
                              <section className="error-title">
                                  Страница не найдена
                              </section>
                              <section className="error-description">
                                  <Link to="Index">Перейти на главную &rarr;</Link>
                              </section>
                          </section>
                      </section>
                      <p className="clear"></p>
                  </section>
              </section>
        );
    }
});

export default NotFoundPage;
