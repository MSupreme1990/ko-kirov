import $ from 'jquery';
import api from 'lib/api';
import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import { Link } from 'react-easy-router';
import OrderQuick from 'shared/components/OrderQuick.jsx';
import {
    modalWindowUnlock,
    formValidate,
    sendToAmo,
    openModal,
    dataLayer_push
} from 'lib/utils';
import Loader from 'shared/components/loader';
import Features from 'shared/components/features';
import PayLate from 'shared/components/PayLate';
import { GetCouponForm } from 'shared/components/forms';
import { ModalLink, ProductModalLink } from 'shared/components/modal';
import PhoneInput from 'inputs/phone_input';
import currency from 'shared/components/currency';
import { cookie, local } from 'easy-storage';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as cartActions from 'actions/cart';

class CartHandler extends Component {
    state = {
        step: 1,
        quick_complete: false,
        name: cookie.get('name') || '',
        email: cookie.get('email') || '',
        phone: cookie.get('phone') || '',
        comment: cookie.get('comment') || '',
        address: cookie.get('address') || '',
        phone_cls: '',
        delivery_index: 1,
        loading: false,
        error: null,
        items: [],
        delay_items: local.get('delay_products', []),
        total: 0
    };

    componentWillMount() {
        const { cartActions } = this.props;
        cartActions.fetchList();
    }

    loadCart(symbol = 'RUB') {
        this.setState({
            loading: true
        });

        api.get('/api/ko/cart/list/json', { symbol }, data => {
            data['loading'] = false;
            this.setState(data, () => {

                AdWords_send({
                    ecomm_pagetype: 'cart',
                    ecomm_totalvalue: this.state.total
                });
            });
        }, () => {
            this.setState({
                error: true,
                loading: false
            });
        });
    }

    handleNameChange(e) {
        this.setState({
            name: e.target.value
        });
        cookie.set('name', e.target.value);
    }

    handleEmailChange(e) {
        this.setState({
            email: e.target.value
        });
        cookie.set('email', e.target.value);
    }

    handlePhoneChange(e) {
        let value = e.target.value.replace(/\D/g, '');

        this.setState({
            phone: value
        });
        cookie.set('phone', value);
    }

    handleCommentChange(e) {
        this.setState({
            comment: e.target.value
        });
        cookie.set('comment', e.target.value);
    }

    handleAddressChange(e) {
        this.setState({
            address: e.target.value
        });
        cookie.set('address', e.target.value);
    }

    handleValidateForm(e) {
        e.preventDefault();

        if (this.state.phone.length < 10) {
            this.setState({ phone_cls: 'error' });
            return;
        }

        let nstep = this.state.step + 1;

        this.setState({
            phone_cls: '',
            step: nstep
        });
        cookie.set('step', nstep);
    }

    handleDeliveryClick(e) {
        e.preventDefault();
        let $this = $(e.target).closest('li');
        this.setState({
            delivery_index: $this.index() + 1
        });
    }

    renderStepHeader(step) {
        const { deffered } = this.props;

        if (step == 1) {
            return (
                <div>
                    {this.renderSteps()}

                    <ul className="cart-tab-list">
                        <li><Link to="Cart" className="active">Корзина</Link></li>
                        <li><Link to="CartDelayed">Отложенные товары ({deffered.length})</Link></li>
                    </ul>
                </div>
            );
        } else if (step == 2) {
            let phoneErrors = this.state.phone_cls == '' ? null : (
                    <li>Укажите ваш телефон</li>
                );

            return (
                <div>
                    {this.renderSteps()}
                    <h1>Введите контактные данные для заказа</h1>

                    <form className="order-form">
                        <div className="row">
                            <div className="columns small-3">
                                <div className="form-row">
                                    <input
                                        defaultValue={this.state.name}
                                        onChange={this.handleNameChange.bind(this)}
                                        placeholder="Введите Ваши фамилию, имя, отчество" name="OrderForm[name]"
                                        type="text"/>
                                    <ul className="errors"></ul>
                                </div>

                                <div className="form-row">
                                    <PhoneInput
                                        defaultValue={this.state.phone}
                                        onChange={this.handlePhoneChange.bind(this)}
                                        className={'phone ' + this.state.phone_cls}
                                        placeholder="Введите Ваш телефон: +7 (  ) ___ - __ - __" name="OrderForm[phone]"
                                        type="text"/>
                                    <ul className="errors">{phoneErrors}</ul>
                                </div>

                                <div className="form-row">
                                    <input
                                        defaultValue={this.state.email}
                                        onChange={this.handleEmailChange.bind(this)}
                                        placeholder="Введите Ваш e-mail" name="OrderForm[email]" type="text"/>
                                    <ul className="errors"></ul>
                                </div>

                                <div className="form-row">
                                    <a href="#" className="prev" onClick={this.handlePrevStep.bind(this)}>Назад</a>
                                </div>
                            </div>
                            <div className="columns small-7">
                                <div className="form-row">
                                    <textarea
                                        defaultValue={this.state.comment}
                                        onChange={this.handleCommentChange.bind(this)}
                                        name="OrderForm[comment]"
                                        placeholder="Ваш комментарий: объем груди, талии, бедер (см)"></textarea>
                                    <ul className="errors"></ul>
                                </div>

                                <div className="form-row">
                                    <a href="#" className="next"
                                       onClick={this.handleValidateForm.bind(this)}>Продолжить</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            );
        } else if (step == 3) {
            return (
                <div>
                    {this.renderSteps()}
                    <h1>Выберите способ доставки</h1>

                    <div className="delivery-container">
                        <ul className="delivery-column">
                            <li onClick={this.handleDeliveryClick.bind(this)}
                                className={this.state.delivery_index == 1 ? 'active' : ''}>
                                Доставка курьером
                            </li>
                            <li onClick={this.handleDeliveryClick.bind(this)}
                                className={this.state.delivery_index == 2 ? 'active' : ''}>
                                Доставка EMS почта России
                            </li>
                        </ul>
                        <div className="delivery-content">
                            <div className={this.state.delivery_index == 1 ? '' : 'hide'}>
                                <h1>Заказать доставку курьером</h1>

                                <form className="delivery-form">
                                    <div className="form-row">
                                        <label htmlFor="address">Введите адрес доставки:</label>
                                        <input defaultValue={this.state.address} className={this.state.address_cls}
                                               onChange={this.handleAddressChange.bind(this)} type="text" id="address"
                                               name="address"
                                               placeholder="Например: 610000, Россия, Кировская область, г. Киров, ул. Архитектора, д. 102, кв. 401"/>
                                    </div>
                                </form>
                                <p>Быстрая, бесплатная доставка в любое удобное для Вас место для примерки (обычно домой
                                    или на работу). Доставка осуществляется в рабочие дни с 9:00 до 18:00.</p>

                                <p>Оплата наличными после примерки. Когда приезжает курьер, у Вас есть 20 минут на
                                    примерку изделия и принятие окончательного решения.</p>

                                <p>Если товар не подошёл, Вы можете сразу вернуть его курьеру.</p>
                            </div>
                            <div className={this.state.delivery_index == 2 ? '' : 'hide'}>
                                <h1>Заказать доставку EMS почты России</h1>

                                <form className="delivery-form">
                                    <div className="form-row">
                                        <label htmlFor="address">Введите адрес доставки:</label>
                                        <input defaultValue={this.state.address}
                                               className={this.state.address_cls}
                                               onChange={this.handleAddressChange.bind(this)}
                                               type="text" id="address"
                                               name="address"
                                               placeholder="Например: 610000, Россия, Кировская область, г. Киров, ул. Архитектора, д. 102, кв. 401"/>
                                    </div>
                                </form>
                                <p>Заказать доставку ЕМС почта России (Доставка воздушным транспортом). Быстрая,
                                    бесплатная доставка авиационным транспортом Почты России.</p>

                                <p>Преимущество данного способа доставки заключается в том, что Вы в любое удобное для
                                    Вас время можете получить Ваш заказ в ближайшем почтовом отделении, предварительно
                                    вскрыв посылку и примерив.</p>

                                <p>Оплата наличными.</p>
                            </div>
                        </div>
                    </div>
                    <div className="row delivery-buttons">
                        <div className="columns small-4">
                            <a href="#" className="prev-button" onClick={this.handlePrevStep.bind(this)}>Назад</a>
                        </div>
                        <div className="columns small-6">
                            <a href="#" className="right next-button" onClick={this.handleComplete.bind(this)}>Завершить
                                оформление заказа</a>
                        </div>
                    </div>
                </div>
            );
        } else if (step == 4) {
            return (
                <div>
                    <h1>Спасибо! Ваша заявка отправлена, в ближайшее время с вами свяжется наш менеджер</h1>
                </div>
            );
        }
    }

    handleComplete(e) {
        e.preventDefault();

        if (this.state.phone.length < 6) {
            this.setState({ phone_cls: 'error' });
            return;
        }

        modalWindowUnlock();

        api.post('/api/ko/cart-order', {
            name: this.state.name,
            email: this.state.email,
            phone: this.state.phone,
            comment: this.state.comment,
            address: this.state.address,
            type: this.state.delivery_index == 1 ? 'Курьер' : 'EMS',
            symbol: window.CURRENCY
        }, data => {
            this.setState({
                step: 4
            });

            dataLayer_push({ 'event': 'CartOrder', 'type': 'fullCheckout' });
        });
    }

    handleNextStep(e) {
        e.preventDefault();
        $("html, body").animate({
            scrollTop: 0
        }, 450);
        this.setState({
            step: this.state.step + 1
        });
        cookie.set('step', this.state.step + 1);
    }

    handlePrevStep(e) {
        e.preventDefault();
        $("html, body").animate({
            scrollTop: 0
        }, 450);
        this.setState({
            step: this.state.step - 1
        });
        cookie.set('step', this.state.step - 1);
    }

    renderSteps() {
        const { step } = this.state;

        return (
            <ul className="cart-steps">
                <li>
                    <span className={step == 1 ? "number active" : "number"}>1</span>
                    <span className="text">Корзина</span>
                </li>
                <li>
                    <span className={step == 2 ? "number active" : "number"}>2</span>
                    <span className="text">Контакты</span>
                </li>
                <li>
                    <span className={step == 3 ? "number active" : "number"}>3</span>
                    <span className="text">Доставка</span>
                </li>
            </ul>
        );
    }

    renderList() {
        const { symbol, total, loading, objects } = this.props;

        if (loading) {
            return <Loader />;
        }

        let header = objects.length == 0 ? null : (
                <thead>
                <tr>
                    <th className="cart-th-photo">Фото</th>
                    <th className="cart-th-name">Наименование</th>
                    <th className="cart-th-size">Размер</th>
                    <th className="cart-th-price">Цена</th>
                    <th className="cart-th-actions">Действия</th>
                </tr>
                </thead>
            );

        let phoneErrors = this.state.phone_cls == '' ? null : (
                <li>Укажите ваш телефон</li>
            );

        let fakeObj = {
            ['price_' + symbol.toLowerCase()]: total
        };

        let footer = this.state.step == 1 && objects.length > 0 ? (
                <div>
                    <ul className="cart-list-footer">
                        <li>
                            <a className="next-button" href="#" onClick={this.handleNextStep.bind(this)}>
                                Оформить заказ</a>
                        </li>
                        <li>
                            <PayLate products={objects}/>
                        </li>
                        <li>
                            <a href="#" onClick={openModal.bind(this, <OrderQuick />, { width: 450 })}
                               className="prev-button">Заказать в 1 клик</a>
                        </li>
                        <li className="cart-list-footer-total">
                            Сумма заказа:
                            <span className="cart-list-footer-price">
                            {currency.format(symbol, fakeObj)}
                        </span>
                        </li>
                    </ul>
                    <p>
                        <Link className="catalog-button" to="CatalogList" params={{ url: 'woman' }}>Вернуться в каталог
                            для
                            покупок</Link>
                    </p>
                </div>
            ) : null;

        return (
            <div>
                <table className="cart-table">
                    {header}
                    <tbody>
                    <tr>
                        <td colSpan="5" className="cart-table-td">
                            <div className="cart-table-banner">
                                <a onClick={openModal.bind(this, <GetCouponForm />, { width: 450 })}
                                   ref="coupon" href="#">Получите фирменный чехол - сумку в подарок!</a>
                            </div>
                        </td>
                    </tr>
                    {this.renderListItems()}
                    </tbody>
                </table>
                {footer}
            </div>
        );
    }

    renderQuickOrderForm() {
        const { name, phone, phone_cls, quick_complete } = this.state;

        let phoneErrors = phone_cls == '' ? null : <li>Укажите ваш телефон</li>;

        return quick_complete ? (
                <div>
                    <h1 className="text-center">
                        Заявка на звонок успешно отправлена. В ближайшее время с вами свяжется наш менеджер.
                    </h1>
                </div>
            ) : (
                <form
                    ref="quick_form"
                    className="quick-cart-order"
                    onSubmit={this.handleSubmitQuick(() => this.setState({ quick_complete: true }))}>
                    <h1>Заказать в 1 клик</h1>

                    <p>Заполните эту форму и наш менеджер свяжется с Вами для подтверждения заказа</p>

                    <div className="form-row">
                        <input type="text"
                               name="OrderQuickForm[name]"
                               onChange={this.handleNameChange.bind(this)}
                               placeholder="Введите Ваше имя"
                               defaultValue={name}/>
                        <ul className="errors"></ul>
                    </div>
                    <div className="form-row">
                        <PhoneInput type="text"
                                    name="OrderQuickForm[phone]"
                                    className={phone_cls}
                                    placeholder="Введите Ваш телефон"
                                    onChange={this.handlePhoneChange.bind(this)}
                                    defaultValue={phone}/>
                        <ul className="errors">{phoneErrors}</ul>
                        <ul className="errors"></ul>
                    </div>
                    <div className="form-row">
                        <input type="submit" value="ЗАКАЗАТЬ В 1 КЛИК" className="button"/>
                    </div>
                    <div className="b-form-row">
                        <div className="b-personal-data">
                            <span className="b-personal-data__check"><input type="checkbox" checked="checked"/></span>
                            Передавая информацию сайту вы принимаете условия
                            <a className="b-personal-data__link"
                               href="/page/private"
                               target="_blank">"политики защиты персональных данных".</a>
                        </div>
                    </div>
                </form>
            );
    }

    handleAddToCart(e) {
        e.preventDefault();

        let $this = $(e.target).closest('a'),
            id = $this.attr('data-id');

        api.post('/api/ko/cart/add/' + id + '/1', {}, data => {
            $.mnotify(data.message);
            let items = this.state.items;
            items.push(_.find(this.state.delay_items, function (t) {
                return t.object.id == id;
            }));
            GlobalEvents.send('cart_update', -1);
            this.setState({
                items: items,
                delay_items: _.without(this.state.delay_items, _.find(this.state.delay_items, function (t) {
                    return t.object.id == id;
                }))
            });
            $.mnotify({
                title: "Товар добавлен в корзину",
                delay: 4000,
                nonblock: {
                    nonblock: true,
                    nonblock_opacity: .2
                }
            });
        });
    }

    handleMove(id, index, e) {
        e.preventDefault();

        if (confirm('Вы действительно хотите переместить данный товар из корзины в отложенные товары?')) {
            const { cartActions } = this.props;
            cartActions.moveToDefferedByIndex(index);
        }
    }

    handleDelete(id, index, e) {
        e.preventDefault();

        if (confirm('Вы действительно хотите удалить данный товар из корзины?')) {
            const { cartActions } = this.props;
            cartActions.removeByIndex(index);
        }
    }

    handleCartSizeChange(id, size, index) {
        const { cartActions } = this.props;
        cartActions.changeSize(id, size, index);
    }

    renderListItems() {
        const { symbol, objects } = this.props;

        return objects.length == 0 ? (
                <tr>
                    <td colSpan="5" className="cart-td-empty">
                        <p className="text-center">
                            Товары отсутствуют.
                        </p>

                        <p className="text-center">
                            <Link to="CatalogList" params={{ url: 'woman' }}>Перейдите в каталог для покупок</Link>.
                        </p>
                    </td>
                </tr>
            ) : objects.map((item, i) => {
                let model = item.object,
                    size = typeof item.data.size != "undefined" ? item.data.size : null;

                let price = currency.format(symbol, model, !currency.hasOldPrice(symbol, model));

                let sizeNodes = item.object.sizes.map((s, t) => {
                    return <option key={'size_' + s + '_' + t} value={s}>{s}</option>;
                });

                let actions = this.state.step == 1 ? (
                        <div>
                            <p>
                                <a href="#" onClick={this.handleMove.bind(this, model.id, i)}>Отложить на потом</a>
                            </p>

                            <p>
                                <a href="#" onClick={this.handleDelete.bind(this, model.id, i)}>Удалить</a>
                            </p>
                        </div>
                    ) : null;

                let sizeOut = this.state.step == 1 ? (
                        <div>
                            <select data-id={model.id} defaultValue={size}
                                    onChange={e => this.handleCartSizeChange(model.id, e.target.value, i)}>
                                <option>Выберите размер</option>
                                {sizeNodes}
                            </select>
                        </div>
                    ) : <div>{size}</div>;

                return (
                    <tr key={'cart-tr-' + model.id + '_' + i}>
                        <td>
                            <ProductModalLink
                                model={model}
                                router={this.context.router}
                                className="product-quick-look padding"
                                linkClassName="product-item-detail">

                                <img src={model.image.image.thumb} alt={model.name}/>
                            </ProductModalLink>

                        </td>
                        <td>
                            <p className="cart-item-name">
                                <ProductModalLink
                                    model={model}
                                    router={this.context.router}
                                    className="product-quick-look padding"
                                    linkClassName="product-item-detail">

                                    {model.name} <span className="cart-item-article">№ {model.code}</span>
                                </ProductModalLink>
                            </p>
                        </td>
                        <td>
                            {sizeOut}
                        </td>
                        <td>
                            {price}
                        </td>
                        <td className="cart-td-actions">
                            <div>{actions}</div>
                        </td>
                    </tr>
                );
            });
    }

    render() {
        let out = null;
        if (this.state.step != 4) {
            out = this.renderList();
        }

        return (
            <div className="row">
                <div className="columns small-12">
                    <Features />

                    <div className="cart-container">
                        {this.renderStepHeader(this.state.step)}
                        {out}
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(state => {
    const { total, deffered, loading, objects } = state.cart;

    return {
        total,
        deffered,
        loading,
        objects,
        symbol: state.currency.symbol
    }
}, dispatch => {
    return {
        cartActions: bindActionCreators(cartActions, dispatch)
    };
})(CartHandler);
