import React, { Component } from 'react';
import $ from 'jquery';
import { findDOMNode } from 'react-dom';
import Loader from 'shared/components/loader';
import Features from 'shared/components/features';
import PhoneInput from 'inputs/phone_input';
import Fly from 'shared/components/fly';
import Timer from 'shared/components/timer';
import CatalogLink from 'shared/components/catalog_link';
import GiftCounter from 'shared/components/gift_counter';
import Best from 'shared/components/best';
import Question from 'shared/components/question';
import Reviews from 'shared/components/reviews';
import SeoText from 'shared/components/seo_text';
import { formValidate, fixRelativeMediaUrls } from 'lib/utils';
import api from 'lib/api';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as pageActions from 'actions/page';

const URL = 'delivery';

class DeliveryHandler extends Component {
    state = {
        message: ''
    };

    componentWillMount() {
        const { actions } = this.props;
        actions.fetchViewIfNeeded(URL);
    }

    handleSubmit(e) {
        e.preventDefault();
        e.stopPropagation();

        var $form = $('#price-delivery-form');

        api.post('/api/ko/price-delivery', $form.serialize(), function (data) {
            formValidate($form, data['errors']);
            if (data['success']) {
                $form.html('<h1>' + data['message'] + '</h1>');
            }
        }.bind(this));
    }

    render() {
        const { page, loading } = this.props;

        if (loading || !page) {
            return <Loader />;
        }

        return (
            <div>
                <div className="row">
                    <div className="columns medium-12">
                        <Features />
                    </div>
                </div>

                <div className="page-container">
                    <div className="row">
                        <div className="columns medium-12">
                            <h1>{page.name}</h1>

                            <div className="row page-extra">
                                <div className="columns medium-8">
                                    <div className="page-extra-container-girl">
                                        <div className="page-extra-girl">
                                            <div className="page-extra-text delivery">
                                                <div className="page-extra-top-text">
                                                    Мы гордимся своими меховыми<br/>
                                                    изделиями и проводим акцию
                                                </div>

                                                <div className="page-extra-bottom-text">
                                                    Бесплатная доставка<br/>
                                                    для примерки по всей<br/>
                                                    России!
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="columns medium-4">
                                    <div className="page-extra-sidebar-container">
                                        <div className="page-extra-sidebar">
                                            <p className="page-extra-title">Преимущества доставки</p>
                                            <ul className="page-extra-list">
                                                <li className="page-extra-list-world">
                                                    Быстрая бесплатная доставка по всей<br/>
                                                    России и СНГ.
                                                </li>
                                                <li className="page-extra-list-prepare">
                                                    Примерка перед покупкой
                                                </li>
                                                <li className="page-extra-list-time">
                                                    Удобное для Вас время доставки, курьер<br/>
                                                    созванивается за 1 час до приезда.
                                                </li>
                                                <li className="page-extra-list-pay">
                                                    Оплата при получении заказа наличным или<br/>
                                                    возможность оформления в кредит.
                                                </li>
                                            </ul>
                                            <p className="arrow"></p>
                                            <form onSubmit={this.handleSubmit.bind(this)} id="price-delivery-form">
                                                <div className="row">
                                                    <div className="columns medium-12">
                                                        <p className="form-title">Рассчитать срок и способ доставки:</p>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="columns medium-6 first">
                                                        <div className="delivery-form-city">
                                                            <input type="text" name="PriceDeliveryForm[city]"
                                                                   placeholder="Введите ваш город"/>
                                                            <ul className="errors"></ul>
                                                        </div>
                                                    </div>
                                                    <div className="columns medium-6 last">
                                                        <div className="delivery-form-phone">
                                                            <PhoneInput name="PriceDeliveryForm[phone]"/>
                                                            <ul className="errors"></ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="columns medium-12">
                                                        <div className="delivery-form-submit">
                                                            <input type="submit" value="РАССЧИТАТЬ ДОСТАВКУ"
                                                                   className="button"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="columns medium-12">
                                                        <div className="b-personal-data">
                                                            <span className="b-personal-data__check">
                                                                <input
                                                                    className="b-personal-data__input" type="checkbox"
                                                                    checked="checked"/>
                                                            </span>
                                                            Нажимая кнопку «Получить консультацию», вы принимаете
                                                            <a className="b-personal-data__link"
                                                               href="/page/private"
                                                               target="_blank">"условия обработки персональных
                                                                данных".</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div className="columns medium-12">
                                    <div className="counter-container">
                                        <div className="counter">
                                            <p className="back-counter-text">
                                                До завершения<br/>
                                                осталось:
                                            </p>
                                            <div className="back-counter-container">
                                                <div id="back-delivery-counter">
                                                    <Timer />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="counter-label"></div>
                                        <div className="counter-text">
                                            Бесплатная доставка на дом по России с примеркой перед покупкой!
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div dangerouslySetInnerHTML={{__html: fixRelativeMediaUrls(page.content)}}></div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="columns medium-12">
                            <Fly />
                            <CatalogLink />
                        </div>
                    </div>
                </div>

                <GiftCounter />
                <Best />
                <Question />
                <Reviews />
                <SeoText />
            </div>
        );
    }
}

export default connect((state, props) => {
    const { view, loading, error } = state.page.view;
    return {
        loading,
        error,
        page: view[URL]
    }
}, dispatch => {
    return {
        actions: bindActionCreators(pageActions, dispatch)
    };
})(DeliveryHandler);
