import React, { Component } from 'react';

import Application from './app';

import ContactsHandler from './pages/contacts';
import SearchHandler from './pages/catalog/search';
import WholesalesHandler from './pages/wholesales';
import ReviewsHandler from './pages/reviews';
import ReviewsVideoHandler from './pages/reviews-video';
import CreditHandler from './pages/credit';
import DeliveryHandler from './pages/delivery';
import CartHandler from './pages/cart';
import CartDelayed from './pages/cart_delayed';
import CatalogProductHandler from './pages/product';
import ProductListHandler from './pages/catalog/product_list';
import NotFoundPage from './pages/404';
import PageRecentHandler from './pages/recent';
import TestHandler from './pages/TestHandler';
import PagesViewHandler from './pages/page';
import KoIndexHandler from './pages/index';
import DummyHandler from './pages/dummy';
import CouponHandler from './pages/coupon';

import CatalogApp from './pages/catalog/app';

export default {
    Test: {path: '/test', component: TestHandler, wrapper: Application},

    KoIndex: {path: '/', component: KoIndexHandler, wrapper: Application},
    Index: {path: '/', component: KoIndexHandler, wrapper: Application},

    Coupon: {path: '/coupon/:code', component: CouponHandler},

    KoContacts: {path: '/contacts', component: ContactsHandler, wrapper: Application},


    KoReviews: {path: '/reviews', component: ReviewsHandler, wrapper: Application},
    Reviews: {path: '/reviews', component: ReviewsHandler, wrapper: Application},
    KoReviewsVideo: {path: '/reviews-video', component: ReviewsVideoHandler, wrapper: Application},

    WholeSales: {path: '/sales', component: WholesalesHandler, wrapper: Application},
    KoSales: {path: '/sales', component: WholesalesHandler, wrapper: Application},
    CreditPage: {path: '/page/credit', component: CreditHandler, wrapper: Application},
    Delivery: {path: '/page/delivery', component: DeliveryHandler, wrapper: Application},
    DeliveryPage: {path: '/page/delivery', component: DeliveryHandler, wrapper: Application},
    PagesView: {path: '/page/:url', component: PagesViewHandler, wrapper: Application},

    Cart: {path: '/cart', component: CartHandler, wrapper: Application},
    CartDelayed: {path: '/cart/delayed', component: CartDelayed, wrapper: Application},

    CatalogProduct: {path: '/catalog/:url', component: CatalogProductHandler, wrapper: Application},
    CatalogRecent: {path: '/recent', component: PageRecentHandler, wrapper: Application},

    Search: {path: '/search', component: SearchHandler, wrapper: CatalogApp},
    CatalogList: {path: '/:url', component: ProductListHandler, wrapper: CatalogApp}
}
