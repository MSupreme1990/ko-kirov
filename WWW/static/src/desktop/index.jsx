import $ from 'jquery';
import 'plugins/social-likes';
import 'plugins/jquery.mnotify';
import fb from 'fancybox';
fb($);

$(document)
    .on('click', '#back-counter a', function (e) {
        e.preventDefault();
        return false;
    })
    .on('click', '.fancybox, [rel="lightbox"]', e => {
        e.preventDefault();

        let $this = $(e.target).closest('a');

        $.fancybox.open([{
            type: 'image',
            href: $this.attr('href'),
            title: ''
        }], {
            openEffect: 'elastic',
            closeEffect: 'elastic',
            nextEffect: 'fade',
            openSpeed: 300,
            padding: 0,
            closeSpeed: 200,
            helpers: {
                title: null,
                buttons: {}
            }
        });
    });

import React from 'react';
import { render } from 'react-dom';
import store from 'store';
import { Provider } from 'react-redux';
import routes from './routes';
import { Router } from 'react-easy-router';
import history from 'historystore';
import * as modalActions from 'actions/modal';
import TweenFunctions from 'tween-functions';

const scrollToTop = () => {
    let startValue = window.pageYOffset;
    let currentTime = 0;
    let startTime = null;
    let id = null;

    let scrollStep = timestamp => {
        if (!startTime) {
            startTime = timestamp;
        }

        currentTime = timestamp - startTime;

        var position = TweenFunctions['easeOutCubic'](currentTime, startValue, 0, 250);

        if (window.pageYOffset <= 0) {
            window.cancelAnimationFrame(id);
        } else {
            window.scrollTo(window.pageYOffset, position);
            id = window.requestAnimationFrame(scrollStep);
        }
    };

    id = window.requestAnimationFrame(scrollStep);
};

history.listen(() => {
    scrollToTop();
    store.dispatch(modalActions.close());
});
let fallback = (props) => <div>Страница не найдена</div>;

render((
    <Provider store={store}>
        <Router history={history} fallback={fallback} routes={routes}/>
    </Provider>
), document.getElementById('app'));
