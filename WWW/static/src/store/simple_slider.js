import Store from 'lib/store.js';
import SimpleSlider from '../wrapper/simple_slider.js';

class SimpleSliderStore extends Store {
    timer = undefined;

    getInitialState() {
        return {
            error: false,
            loading: false,
            data: [],
            model: {},
            meta: {},
            styles: {},
            page: 1
        };
    }

    loading(value) {
        this.setState({
            loading: value
        });
    }

    getNextPage() {
        let page = this._state.page - 1;
        if (page >= this._state.data.length - 1) {
            page = 0;
        } else {
            page += 1;
        }
        return page;
    }

    nextSlide() {
        this.setSlide(this.getNextPage());
    }

    setSlide(page) {
        clearTimeout(this.timer);
        this.setState({
            page: page + 1,
            model: this._state.data[page],
            styles: {
                opacity: 1
            }
        });
    }

    getPrevPage() {
        let page = this._state.page - 1;
        if (page <= 0) {
            page = this._state.data.length - 1;
        } else {
            page -= 1;
        }
        return page;
    }

    prevSlide() {
        this.setSlide(this.getPrevPage());
    }
 
    startSlider() {
        clearTimeout(this.timer);
        this.timer = setTimeout(() => {
            this.setState({
                styles: {
                    opacity: 0
                }
            });

            setTimeout(() => {
                this.nextSlide();
                this.startSlider();
            }, 350);
        }, 3500);
    }

    stopSlider() {
        clearTimeout(this.timer);
    }

    list(params = {}) {
        this.loading(true);
        return SimpleSlider
            .list(params)
            .then(this.success.bind(this));
    }

    success(data) {
        this.setState({
            loading: false,
            numPages: data.length,
            data: data,
            model: data[this._state.page - 1]
        });
    }
}

const store = new SimpleSliderStore;

export default store;