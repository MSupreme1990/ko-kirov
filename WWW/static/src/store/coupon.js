import Store from 'lib/store.js';
import Coupon from 'wrapper/coupon.js';

class CouponStore extends Store {
    getInitialState() {
        return {
            loading: true,
            error: false,
        };
    }

    getOrFetch(code, params = {}) {
        this.loading(true);
        Coupon
            .view(code, params)
            .then(data => {
                data['loading'] = false;
                if ( !data['error'] ) {
                    this.setState({ coupon: data });
                }
            });
    }

    loading(value) {
        this.setState({
            loading: value
        });
    }

    list(params = {}) {
        this.loading(true);
        return Coupon
            .list(params)
            .then(this.success.bind(this));
    }

    success(data) {
        this.setState(data);
    }

    error(xhr) {
        this.setState({
            coupon: {
                error: true,
                loading: false
            }
        });
    }
}

const store = new CouponStore();

export default store;