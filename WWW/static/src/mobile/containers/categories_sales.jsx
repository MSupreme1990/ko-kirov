import React, { Component } from 'react';
import { Link } from 'react-easy-router';
import Category from 'shared/components/product/category';
import api from 'lib/api';
import { cookie } from 'easy-storage';
import { chunk, openModal, sendToAmo, formValidate } from 'lib/utils';
import Slider from 'react-slick';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainActions from 'actions/main';

class SalesBackForm extends Component {
    state = {
        pk: null,
        message: ""
    };

    handleSubmit(e) {
        e.preventDefault();
        var $form = $('#SalesBack-form');

        api.post('/api/ko/salesback', $form.serialize(), data => {

            sendToAmo($form);
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    message: data['message']
                });
            }
        });
    }

    handleEmailChange(e) {
        cookie.set('email', e.target.value);
    }

    handleNameChange(e) {
        cookie.set('name', e.target.value);
    }

    render() {
        var email = cookie.get('email');
        var name = cookie.get('name');
        return (
            <form id="SalesBack-form" className="row" onSubmit={this.handleSubmit.bind(this)}>
                <h1 className={this.state.message == '' ? 'hide' : ''}>{this.state.message}</h1>
                <div className={this.state.message == '' ? '' : 'hide'}>

                    <h3>
                        Заполните форму и мы вышлем Вам оптовый каталог в течение 5 минут!
                    </h3>

                    <div className="inputs">
                        <div className="form-row">
                            <label htmlFor="sbf_email">Введите свой e-mail</label>
                            <input id="sbf_email" type="text" onChange={this.handleEmailChange.bind(this)} name="SalesBackForm[email]"
                                   defaultValue={email} placeholder="example@example.ru" />
                            <ul className="errors"></ul>
                            <div className="hint">На эту почту мы отправим наш каталог</div>
                        </div>
                        <div className="form-row">
                            <label>Введите свой телефон</label>
                            <PhoneInput name="SalesBackForm[phone]" />
                            <ul className="errors"></ul>
                            <div className="hint">Мы убедимся что вы получили каталог</div>
                        </div>
                        <div className="form-row">
                            <input type="submit" value="Получить оптовый каталог" className="button" />
                        </div>

                        <div className="comment">Заполнение заявки Вас ни к чему не обязывает!</div>
                    </div>

                </div>
            </form>
        );
    }
}

class CategoriesSales extends Component {
    static defaultProps = {
        models: [],
        prefix: "",
        count: 5
    };

    componentWillMount() {
        const { mainActions } = this.props;
        mainActions.fetchMainIfNeeded();
    }

	render() {
        const sliderConfig = {
            dots: false,
            arrows: true,
            infinite: false,
            speed: 500,
            slidesToShow: 2,
            slidesToScroll: 2
        };

		const { categories } = this.props;

		var products = chunk(categories, this.props.count).map((models, i) => {
            var nodes = models.map((model, t) => (
                <li key={i + '_' + t}><Category model={model} /></li>
            ));

            return (
                <div className="category-list-container" key={'csales' + i}>
                    <Slider className='slider' {...sliderConfig}>{nodes}</Slider>
                    <hr/>
                    <a href="#" className='button' onClick={openModal.bind(this, <SalesBackForm />)}>
                        Смотреть все
                    </a>
                </div>
            );
        });

        return <div>{products}</div>;
	}
}

export default connect((state, props) => {
    return {
        ...state.main
    }
}, dispatch => {
    return {
        mainActions: bindActionCreators(mainActions, dispatch)
    };
})(CategoriesSales);