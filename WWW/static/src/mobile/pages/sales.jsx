import $ from 'jquery';
import React, { Component } from 'react';
import PhoneInput from 'inputs/phone_input';
import { cookie } from 'easy-storage';
import SalesContent from 'mobile/containers/sales_content';
import SalesForm from 'mobile/components/sales_form';
import Question from 'mobile/components/question';
import Reviews from 'mobile/components/reviews';
import CategoriesSales from 'mobile/containers/categories_sales';
import BestSales from 'shared/components/best_sales';
import api from 'lib/api';
import { formValidate, fixRelativeMediaUrls } from 'lib/utils';

export default class SalesHandler extends Component {
    state = {
        message: ''
    };

    handleSubmit(e) {
        e.preventDefault();
        e.stopPropagation();

        var $form = $(e.target).closest('form');

        api.post('/api/ko/sales', $form.serialize(), function (data) {
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    message: data['message']
                });
            }
        }.bind(this));
    }

    render() {
        let name = cookie.get('name');

        const { message } = this.state;

        return (
            <div className="delivery-page pages-page">
                <div className="row">
                    <div className="columns small-12">
                        <h1>Элитные меховые изделия оптом от производителя</h1>
                    </div>
                </div>

                <div className="extra-container">
                    <div className="row">
                        <div className="columns small-12">
                            <div className="extra">
                                <div className="extra-content">
                                    <p className="page-extra-title">Преимущества</p>
                                    <ul className="page-extra-list sales">
                                        <li className="page-extra-list-clothes">
                                            Опт от 3 изделий
                                        </li>
                                        <li className="page-extra-list-prepare">
                                            Покупайте не размерными рядами
                                        </li>
                                        <li className="page-extra-list-5000">
                                            Пошив по индивидуальным заказам любых размеров
                                        </li>
                                        <li className="page-extra-list-300">
                                            Вся наша продукция промаркирована RFID-чипами
                                        </li>
                                        <li className="page-extra-list-auto">
                                            Бесплатная упаковка и доставка до терминала транспортной компании
                                        </li>
                                    </ul>
                                </div>

                                <div className="extra-form">
                                    <p className="arrow"></p>
                                    <form onSubmit={this.handleSubmit.bind(this)} id="price-delivery-form">
                                        <h1 className={message == '' ? 'hide' : ''}>{message}</h1>
                                        <div className={message == '' ? '' : 'hide'}>
                                            <div className="row">
                                                <div className="columns small-12">
                                                    <p className="form-title">Получите доступ к оптовым ценам</p>
                                                    <p>+ подарок — систему «Увеличение прибыли магазина меховых изделий в 2,5 раза»</p>
                                                </div>
                                            </div>
                                            <div className="inputs">
                                                <div className="row">
                                                    <div className="columns small-6 first">
                                                        <input type="text" name="SalesForm[name]"
                                                               defaultValue={name} placeholder="Введите ваше имя"/>
                                                        <ul className="errors"></ul>
                                                    </div>
                                                    <div className="columns small-6 last">
                                                        <PhoneInput name="SalesForm[phone]"/>
                                                        <ul className="errors"></ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="buttons">
                                                <div className="row">
                                                    <div className="columns small-12">
                                                        <input type="submit" value="ПОЛУЧИТЬ" className="button"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="b-personal-data">
                                            <span className="b-personal-data__check">
                                                <input
                                                    className="b-personal-data__input" type="checkbox"
                                                    checked="checked"/>
                                            </span>
                                                Нажимая кнопку «Получить консультацию», вы принимаете
                                                <a className="b-personal-data__link"
                                                   href="/page/private"
                                                   target="_blank">"условия обработки персональных данных".</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <BestSales />
                <SalesForm />

                <div className="row">
                    <div className="columns medium-12 large-12">
                        <div className="product-list-container">
                            <h3 className="text-center category-list-container-title">Наш ассортимент</h3>
                            <CategoriesSales />
                        </div>
                    </div>
                </div>

                <SalesForm />

                <SalesContent />
                <SalesForm />

                <div className="row">
                    <div className="columns medium-12 large-12 work-container">
                        <h2>С кем мы хотим работать</h2>

                        <ul className="small-block-grid-2 work-list">
                            <li className="work-list-icon-sack">
                                <p className="title">Владельцы розничных магазинов меха и кожи</p>
                                <p>Вы сможете закупать уникальный и разнообразный
                                    ассортимент в одном месте.</p>
                            </li>
                            <li className="work-list-icon-shop">
                                <p className="title">Бизнесы смежных тематик</p>
                                <p>Выездная торговля, магазины одежды, модные бутики,
                                    свадебные салоны.</p>
                            </li>
                            <li className="work-list-icon-people">
                                <p className="title">Совместные закупки</p>
                                <p>Имеем большой опыт работы с организаторами
                                    совместных закупок, бронируем все нужные изделия.</p>
                            </li>
                        </ul>
                    </div>
                </div>

                <div className="how-work-container">
                    <div className="row">
                        <div className="columns medium-12 large-12">
                            <h2>Схема нашей работы</h2>
                            <ul className="small-block-grid-2 how-work-list">
                                <li className="img1">
                                    <p className="text">
                                        Вы оставляете бесплатную заявку
                                    </p>
                                </li>
                                <li className="img2">
                                    <p className="text">
                                        Мы высылаем Вам на почту оптовый каталог
                                        и полезную маркетинговую информацию
                                    </p>
                                </li>
                                <li className="img3">
                                    <p className="text">
                                        Выделяем Вам персонального менеджера,
                                        который консультирует Вас по всем вопросам
                                    </p>
                                </li>
                                <li className="img4">
                                    <p className="text">
                                        Вы оформляете заказ. Менеджер помогает
                                        Вам сформировать список
                                        необходимого товара.
                                    </p>
                                </li>
                                <li className="img5">
                                    <p className="text">
                                        Выставление счета. Оплата через банк, р/с.
                                    </p>
                                </li>
                                <li className="img6">
                                    <p className="text">
                                        Бесплатная упаковка и доставка до терминала транспортной компании, что позволяет экономить на каждой отправке до 1000 руб.
                                    </p>
                                </li>
                                <li className="img7">
                                    <p className="text">
                                        Доведение Вас до продажи всего
                                        закупленного товара. Получениет прибыли
                                    </p>
                                </li>
                                <li className="img8">
                                    <p className="text">
                                        Повторное обращение. Вы в статусе
                                        «постоянный партнер». Предоставление скидок, бонусов.
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="columns small-12">
                        <div className="conditions-container">
                            <h2>Наши первоначальные условия:</h2>
                            <ul className="small-block-grid-1 money-list">
                                <li>
                                    <p className="count">1</p>
                                    <p className="title">
                                        Минимальная партия от 3 изделий.
                                    </p>
                                </li>
                                <li>
                                    <p className="count">2</p>
                                    <p className="title">
                                        Продажа только с чипами в системе маркировка, предоплата 100% по безналичному расчёту.
                                    </p>
                                </li>
                                <li>
                                    <p className="count">3</p>
                                    <p className="title">
                                        Бесплатная упаковка и доставка до терминала транспортной компании.
                                    </p>
                                </li>
                                <li>
                                    <p className="count">4</p>
                                    <p className="title">
                                        Доставка от терминала за счёт заказчика, любой транспортной компанией.
                                    </p>
                                </li>
                            </ul>

                            <h2 className="second">Давайте вместе добиваться успеха!</h2>
                            <p className="deco-text">
                                Мы настроены исключительно на долгосрочное сотрудничество, поэтому для нас очень важно,
                                чтобы Вы всегда оставались довольны качеством и сервисом, и заказывали у нас снова и
                                снова.
                            </p>
                        </div>
                    </div>
                </div>

                <Question />
                <Reviews />
            </div>
        );
    }
}
