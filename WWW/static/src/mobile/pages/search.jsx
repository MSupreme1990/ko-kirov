import React, { Component, PropTypes } from 'react';
import { findDOMNode } from 'react-dom';
import Product from 'mobile/components/catalog/product';
import Loader from 'shared/components/loader';
import Paginator from 'shared/components/pager';
import shallowequal from 'shallowequal';
import { Link } from 'react-easy-router';

import * as productActions from 'actions/product';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class CatalogHandler extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    state = {
        loading: true,

        // Sidebar
        woman: undefined,
        man: {},

        // Main area
        category: [],

        // Products
        models: [],
        models_origin: [],
        group: [],

        // Filter params
        sizes: [],
        min_size: 0,
        min_size_value: 0,
        max_size: 0,
        max_size_value: 0,
        sort: {},

        // Error state
        error: false,

        // pagination
        currentPage: 1,
        perPage: 6,
        numPages: 1
    };

    componentWillMount() {
        const { query, filters, productActions } = this.props;
        productActions.search(query.q, filters);
    }

    componentWillReceiveProps(nextProps) {
        const { filters, query, productActions } = nextProps;
        if (shallowequal(this.props.filters, filters) === false) {
            productActions.search(query.q, filters);
        }
    }

    onPaginate(e, page) {
        e.preventDefault();

        const { query, params } = this.props;

        this.context.router.to('Search', params, {
            ...query,
            page
        });
    }

    renderProducts() {
        const { objects } = this.props;

        let nodes = objects.map(product => (
            <li key={'pr-item-' + product.id}>
                <Product product={product}/>
            </li>
        ));

        return <ul className="small-block-grid-2">{nodes}</ul>;
    }

    renderPager() {
        const { objects, query, meta } = this.props;

        if (objects.length == 0) {
            return null;
        }

        return (
            <div className="page-pager">
                <div className="row">
                    <div className="columns small-12">
                        <Paginator
                            page={Number(query.page || 1)}
                            prevText='<'
                            nextText='>'
                            numPages={meta.pages_count}
                            onClick={this.onPaginate.bind(this)}/>
                    </div>
                </div>
            </div>
        );
    }

    renderNextButton() {
        const { loading, objects, query, params, meta } = this.props;

        if (
            loading || 
            objects.length == 0 ||
            meta.page_count == 1 ||
            query.page >= meta.pages_count
        ) {
            return null;
        }

        let newQuery = {
            ...query,
            page: Number(query.page || 1) + 1
        };

        return (
            <div className="big-next-page">
                <div className="row">
                    <div className="columns small-12">
                        <Link to="Search" params={params} query={newQuery}>
                            Следующая страница</Link>
                    </div>
                </div>
            </div>
        );
    }

    handleSubmit(e) {
        e.preventDefault();

        let node = findDOMNode(this.refs.q);

        const { params, query } = this.props;

        this.context.router.to('Search', params, {
            ...query,
            q: node.value
        });
    }

    renderForm() {
        const { query } = this.props;

        return (
            <div className="row">
                <div className="columns small-12">
                    <form action="" className="search-form" onSubmit={this.handleSubmit.bind(this)}>
                        <h2>Поиск по запросу:</h2>
                        <input ref="q" type="text" defaultValue={query.q} placeholder="Поиск..."/>
                        <input type="submit" className="button" value="Поиск"/>
                    </form>
                </div>
            </div>
        );
    }

    render() {
        const { loading } = this.props;

        if (loading) {
            return <Loader />;
        }

        return (
            <div className="page-catalog">
                {this.renderForm()}

                {this.renderPager()}

                <div className="products">
                    <div className="row">
                        <div className="columns small-12">
                            {this.renderProducts()}
                        </div>
                    </div>
                </div>

                {this.renderNextButton()}
            </div>
        );
    }
}

export default connect((state, props) => {
    const { meta, objects, loading, error } = state.product.search;

    const { query } = props;

    return {
        meta,
        filters: {
            page: query['page'],
            q: query['q'],
            page_size: 10
        },
        objects,
        loading,
        error
    };
}, dispatch => {
    return {
        productActions: bindActionCreators(productActions, dispatch)
    };
})(CatalogHandler);