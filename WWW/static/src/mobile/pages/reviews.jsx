import $ from 'jquery';
import api from 'lib/api';
import { formValidate } from 'lib/utils';
import { cookie } from 'easy-storage';
import { chunk } from 'lib/utils';
import React, { Component } from 'react';
import ReviewVideo from 'shared/containers/review/review_video';
import ReviewsText from 'shared/containers/review/reviews_text';
import GiftCounter from 'mobile/components/gift_counter';
import CatalogLink from 'shared/components/catalog_link';
import Paginator from 'shared/components/pager';
import PhoneInput from 'inputs/phone_input';

export default class ReviewsHandler extends Component {
    currentPage = 1;
    maxPages = 1;
    numPages = 1;
    reviews = {
        perPage: 50
    };
    video = {
        perPage: 4
    };

    state = {
        reviews: {
            data: [],
            models: []
        },
        video: {
            data: [],
            models: []
        }
    };

    handleSubmit(e) {
        e.preventDefault();

        var $form = $('#review-form');

        api.post('/api/ko/reviews_create', $form.serialize(), data => {
            formValidate($form, data['errors']);
            if (data['success']) {
                $form.html('<h4>' + data['message'] + '</h4>');
            }
        });
    }

    handleEmailChange(e) {
        cookie.set('email', e.target.value);
    }

    handleNameChange(e) {
        cookie.set('name', e.target.value);
    }

    componentWillMount() {
        api.get("/api/ko/reviews", {
            unlimit: true
        }, data => {
            var chunkReviews = chunk(data.reviews, this.reviews.perPage),
                chunkVideo = chunk(data.video, this.video.perPage),
                reviews = {
                    data: chunkReviews,
                    models: chunkReviews[this.currentPage - 1]
                },
                video = {
                    data: chunkVideo,
                    models: chunkVideo[this.currentPage - 1]
                },
                r = {
                    reviews: reviews,
                    video: video
                };

            this.numPages = chunkReviews.length;

            this.setState({
                reviews: reviews,
                video: video
            });
        });
    }

    onPaginate (e, page) {
        this.currentPage = Number(page);
        this.setState({
            reviews: {
                models: this.state.reviews.data[this.currentPage - 1],
                data: this.state.reviews.data
            },
            video: {
                models: this.state.video.data[this.currentPage - 1],
                data: this.state.video.data
            }
        });

        $('html, body').animate({
            scrollTop: $('.reviews-full-container').offset().top
        }, 500);
    }

    renderReviews () {
        return <ReviewsText reviews={this.state.reviews.models.slice(this.currentPage, 10)} />;
    }

    renderVideo () {
        var reviewsNodes = null;
        if (this.state.video.models) {
            reviewsNodes = this.state.video.models.map(model => (
                <li key={'v' + model.id}>
                    <ReviewVideo key={model.id} model={model} />
                </li>
            ));
        }

        return (
            <ul className="small-block-grid-2">
                {reviewsNodes}
            </ul>
        );
    }

    render() {
        var name = cookie.get('name'),
            email = cookie.get('email'),
            reviews = this.renderReviews(),
            video = this.renderVideo();

        return (
            <div className="reviews-page-container">
                <div className="row">
                    <div className="columns small-12">
                        <h1>Отзывы наших Покупателей</h1>

                        <div className="reviews-full-container">
                            <div className="row">
                                <div className="columns small-12">
                                    <div className="video-container">
                                        {video}
                                    </div>

                                    {reviews}
                                    <Paginator
                                        page={Number(this.currentPage)}
                                        numPages={this.numPages}
                                        maxPages={this.maxPages}
                                        onClick={this.onPaginate.bind(this)} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="columns small-12">
                        <form id="review-form" method="post" onSubmit={this.handleSubmit.bind(this)}>
                            <h4>Оставьте свой отзыв</h4>
                            <div className="review-form-row form-row">
                                <input type="text" name="ReviewCreateForm[name]" onChange={this.handleNameChange.bind(this)} defaultValue={name} placeholder="Введите ваше имя" />
                                <ul className="errors"></ul>
                            </div>
                            <div className="review-form-row form-row">
                                <PhoneInput name="ReviewCreateForm[phone]" />
                                <ul className="errors"></ul>
                            </div>
                            <div className="review-form-row form-row">
                                <input type="text" name="ReviewCreateForm[email]" onChange={this.handleEmailChange.bind(this)} defaultValue={email} placeholder="Введите ваше e-mail" />
                                <ul className="errors"></ul>
                            </div>
                            <div className="review-form-row form-row">
                                <textarea name="ReviewCreateForm[content]" placeholder="Ваш отзыв"></textarea>
                                <ul className="errors"></ul>
                            </div>
                            <div className="review-form-row form-row">
                                <input type="submit" value="ОСТАВИТЬ ОТЗЫВ" className="button" />
                            </div>
                            <div className="b-personal-data">
                                <span className="b-personal-data__check"><input type="checkbox" checked="checked"/></span>
                                Передавая информацию сайту вы принимаете условия
                                <a className="b-personal-data__link"
                                   href="/page/private"
                                   target="_blank">"политики защиты персональных данных".</a>
                            </div>
                        </form>
                    </div>
                </div>
                <div className="row">
                    <div className="columns small-12">
                        <CatalogLink />
                    </div>
                </div>
                <GiftCounter />
            </div>
        );
    }
}
