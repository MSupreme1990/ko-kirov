import React from 'react';
import { Link } from 'react-easy-router';

export default props => {
    return (
        <div className="error-page">
            <h1>Страница не найдена</h1>
            <p>
                <Link to="Index">Перейти на главную &rarr;</Link>
            </p>
        </div>
    )
};