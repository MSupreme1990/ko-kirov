import React, { Component, PropTypes } from 'react';
import ActiveCategory from 'mobile/components/catalog/active_category';
import Product from 'mobile/components/catalog/product';
import Banner from 'shared/components/banner';
import Loader from 'shared/components/loader';
import SizeRange from 'shared/components/size_range';
import Paginator from 'shared/components/pager';
import shallowequal from 'shallowequal';
import { Link } from 'react-easy-router';

import * as productActions from 'actions/product';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class CatalogHandler extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    state = {
        loading: true,

        // Sidebar
        woman: undefined,
        man: {},

        // Main area
        category: [],

        // Products
        models: [],
        models_origin: [],
        group: [],

        // Filter params
        sizes: [],
        min_size: 0,
        min_size_value: 0,
        max_size: 0,
        max_size_value: 0,
        sort: {},

        // Error state
        error: false,

        // pagination
        currentPage: 1,
        perPage: 40,
        numPages: 1
    };

    componentWillMount() {
        const { params, filters, productActions } = this.props;
        productActions.fetchList(params.url, filters);
    }

    componentWillReceiveProps(nextProps) {
        const { filters, params, productActions } = nextProps;
        if (
            this.props.params.url != nextProps.params.url ||
            shallowequal(this.props.filters, filters) === false
        ) {
            productActions.fetchList(params.url, filters);
        }
    }

    onChangeSize(min_size, max_size) {
        const { params, query } = this.props;

        this.context.router.to(this.getRoute(), params, {
            ...query,
            min_size,
            max_size
        });
    }

    onPaginate(e, page) {
        e.preventDefault();

        this.context.router.to('CatalogList', this.props.params, {
            ...this.props.query,
            page
        });
    }

    handlerFilterKey(key, e) {
        e.preventDefault();

        const { query, params } = this.props;

        this.context.router.to(this.getRoute(), params, {
            ...query,
            order: query.order == key ? '-' + key : key
        });
    }

    renderFilterItem(key, name) {
        const { query } = this.props;

        let cls = '';
        if (query.order == key) {
            cls = 'asc';
        } else if (query.order == '-' + key) {
            cls = 'desc';
        }

        return <a href="#" className={cls}
                  onClick={this.handlerFilterKey.bind(this, key)}>{name}</a>;
    }

    renderFilter() {
        const { query } = this.props;

        return (
            <div className="product-filter-container">
                <div className="row">
                    <div className="columns small-12">
                        <p>Сортировать по:</p>
                        <ul className="filter-list">
                            <li>{this.renderFilterItem('is_popular', 'Популярности')}</li>
                            <li>{this.renderFilterItem('price', 'Цене')}</li>
                            <li>{this.renderFilterItem('discount_percentage', 'Скидке')}</li>
                            <li>{this.renderFilterItem('update', 'Обновлению')}</li>
                        </ul>
                    </div>

                    <div className="columns small-12">
                        {this.renderPageSize()}
                    </div>

                    <div className="columns small-12">
                        <div className="range-input">
                            <p>Размер:</p>
                            <SizeRange
                                min={Number(query.min_size)}
                                max={Number(query.max_size)}
                                onChange={this.onChangeSize.bind(this)}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderPageSize() {
        const { filters } = this.props;

        let steps = [40, 80, 100, 200],
            nodes = steps.map((size, i) => (
                <a key={'page-size' + size} href="#" onClick={this.handlePageSize.bind(this, size)}
                   className={(!filters.page_size && i === 0 || filters.page_size == size) ? 'active' : ''}>{size}</a>
            ));

        return (
            <div className="page-size-container">
                <div className="page-size">
                    <p className="filter-title">Показывать по:</p>
                    <p>{nodes}</p>
                </div>
            </div>
        );
    }

    getRoute() {
        return 'CatalogList';
    }

    handlePageSize(page_size, e) {
        e.preventDefault();

        this.context.router.to(this.getRoute(), this.props.params, {
            ...this.props.query, page_size, page: 1
        });
    }

    renderProducts() {
        const { objects, category } = this.props;

        let nodes = objects.map(product => (
            <li key={'pr-item-' + product.id}>
                <Product product={product} category={category}/>
            </li>
        ));

        return <ul className="small-block-grid-2">{nodes}</ul>;
    }

    renderPager() {
        const { query, meta } = this.props;

        return (
            <div className="page-pager">
                <div className="row">
                    <div className="columns small-12">
                        <Paginator
                            page={Number(query.page || 1)}
                            prevText='<'
                            nextText='>'
                            numPages={meta.pages_count}
                            onClick={this.onPaginate.bind(this)}/>
                    </div>
                </div>
            </div>
        );
    }

    renderNextButton() {
        const { query, params, meta, loading } = this.props;

        if (loading || query.page >= meta.pages_count || meta.pages_count == 1) {
            return null;
        }

        let newQuery = {
            ...query,
            page: Number(query.page || 1) + 1
        };

        return (
            <div className="big-next-page">
                <div className="row">
                    <div className="columns small-12">
                        <Link to="CatalogList" params={params} query={newQuery}>
                            Следующая страница</Link>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const { params, error, meta, category, loading } = this.props;

        if (error) {
            return <div>Страница не найдена</div>;
        }

        if (loading) {
            return <Loader />;
        }

        return (
            <div className="page-catalog">
                <ActiveCategory category={category}/>

                <div className="row">
                    <div className="columns small-12">
                        <Banner url={params.url}/>
                    </div>
                </div>

                {this.renderPager()}

                <div className="products">
                    <div className="row">
                        <div className="columns small-12">
                            {this.renderFilter()}
                            {this.renderProducts()}
                        </div>
                    </div>
                </div>

                {this.renderNextButton()}
            </div>
        );
    }
}

export default connect((state, props) => {
    const { category, meta, objects, loading, error } = state.product;

    const { query } = props;
    let fields = [
            'page', 'page_size', 'min_size',
            'max_size', 'order'
        ],
        filters = {};
    fields.map(field => {
        filters[field] = query[field];
    });

    return {
        meta,
        filters,
        category,
        objects,
        loading,
        error
    };
}, dispatch => {
    return {
        productActions: bindActionCreators(productActions, dispatch)
    };
})(CatalogHandler);