import React, { Component } from 'react';
import { openModal, nl2br } from 'lib/utils';
import { Link } from 'react-easy-router';
import Geo from 'shared/components/geo';
import { SalesBackForm } from 'shared/components/product/categories_sales';
import Loader from 'shared/components/loader';
import Page from 'shared/components/page';
import OfficeList from 'mobile/components/office_list';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as officeActions from 'actions/office';
import * as pageActions from 'actions/page';
import * as geoActions from 'actions/geo';

const URL = 'rekvizity';

class ContactsHandler extends Component {
    componentWillMount() {
        const { geoActions, pageActions } = this.props;
        geoActions.fetchList();
        pageActions.fetchViewIfNeeded(URL);
    }

    renderPage() {
        const { page, loading } = this.props;

        if (loading || !page) {
            return <Loader />;
        }

        return <Page h1={false} catalog_link={false} page={page}/>;
    }

    renderHeader() {
        const { loading, city } = this.props.geo;

        let cityName = city.name2 ? city.name2 : city.name;

        if (city.id == 1) {
            return <h1>Магазины в г. <a onClick={openModal.bind(this, <Geo />)}>{cityName}</a></h1>;
        } else {
            return (
                <h1>
                    Пункт выдачи в г. <a href="#" onClick={openModal.bind(this, <Geo />)}>{cityName}</a>
                    (для заказа в интернет-магазине)
                </h1>
            );
        }
    }

    render() {
        const {
            geo
        } = this.props;
        
        return (
            <div className="contacts-page">
                <div className="row contacts-container">
                    <div className="columns small-12">
                        {this.renderHeader()}

                        <div className="contacts-intro-container">
                            <div className="contacts-intro">
                                График работы менеджеров интернет магазина «Кожаная одежда» (по МСК): ежедневно с 9:00
                                до 22:00. Заявку на сайте Вы можете оставить <b>круглосуточно</b>, указав
                                удобное для Вас время для подтверждения заказа.
                            </div>
                        </div>
                    </div>
                </div>
                {!geo.loading && <OfficeList site_id={geo.site.id} />}
                {this.renderPage()}
            </div>
        );
    }
}

export default connect(state => {
    const { view, loading, error } = state.page.view;
    return {
        ...state.office,
        loading,
        error,
        page: view[URL],
        geo: state.geo
    }
}, dispatch => {
    return {
        pageActions: bindActionCreators(pageActions, dispatch),
        geoActions: bindActionCreators(geoActions, dispatch),
        officeActions: bindActionCreators(officeActions, dispatch)
    };
})(ContactsHandler);
