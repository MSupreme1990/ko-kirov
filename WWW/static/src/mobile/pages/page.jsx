import React, { Component } from 'react';
import Page from 'shared/components/page';
import GiftCounter from 'mobile/components/gift_counter';
import Reviews from 'mobile/components/reviews';
import Best from 'mobile/components/best';
import NotFound from 'mobile/pages/not_found';
import Loader from 'shared/components/loader';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as pageActions from 'actions/page';

class PageHandler extends Component {
    componentWillMount() {
        const { actions, params } = this.props;
        actions.fetchViewIfNeeded(params.url);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.params.url != nextProps.params.url) {
            const { actions, params } = nextProps;
            actions.fetchViewIfNeeded(params.url);
        }
    }

    render() {
        const { loading, page, error } = this.props;

        if (error) {
            return <NotFound />;
        }

        if (loading || !page) {
            return <Loader />;
        }

        return (
            <div className="pages-page">
                <div className="pages-content-container">
                    <Page page={page} />
                </div>
                <GiftCounter />
                <Best />
                <Reviews />
            </div>
        );
    }
}

export default connect((state, props) => {
    const { view, loading, error } = state.page.view;
    return {
        loading,
        error,
        page: view[props.params.url]
    }
}, dispatch => {
    return {
        actions: bindActionCreators(pageActions, dispatch)
    };
})(PageHandler);