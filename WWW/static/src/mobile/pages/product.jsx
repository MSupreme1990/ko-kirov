import React, { Component, PropTypes } from 'react';
import { Link } from 'react-easy-router';
import Slider from 'react-slick';
import ProductGift from 'mobile/components/product_gift';
import { openModal } from 'lib/utils';
import CartButton from 'mobile/inputs/cart_button';
import GiftCounter from 'mobile/components/gift_counter';
import Reviews from 'mobile/components/reviews';
import { ModalLink } from 'shared/components/modal';
import currency from 'shared/components/currency';
import Loader from 'shared/components/loader';
import QuickOrder from 'mobile/components/catalog/quick_order';
import DetectSize from 'mobile/containers/detect_size';
import Sizes from 'shared/components/sizes';
import RelatedSlider from 'mobile/components/catalog/slider_related';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as recentActions from 'actions/recent';
import * as productActions from 'actions/product';
import * as cartActions from 'actions/cart';

class ProductHandler extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    state = {
        model: null,
        category: null,
        cart: false,
        size: null
    };

    componentWillMount() {
        const { params, productActions } = this.props;
        productActions.fetchViewIfNeeded(params.url);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.params.url != nextProps.params.url) {
            const { productActions } = nextProps;
            productActions.fetchViewIfNeeded(nextProps.params.url);
        }
    }

    hndlCartClick(e) {
        this.setState({ cart: true });
    }

    renderContent() {
        const { product } = this.props;

        if (product.content) {
            return (
                <div className="content-container">
                    <div className="row">
                        <div className="columns small-12">
                            <div dangerouslySetInnerHTML={{__html: product.content}}></div>
                        </div>
                    </div>
                </div>
            );
        }

        return null;
    }

    render() {
        const { symbol, loading, error, product } = this.props;

        if (error) {
            return <NotFoundPage />;
        }

        if (loading || !product) {
            return <Loader />;
        }

        let currencyNode = null;
        if (currency.hasOldPrice(symbol.toLowerCase(), product)) {
            currencyNode = (
                <span className="product-item-old-price">
                    {currency.format(symbol.toLowerCase(), product, true)}
                </span>
            );
        }

        return (
            <div className="page-product">
                <div className="top-buttons">
                    <div className="row">
                        <div className="columns small-9 name-button-container">
                            <Link to="Catalog" params={{url: product.category.url}}
                                  className="name-to_catalog">
                                <span className="name">
                                    {product.name} №{product.code}
                                </span>
                            </Link>
                        </div>

                        <div className="columns small-3">
                            <CartButton className="small" product={product}
                                        onClick={this.hndlCartClick.bind(this)}/>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="columns small-12">
                        <Slider className='slider' {...{
                            dots: true,
                            arrows: false,
                            infinite: false,
                            speed: 500,
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }}>
                            {product.images.map((image, i) => (
                                <div className="image" key={'sl-im-' + i}>
                                    <a href={image.original} className="fancybox">
                                        <img src={image.thumb} alt={product.name}/>
                                    </a>
                                </div>
                            ))}
                        </Slider>
                    </div>
                </div>

                <div className="row">
                    <div className="columns small-12">
                        <p className="price">
                            <span className="price-title">Цена: </span>
                            <span className="product-item-price">{currency.format(symbol.toLowerCase(), product)} </span>
                            {currencyNode}
                        </p>
                    </div>
                </div>

                <div className="sizes-container">
                    <div className="row">
                        <div className="columns small-6">
                            <p>Размеры в наличии:</p>
                            <p>
                                <span className="detect-size-container">
                                    <a href="#" className="detect-size"
                                       onClick={openModal.bind(this, <DetectSize mobile={true} woman={product.woman == "1"}/>)}>
                                        Определить свой размер
                                    </a>
                                </span>
                            </p>
                        </div>
                        <div className="columns small-6">
                            <Sizes product={product} className="product-detail-sizes-list"/>
                        </div>
                    </div>
                </div>

                <QuickOrder product={product} product_id={product.id}/>
                {this.renderContent()}

                <div className="to_cart-container">
                    <div className="row">
                        <div className="columns small-2">&nbsp;</div>
                        <div className="columns small-8 container">
                            <CartButton
                                onClick={this.hndlCartClick.bind(this)}
                                product={product}/>
                        </div>
                        <div className="columns small-2">&nbsp;</div>
                    </div>
                </div>

                <ProductGift />
                <Reviews />
                <GiftCounter />

                <div className="related-container">
                    <div className="row">
                        <div className="columns small-12">
                            <RelatedSlider product={product}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect((state, props) => {
    const { loading, error, view } = state.product.view;
    return {
        symbol: state.currency.symbol,
        loading,
        error,
        product: view[props.params.url]
    };
}, dispatch => {
    return {
        cartActions: bindActionCreators(cartActions, dispatch),
        productActions: bindActionCreators(productActions, dispatch),
        recentActions: bindActionCreators(recentActions, dispatch)
    };
})(ProductHandler);
