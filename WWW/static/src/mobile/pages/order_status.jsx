import React, { Component } from 'react';
import $ from 'jquery';
import api from 'lib/api';
import { sendToAmo, formValidate } from 'lib/utils';
import PhoneInput from 'inputs/phone_input';

export default class OrderStatus extends Component {
    state = {
        message: ''
    };

    handleSubmit(e) {
        e.preventDefault();

        var $form = $('#order-status');

        api.post('/api/ko/question', $form.serialize(), data => {
            sendToAmo($form);
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    message: data['message']
                });
            }
        });
    }

    render() {
        return (
            <div className="order-status-container">
                <h1>Отследить заказ</h1>
                <ul className="order-status-list">
                    <li className="sdek">
                        <a href="http://www.edostavka.ru/track.html" target="_blank">Отследить</a>
                        <span>СДЭК</span>
                    </li>
                    <li className="spsr">
                        <a href="http://www.spsr.ru/ru/service/monitoring" target="_blank">Отследить</a>
                        <span>SPSR</span>
                    </li>
                    <li className="dpd">
                        <a href="http://www.dpd.ru/ols/trace2/eshop.do2" target="_blank">Отследить</a>
                        <span>DPD</span>
                    </li>
                    <li className="ems">
                        <a href="http://www.emspost.ru/ru/tracking/" target="_blank">Отследить</a>
                        <span>EMS (Почта России)</span>
                    </li>
                    <li className="line">
                        <a href="http://dellin.ru/tracker/" target="_blank">Отследить</a>
                        <span>Деловые линии</span>
                    </li>
                    <li className="pek">
                        <a href="https://kabinet.pecom.ru/status/" target="_blank">Отследить</a>
                        <span>ПЭК</span>
                    </li>
                    <li className="auto">
                        <a href="http://www.ae5000.ru/recipients/state/" target="_blank">Отследить</a>
                        <span>Автотрейдинг</span>
                    </li>
                </ul>
                <form id="order-status" onSubmit={this.handleSubmit.bind(this)}>
                    <h1 className={this.state.message == '' ? 'hide' : ''}>{this.state.message}</h1>
                    <div className={this.state.message == '' ? '' : 'hide'}>
                        <div className="row">
                            <div className="columns small-12">
                                <h4>Появились вопросы?</h4>
                                <p>Оставьте номер телефона и наш менеджер проконсультирует по всем интересующим
                                    вопросам</p>
                                <div className="form-row">
                                    <input type="text" name="QuestionForm[name]" placeholder="Введите ваше имя"/>
                                    <ul className="errors"></ul>
                                </div>
                                <div className="form-row">
                                    <PhoneInput name="QuestionForm[phone]"/>
                                    <ul className="errors"></ul>
                                </div>
                                <div className="form-row">
                                    <input type="submit" value="ОТПРАВИТЬ" className="button"/>
                                </div>
                                <div className="form-row">
                                    <div className="b-personal-data">
                                            <span className="b-personal-data__check">
                                                <input
                                                    className="b-personal-data__input" type="checkbox"
                                                    checked="checked"/>
                                            </span>
                                        Нажимая кнопку «Получить консультацию», вы принимаете
                                        <a className="b-personal-data__link"
                                           href="/page/private"
                                           target="_blank">"условия обработки персональных данных".</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}
