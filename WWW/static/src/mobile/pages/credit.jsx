import $ from 'jquery';
import React, { Component } from 'react';
import PhoneInput from 'inputs/phone_input';
import { cookie } from 'easy-storage';
import Reviews from 'mobile/components/reviews';
import CatalogLink from 'shared/components/catalog_link';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CreditContent from 'mobile/containers/credit_content';
import api from 'lib/api';
import { formValidate, fixRelativeMediaUrls } from 'lib/utils';

class CreditHandler extends Component {
    state = {
        message: ''
    };

    handleSubmit(e) {
        e.preventDefault();
        e.stopPropagation();

        var $form = $(e.target).closest('form');

        api.post('/api/ko/credit_possible', $form.serialize(), function (data) {
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    message: data['message']
                });
            }
        }.bind(this));
    }

    render() {
        let name = cookie.get('name');

        const { message } = this.state;

        return (
            <div className="delivery-page pages-page">

                <div className="extra-container">
                    <div className="row">
                        <div className="columns small-12">
                            <div className="extra">
                                <div className="extra-content">
                                    <p className="page-extra-title">Достоинства онлайн-кредитования</p>
                                    <ul className="page-extra-list">
                                        <li className="page-extra-list-mouse">
                                            <span className="multiline">
                                                Для оформления покупки и кредита Вам не нужно выходить из дома
                                            </span>
                                        </li>
                                        <li className="page-extra-list-time">
                                            <span className="multiline">
                                                В 95% случаев решения по онлайн кредиту принимается за 2 минуты
                                            </span>
                                        </li>
                                        <li className="page-extra-list-zero">
                                            <span className="multiline">
                                                0% по кредиту до даты первого платежа
                                            </span>
                                        </li>
                                        <li className="page-extra-list-calendar">
                                            <span className="multiline">
                                                Бесплатное досрочное погашение
                                            </span>
                                        </li>
                                    </ul>
                                </div>

                                <div className="extra-form">
                                    <p className="arrow"></p>
                                    <form onSubmit={this.handleSubmit.bind(this)} id="price-delivery-form">
                                        <h1 className={message == '' ? 'hide' : ''}>{message}</h1>
                                        <div className={message == '' ? '' : 'hide'}>
                                            <div className="row">
                                                <div className="columns small-12">
                                                    <p className="form-title">Рассчитать возможность оформления
                                                        кредита:</p>
                                                </div>
                                            </div>
                                            <div className="inputs">
                                                <div className="row">
                                                    <div className="columns small-6 first">
                                                        <input type="text" name="CreditPossibleForm[name]"
                                                               defaultValue={name} placeholder="Введите ваше имя"/>
                                                        <ul className="errors"></ul>
                                                    </div>
                                                    <div className="columns small-6 last">
                                                        <PhoneInput name="CreditPossibleForm[phone]"/>
                                                        <ul className="errors"></ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="buttons">
                                                <div className="row">
                                                    <div className="columns small-12">
                                                        <input type="submit" value="РАССЧИТАТЬ" className="button"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="b-personal-data">
                                            <span className="b-personal-data__check">
                                                <input
                                                    className="b-personal-data__input" type="checkbox"
                                                    checked="checked"/>
                                            </span>
                                                Нажимая кнопку «Получить консультацию», вы принимаете
                                                <a className="b-personal-data__link"
                                                   href="/page/private"
                                                   target="_blank">"условия обработки персональных данных".</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="delivery-content-container pages-content-container">
                    <div className="row">
                        <div className="columns small-12">
                            <CreditContent />
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="columns small-12">
                        <CatalogLink />
                    </div>
                </div>
                <Reviews />
            </div>
        );
    }
}

export default CreditHandler;
