import React, { PropTypes, Component } from 'react';
import { Link } from 'react-easy-router';
import GiftCounter from 'mobile/components/gift_counter';
import Categories from 'mobile/components/catalog/categories'
import SimpleSlider from 'shared/components/index/slider_simple'
import SalesForm from 'mobile/components/sales_form';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Popular from 'mobile/components/catalog/popular';

class IndexHandler extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    render() {
        return (
            <div className="index-page">
                <div className="row">
                    <SimpleSlider/>
                </div>

                <Categories/>

                <div className="row">
                    <div className="columns small-12">
                        <h1>Рекомендуем</h1>
                        <Popular count={2}/>
                    </div>
                </div>

                <div className="links-to-pages">
                    <div className="row">
                        <div className="columns small-12">
                            <ul className="small-block-grid-2">
                                <li>
                                    <Link to="Delivery">Доставка и оплата</Link>
                                </li>
                                <li>
                                    <Link to="Reviews">Отзывы</Link>
                                </li>
                                <li>
                                    <Link to="HowOrder">Как сделать заказ?</Link>
                                </li>
                                <li>
                                    <Link to="Page" params={{url:'garant'}}>Гарантийные условия</Link>
                                </li>
                                <li>
                                    <Link to="Page" params={{url:'rekvizity'}}>Связатся с нами</Link>
                                </li>
                                <li>
                                    <Link to="Contacts">Контакты</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <GiftCounter />
                <SalesForm />
            </div>
        );
    }
}

export default connect(state => {
    return {};
})(IndexHandler);