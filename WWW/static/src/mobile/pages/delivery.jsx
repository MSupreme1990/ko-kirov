import $ from 'jquery';
import React, { Component } from 'react';
import PhoneInput from 'inputs/phone_input';
import Reviews from 'mobile/components/reviews';
import Best from 'mobile/components/best';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CatalogLink from 'shared/components/catalog_link';
import api from 'lib/api';
import { formValidate, fixRelativeMediaUrls } from 'lib/utils';
import DeliveryContent from 'mobile/containers/delivery_content';

export default class DeliveryHandler extends Component {
    state = {
        message: ''
    };

    handleSubmit(e) {
        e.preventDefault();
        e.stopPropagation();

        var $form = $(e.target).closest('form');

        api.post('/api/ko/price-delivery', $form.serialize(), data => {
            formValidate($form, data['errors']);
            if (data['success']) {
                $form.html('<h1>' + data['message'] + '</h1>');
            }
        });
    }

    render() {
        return (
            <div className="delivery-page pages-page">

                <div className="extra-container">
                    <div className="row">
                        <div className="columns small-12">
                            <div className="extra">
                                <div className="extra-content">
                                    <p className="page-extra-title">Преимущества доставки</p>
                                    <ul className="page-extra-list">
                                        <li className="page-extra-list-world">
                                            Быстрая бесплатная доставка по всей<br/>
                                            России и СНГ.
                                        </li>
                                        <li className="page-extra-list-prepare">
                                            Примерка перед покупкой
                                        </li>
                                        <li className="page-extra-list-time">
                                            Удобное для Вас время доставки, курьер<br/>
                                            созванивается за 1 час до приезда.
                                        </li>
                                        <li className="page-extra-list-pay">
                                            Оплата при получении заказа наличным или<br/>
                                            возможность оформления в кредит.
                                        </li>
                                    </ul>
                                </div>

                                <div className="extra-form">
                                    <p className="arrow"></p>
                                    <form onSubmit={this.handleSubmit.bind(this)} id="price-delivery-form">
                                        <h1 className={this.state.message == '' ? 'hide' : ''}>{this.state.message}</h1>
                                        <div className={this.state.message == '' ? '' : 'hide'}>
                                            <div className="row">
                                                <div className="columns large-10">
                                                    <p className="form-title">Рассчитать срок и способ доставки:</p>
                                                </div>
                                            </div>
                                            <div className="inputs">
                                                <div className="row">
                                                    <div className="columns small-6 first">
                                                        <input type="text" name="PriceDeliveryForm[city]" placeholder="Введите ваш город" />
                                                        <ul className="errors"></ul>
                                                    </div>
                                                    <div className="columns small-6 last">
                                                        <PhoneInput name="PriceDeliveryForm[phone]" />
                                                        <ul className="errors"></ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="buttons">
                                                <div className="row">
                                                    <div className="columns large-10">
                                                        <input type="submit" value="РАССЧИТАТЬ ДОСТАВКУ" className="button" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="b-personal-data">
                                            <span className="b-personal-data__check">
                                                <input
                                                    className="b-personal-data__input" type="checkbox"
                                                    checked="checked"/>
                                            </span>
                                                Нажимая кнопку «Получить консультацию», вы принимаете
                                                <a className="b-personal-data__link"
                                                   href="/page/private"
                                                   target="_blank">"условия обработки персональных данных".</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="delivery-content-container pages-content-container">
                    <div className="row">
                        <div className="columns small-12">
                            <DeliveryContent />
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="columns small-12">
                        <CatalogLink />
                    </div>
                </div>
                <Best />
                <Reviews />
            </div>
        );
    }
}
