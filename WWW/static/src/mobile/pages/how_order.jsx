import $ from 'jquery';
import api from 'lib/api';
import { sendToAmo, formValidate } from 'lib/utils';
import React, { Component } from 'react';
import PhoneInput from 'inputs/phone_input';
import { cookie } from 'easy-storage';
import settings from 'config/settings';

export default class HowOrder extends Component {
    state = {
        pk: null,
        message: ""
    };

    handleSubmit(e) {
        e.preventDefault();

        var $form = $('#question-form-modal');

        api.post('/api/ko/question', $form.serialize(), data => {
            sendToAmo($form);
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    pk: data['pk']
                });
            }
        });
    }

    handleClose(e) {
        e.preventDefault();
        this.setState({ pk: null });
    }

    handleExtraSubmit(e) {
        e.preventDefault();

        var $form = $('#question-extra-form');

        api.post('/api/ko/question_extra', $form.serialize(), data => {
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    message: data['message']
                });

                // setTimeout(() => this.setState({ pk: null }), 3500);
            }
        });
    }

    handleNameChange(e) {
        cookie.set('name', e.target.value);
    }

    handleEmailChange(e) {
        cookie.set('email', e.target.value);
    }

    renderExtra() {
        if (this.state.pk == null) {
            return <span />;
        }
        var email = cookie.get('email');
        return (
            <div className={this.state.message == '' ? 'extra-form-popup' : 'extra-form-popup complete'}>
                <h1 className={this.state.message == '' ? 'hide' : ''}>{this.state.message}</h1>
                <a href="#" className="extra-form-close" onClick={this.handleClose.bind(this)}>&times;</a>
                <form id="question-extra-form" className={this.state.message == '' ? 'row' : 'row hide'}
                      onSubmit={this.handleExtraSubmit.bind(this)}>
                    <div className="row">
                        <div className="columns medium-12">
                            <p className="extra-form-title">Ваша заявка успешно отправлена. Уточните, пожалуйста,<br/>
                                следующие данные:</p>
                        </div>
                        <div className="columns medium-6">
                            <div>
                                <input type="text" onChange={this.handleEmailChange.bind(this)} name="QuestionForm[email]"
                                       defaultValue={email} placeholder="Введите ваш e-mail"/>
                                <ul className="errors"></ul>
                            </div>
                            <div>
                                <input type="hidden" name="QuestionForm[id]" value={this.state.pk}/>
                                <input type="submit" value="ОТПРАВИТЬ" className="button"/>
                            </div>
                        </div>
                        <div className="columns medium-6">
                            <textarea name="QuestionForm[question]" placeholder="Введите ваш вопрос"></textarea>
                            <ul className="errors"></ul>
                        </div>
                    </div>
                </form>
            </div>
        );
    }

    render() {
        var modal = this.renderExtra();
        var name = cookie.get('name');
        return (
            <div className="how-order-modal">
                <h1>Как сделать заказ?</h1>
                <ul className="small-block-grid-2 medium-block-grid-3 how-order-list">
                    <li className="how-order-item">
                        <img src={"/static/dist/images/main/how-order-product.png"}/>
                        <p className="how-order-product">
                            Вы выбираете изделие<br/>из каталога
                        </p>
                    </li>
                    <li className="how-order-item">
                        <img src={"/static/dist/images/main/how-order-request.png"}/>
                        <p className="how-order-request">
                            Оставляете бесплатную заявку
                        </p>
                    </li>
                    <li className="how-order-item">
                        <img src={"/static/dist/images/main/how-order-phone.png"}/>
                        <p className="how-order-phone">
                            С Вами связывается персональный<br/>
                            менеджер, подтверждает заявку и<br/>
                            консультирует по всем вопросам
                        </p>
                    </li>
                    <li className="how-order-item big">
                        <img src={"/static/dist/images/main/how-order-delivery.png"}/>
                        <p className="how-order-delivery">
                            Доставка заказа до двери для<br/>
                            бесплатной примерки
                        </p>
                    </li>
                    <li className="how-order-item big">
                        <img src={"/static/dist/images/main/how-order-payment.png"}/>
                        <p className="how-order-payment">
                            Расчет на месте удобным для Вас<br/>
                            способом. Вы в статусе постоянный<br/>
                            покупатель.
                        </p>
                    </li>
                </ul>
                <div className="question-container-modal">
                    <div className="row">
                        {modal}
                        <div className="columns small-12 medium-12">
                            <div className="left-side-container">
                                <h3>Появились вопросы?</h3>
                                <p>Оставьте номер телефона и наш менеджер примет
                                    Ваш заказ и проконсультирует по всем интересующим вопросам</p>
                            </div>
                            <form id="question-form-modal" onSubmit={this.handleSubmit.bind(this)}>
                                <div className="row">
                                    <div className="columns small-12">
                                        <div className="form-row">
                                            <input type="text" onChange={this.handleNameChange.bind(this)} defaultValue={name}
                                                   name="QuestionForm[name]" placeholder="Введите ваше имя"/>
                                            <ul className="errors"></ul>
                                        </div>
                                        <div className="form-row">
                                            <PhoneInput name="QuestionForm[phone]"/>
                                            <ul className="errors"></ul>
                                        </div>
                                        <div className="form-row">
                                            <input type="submit" value="ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ" className="button"/>
                                        </div>
                                        <div className="form-row">
                                            <div className="b-personal-data">
                                            <span className="b-personal-data__check">
                                                <input
                                                    className="b-personal-data__input" type="checkbox"
                                                    checked="checked"/>
                                            </span>
                                                Нажимая кнопку «Получить консультацию», вы принимаете
                                                <a className="b-personal-data__link"
                                                   href="/page/private"
                                                   target="_blank">"условия обработки персональных данных".</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
