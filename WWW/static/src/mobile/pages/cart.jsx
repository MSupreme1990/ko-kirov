import api from 'lib/api';
import React, { PropTypes, Component } from 'react';
import { cookie, local } from 'easy-storage';
import { dataLayer_push, formValidate, AdWords_send } from 'lib/utils';
import RenderProduct from 'mobile/components/cart/render-products';
import { Link } from 'react-easy-router';
import { ModalLink } from 'shared/components/modal';
import PhoneInput from 'inputs/phone_input';
import currency from 'shared/components/currency.js';
import PayLate from 'shared/components/PayLate';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as cartActions from 'actions/cart';

class CartHandler extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    state = {
        // step: cookie.get('step') || 1,
        step: 1,
        quick_complete: false,
        name: cookie.get('name') || '',
        email: cookie.get('email') || '',
        phone: cookie.get('phone') || '',
        comment: cookie.get('comment') || '',
        address: cookie.get('address') || '',
        phone_cls: '',
        delivery_index: 1,
        loading: false,
        error: null,
        delay_items: local.get('delay_products', []),
        total: 0,
        delay: false
    };


    componentWillMount() {
        const { cartActions } = this.props;
        cartActions.fetchList();
    }

    handleCommentChange(e) {
        this.setState({
            comment: e.target.value
        });
        cookie.set('comment', e.target.value);
    }

    handleValidateForm(e) {
        e.preventDefault();

        if (this.state.phone.length < 10) {
            this.setState({ phone_cls: 'error' });
            return;
        }

        let nstep = this.state.step + 1;

        this.setState({
            phone_cls: '',
            step: nstep
        });
        cookie.set('step', nstep);
    }

    handleNextStep(e) {
        e.preventDefault();

        this.setState({
            step: this.state.step + 1
        });
        cookie.set('step', this.state.step + 1);
    }

    handlePrevStep(e) {
        e.preventDefault();

        this.setState({
            step: this.state.step - 1
        });
        cookie.set('step', this.state.step - 1);
    }

    handleNameChange(e) {
        this.setState({
            name: e.target.value
        });
        cookie.set('name', e.target.value);
    }

    handleEmailChange(e) {
        this.setState({
            email: e.target.value
        });
        cookie.set('email', e.target.value);
    }

    handlePhoneChange(e) {
        let value = e.target.value.replace(/\D/g, '');

        this.setState({
            phone: value
        });
        cookie.set('phone', value);
    }

    handleComplete(e) {
        e.preventDefault();

        if (this.state.phone.length < 6) {
            this.setState({ phone_cls: 'error' });
            return;
        }

        api.post('/api/ko/cart-order', {
            name: this.state.name,
            email: this.state.email,
            phone: this.state.phone,
            comment: this.state.comment,
            address: this.state.address,
            type: this.state.delivery_index == 1 ? 'Курьер' : 'EMS',
            symbol: window.CURRENCY
        }, data => {
            this.setState({
                step: 4
            });

            dataLayer_push({ 'event': 'CartOrder', 'type': 'fullCheckout' });
        });
    }

    handleDelete(id, index, e) {
        e.preventDefault();

        if (confirm('Вы действительно хотите удалить данный товар из корзины?')) {
            const { cartActions } = this.props;
            cartActions.removeByIndex(index);
        }
    }

    handleMove(e) {
        e.preventDefault();

        if (confirm('Вы действительно хотите переместить данный товар из корзины в отложенные товары?')) {
            let $this = $(e.target).closest('a'),
                pk = $this.attr('data-id'),
                index = Number($this.closest('.cart-item').index());

            api.post('/api/ko/cart/delete/' + index, {}, data => {
                if (data.status) {
                    let delay_items = this.state.delay_items,
                        obj = _.find(this.state.items, t => {
                            return t.object.id == pk;
                        });
                    delay_items.push(obj);

                    this.setState({
                        delay_items: delay_items,
                        items: _.without(this.state.items, _.find(this.state.items, t => {
                            return t.object.id == pk;
                        }))
                    }, () => {
                        this.saveDelayItems();
                    });

                    $.mnotify(data.message);
                }
            });
        }
    }

    handleAddressChange(e) {
        this.setState({
            address: e.target.value
        });
        cookie.set('address', e.target.value);
    }

    handleDeleteDelay(e) {
        e.preventDefault();

        if (confirm('Вы действительно хотите удалить данный товар из отложенных товаров?')) {
            let $this = $(e.target).closest('a'),
                pk = $this.attr('data-id');

            this.setState({
                delay_items: this.state.delay_items.filter(t => {
                    return t.object.id != pk;
                })
            }, () => {
                this.saveDelayItems();
            });
        }
    }

    handleDelete(id, index, e) {
        e.preventDefault();

        if (confirm('Вы действительно хотите удалить данный товар из корзины?')) {
            const { cartActions } = this.props;
            cartActions.removeByIndex(index);
        }
    }

    handleCartSizeChange(id, size, index) {
        const { cartActions } = this.props;
        cartActions.changeSize(id, size, index);
    }

    renderStep1Header() {
        const { objects } = this.props;

        if (objects.length == 0) {
            return (
                <div className="row">
                    <div className="columns small-12">
                        <h5>Корзина пуста</h5>

                        <div className="to-catalog-link">
                            <Link to="Index">Вернутся в каталог для покупок</Link>
                        </div>
                    </div>
                </div>
            );
        }

        let nodes = objects.map((item, i) => {
            let size = item.data.size || '';
            let model = item.object;
            let sizeNodes = item.object.sizes.map((s, t) => {
                return <option key={'size_' + s + '_' + t} value={s}>{s}</option>;
            });
            return (
                <div className="cart-item" key={'cart-item-' + i}>
                    <div className="row"
                         key={'cart-product' + item.object.id + '-' + size}>
                        <div className="columns small-6">
                            <img src={item.object.image.image.thumb} alt=""/>
                        </div>
                        <div className="columns small-6">
                            <div className="product-info">
                                <div className="name">
                                    {item.object.name}
                                    <span className="code">№ {item.object.code}</span>
                                </div>

                                <div className="size">
                                    <div className="row">
                                        <div className="columns small-3">
                                            <span className="title">Размер:</span>
                                        </div>
                                        <div className="columns small-9">
                                            <select data-id={item.object.id}
                                                    defaultValue={size}
                                                    style={{ width: '100%' }}
                                                    onChange={e => this.handleCartSizeChange(model.id, e.target.value, i)}>
                                                <option>Выберите размер</option>
                                                {sizeNodes}
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div className="price-contaienr">
                                    Цена:
                                    <span className="price">
                                        {currency.format(this.props.symbol, item.object)}
                                    </span>
                                </div>
                            </div>
                            <p>&nbsp;</p>
                            <a href="#"
                               onClick={this.handleDelete.bind(this, model.id, i)}
                               data-id={item.object.id}>
                                Удалить из корзины
                            </a>
                        </div>
                    </div>
                </div>
            );
        });

        return (
            <div className="cart-container">
                <div className="products-container">
                    <div className="row">
                        <div className="columns small-12">{nodes}</div>
                    </div>
                </div>
                <div className="price-summary-container">
                    <span className="text">Сумма заказа:</span>
                    <span className="price">
                        {this.props.total}
                    </span>
                </div>
            </div>
        );
    }

    renderStep1Bottom() {
        const { objects } = this.props;

        if (objects.length == 0) {
            return null;
        }

        let phoneErrors = this.props.phone_cls == '' ? null : (
                <li>Укажите ваш телефон</li>
            );

        return (
            <div className="buttons-container">
                <div className="row">
                    <div className="columns small-6">
                        <PayLate
                            modalStyle={{
                                width: '100%',
                                height: '100%',
                                left: 0,
                                top: 0,
                                margin: 0
                            }}
                            className="b-button b-button_small"
                            products={objects}/>
                    </div>
                    <div className="columns small-6">
                        <ModalLink className="quick-cart-modal" linkClassName="b-button b-button_small"
                                   name="Заказать в 1 клик">
                            <div className="one-click-order">
                                <div className="row">
                                    <div className="columns small-12">
                                        <form
                                            ref="quick_form"
                                            className="quick-cart-order"
                                            onSubmit={this.handleSubmitQuick.bind(this)}>
                                            <h1 className={this.state.quick_complete ? 'text-center' : 'hide text-center'}>
                                                Заявка на звонок успешно отправлена. В ближайшее время с
                                                вами свяжется наш менеджер.
                                            </h1>

                                            <div className={this.state.quick_complete ? 'hide' : ''}>
                                                <h1>Заказать в 1 клик</h1>

                                                <p>Заполните эту форму и наш менеджер свяжется с Вами для
                                                    подтверждения заказа</p>

                                                <div className="form-row">
                                                    <input type="text"
                                                           name="OrderQuickForm[name]"
                                                           onChange={this.props.onNameChange}
                                                           placeholder="Введите Ваше имя"
                                                           defaultValue={this.state.name}/>
                                                    <ul className="errors"></ul>
                                                </div>
                                                <div className="form-row">
                                                    <PhoneInput type="text"
                                                                name="OrderQuickForm[phone]"
                                                                className={this.state.phone_cls}
                                                                placeholder="Введите Ваш телефон"
                                                                onChange={this.props.onPhoneChange}
                                                                defaultValue={this.state.phone}/>
                                                    <ul className="errors">{phoneErrors}</ul>
                                                    <ul className="errors"></ul>
                                                </div>
                                                <div className="form-row">
                                                    <input type="submit" value="ЗАКАЗАТЬ В 1 КЛИК"
                                                           className="button"/>
                                                </div>
                                                <div className="b-form-row">
                                                    <div className="b-personal-data">
                                                        <span className="b-personal-data__check"><input type="checkbox" checked="checked"/></span>
                                                        Передавая информацию сайту вы принимаете условия
                                                        <a className="b-personal-data__link"
                                                           href="/page/private"
                                                           target="_blank">"политики защиты персональных данных".</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </ModalLink>
                    </div>
                    <div className="columns small-12">
                        <br/>
                        <a className="b-button b-button_primary b-button_small" href="#" onClick={this.handleNextStep.bind(this)}>Оформить
                            заказ</a>
                    </div>
                </div>
            </div>
        );
    }

    saveDelayItems() {
        local.set('delay_products', _.uniq(this.state.delay_items, function (item, key, a) {
            return item.object.id;
        }));
    }

    handleDeliveryClick(e) {
        e.preventDefault();
        let $this = $(e.target).closest('li');
        this.setState({
            delivery_index: $this.index() + 1
        });
    }

    handleSubmitQuick(e) {
        e.preventDefault();

        let form = React.findDOMNode(this.refs.quick_form),
            $form = $(form),
            params = $form.serialize();

        api.post('/api/ko/cart-order-quick', params, data => {
            formValidate($form, data['errors']);
            if (data['status']) {
                this.setState({
                    quick_complete: true
                });

                dataLayer_push({ 'event': 'CartOrder', 'type': 'oneClick' });
            }
        });
    }

    render() {
        let content = '';

        const { symbol, loading, total, objects } = this.props;

        if (loading) {
            return <div>Загрузка...</div>;
        }

        let phoneErrors = this.state.phone_cls == '' ? null : (
                <li>Укажите ваш телефон</li>
            );

        switch (this.state.step) {
            case 2:
                content = (
                    <div className="step2">
                        <div className="row">
                            <div className="columns small-12">
                                <h1>Введите контактные данные для заказа</h1>
                            </div>
                        </div>

                        <div className="row">
                            <div className="columns small-12">
                                <div className="form-container">
                                    <form className="order-form">
                                        <div className="row">
                                            <div className="columns small-12">
                                                <div className="form-row">
                                                    <input
                                                        defaultValue={this.state.name}
                                                        onChange={this.props.onNameChange}
                                                        placeholder="Введите Ваши фамилию, имя, отчество"
                                                        name="OrderForm[name]"
                                                        type="text"/>
                                                    <ul className="errors"></ul>
                                                </div>

                                                <div className="form-row">
                                                    <PhoneInput
                                                        defaultValue={this.state.phone}
                                                        onChange={this.props.onPhoneChange}
                                                        className={'phone ' + this.state.phone_cls}
                                                        placeholder="Введите Ваш телефон: +7 (  ) ___ - __ - __"
                                                        name="OrderForm[phone]"
                                                        type="text"/>
                                                    <ul className="errors">{phoneErrors}</ul>
                                                </div>

                                                <div className="form-row">
                                                    <input
                                                        defaultValue={this.props.email}
                                                        onChange={this.props.onEmailChange}
                                                        placeholder="Введите Ваш e-mail" name="OrderForm[email]"
                                                        type="text"/>
                                                    <ul className="errors"></ul>
                                                </div>
                                                <div className="form-row">
                                                <textarea
                                                    defaultValue={this.props.comment}
                                                    onChange={this.props.onCommentChange}
                                                    name="OrderForm[comment]"
                                                    placeholder="Ваш комментарий: объем груди, талии, бедер (см)"
                                                    rows="10"></textarea>
                                                    <ul className="errors"></ul>
                                                </div>

                                                <div className="buttons-container">
                                                    <div className="row">
                                                        <div className="columns small-6">
                                                            <a className="prev-button" href="#"
                                                               onClick={this.handlePrevStep.bind(this)}>Назад</a>
                                                        </div>
                                                        <div className="columns small-6">
                                                            <a className="next-button" href="#"
                                                               onClick={this.handleNextStep.bind(this)}>Продолжить</a>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="columns small-12">
                                <RenderProduct
                                    symbol={symbol}
                                    objects={objects}
                                    total={total}
                                />
                            </div>
                        </div>
                    </div>
                );
                break;

            case 3:
                content = (
                    <div className="step3">
                        <div className="row">
                            <div className="columns small-12">
                                <h1>Выберите способ доставки</h1>
                            </div>
                        </div>

                        <div className="form-container">
                            <div className="row">
                                <div className="columns small-12">
                                    <div className="delivery-container">
                                        <ul className="delivery-column">
                                            <li onClick={this.handleDeliveryClick.bind(this)}
                                                className={this.state.delivery_index == 1 ? 'active' : ''}>
                                                Доставка курьером
                                            </li>
                                            <li onClick={this.handleDeliveryClick.bind(this)}
                                                className={this.state.delivery_index == 2 ? 'active' : ''}>
                                                <span className="text">Доставка EMS почта <br/> России</span>
                                            </li>
                                        </ul>
                                        <div className="delivery-content">
                                            <div
                                                className={"delivery-content-pane " + (this.state.delivery_index == 1 ? '' : 'hide')}>
                                                <h1>Заказать доставку курьером</h1>

                                                <form className="delivery-form">
                                                    <div className="form-row">
                                                        <label htmlFor="address">Введите адрес доставки:</label>
                                                        <textarea
                                                            defaultValue={this.state.address}
                                                            className={this.props.address_cls}
                                                            onChange={this.handleAddressChange.bind(this)} type="text"
                                                            id="address" name="address"
                                                            placeholder="Например: 610000, Россия, Кировская область, г. Киров, ул. Архитектора, д. 102, кв. 401"
                                                            rows="2"></textarea>
                                                    </div>
                                                </form>
                                                <p>Быстрая, бесплатная доставка в любое удобное для Вас место для
                                                    примерки
                                                    (обычно домой
                                                    или на работу). Доставка осуществляется в рабочие дни с 9:00 до
                                                    18:00.</p>

                                                <p>Оплата наличными после примерки. Когда приезжает курьер, у Вас есть
                                                    20 минут
                                                    на
                                                    примерку изделия и принятие окончательного решения.</p>

                                                <p>Если товар не подошёл, Вы можете сразу вернуть его курьеру.</p>
                                            </div>
                                            <div
                                                className={"delivery-content-pane " + (this.state.delivery_index == 2 ? '' : 'hide')}>
                                                <h1>Заказать доставку EMS почты России</h1>

                                                <form className="delivery-form">
                                                    <div className="form-row">
                                                        <label htmlFor="address">Введите адрес доставки:</label>
                                                        <textarea
                                                            defaultValue={this.state.address}
                                                            className={this.state.address_cls}
                                                            onChange={this.handleAddressChange.bind(this)}
                                                            type="text" id="address"
                                                            name="address"
                                                            placeholder="Например: 610000, Россия, Кировская область, г. Киров, ул. Архитектора, д. 102, кв. 401"
                                                            rows="2"></textarea>
                                                    </div>
                                                </form>
                                                <p>Заказать доставку ЕМС почта России (Доставка воздушным транспортом).
                                                    Быстрая,
                                                    бесплатная доставка авиационным транспортом Почты России.</p>

                                                <p>Преимущество данного способа доставки заключается в том, что Вы в
                                                    любое
                                                    удобное для
                                                    Вас время можете получить Ваш заказ в ближайшем почтовом отделении,
                                                    предварительно
                                                    вскрыв посылку и примерив.</p>

                                                <p>Оплата наличными.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div className="buttons-container">
                            <div className="row">
                                <div className="columns small-6">
                                    <a className="prev-button" href="#" onClick={this.handlePrevStep.bind(this)}>
                                        Назад</a>
                                </div>
                                <div className="columns small-6">
                                    <a className="next-button" href="#" onClick={this.handleComplete.bind(this)}>
                                        Завершить оформление заказа</a>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="columns small-12">
                                <RenderProduct
                                    symbol={symbol}
                                    objects={objects}
                                    total={total}
                                />
                            </div>
                        </div>

                    </div>
                );
                break;

            case 4:
                content = <h1>Спасибо! Ваша заявка отправлена, в ближайшее время с вами свяжется наш менеджер</h1>;
                break;

            default:
                content = (
                    <div className="step1">
                        <div className="row">
                            <div className="columns small-12">
                                <h1>Корзина</h1>
                            </div>
                        </div>

                        {this.renderStep1Header()}

                        {this.renderStep1Bottom()}

                    </div>
                );
        }

        return <div className="cart-page">{content}</div>;
    }
}

export default connect(state => {
    const { total, deffered, loading, objects } = state.cart;

    return {
        total,
        deffered,
        loading,
        objects,
        symbol: state.currency.symbol
    }
}, dispatch => {
    return {
        cartActions: bindActionCreators(cartActions, dispatch)
    };
})(CartHandler);
