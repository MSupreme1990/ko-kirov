import $ from 'jquery';
import 'plugins/social-likes';
import 'plugins/jquery.mnotify';
import fb from 'fancybox';
fb($);

$(() => {
    $(document).on('click', '.fancybox, [rel="lightbox"]', e => {
        e.preventDefault();

        let $this = $(e.target).closest('a');

        $.fancybox.open([{
                type: 'image',
                href : $this.attr('href'),
                title : ''
        }], {
            openEffect: 'elastic',
            closeEffect: 'elastic',
            nextEffect: 'fade',
            openSpeed: 300,
            padding: 0,
            closeSpeed: 200,
            helpers: {
                title: null,
                buttons: {}
            }
        });
    });
});

import React from 'react';
import { render } from 'react-dom';
import store from 'store';
import { Provider } from 'react-redux';
import routes from './routes';
import { Router } from 'react-easy-router';
import history from 'historystore';
import * as modalActions from 'actions/modal';

history.listen(() => {
    store.dispatch(modalActions.close());
    $('html, body').animate({
        scrollTop: 0
    }, 650);
});
let fallback = (props) => <div>Страница не найдена</div>;

render((
    <Provider store={store}>
        <Router history={history} fallback={fallback} routes={routes}/>
    </Provider>
), document.getElementById('app'));
