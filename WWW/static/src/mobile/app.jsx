import React, { Component, PropTypes } from 'react';
import Menu from 'mobile/components/header/menu'
import { Ga, Ya, AdWords } from 'shared/components/analytics';
import Footer from 'mobile/components/footer';
import Modal from 'mobile/components/global_modal';

export default class App extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    getChildren() {
        return this.props.children;
    }

    getChildren() {
        return React.cloneElement(this.props.children, {

        });
    }

    render() {
        return (
            <div>
                <Menu />
                <div id="wrapper">
                    <div className="content_container">
                        {this.getChildren()}
                    </div>
                </div>
                <Ya id='32044066'/>
                <AdWords id='955403784'/>
                <Modal />
                <Footer />
            </div>
        );
    }
}