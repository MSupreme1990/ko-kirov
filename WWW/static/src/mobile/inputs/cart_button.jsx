import React, { Component, PropTypes } from 'react';
import { Link } from 'react-easy-router';

import * as cartActions from 'actions/cart';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class CartButton extends Component {
    static propTypes = {
        product: PropTypes.object.isRequired
    };

    static defaultProps = {
        className: ''
    };

    handleOnClick(e) {
        e.preventDefault();

        const { cartActions, product, size, onClick } = this.props;
        cartActions.addToCart(product.id, size);

        if (onClick) {
            onClick(e);
        }
    }

    render() {
        const { in_cart, className } = this.props;

        if (in_cart) {
            return (
                <div className={'link-add-cart-container ' + className}>
                    <Link to="Cart" className={'link-add-cart to-cart ' + className}>
                        <i></i>
                        <span className="text">
                            Перейти в корзину
                        </span>
                    </Link>
                </div>
            );
        }

        return (
            <div className={'link-add-cart-container ' + className}>
                <a className={'link-add-cart ' + className}
                   href="#"
                   onClick={this.handleOnClick.bind(this)}>
                    <i></i>
                    <span className="text">Добавить в корзину</span>
                </a>
            </div>
        );
    }
}

export default connect((state, props) => {
    return {
        in_cart: state.cart.in_cart.indexOf(props.product.id) > -1
    };
}, dispatch => {
    return {
        cartActions: bindActionCreators(cartActions, dispatch)
    };
})(CartButton);