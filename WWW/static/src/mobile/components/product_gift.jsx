import React, { Component } from 'react';

export default class ProductGift extends Component {
    render() {
        return (
            <div className="product-gift-container">
                <div className="row">
                    <div className="columns small-12">
                        <h2>Вместе с изделием Вы получаете</h2>
                    </div>
                    <div className="columns small-6">
                        <div className="product-gift-item">
                            <p className="payment">
                                Товарный чек — гарантийный талон
                            </p>
                        </div>
                    </div>
                    <div className="columns small-6">
                        <div className="product-gift-item">
                            <p className="remember">
                                Памятку по уходу за мехом
                            </p>
                        </div>
                    </div>
                    <div className="columns small-6">
                        <div className="product-gift-item">
                            <p className="garant">
                                Гарантию качества сроком на один год
                            </p>
                        </div>
                    </div>
                    <div className="columns small-6">
                        <div className="product-gift-item">
                            <p className="bag">
                                Фирменный чехол-сумку, обработанный антимольной пропиткой
                            </p>
                        </div>
                    </div>
                    <div className="columns small-3">&nbsp;</div>
                    <div className="columns small-6">
                        <div className="product-gift-item">
                            <p className="discount">
                                Дисконтную карту
                            </p>
                        </div>
                    </div>
                    <div className="columns small-3">&nbsp;</div>
                </div>
            </div>
        )
    }
};