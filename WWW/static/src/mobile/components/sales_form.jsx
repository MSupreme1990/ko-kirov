import $ from 'jquery';
import api from 'lib/api';
import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import PhoneInput from 'inputs/phone_input';
import { cookie } from 'easy-storage';
import { dataLayer_push, sendToAmo, formValidate } from 'lib/utils';

class SalesForm extends Component {
    state = {
        pk: null, 
        message: ""
    };

    handleSubmit(e) {
        e.preventDefault();

        var $form = $('#sales-form');

        api.post('/api/ko/sales', $form.serialize(), function (data) {
            formValidate($form, data['errors']);
            if (data['success']) {
                // google tag manager
                dataLayer_push({
                    'event': 'submit',
                    'action': 'gift'
                });

                this.setState({
                    message: data['message']
                });
            }
        }.bind(this));
    }

    handleNameChange(e) {
        cookie.set('name', e.target.value);
    }

    render() {
        var name = cookie.get('name');
        return (
            <div className="sales-form-container">
                <div className="sales-form-header">
                    <div className="row">
                        <div className="columns small-12">
                            <h4>Получите доступ к оптовым ценам</h4>
                            <p>+ БЕСПЛАТНО проверенную систему «Увеличение прибыли магазина меховых изделий в 2,5 раза»</p>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="columns small-12">
                        <h1 className={this.state.message == '' ? 'hide' : ''}>{this.state.message}</h1>
                        <div className={this.state.message == '' ? '' : 'hide'}>
                            <form ref="form" id="sales-form"
                                    onSubmit={this.handleSubmit.bind(this)}>
                                <div className="columns small-6 form-row">
                                    <input type="text" onChange={this.handleNameChange.bind(this)}
                                           name="SalesForm[name]"
                                           defaultValue={name} placeholder="Введите ваше имя"/>
                                    <ul className="errors"></ul>
                                </div>
                                <div className="columns small-6 form-row">
                                    <PhoneInput name="SalesForm[phone]"/>
                                    <ul className="errors"></ul>
                                </div>
                                <div className="columns small-12">
                                    <input type="submit" value="ПОЛУЧИТЬ" className="button"/>
                                </div>
                                <div className="columns small-12">
                                    <div className="b-personal-data">
                                            <span className="b-personal-data__check">
                                                <input
                                                    className="b-personal-data__input" type="checkbox"
                                                    checked="checked"/>
                                            </span>
                                        Нажимая кнопку «Получить консультацию», вы принимаете
                                        <a className="b-personal-data__link"
                                           href="/page/private"
                                           target="_blank">"условия обработки персональных данных".</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SalesForm;
