import $ from 'jquery';
import Timer from 'shared/components/timer';
import { findDOMNode } from 'react-dom';
import api from 'lib/api';
import { cookie } from 'easy-storage';
import React from 'react';
import PhoneInput from 'inputs/phone_input';
import { Link } from 'react-easy-router';
import { dataLayer_push, sendToAmo, formValidate } from 'lib/utils';

var GiftCounter = React.createClass({
    contextTypes: {
        router: React.PropTypes.object
    },
    getInitialState: function () {
        return {
            pk: null,
            message: ""
        };
    },
    handleClose: function (e) {
        e.preventDefault();
        this.setState({ pk: null });
    },
    handleExtraSubmit: function (e) {
        e.preventDefault();

        let form = findDOMNode(this.refs.extra);
        var $form = $(form);

        api.post('/api/ko/gift_extra', $form.serialize(), function (data) {
            sendToAmo($form);
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    message: data['message']
                });

                setTimeout(function () {
                    this.setState({ pk: null });
                }.bind(this), 2500);
            }

            if (data['code']) {
                this.context.router.to("Coupon", { 'code': data['code'] }, {});
            }
        }.bind(this));
    },
    handleEmailChange: function (e) {
        cookie.set('email', e.target.value);
    },
    renderExtra: function () {
        if (this.state.pk == null) {
            return <span />;
        }
        var email = cookie.get('email');
        return (
            <div className={this.state.message == '' ? 'extra-form-popup' : 'extra-form-popup complete'}>
                <h1 className={this.state.message == '' ? 'hide' : ''}>{this.state.message}</h1>
                <a href="#" className="extra-form-close" onClick={this.handleClose}>&times;</a>
                <form ref="extra" className={this.state.message == '' ? '' : 'hide'}
                      onSubmit={this.handleExtraSubmit}>
                    <div className="row">
                        <div className="columns small-12">
                            <p className="extra-form-title">Заявка на подарок успешно отправлена. Для того чтобы
                                распечатать купон на скидку, введите следующие данные:</p>

                            <div className="form-row">
                                <input type="text" onChange={this.handleEmailChange} name="GiftForm[email]"
                                       defaultValue={email} placeholder="Введите ваш e-mail"/>
                                <ul className="errors"></ul>
                            </div>
                            <div className="form-row">
                                <textarea name="GiftForm[question]" placeholder="Введите ваш вопрос"></textarea>
                                <ul className="errors"></ul>
                            </div>
                            <div className="form-row">
                                <input type="hidden" name="GiftForm[id]" value={this.state.pk}/>
                                <input type="submit" value="ОТПРАВИТЬ" className="button"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    },
    handleSubmit: function (e) {
        e.preventDefault();

        let form = findDOMNode(this.refs.form);
        var $form = $(form);

        api.post('/api/ko/gift', $form.serialize(), data => {
            formValidate($form, data['errors']);
            if (data['success']) {
                // google tag manager
                dataLayer_push({
                    'event': 'submit',
                    'action': 'gift'
                });

                this.setState({
                    pk: data['pk']
                });
            }
        });
    },
    handleNameChange: function (e) {
        cookie.set('name', e.target.value);
    },
    render: function () {
        var modal = this.renderExtra();
        var name = cookie.get('name');
        return (
            <div className="gift-container">
                <div className="gift-counter-container">
                    <div className="row">
                        <div className="columns small-12">
                            <h3>Получите фирменный чехол - сумку в подарок!</h3>
                            {modal}
                            <div className="gift-counter">
                                <div className="gift-block">
                                    <Timer />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <form ref="form" onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="columns small-6">
                            <div className="form-row">
                                <input type="text" onChange={this.handleNameChange} name="GiftForm[name]"
                                       defaultValue={name} placeholder="Введите ваше имя"/>
                                <ul className="errors"></ul>
                            </div>
                        </div>

                        <div className="columns small-6">
                            <div className="form-row">
                                <PhoneInput name="GiftForm[phone]"/>
                                <ul className="errors"></ul>
                            </div>
                        </div>
                        <div className="columns small-12">
                            <div className="text-center">
                                <input type="submit" value="ПОЛУЧИТЬ ПОДАРОК" className="button"/>
                            </div>
                        </div>
                        <div className="columns small-12">
                            <div className="b-personal-data">
                                            <span className="b-personal-data__check">
                                                <input
                                                    className="b-personal-data__input" type="checkbox"
                                                    checked="checked"/>
                                            </span>
                                Нажимая кнопку «Получить консультацию», вы принимаете
                                <a className="b-personal-data__link"
                                   href="/page/private"
                                   target="_blank">"условия обработки персональных данных".</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
});

export default GiftCounter;
