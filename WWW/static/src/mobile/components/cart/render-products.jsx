import React, { Component } from 'react';
import { Link } from 'react-easy-router';
import { ModalLink } from 'shared/components/modal';
import currency from 'shared/components/currency.js';

export default class Step2 extends Component {
    static contextTypes = {
        router: React.PropTypes.object
    };

    static propTypes = {
        objects: React.PropTypes.array.isRequired,
        symbol: React.PropTypes.string.isRequired
    };

    renderNodes() {
        const { symbol, objects } = this.props;

        if (objects.length == 0) {
            return (
                <div className="row">
                    <div className="columns small-12">
                        <h5>Корзина пуста</h5>
                    </div>
                </div>
            );
        }

        let nodes = objects.map(item => {
            let size = item.data.size || '-';
            return (
                <div className="cart-item">
                    <div className="row"
                         key={'cart-product' + item.object.id + '-' + size}>
                        <div className="columns small-6">
                            <img src={item.object.image.image.thumb} alt=""/>
                        </div>
                        <div className="columns small-6">
                            <div className="product-info">
                                <div className="name">
                                    <Link to={'/catalog/' +item.object.url}>
                                        {item.object.name}
                                        <span className="code">№ {item.object.code}</span>
                                    </Link>
                                </div>
                                <div className="size">
                                    <div className="row">
                                        <div className="columns small-6">
                                            <span className="title">Размер:</span>
                                        </div>
                                        <div className="columns small-6">
                                            <span className="sizeValue">{size}</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="price-contaienr">
                                    Цена: <span className="price">{currency.format(symbol, item.object)}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        });

        return (
            <div className="cart-container">
                <div className="products-container">
                    <div className="row">
                        <div className="columns small-12">
                            {nodes}
                        </div>
                    </div>
                </div>

                <div className="price-summary-container">
                    <span className="text">Сумма заказа:</span>
                    <span className="price">
                        {this.props.total}
                    </span>
                </div>
            </div>
        );
    }

    render() {
        return <div className="cart-products">{this.renderNodes()}</div>;
    }
};
