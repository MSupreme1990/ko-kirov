import $ from 'jquery';
import Feedback from 'shared/components/feedback';
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as officeActions from 'actions/office';
import * as geoActions from 'actions/geo';
import Loader from 'shared/components/loader';
import Map from 'shared/containers/map';
import { nl2br, openModal } from 'lib/utils';
import { SalesBackForm } from 'shared/components/product/categories_sales';
import { Link } from 'react-easy-router';
import Error from 'shared/containers/error';

class OfficeList extends Component {
    componentWillMount() {
        const { geo, officeActions } = this.props;
        officeActions.fetchListIfNeeded(geo.site.id);
    }

    handleOpenMarker(map, id, e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: $("#" + map).offset().top
        }, 500);

        let marker = this.refs[map].getMarker(id);
        if (marker) {
            marker.openPopup();
        }
    }

    renderMain(objects = [], map = null, params = {}) {
        let officeNodes = objects.map(model => {
            let imageContainer = null;
            if (model.images.length > 0) {
                let images = model.images.map(image => (
                    <li key={'cimg' + image.id}>
                        <a className="fancybox" rel={model.id} href={image.image.original}>
                            <img src={image.image.thumb}/>
                        </a>
                    </li>
                ));

                imageContainer = (
                    <div className="map-images">
                        <ul className="map-images-list small-block-grid-3">{images}</ul>
                    </div>
                );
            }

            return (
                <li key={'of' + model.id}>
                    <div className="map-office-list">
                        <div className="row">
                            <div className="columns small-12">
                                <div className="map-icon-map office-icon">
                                    <p>{model.name}</p>

                                    <p className="map-link">
                                        <a onClick={this.handleOpenMarker.bind(this, map, model.id)}
                                           href="#">Показать на карте</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="columns small-6">
                                <div className="map-icon-phone office-icon">{model.phone}</div>
                            </div>
                            <div className="columns small-6">
                                <div className="map-icon-worktime office-icon"
                                     dangerouslySetInnerHTML={{__html: nl2br(model.worktime)}}></div>
                            </div>
                        </div>
                        {imageContainer}
                    </div>
                </li>
            );
        });

        let offices = objects.length == 0 ? (
            <ul className="address-list">
                <li className="empty">Адреса отсутствуют</li>
            </ul>
        ) : <ul className="address-list">{officeNodes}</ul>;

        return (
            <div className="row">
                <div className="columns small-12">
                    <div className="map-container offices-container">
                        {offices}
                        <Map lat={params.lat} lng={params.lng} zoom={params.zoom}
                             ref={map} id={map} objects={objects}/>
                    </div>
                </div>
            </div>
        );
    }

    renderExtra(objects, map, params = {}) {
        const { geo } = this.props;

        let extraMap = '';
        if (geo.site.id != 1) {
            extraMap = (
                <div>
                    <h2 className="text-center">Центральные магазины "Кожанная Одежда" в Кирове</h2>
                    {this.renderMain(objects, map, params)}
                </div>
            );
        }

        return (
            <div className="contacts__extra contacts__departament">
                <div className="row">
                    <div className="columns medium-12">
                        {extraMap}
                        <div className="extra__container">
                            <div className="padding-container">
                                <div className="row">
                                    <div className="columns medium-12">
                                        <h2>Отдел продаж</h2>
                                    </div>
                                    <div className="columns small-12">
                                        <div className="departament">
                                            <ul className="sale__list">
                                                <li className="phone">
                                                    <p className="title">Горячая линия: +7 (800) 500-14-22</p>

                                                    <p className="title-help">Звоните бесплатно со всех телефонов</p>
                                                </li>
                                                <li className="time">
                                                    ПН-ВС: с 9:00 до 21:00
                                                </li>
                                                <li className="mail">
                                                    aamt@ko-kirov.ru
                                                </li>
                                            </ul>

                                            <div className="container__yellow-border">
                                                <p>Телефон в Москве: +7 (499) 703-38-68</p>
                                                <p>Телефон в Санкт-Петербурге: +7 (812) 309-35-22</p>
                                                <p>Телефон в Нижнем Новгороде: +7 (831) 429-05-99</p>
                                                <p>Телефон в Кирове: +7 (8332) 68-03-41</p>
                                            </div>
                                            <p className="contacts__feedback-link">
                                                <a onClick={openModal.bind(this, <Feedback />)} href="#">Связаться с
                                                    нами</a>
                                            </p>
                                        </div>

                                        <div className="departament">
                                            <h2>Оптовый отдел</h2>
                                            <ul className="sale__list small">
                                                <li className="phone">
                                                    +7 (912) 734-43-39<br/>
                                                    Руководитель отдела Бажанов Александр Андреевич
                                                </li>
                                                <li className="time">
                                                    ПН-ПТ: с 9:00 до 21:00<br/>
                                                    СБ-ВС: с 11:00 до 19:00
                                                </li>
                                                <li className="mail">
                                                    opt@ko-kirov.ru
                                                </li>
                                            </ul>
                                            <p className="contacts__feedback-link small">
                                                <a href="#" onClick={openModal.bind(this, <SalesBackForm />)}>Связаться
                                                    с
                                                    нами</a>
                                            </p>
                                        </div>

                                        <div className="departament">
                                            <h2>Отдел качества</h2>
                                            <ul className="sale__list small">
                                                <li className="phone">
                                                    +7 (912) 734-43-39<br/>
                                                    Руководитель отдела Бажанова Анастасия Анатольевна
                                                </li>
                                                <li className="time">
                                                    ПН-ПТ: с 9:00 до 19:00<br/>
                                                    По московскому времени
                                                </li>
                                                <li className="mail">
                                                    good@ko-kirov.ru
                                                </li>
                                            </ul>
                                            <p className="contacts__feedback-link small">
                                                <Link to="KoReviews">
                                                    Оставить отзыв
                                                </Link>

                                            </p>
                                        </div>

                                        <div className="departament">
                                            <h2>Отдел маркетинга и рекламы</h2>
                                            <ul className="sale__list small">
                                                <li className="time">
                                                    ПН-ПТ: с 9:00 до 19:00
                                                </li>
                                                <li className="mail">
                                                    marketing@ko-kirov.ru
                                                </li>
                                                <li>
                                                    Все предложения Вы можете отправлять на электронную почту,
                                                    мы обязательно с ними ознакомимся.
                                                </li>
                                            </ul>
                                            <p className="contacts__feedback-link small">
                                                <a onClick={openModal.bind(this, <Feedback />)} href="#">Связаться с
                                                    нами</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const {
            geo,
            defaultObjects,
            objects,
            loading,
            error,
            message
        } = this.props;

        if (loading) {
            return <Loader />;
        }

        if (error) {
            return <Error message={message}/>;
        }

        let params = {
            lat: parseFloat(geo.site.lat) || 58.6029,
            lng: parseFloat(geo.site.lng) || 49.6681,
            zoom: parseInt(geo.site.zoom) || 12
        };

        return (
            <div>
                {this.renderMain(objects, 'map', params)}
                {this.renderExtra(defaultObjects, 'map_extra', params)}
            </div>
        );
    }
}

export default connect(state => {
    return {
        ...state.office,
        geo: state.geo
    };
}, dispatch => {
    return {
        officeActions: bindActionCreators(officeActions, dispatch),
        geoActions: bindActionCreators(geoActions, dispatch)
    };
})(OfficeList);
