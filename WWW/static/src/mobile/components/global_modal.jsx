import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as modalActions from 'actions/modal';
import { SkyLightStateless } from 'react-skylight';

const modal = props => {
    const { queue, actions } = props;

    if (queue.length === 0) {
        return null;
    }

    const { component, isVisible, style } = queue[queue.length - 1];

    const titleStyle = {
        display: 'none'
    };

    const overlayStyles = {
        zIndex: 1002
        // pointerEvents: 'none'
    };

    const closeButtonStyle = {
        cursor: 'pointer',
        position: 'absolute',
        fontSize: '1.8em',
        right: 0,
        top: 0,
        width: 32,
        height: 32,
        textAlign: 'center',
        lineHeight: '29px',
        zIndex: 200
    };

    let dialogStyles = {
        zIndex: 1003,
        padding: 20,
        borderRadius: 0,
        position: "fixed",
        // top: (document.documentElement.scrollTop || document.body.scrollTop) + 60,
        // left: "50%",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        margin: 0,
        overflowY: 'auto',
        width: '100%',
        height: '100%',
        ...style || {}
    };

    const closeCallback = () => {
        if (queue.length > 0) {
            queue.pop();
            actions.close();
        } else {
            actions.close();
        }
    };

    const cfg = {
        isVisible,
        dialogStyles,
        titleStyle,
        closeButtonStyle,
        overlayStyles,
        onCloseClicked: closeCallback,
        onOverlayClicked: closeCallback
    };

    return <SkyLightStateless {...cfg}>{component}</SkyLightStateless>;
};

export default connect(state => state.modal, dispatch => {
    return {
        actions: bindActionCreators(modalActions, dispatch)
    };
})(modal);