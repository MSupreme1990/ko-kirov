import React, { Component, PropTypes } from 'react';
import Loader from 'shared/components/loader';
import { Link } from 'react-easy-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as categoryActions from 'actions/category';
import { chunk } from 'lib/utils';

class Categories extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    render() {
        const { loading, objects } = this.props;

        if (loading) {
            return <Loader />;
        }

        let categories = objects.map(root => {
            let children = chunk(root.items, 2).map((models, i) => {
                let childNodes = models.map(model => (
                    <td key={"subsub-category-" + model.id}>
                        <Link to="Catalog" params={{url: model.url}}>
                            {model.name}</Link>
                    </td>
                ));

                return <tr key={"subsub-row-" + i}>{childNodes}</tr>;
            });

            return (
                <div className="category_menu_container" key={"root-" + root.id}>
                    <div className="row">
                        <div className="columns small-12">
                            <div className="category_title woman inactive">
                                <div className="title_container">
                                    {root.name}
                                </div>
                            </div>
                            <table className="categories">
                                <tbody>{children}</tbody>
                            </table>
                        </div>
                    </div>
                </div>
            );
        });

        return <div className="categories_block_container">{categories}</div>;
    }
}

export default connect(state => state.category, dispatch => {
    return {
        categoryActions: bindActionCreators(categoryActions, dispatch)
    };
})(Categories);