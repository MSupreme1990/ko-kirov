import React from 'react';
import Slider from 'react-slick';
import { makeId } from 'lib/utils';
import Product from 'mobile/components/catalog/product';

var ProductSlider = React.createClass({
    getInitialState: function () {
        return {
            prefix: makeId(),
            models: []
        };
    },

    componentDidMount() {
        this.loadData(this.props);
    },

    componentWillReceiveProps: function (nextProps) {
        this.loadData(nextProps);
    },

    loadData: function (props) {
        this.setState({
            prefix: props.prefix || this.state.prefix,
            models: props.data
        });
    },

    render: function () {
        let prefix = this.state.prefix;

        const sliderConfig = {
            dots: false,
            arrows: true,
            infinite: false,
            speed: 500,
            slidesToShow: 2,
            slidesToScroll: 2
        };

        return (
            <div className="product-slider">
                <h3 className="product-list-title">{this.props.title}</h3>

                <Slider className='slider' {...sliderConfig}>
                    {this.props.data.map(model => (
                        <div key={prefix + model.id}>
                            <Product product={model} group={this.props.data}
                                     key={prefix + model.id+'-product'}/>
                        </div>
                    ))}
                </Slider>
            </div>
        );
    }
});

export default ProductSlider;