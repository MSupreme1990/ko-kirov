import React, { Component, PropTypes } from 'react';
import { Link } from 'react-easy-router';
import currency from 'shared/components/currency';
import { connect } from 'react-redux';

class Product extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    static propTypes = {
        product: PropTypes.object.isRequired
    };

    state = {
        model: null,
        image: null,
        category: null
    };

    render() {
        const { symbol, product } = this.props;

        let discountPercentage = null;
        if (product.discount_percentage) {
            discountPercentage = <span className="product-item-discount-percentage">-{product.discount_percentage}%</span>;
        }

        let discount = null;
        if (typeof product.discount !== "undefined" && product.discount.name > 0) {
            discount = <p className="product-item-discount">{product.discount.name}</p>;
        }

        let oldPrice = null;
        if (currency.hasOldPrice(symbol.toLowerCase(), product)) {
            oldPrice = (
                <span className="product-item-old-price">
                    {currency.format(symbol.toLowerCase(), product, true)}
                </span>
            );
        }

        return (
            <div className="product-item">
                <Link to="Product" params={{url: product.url, category: product.category}}>
                    <div className="image-holder">
                        <img src={product.images[0].thumb} alt={product.name}/>
                        {discountPercentage}
                        {discount}
                    </div>
                    <p className="name">{product.name}</p>
                    <p className="article">№{product.code}</p>

                    <p className="price">
                        <span className="product-item-price">
                            {currency.format(symbol.toLowerCase(), product)}
                        </span>
                        {oldPrice}
                    </p>


                </Link>
            </div>
        );
    }
}

export default connect(state => {
    return {
        symbol: state.currency.symbol
    };
})(Product);