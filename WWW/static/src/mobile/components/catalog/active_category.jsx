import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as categoryActions from 'actions/category';
import Loader from 'shared/components/loader';
import { Link } from 'react-easy-router';
import { chunk } from 'lib/utils';
import classNames from 'classnames';
import { find } from 'lodash';

class ActiveCategory extends Component {
    static propTypes = {
        category: PropTypes.object.isRequired
    };

    static contextTypes = {
        router: PropTypes.object
    };

    state = {
        show: false
    };

    findChildren(root) {
        const { category } = this.props;
        if (category.url == root.url) {
            return root.items;
        }
        return this.iterateCategories(root.items) || [];
    }

    iterateCategories(objects) {
        const { category } = this.props;

        for (let i = 0; i < objects.length; i++) {
            if (objects[i].url == category.url) {
                return objects[i].items;
            } else {
                return this.iterateCategories(objects[i].items);
            }
        }
    }

    findParentItems(root) {
        return this.iterateParentItems(root.items);
    }

    iterateParentItems(objects) {
        const { category } = this.props;

        let parent = find(objects, { url: category.url });
        if (parent) {
            return parent;
        } else {
            for (let i = 0; i < objects.length; i++) {
                parent = this.iterateParentItems(objects[i].items);
                if (parent) {
                    return objects[i];
                }
            }
        }

        return null;
    }

    findParent(root) {
        const { category } = this.props;
        let parent = find(root.items, { url: category.url });
        if (parent) {
            return root;
        }
        return this.iterateParent(root.items);
    }

    iterateParent(objects) {
        let parent;
        for (let i = 0; i < objects.length; i++) {
            parent = this.iterateParentItems(objects[i].items);
            if (parent) {
                return objects[i];
            }
        }

        return null;
    }

    handleCollapse(e) {
        e.preventDefault();

        this.setState({
            show: !this.state.show
        });
    }

    render() {
        const { loading, objects, category } = this.props;

        if (loading) {
            return <Loader />;
        }

        let root = find(objects, {
            url: category.woman ? 'woman' : 'man'
        });

        let items = this.findChildren(root),
            parent = null;
        if (items.length == 0) {
            parent = this.findParentItems(root);
            if (parent) {
                items = parent.items;
            }
        }

        let children = chunk(items, 2).map((models, i) => {
            let childNodes = models.map(model => {
                let cls = classNames({
                    active: model.id == category.id
                });
                return (
                    <td key={"subsub-category-" + model.id}>
                        <Link to="Catalog"
                              params={{ url: model.url }}
                              className={cls}>{model.name}</Link>
                    </td>
                );
            });

            return <tr key={"subsub-row-" + i}>{childNodes}</tr>;
        });

        if (category.level == 1) {
            return (
                <div className="categories_block_container">
                    <div className="category_menu_container" key={"root-" + root.id}>
                        <div className="row">
                            <div className="columns small-12">
                                <div className="category_title woman inactive">
                                    <div className="title_container">
                                        {root.name}
                                    </div>
                                </div>
                                <table className="categories">
                                    <tbody>{children}</tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            let backParent = this.findParent(root),
                back = backParent ? (
                        <div className="return">
                            <Link to="CatalogList" params={{ url: backParent.url }}>&nbsp;</Link>
                        </div>
                    ) : null,
                table = this.state.show ? (
                        <table className="categories">
                            <tbody>{children}</tbody>
                        </table>
                    ) : null;

            return (
                <div className="categories_block_container">
                    <div className="category_menu_container" key={"root-" + root.id}>
                        <div className="row">
                            <div className="columns small-12">
                                <div className="category_title woman active">
                                    {back}
                                    <div className="title_container">
                                        {root.name}
                                    </div>
                                </div>
                                <div className="subcategory_title">
                                    <a href="#" onClick={this.handleCollapse.bind(this)}>{category.name}</a>
                                </div>
                                {table}
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

export default connect(state => state.category, dispatch => {
    return {
        categoryActions: bindActionCreators(categoryActions, dispatch)
    };
})(ActiveCategory);
