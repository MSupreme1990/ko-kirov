import $ from 'jquery';
import React from 'react';
import Cookie from 'js-cookie';
import PhoneInput from 'inputs/phone_input';
import QuickOrderMixin from 'mixins/quick_order_mixin';
import SocialModal from 'shared/components/social_modal';
import PayLateForm from 'shared/components/PayLateForm';

let QuickOrder = React.createClass({
    mixins: [QuickOrderMixin],
    handleTabClick: function (e) {
        e.preventDefault();
        let $this = $(e.target),
            $li = $this.parent(),
            index = $li.index(),
            $tabs = $this.closest('.tabs'),
            $panels = $tabs.find('.tab-panel');
        $panels.removeClass(' is-active');
        $panels.eq(index).addClass(' is-active');

        $tabs.find('.tabs-menu li').removeClass('is-active');
        $li.addClass('is-active');
    },
    handlePhoneChange: function (e) {
        this.setState({
            phone: e.target.value
        });
    },
    render: function () {
        const { product } = this.props;

        let extraOrder = null,
            extraCredit = null,
            tabActive = 1;
        if (this.state.extraOrder) {
            extraOrder = this.getExtraForm('QuickOrderForm', true);
            // extraOrder = <p className="extra message">Ваша заявка успешно отправлена</p>;
        }
        if (this.state.extraCredit) {
            tabActive = 2;
            extraCredit = this.getExtraForm('CreditOrderForm', true);
            // extraCredit = <p className="extra message">Ваша заявка успешно отправлена</p>;
        }

        let name = Cookie.get('name');
        let currentSize = Cookie.get('current_size');

        return (
            <div className="quick-order">
                <SocialModal ref="modal" />
                <div className="tabs">
                    <div className="tabs-navigation">
                        <ul className="tabs-menu">
                            <li className="tabs-menu-item is-active">
                                <a onClick={this.handleTabClick} href="javascript:;">Быстрый заказ</a>
                            </li>
                            <li className="tabs-menu-item">
                                <a onClick={this.handleTabClick} href="javascript:;">Купить в рассрочку</a>
                            </li>
                        </ul>
                    </div>
                    <div className="tab-panel is-active">
                        <div className="row">
                            <div className="columns small-12">
                                <p>Для бесплатной примерки введите свой номер телефона и нажмите «Заказать в один клик».
                                    Менеджер позвонит Вам и уточнит детали заказа.</p>

                                <form id="quick-order-form" onSubmit={this.handleQuickOrderSubmit}>
                                    {(()=> {
                                        if (extraOrder) {
                                            return extraOrder;
                                        }

                                        return (
                                            <div className="row">
                                                <div className="columns small-6">
                                                    <input type="text" name="QuickOrderForm[name]" onChange={this.handleNameChange}
                                                           defaultValue={name} placeholder="Введите ваше имя"/>
                                                    <ul className="errors"></ul>
                                                </div>
                                                <div className="columns small-6">
                                                    <PhoneInput name="QuickOrderForm[phone]" onChange={this.handlePhoneChange} />
                                                    <ul className="errors"></ul>
                                                </div>
                                                <div className="columns small-12">
                                                    <input type="hidden" name="QuickOrderForm[size]" value={currentSize}/>
                                                    <input type="hidden" name="QuickOrderForm[product]" value={this.state.product_id}/>

                                            <span className="submit-container">
                                                 <input type="submit" value="ЗАКАЗАТЬ В ОДИН КЛИК"
                                                        className={this.state.phone.length > 0 ? "order-button yellow" : "order-button maroon"}/>
                                            </span>
                                                </div>
                                                <div className="columns small-12">
                                                    <div className="b-personal-data">
                                                        <span className="b-personal-data__check"><input type="checkbox"
                                                                                                        checked="checked"/></span>
                                                        Передавая информацию сайту вы принимаете условия
                                                        <a className="b-personal-data__link"
                                                           href="/page/private"
                                                           target="_blank">"политики защиты персональных данных".</a>
                                                    </div>
                                                </div>
                                            </div>
                                        );
                                    })()}


                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="tab-panel">
                        <div className="row">
                            <div className="columns small-12">
                                <PayLateForm products={
                                    [{
                                        quantity: 1,
                                        price: product.price_rub || product.price_old_rub,
                                        object: product
                                    }]
                                } />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

export default QuickOrder;
