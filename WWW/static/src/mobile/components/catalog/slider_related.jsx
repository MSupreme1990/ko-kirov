import React, { Component, PropTypes } from 'react';
import Loader from 'shared/components/loader.jsx';
import SliderProduct from 'shared/containers/product/slider';

import * as productActions from 'actions/product';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class RelatedSlider extends Component {
    static propTypes = {
        product: PropTypes.object.isRequired
    };

    componentWillMount() {
        const { actions, product } = this.props;
        actions.fetchRelatedIfNeeded(product.url);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.product.id != nextProps.product.id) {
            const { actions, product } = this.props;
            actions.fetchRelatedIfNeeded(product.url);
        }
    }

    render() {
        const { error, loading, objects } = this.props;

        if (loading) {
            return <Loader />;
        }

        if (error) {
            return null;
        }

        return (
            <div className="relatedSlider">
                <h1>Вместе с этой моделью смотрят</h1>
                <SliderProduct data={objects}/>
            </div>
        );
    }
}

export default connect((state, props) => {
    const { loading, error, related } = state.product.related;
    return {
        loading,
        error,
        objects: related[props.product.url] || []
    };
}, dispatch => {
    return {
        actions: bindActionCreators(productActions, dispatch)
    }
})(RelatedSlider);
