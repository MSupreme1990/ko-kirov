import { chunk } from 'lib/utils';
import React, { Component } from 'react';
import { Link } from 'react-easy-router';
import ReviewsVideo from 'shared/components/review/reviews_video';
import ReviewsText from 'shared/components/review/reviews_text';

export default class Reviews extends Component {
    state = {
        text: false,
        video: true,
        small: false
    };

    handleVideo (e) {
        e.preventDefault();
        this.setState({
            video: true,
            text: false
        });
    }

    handleText (e) {
        e.preventDefault();
        this.setState({
            text: true,
            video: false
        });
    }

    render () {
        return (
            <div className="reviews-mixed-container">
                <div className="row">
                    <div className="columns large-12">
                        <ul className="reviews-switch">
                            <li className={this.state.video ? 'active' : ''}>
                                <a href="#" onClick={this.handleVideo.bind(this)}>
                                    Видеоотзывы наших<br/>
                                    покупателей</a>
                            </li>
                            <li className={this.state.text ? 'active' : ''}>
                                <a href="#" onClick={this.handleText.bind(this)}>
                                    Текстовые отзывы<br/>
                                    наших покупателей</a>
                            </li>
                        </ul>
                        <div className="review-tabbed-container">
                            <div className={this.state.video ? 'reviews-block-container' : 'reviews-block-container hide'}>
                                <ReviewsVideo count={2} />
                            </div>
                            <div className={this.state.text ? 'reviews-expert-container' : 'reviews-expert-container hide'}>
                                <ReviewsText />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
