import React, { Component, PropTypes } from 'react';
import { Link } from 'react-easy-router';
import { cookie } from 'easy-storage';
import { ModalLink } from 'shared/components/modal';
import RequestCall from 'shared/components/request_call';
import Feedback from 'shared/components/feedback';
import { openModal } from 'lib/utils';
import Geo from 'shared/components/geo';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as geoActions from 'actions/geo';

class Footer extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    state = {
        showForm: false
    };

    componentWillMount() {
        const { geoActions } = this.props;
        geoActions.fetchListIfNeeded();

        if (this.props && this.props.form) {
            this.setState({ showForm: this.props.form || false });
        }
    };

    componentWillReceiveProps(nextProps) {
        this.setState({ showForm: nextProps.form || false });
    };

    handleGoToDesktop(e) {
        e.preventDefault();
        cookie.set('MOBILE', false);
        window.location.reload(true);
    }

    render() {
        const { loading, city } = this.props.geo;
        const year = new Date().getFullYear();

        if (loading) {
            return null;
        }

        return (
            <div id="footer" className="footer">
                <div className="footer-content-block">
                    <div className="row">
                        <div className="columns small-6">
                            <div className="phone">
                                <a href="tel:8-800-500-14-22">Тел.: 8 800 500 14 22</a>
                            </div>
                            <div className="phone-text">Звоните по России бесплатно</div>
                            <div className="city">
                                Город: <a href="#" onClick={openModal.bind(this, <Geo />)}>{city.name}</a>
                            </div>
                        </div>
                        <div className="columns small-6">
                            <div className="email">
                                E-mail: <a href="mailto:aamt@mail.ru">aamt@mail.ru</a>
                            </div>
                            <div className="send-to-ceo">
                                <a href="#"
                                   onClick={openModal.bind(this, <Feedback />)}>Написать директору</a>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="columns small-3">&nbsp;</div>
                        <div className="columns small-6">
                            <a href="#" className="request-call"
                               onClick={openModal.bind(this, <RequestCall />)}>
                                Заказать звонок
                            </a>
                        </div>
                        <div className="columns small-3">&nbsp;</div>
                    </div>

                    <div className="row">
                        <div className="columns small-12">
                            <ul className="social-list">
                                <li>
                                    <a href="http://vk.com/kokirov" target="_blank" className="vk">Вконтакте</a>
                                </li>
                                <li>
                                    <a href="http://instagram.com/kokirov.ru" target="_blank" className="instagram">Instagram</a>
                                </li>
                                <li>
                                    <a href="http://ok.ru/kokirov" target="_blank" className="ok">Одноклассники</a>
                                </li>
                                <li>
                                    <a href="http://facebook.com/groups/922330274477726" target="_blank" className="fb">Facebook</a>
                                </li>
                                <li>
                                    <a href="http://twitter.com/Ko_kirovRu" target="_blank"
                                       className="twitter">Twitter</a>
                                </li>
                                <li>
                                    <a href="https://plus.google.com/+%D0%9A%D0%BE%D0%B6%D0%B0%D0%BD%D0%B0%D1%8F%D0%BE%D0%B4%D0%B5%D0%B6%D0%B4%D0%B043"
                                       target="_blank" className="google">Google+</a>
                                </li>
                                <li className="last">&nbsp;</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="links-block">
                    <div className="row">
                        <div className="columns small-12">
                            <div className="links">
                                <a href="#" onClick={this.handleGoToDesktop.bind(this)}>
                                    Перейти на версию для планшетов и ПК.
                                </a>
                            </div>
                        </div>
                    </div>

                </div>

                <div className="copyright-block">
                    <div className="row">
                        <div className="columns small-12">
                            <div className="copyright">
                                &copy; 2003-{year} Сеть магазинов «Мехарди» <br/>
                                <a href="/page/private">Политика защиты персональных данных</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default connect(state => {
    return {
        geo: state.geo
    };
}, dispatch => {
    return {
        geoActions: bindActionCreators(geoActions, dispatch)
    };
})(Footer);
