import $ from 'jquery';
import React, { Component } from 'react';
import settings from 'config/settings';

export default class Best extends Component {
    handleClick(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $(".reviews-block-container").offset().top
        }, 500);
    }

    render() {
        return (
            <div className="best-container">
                <div className="row">
                    <div className="columns small-12 best-info-container">
                        <h2>15 лет на рынке меха и кожи</h2>
                        <p>Почему более 100000 клиентов выбрали нас?</p>
                    </div>
                    <div className="columns small-12">
                        <ul className="small-block-grid-2">
                            <li className="best-item">
                                <img src={"/static/dist/images/inner/best_big.png"}/>
                                <p className="big">
                                    Мы используем лучшие мировые технологии производства, что позволяет держать цену
                                    максимально выгодной для Покупателя.
                                </p>
                            </li>
                            <li className="best-item">
                                <img src={"/static/dist/images/inner/best_many.png"}/>
                                <p className="many">
                                    У нас более 100 000 товаров в наличии в магазинах и готовы для доставки по
                                    всей России
                                </p>
                            </li>
                            <li className="best-item">
                                <img src={"/static/dist/images/inner/best_star.png"}/>
                                <p className="star">
                                    Наши изделия носят известные звёзды шоу-бизнеса «Азиза», Светлана Пермякова, Анна Семенович
                                </p>
                            </li>
                            <li className="best-item">
                                <img src={"/static/dist/images/inner/best_geo.png"}/>
                                <p className="geo">
                                    Наши изделия разрабатываются итальянскими дизайнерами Andrea Nicolette, Lorenzo Conti и
                                    участвуют в международных показах мод в Москве, Милане и Париже.
                                </p>
                            </li>
                            <li className="best-item">
                                <img src={"/static/dist/images/inner/best_gost.png"}/>
                                <p className="gost">
                                    Вся наша продукция сертифицирована и соответствует ГОСТ 32084-2013
                                    ТР ТС 017/2011
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}
