import React, { Component, PropTypes } from 'react';
import { Link } from 'react-easy-router';
import Autocomplete from 'shared/components/product/autocomplete';
import RequestCall from 'shared/components/request_call';
import { openModal } from 'lib/utils';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as currencyActions from 'actions/currency';
import Logo from 'shared/containers/logo';

class Header extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    static propTypes = {
        q: PropTypes.string
    };

    static defaultProps = {
        q: ''
    };

    state = {
        show_search: false
    };

    componentWillMount() {
        const { actions } = this.props;
        actions.fetchListIfNeeded();
    }

    handleMenu(e) {
        e.preventDefault();
        this.props.showMenu(e);
    }

    handleOpenSearch(e) {
        e.preventDefault();
        this.setState({
            show_search: !this.state.show_search
        });
    }

    renderSearch() {
        const { q } = this.props;
        const { show_search } = this.state;

        return show_search ? <div className="header-search"><Autocomplete q={q} /></div> : null;
    }

    render() {
        return (
            <header id="header">
                <div className="header_container">
                    <div className="row">
                        <div className="columns small-4">
                            <div className="header-links">
                                <a href="#" className="menu"
                                   onClick={this.handleMenu.bind(this)}>
                                    <span className="icon menu">Меню</span>
                                </a>
                                <a className="search" href="#" onClick={this.handleOpenSearch.bind(this)}>
                                    <span className="icon search">Поиск</span>
                                </a>
                            </div>
                        </div>
                        <div className="columns small-4">
                            <div className="text-center">
                                <Link to="Index" className="logo" id="logo" title="Кожаная одежда">
                                    <Logo />
                                </Link>
                            </div>
                        </div>
                        <div className="columns small-4">
                            <div className="text-right">
                                <div className="header-links">
                                    <Link to="Cart" className="cart">
                                        <span className="icon cart">Корзина</span>
                                    </Link>
                                    <a href="tel:+78005001422" className="phone">
                                        <span className='icon phone'>Корзина</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="advantages_container">
                    <div className="row">
                        <div className="columns small-7">
                            <div className="delivery">
                                <div className="block delivery">
                                    <Link to="Delivery">
                                        Бесплатная доставка<br/>
                                        на дом по всей России
                                    </Link>
                                </div>
                            </div>
                        </div>
                        <div className="columns small-5">
                            <div className="fitting">
                                <div className="block fitting">
                                    Примерка перед<br/>покупкой
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.renderSearch()}
            </header>
        );
    }
}

export default connect(state => state.currency, dispatch => {
    return {
        actions: bindActionCreators(currencyActions, dispatch)
    };
})(Header);