import $ from 'jquery';
import React, { Component, PropTypes } from 'react';
import { Link } from 'react-easy-router';
import Header from 'mobile/components/header/header'
import { connect } from 'react-redux';
import MenuItem from 'mobile/components/header/menu_item';
import { bindActionCreators } from 'redux';
import * as menuActions from 'actions/menu';
import * as categoryActions from 'actions/category';

class Menu extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    state = {
        background: false,
        show: false,
        man: false
    };

    componentWillMount() {
        const { menuActions, categoryActions } = this.props;
        categoryActions.fetchListIfNeeded();
        menuActions.fetchListIfNeeded();
    }

    prevent(e) {
        e = e || window.event;
        if (e) {
            e.preventDefault();
        }
    }

    close() {
        this.setState({ background: false });

        setTimeout(() => {
            this.setState({ show: false });
            $('body').css({
                'overflow': '',
                'padding-right': ''
            });
        }, 330)
    }

    open() {
        this.setState({ show: true });

        setTimeout(() => {
            this.setState({ background: true });
            $('body').css({
                'overflow': 'hidden',
                'padding-right': '0px'
            });
        }, 330)
    }

    handleLinkClick() {
        this.close();
    }

    handleClose(e) {
        this.prevent(e);
        this.close();
    }

    handleOpen(e) {
        this.prevent(e);
        this.open();
    }

    handleMan(e) {
        this.prevent(e);
        this.setState({ man: true });
    }

    handleWoMan(e) {
        this.prevent(e);
        this.setState({ man: false });
    }

    renderCategory(key) {
        const { loading, objects } = this.props.category;

        if (loading) {
            return null;
        }

        return objects.map((root, t) => {
            if (root.url != key) {
                return null;
            }

            let nodes = root.items.map((category, i) => (
                <li key={'menu-category-' + key + '-' + i}>
                    <Link onClick={this.handleLinkClick.bind(this)}
                          to="CatalogList" params={{ url: category.url }}>{category.name}</Link>
                </li>
            ));

            return <ul key={"foter-menu-" + t} className="footer-menu">{nodes}</ul>;
        });
    }

    renderMenuNodes(key) {
        let objects = [
            { name: 'Отзывы', route: 'Reviews', params: {} },
            { name: 'Доставка и оплата', route: 'Delivery', params: {} },
            { name: 'Кредит', route: 'Credit', params: {} },
            {
                name: 'Сервис', route: '', params: {},
                items: [
                    { name: 'Узнать статус заказа', route: 'OrderStatus', params: {} },
                    { name: 'Как сделать заказ', route: 'HowOrder', params: {} },
                    { name: 'Оплата и доставка', route: 'Credit', params: {} },
                    { name: 'Гарантийные условия', route: 'Page', params: { url: 'garant' } },
                    { name: 'Кредит и рассрочка', route: 'Credit', params: {} },
                    { name: 'Политика конфиденциальности', route: 'Page', params: { url: 'private' } },
                    { name: 'Реквизиты', route: 'Page', params: { url: 'rekvizity' } }
                ]
            },
            { name: 'Оптовый отдел', route: 'WholeSales', params: {} },
            { name: 'Контакты', route: 'Contacts', params: {} }
        ];

        return objects.map((model, i) => (
            <MenuItem onClick={this.handleLinkClick.bind(this)} model={model}
                      key={"mm-" + key + "-" + i}/>
        ));
    }

    render() {
        const { loading } = this.props.menu;

        if (loading) {
            return <p>Загрузка...</p>;
        }

        let cls = '';
        if (this.state.show) {
            cls += ' showed ';
        }
        if (this.state.background) {
            cls += ' dark ';
        }

        return (
            <div>
                <div id="menu" className={cls}>
                    <div className="buttons_container">
                        <div className="mans">
                            <div className="row">
                                <div className="columns small-6 ">
                                    <div className={'mobile-menu-item ' + (!this.state.man ? 'active' : '')}
                                         onClick={this.handleWoMan.bind(this)}>
                                        Для женщин
                                    </div>
                                </div>
                                <div className="columns small-6 ">
                                    <div className={'mobile-menu-item ' + (this.state.man ? 'active' : '')}
                                         onClick={this.handleMan.bind(this)}>
                                        Для мужчин
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="close" onClick={this.handleClose.bind(this)}>

                        </div>
                    </div>
                    <div className="menu_container">
                        <div className="row">
                            <div className="columns small-12">
                                <ul>
                                    <li>
                                        <Link to="Index">Главная</Link>
                                        {this.renderCategory(this.state.man ? 'man' : 'woman')}
                                    </li>
                                    {this.renderMenuNodes(this.state.man ? 'man' : 'woman')}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <Header showMenu={this.handleOpen.bind(this)}/>
            </div>
        );
    }
}

export default connect(state => {
    return {
        category: state.category,
        menu: state.menu
    }
}, dispatch => {
    return {
        categoryActions: bindActionCreators(categoryActions, dispatch),
        menuActions: bindActionCreators(menuActions, dispatch)
    };
})(Menu);