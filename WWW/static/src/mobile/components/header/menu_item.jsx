import React from 'react';
import { Link } from 'react-easy-router';
import { openModal } from 'lib/utils';

const MenuItem = props => {
    const { model, onClick } = props;

    let children = [];
    if (model.items && model.items.length > 0) {
        children = <ul className="footer-menu">{model.items.map((item, i) => <MenuItem key={'child_menu_' + model.id + i} onClick={onClick} model={item} />)}</ul>;
    }

    return (
        <li className={model.items && model.items.length > 0 ? 'has-nested' : ''}>
            <Link onClick={onClick} to={model.route} params={model.params}>{model.name}</Link>
            {children}
        </li>
    );
};

export default MenuItem;