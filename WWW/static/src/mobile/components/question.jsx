import $ from 'jquery';
import React, { Component } from 'react';
import { cookie } from 'easy-storage';
import PhoneInput from 'inputs/phone_input';
import api from 'lib/api';
import { sendToAmo, formValidate } from 'lib/utils';

export default class Question extends Component {
    state = {
        pk: null,
        message: ""
    };

    handleSubmit(e) {
        e.preventDefault();

        var $form = $('#question-form');

        api.post('/api/ko/question', $form.serialize(), data => {
            sendToAmo($form);
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    pk: data['pk']
                });
            }
        });
    }

    handleClose(e) {
        e.preventDefault();
        this.setState({ pk: null });
    }

    handleExtraSubmit(e) {
        e.preventDefault();

        var $form = $('#question-extra-form');

        api.post('/api/ko/question_extra', $form.serialize(), data => {
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    message: data['message']
                });

                setTimeout(() => {
                    this.setState({ pk: null });
                }, 2500);
            }
        });
    }

    handleNameChange(e) {
        cookie.set('name', e.target.value);
    }

    handleEmailChange(e) {
        cookie.set('email', e.target.value);
    }

    renderExtra() {
        if (this.state.pk == null) {
            return <span />;
        }
        var email = cookie.get('email');
        return (
            <div className={this.state.message == '' ? 'extra-form-popup' : 'extra-form-popup complete'}>
                <h1 className={this.state.message == '' ? 'hide' : ''}>{this.state.message}</h1>
                <a href="#" className="extra-form-close" onClick={this.handleClose.bind(this)}>&times;</a>
                <form id="question-extra-form" className={this.state.message == '' ? '' : 'hide'}
                      onSubmit={this.handleExtraSubmit.bind(this)}>
                    <div className="row">
                        <div className="columns small-12">
                            <p className="extra-form-title">
                                Ваша заявка успешно отправлена. Уточните, пожалуйста, следующие данные:
                            </p>
                            <div className="form-row">
                                <input type="text" onChange={this.handleEmailChange.bind(this)}
                                       name="QuestionForm[email]"
                                       defaultValue={email} placeholder="Введите ваш e-mail"/>
                                <ul className="errors"></ul>
                            </div>
                            <div className="form-row">
                                <textarea name="QuestionForm[question]" placeholder="Введите ваш вопрос"></textarea>
                                <ul className="errors"></ul>
                            </div>
                            <div className="form-row">
                                <input type="hidden" name="QuestionForm[id]" value={this.state.pk}/>
                                <input type="submit" value="ОТПРАВИТЬ" className="button"/>
                            </div>
                            <div className="form-row">
                                <div className="b-personal-data">
                                            <span className="b-personal-data__check">
                                                <input
                                                    className="b-personal-data__input" type="checkbox"
                                                    checked="checked"/>
                                            </span>
                                    Нажимая кнопку «Получить консультацию», вы принимаете
                                    <a className="b-personal-data__link"
                                       href="/page/private"
                                       target="_blank">"условия обработки персональных данных".</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }

    render() {
        var modal = this.renderExtra();
        var name = cookie.get('name');
        return (
            <div className="question-container">
                <div className="row">
                    {modal}
                    <div className="columns small-12 left-side">
                        <div className="left-side-container">
                            <h3>Появились вопросы?</h3>
                            <p>Оставьте номер телефона и наш менеджер примет
                                Ваш заказ и проконсультирует по всем интересующим вопросам</p>
                        </div>

                        <form id="question-form" onSubmit={this.handleSubmit.bind(this)}>
                            <div className="row">
                                <div className="columns small-6">
                                    <div className="form-row">
                                        <input type="text" onChange={this.handleNameChange.bind(this)}
                                               defaultValue={name}
                                               name="QuestionForm[name]" placeholder="Введите ваше имя"/>
                                        <ul className="errors"></ul>
                                    </div>
                                </div>
                                <div className="columns small-6">
                                    <div className="form-row">
                                        <PhoneInput name="QuestionForm[phone]"/>
                                        <ul className="errors"></ul>
                                    </div>
                                </div>
                                <div className="columns small-12">
                                    <input type="submit" value="ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ" className="button"/>
                                </div>
                                <div className="columns small-12">
                                    <div className="b-personal-data">
                                            <span className="b-personal-data__check">
                                                <input
                                                    className="b-personal-data__input" type="checkbox"
                                                    checked="checked"/>
                                            </span>
                                        Нажимая кнопку «Получить консультацию», вы принимаете
                                        <a className="b-personal-data__link"
                                           href="/page/private"
                                           target="_blank">"условия обработки персональных данных".</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}
