import React, { Component } from 'react';

import Application from './app.jsx';
import ContactsHandler from './pages/contacts';
import CreditHandler from './pages/credit';
import DeliveryHandler from './pages/delivery';
import PageHandler from './pages/page';
import CartHandler from './pages/cart';
import ProductHandler from './pages/product';
import CatalogHandler from './pages/catalog';
import IndexHandler from './pages/index';
import SearchHandler from './pages/search';
import ReviewsHandler from './pages/reviews';
import WholesalesHandler from './pages/sales';
import HowOrderHandler from './pages/how_order';
import OrderStatusHandler from './pages/order_status';

export default {
    Index: {path: '/', component: IndexHandler, wrapper: Application},
    Search: {path: '/search', component: SearchHandler, wrapper: Application},
    Reviews: {path: '/reviews', component: ReviewsHandler, wrapper: Application},
    KoReviews: {path: '/reviews', component: ReviewsHandler, wrapper: Application},
    Cart: {path: '/cart', component: CartHandler, wrapper: Application},

    HowOrder: {path: '/how_order', component: HowOrderHandler, wrapper: Application},
    OrderStatus: {path: '/order_status', component: OrderStatusHandler, wrapper: Application},

    WholeSales: {path: '/sales', component: WholesalesHandler, wrapper: Application},
    KoSales: {path: '/sales', component: WholesalesHandler, wrapper: Application},
    Contacts: {path: '/contacts', component: ContactsHandler, wrapper: Application},
    Credit: {path: '/page/credit', component: CreditHandler, wrapper: Application},
    Delivery: {path: '/page/delivery', component: DeliveryHandler, wrapper: Application},
    Page: {path: '/page/:url', component: PageHandler, wrapper: Application},
    CatalogProduct: {path: '/catalog/:url', component: ProductHandler, wrapper: Application},
    Product: {path: '/catalog/:url', component: ProductHandler, wrapper: Application},
    Catalog: {path: '/:url', component: CatalogHandler, wrapper: Application},
    CatalogList: {path: '/:url', component: CatalogHandler, wrapper: Application}
}
