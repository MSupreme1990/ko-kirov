import api from 'lib/api';

export default {
    main() {
        return api.get('/api/catalog/index');
    }
};