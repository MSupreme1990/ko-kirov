import api from 'lib/api';

export default {
    view(code, params = {}) {
        return api.get('/api/ko/coupon/' + code, params);
    }
};