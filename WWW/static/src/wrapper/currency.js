import api from 'lib/api';

export default {
    list() {
        return api.get('/api/currency/list');
    }
};