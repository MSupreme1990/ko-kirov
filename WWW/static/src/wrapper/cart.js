import api from 'lib/api';

export default {
    add(id, size) {
        return api.post('/api/ko/cart/add/' + id + '/1', { size });
    },

    removeByIndex(index, params = {}) {
        return api.delete('/api/ko/cart/delete/' + index, params);
    },

    list(params = {}) {
        return api.get('/api/ko/cart/list');
    },

    changeSize(id, size, index) {
        return api.post('/api/ko/cart/update/' + index + '/' + id + '/1', {
            extra: { size: size }
        });
    }
};