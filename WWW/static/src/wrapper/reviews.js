import api from 'lib/api';

export default {
    list() {
        return api.get("/api/ko/reviews");
    },
    
    video() {
        return api.get("/api/ko/video_reviews");
    }
};