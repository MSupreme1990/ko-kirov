import api from 'lib/api';

export default {
    get(url, params = {}) {
        return api.getCached("/api/catalog/v1-1/category/get/" + url, params);
    }
};