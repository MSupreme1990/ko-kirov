import api from 'lib/api';

export default {
    all(params = {}) {
        return api.get("/api/catalog/v1-1/all-products", params);
    },

    related(url) {
        return api.get('/api/ko/product/related', { url });
    },

    list(url, params) {
        return api.get('/api/ko/product/list', { ...params, url });
    },

    new() {
        return api.get('/api/ko/product/new');
    },

    popular() {
        return api.get('/api/ko/product/popular');
    },

    autocomplete(q) {
        return api.get('/api/ko/product/autocomplete', { q });
    },

    search(q, filters = {}) {
        return api.get('/api/ko/product/search', { q, ...filters });
    },

    view(url) {
        return api.get('/api/ko/product/view', { url });
    },

    imagesById(id, params = {}) {
        return api.getCached("/api/catalog/v1-1/product/images/" + id, params);
    }
};