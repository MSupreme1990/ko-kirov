import api from 'lib/api';

export default {
    view(url) {
        return api.get('/api/page/' + url);
    }
};