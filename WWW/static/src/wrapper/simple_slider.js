import api from 'lib/api';

export default {
    get(id, params = {}) {
        return api.get('/api/slider');
    },

    list(params = {}) {
        return api.get('/api/slider/');
    }
};