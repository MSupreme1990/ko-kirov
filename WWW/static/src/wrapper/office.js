import api from 'lib/api';

export default {
    list(params = {}) {
        return api.get('/api/offices/', params);
    }
};
