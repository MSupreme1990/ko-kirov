import api from 'lib/api';

export default {
    view(url) {
        return api.getCached("/api/ko/get/seo/" + url);
    }
};