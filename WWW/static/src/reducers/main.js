import c from '../constants';

const initialState = {
    loading: false,
    categories: [],
    new_models: [],
    popular: []
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case c.MAIN_RECEIVE:
            return {
                ...state,
                categories: action.categories,
                new_models: action.new_models,
                popular: action.popular,
                loading: action.loading
            };

        case c.MAIN_LOADING:
            return {
                ...state,
                loading: action.loading
            };

        default:
            return state
    }
}