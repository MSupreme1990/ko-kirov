import c from '../constants';
import { local } from 'easy-storage';

const initialState = {
    loading: false,
    objects: [],
    error: false,
    received: false
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case c.BANNER_RECEIVE:
            return {
                ...state,
                objects: action.objects,
                received: action.received,
                error: action.error,
                loading: action.loading
            };

        case c.BANNER_LOADING:
            return {
                ...state,
                loading: action.loading
            };

        default:
            return state
    }
}