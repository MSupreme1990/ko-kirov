import c from '../constants';

const initialState = {
    loading: false,
    objects: [],
    received: false,
    error: false,
    message: null,
    defaultObjects: []
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case c.OFFICE_RECEIVE:
            return {
                ...state,
                loading: action.loading,
                received: action.received,
                defaultObjects: action.defaultObjects,
                objects: action.objects,
                error: action.error,
                message: action.message
            };

        case c.OFFICE_LOADING:
            return {
                ...state,
                loading: action.loading
            };

        default:
            return state
    }
}