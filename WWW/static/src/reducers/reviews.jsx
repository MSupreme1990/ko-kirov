import c from '../constants';

const initialState = {
    meta: {},
    loading: false,
    error: false,
    objects: [],
    didInvalidate: false,
    video: []
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case c.REVIEWS_INVALIDATE:
            return {
                ...state,
                didInvalidate: action.didInvalidate
            };
        case c.REVIEWS_LOADING:
            return {
                ...state,
                loading: action.loading
            };
        case c.REVIEWS_RECEIVE:
            return {
                ...state,
                didInvalidate: action.didInvalidate,
                objects: action.objects,
                meta: action.meta,
                loading: action.loading,
                video: action.video
            };

        default:
            return state
    }
}
