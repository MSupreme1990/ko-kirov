import c from '../constants';
import { local } from 'easy-storage';
import { find } from 'lodash';

const initialState = {
    models: [],
    date: undefined
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case c.RECENT_ADD:
            return find(state.models, { id: action.product.id }) ? state : {
                    models: [...state.models, action.product],
                    date: new Date()
                };

        case c.RECENT_INVALIDATE:
            return initialState;

        case c.RECENT_RECEIVE:
            return {
                ...state,
                models: action.models
            };

        default:
            return state
    }
}
