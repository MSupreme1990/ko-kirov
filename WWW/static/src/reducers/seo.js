import c from '../constants';

const initialState = {
    loading: false,
    objects: {}
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case c.SEO_RECEIVE:
            if (action.url in state.objects) {
                return {
                    ...state,
                    loading: action.loading
                };
            } else {
                return {
                    ...state,
                    loading: action.loading,
                    objects: {
                        ...state.objects,
                        [action.url]: action.objects
                    }
                };
            }

        case c.SEO_LOADING:
            return {
                ...state,
                loading: action.loading
            };

        default:
            return state
    }
}