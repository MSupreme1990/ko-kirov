import c from '../constants';

const initialState = {
    objects: [],
    loading: false,
    category: {},
    error: false,
    breadcrumbs: [],
    received: false,
    meta: {},
    new: {
        loading: false,
        objects: []
    },
    popular: {
        loading: false,
        objects: []
    },
    autocomplete: {
        loading: false,
        objects: []
    },
    search: {
        loading: false,
        objects: [],
        meta: {}
    },
    related: {
        related: {},
        loading: false,
        error: false
    },
    view: {
        view: {},
        loading: false,
        error: false
    }
};

export default (state = initialState, action = {}) => {
    let { view } = state;
    switch (action.type) {
        case c.PRODUCT_RECEIVE:
            return {
                ...state,
                objects: action.objects,
                error: action.error,
                meta: action.meta,
                loading: action.loading,
                category: action.category,
                received: action.received,
                breadcrumbs: action.breadcrumbs
            };
        case c.PRODUCT_LOADING:
            return {
                ...state,
                loading: action.loading
            };

        case c.PRODUCT_VIEW_ERROR:
            return {
                ...state,
                view: {...view, error: action.error}
            };
        case c.PRODUCT_VIEW_LOADING:
            return {
                ...state,
                view: {...view, loading: action.loading}
            };
        case c.PRODUCT_VIEW_RECEIVE:
            let { error, loading, url, product } = action;
            return {
                ...state,
                view: {
                    view: {...view.view, [url]: product},
                    error,
                    loading
                }
            };
        case c.PRODUCT_VIEW_INVALIDATE:
            delete view.view[action.url];
            return {
                ...state,
                view: view
            };

        case c.PRODUCT_RECEIVE_LOADING:
            return {
                ...state,
                related: {
                    ...state.related,
                    loading: action.loading
                }
            };
        case c.PRODUCT_RECEIVE_RELATED:
            return {
                ...state,
                related: {
                    related: {...state.related.related, [action.url]: action.objects},
                    loading: action.loading,
                    error: action.error
                }
            };
        case c.PRODUCT_SEARCH_RECEIVE:
            return {
                ...state,
                search: {
                    loading: action.loading,
                    objects: action.objects,
                    meta: action.meta
                }
            };

        case c.PRODUCT_SEARCH_LOADING:
            return {
                ...state,
                search: {
                    loading: true,
                    objects: [],
                    meta: {}
                }
            };
        case c.PRODUCT_AUTOCOMPLETE_RECEIVE:
            return {
                ...state,
                autocomplete: {
                    loading: action.loading,
                    objects: action.objects
                }
            };

        case c.PRODUCT_AUTOCOMPLETE_LOADING:
            return {
                ...state,
                autocomplete: {
                    loading: true,
                    objects: []
                }
            };

        case c.PRODUCT_NEW_LOADING:
            return {
                ...state,
                new: {
                    loading: true,
                    objects: []
                }
            };
        case c.PRODUCT_NEW_RECEIVE:
            return {
                ...state,
                new: {
                    loading: false,
                    objects: action.objects
                }
            };

        case c.PRODUCT_POPULAR_LOADING:
            return {
                ...state,
                popular: {
                    loading: true,
                    objects: []
                }
            };
        case c.PRODUCT_POPULAR_RECEIVE:
            return {
                ...state,
                popular: {
                    loading: false,
                    objects: action.objects
                }
            };

        default:
            return state
    }
}
