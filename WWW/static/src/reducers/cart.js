import c from '../constants';

const initialState = {
    loading: true,
    received: false,
    objects: [],
    deffered: [],
    count: 0,
    total: 0,
    didInvalidate: false,
    in_cart: []
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case c.CART_ADD:
            return {
                ...state,
                count: state.count + 1,
                in_cart: [...state.in_cart, action.id]
            };
        case c.CART_INVALIDATE:
            return {
                ...state,
                didInvalidate: action.didInvalidate
            };
        case c.CART_LOADING:
            return {
                ...state,
                loading: action.loading
            };
        case c.CART_RECEIVE:
            return {
                ...state,
                loading: false,
                total: action.total,
                objects: action.objects,
                received: true
            };
        case c.CART_DEFFERED_ADD:
            return {
                ...state,
                deffered: [
                    ...state.deffered,
                    action.product
                ]
            };
        case c.CART_DEFFERED_REMOVE:
            return {
                ...state,
                deffered: state.deffered.splice(action.index, 1)
            };
        case c.CART_CHANGE_SIZE:
            return {
                ...state,
                count: state.count - 1
            };
        case c.CART_REMOVE:
            return {
                ...state,
                count: state.count - 1
            };
        default:
            return state
    }
}
