import c from '../constants';
import {
    modalWindowUnlock,
    modalWindowLock
} from 'lib/utils';

const initialState = {
    queue: []
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case c.MODAL_OPEN:
            modalWindowLock();
            return {
                ...state,
                queue: [...state.queue, {
                    component: action.component,
                    style: action.style,
                    isVisible: action.isVisible
                }]
            };

        case c.MODAL_CLOSE:
            let queue = state.queue.slice(state.queue.length - 1);
            if (queue.length === 0) {
                modalWindowUnlock();
            }
            return { ...state, queue };

        case c.MODAL_CLEAR:
            modalWindowUnlock();
            return { ...state, queue: [] };

        default:
            return state
    }
}
