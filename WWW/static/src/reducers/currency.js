import c from '../constants';
import { local } from 'easy-storage';

let currencyData = {
    loading: false,
    received: false,
    objects: {},
    symbol: 'RUB',
    currency: undefined
};
const initialState = local.get('currency', currencyData);

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case c.CURRENCY_RECEIVE:
            return {
                ...state,
                loading: action.loading,
                objects: action.objects,
                currency: action.currency,
                received: action.received,
                symbol: action.symbol
            };
        case c.CURRENCY_LOADING:
            return {
                ...state,
                loading: action.loading
            };
        case c.CURRENCY_SET:
            return {
                ...state,
                symbol: action.symbol,
                currency: state.objects[action.symbol]
            };

        default:
            return state
    }
}