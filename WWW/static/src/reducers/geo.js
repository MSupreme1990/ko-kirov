import c from '../constants';
import { local } from 'easy-storage';

const initialState = {
    received: false,
    loading: false,
    site: {},
    city: {},
    regions: [],
    sites: [],
    cities: [],
    popular: [],

    current_cities: []
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case c.GEO_CURRENT_CITIES:
            return {
                ...state,
                current_cities: action.current_cities
            };
        case c.GEO_RECEIVE:
            return {
                ...state,
                site: action.site,
                city: action.city,
                regions: action.regions,
                popular: action.popular,
                sites: action.sites,
                cities: action.cities,
                loading: action.loading,
                received: action.received
            };
        case c.GEO_LOADING:
            return {
                ...state,
                loading: action.loading
            };
        case c.GEO_ERROR:
            return {
                ...state,
                error: action.error,
                loading: action.loading
            };

        default:
            return state
    }
}