import c from '../constants';

const initialState = {
    loading: false,
    objects: [],
    view: {
        view: {},
        loading: false,
        error: false
    }
};

export default (state = initialState, action = {}) => {
    let { view } = state;

    switch (action.type) {
        case c.PAGE_RECEIVE:
            return {
                ...state,
                objects: action.objects
            };

        case c.PAGE_LOADING:
            return {
                ...state,
                loading: action.loading
            };

        case c.PAGE_VIEW_ERROR:
            return {
                ...state,
                view: {...view, error: action.error}
            };

        case c.PAGE_VIEW_LOADING:
            return {
                ...state,
                view: {...view, loading: action.loading}
            };

        case c.PAGE_VIEW_RECEIVE:
            let viewObjects = view.view;
            let { error, loading, url, page } = action;
            let newView = {
                view: {...viewObjects, [url]: page},
                error,
                loading
            };
            return {
                ...state,
                view: newView
            };

        case c.PAGE_VIEW_INVALIDATE:
            delete view.view[action.id];
            return {
                ...state,
                view: view
            };

        default:
            return state
    }
}