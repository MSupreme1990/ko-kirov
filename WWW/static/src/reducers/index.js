import reviews from 'reducers/reviews';
import modal from 'reducers/modal';
import recent from 'reducers/recent';
import office from 'reducers/office';
import page from 'reducers/page';
import main from 'reducers/main';
import product from 'reducers/product';
import geo from 'reducers/geo';
import menu from 'reducers/menu';
import currency from 'reducers/currency';
import sizes from 'reducers/sizes';
import category from 'reducers/category';
import banner from 'reducers/banner';
import cart from 'reducers/cart';
import coupon from 'reducers/coupon';
import seo from 'reducers/seo';
import scripts from 'reducers/scripts';

export {
    scripts,
    seo,
    coupon,
    cart,
    banner,
    category,
    sizes,
    currency,
    menu,
    geo,
    reviews,
    modal,
    recent,
    office,
    page,
    product,
    main
}