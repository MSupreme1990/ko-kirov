import c from '../constants';

const initialState = {
    loading: false,
    received: false,
    error: false,
    menu: {}
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case c.MENU_RECEIVE:
            return {
                ...state,
                loading: action.loading,
                received: action.received,
                error: action.error,
                menu: action.menu
            };

        case c.MENU_LOADING:
            return {
                ...state,
                loading: action.loading
            };

        default:
            return state
    }
}
