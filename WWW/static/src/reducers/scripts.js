import c from '../constants';

const initialState = {
    vk: false,
    yandex: false
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case c.SCRIPTS_LOADING:
            return {
                ...state,
                [action.script]: action.loading
            };

        default:
            return state
    }
}