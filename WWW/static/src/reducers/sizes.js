import c from '../constants';
import { local } from 'easy-storage';

const initialState = local.get('size', {
    currentSize: null
});

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case c.SIZE_SET:
            return {...state, currentSize: action.size};

        default:
            return state
    }
}
