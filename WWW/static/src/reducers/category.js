import c from '../constants';
import { local } from 'easy-storage';

const initialState = local.get('category', {
    loading: false,
    error: false,
    objects: [],
    received: false
});

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case c.CATEGORY_RECEIVE:
            return {
                ...state,
                loading: action.loading,
                objects: action.objects,
                received: action.received,
                error: action.error
            };
        case c.CATEGORY_LOADING:
            return {
                ...state,
                loading: action.loading
            };

        default:
            return state
    }
}