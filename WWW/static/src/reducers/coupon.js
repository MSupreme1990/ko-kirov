import c from '../constants';

const initialState = {
    loading: false,
    objects: [],
    view: {
        view: {},
        loading: false,
        error: false
    }
};

export default (state = initialState, action = {}) => {
    let { view } = state;

    switch (action.type) {
        case c.COUPON_RECEIVE:
            return {
                ...state,
                objects: action.objects
            };

        case c.COUPON_LOADING:
            return {
                ...state,
                loading: action.loading
            };

        case c.COUPON_VIEW_ERROR:
            return {
                ...state,
                view: {...view, error: action.error}
            };

        case c.COUPON_VIEW_LOADING:
            return {
                ...state,
                view: {...view, loading: action.loading}
            };

        case c.COUPON_VIEW_RECEIVE:
            let viewObjects = view.view;
            let { error, loading, code, coupon } = action;
            let newView = {
                view: {...viewObjects, [code]: coupon},
                error,
                loading
            };
            return {
                ...state,
                view: newView
            };

        case c.COUPON_VIEW_INVALIDATE:
            delete view.view[action.code];
            return {
                ...state,
                view: view
            };

        default:
            return state
    }
}