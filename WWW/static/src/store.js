import {
    createStore,
    applyMiddleware,
    combineReducers,
    compose
} from 'redux';

import thunkMiddleware from 'redux-thunk';
// import createLogger from 'redux-logger';
import * as reducers from './reducers/index';
import settings from 'config/settings';

const rootReducer = combineReducers(reducers);

let createStoreWithMiddleware;

// if (settings.VERBOSE) {
//     createStoreWithMiddleware = compose(
//         applyMiddleware(thunkMiddleware, createLogger())
//     )(createStore);
// } else {
    createStoreWithMiddleware = compose(
        applyMiddleware(thunkMiddleware)
    )(createStore);
// }

function configureStore(initialState) {
    const store = createStoreWithMiddleware(rootReducer, initialState);

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('reducers', () => {
            const nextRootReducer = require('./reducers/index');
            store.replaceReducer(nextRootReducer)
        })
    }

    return store
}

const store = configureStore();
export default store;
