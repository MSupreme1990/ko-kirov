import L from 'leaflet';

let googleOptions = {
    attribution: "Map data &copy; <a href='http://googlemaps.com'>Google</a>",
    detectRetina: L.Browser.retina,
    subdomains: [0, 1, 2, 3]
};
if (L.Browser.retina) {
    googleOptions = { ...googleOptions, tileSize: 512 };
}

const GoogleTileLayer = L.tileLayer(
    "//mt{s}.googleapis.com/vt?x={x}&y={y}&z={z}" + (L.Browser.retina ? '&style=high_dpi&w=512' : ''),
    googleOptions
);

const GisTileLayer = L.tileLayer('http://tile{s}.maps.2gis.com/tiles?x={x}&y={y}&z={z}', {
    subdomains: [0, 1, 2, 3],
    attribution: '<a href="http://www.2gis.ru" target="_blank" title="Программа 2ГИС — Городской Информационный Справочник. Скачивайте бесплатно!">&copy; 2ГИС — Городской Информационный Справочник</a>'
});

const YandexTileLayer = L.tileLayer("//vec0{s}.maps.yandex.net/tiles?l=map&x={x}&y={y}&z={z}", {
    attribution: "Map data &copy; <a href='http://maps.yandex.ru'>Yandex.Maps</a>",
    subdomains: [1, 2, 3, 4],
    // see https://github.com/Leaflet/Leaflet/pull/2020
    // crs: L.CRS.EPSG3395
    crs: L.CRS.EPSG3395
});

export {
    GoogleTileLayer,
    GisTileLayer,
    YandexTileLayer,
};