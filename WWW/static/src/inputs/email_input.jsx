import React, { Component } from 'react';
import { cookie } from 'easy-storage';

export default class EmailInput extends Component {
    _default = undefined;
    _key = 'email';

    componentWillMount() {
        this._default = cookie.get(this._key);
    }

    handleChange(e) {
        cookie.set(this._key, e.target.value);

        if (this.props.onChange) {
            this.props.onChange(e);
        }
    }

    handleKeyUp(e) {
        cookie.set(this._key, e.target.value);
        if (this.props.onKeyUp) {
            this.props.onKeyUp(e);
        }
    }

    render() {
        return (
            <span className="input_span">
                <input type="text"
                       name={this.props.name || ''}
                       onChange={this.handleChange.bind(this)}
                       onKeyUp={this.handleKeyUp.bind(this)}
                       placeholder={this.props.placeholder || 'Введите Ваш e-mail'}
                       className={this.props.className || 'email'}
                       defaultValue={this.props.defaultValue || this._default}/>
                <ul className="errors"> </ul>
            </span>
        );
    }
}
