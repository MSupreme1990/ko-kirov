import $ from 'jquery';
import { findDOMNode } from 'react-dom';
import React, { Component, PropTypes } from 'react';
import api from 'lib/api';
import { sendToAmo, formValidate } from 'lib/utils';
import { Link } from 'react-easy-router';
import Loader from 'shared/components/loader.jsx';
import PhoneInputMask from 'inputs/phone_input_mask';

import * as cartActions from 'actions/cart';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class QuickOrderButton extends Component {
    static propTypes = {
        product: PropTypes.object.isRequired
    };

    state = {
        loading: false,
        show_form: false,
        message: null
    };

    handleOnClick(e) {
        e.preventDefault();

        const { cartActions, product, size, onClick } = this.props;
        cartActions.addToCart(product.id, size);

        this.setState({
            show_form: true
        });

        if (onClick) {
            onClick(e);
        }
    }

    handleQuickOrderHide(e) {
        e.preventDefault();
        
        this.setState({
            show_form: false
        });
    }

    handleQuickOrderSubmit(e) {
        e.preventDefault();

        let $form = $(findDOMNode(this.refs.form));
        this.setState({ loading: true });
        api.post('/api/ko/quick_order', $form.serialize(), data => {
            if (data['errors']) {
                this.setState({ loading: false }, ()=> {
                    formValidate($(findDOMNode(this.refs.form)), data['errors']);
                });
            }

            if (data['complete'] || data['success']) {
                this.setState({ message: data['message'], loading: false });
                setTimeout(()=> {
                    this.setState({ showQO: false });
                }, 5000);
                sendToAmo($form);
            }
        });
    }

    renderButton() {
        const { in_cart } = this.props;

        if (in_cart) {
            return (
                <Link to="Cart" className={'link-add-cart ' + (this.props.className || '')}>
                    Перейти в корзину
                </Link>
            );
        }

        return (
            <a className={'link-add-cart ' + (this.props.className || '')}
               href="#" onClick={this.handleOnClick.bind(this)}>
                Добавить в корзину
            </a>
        );
    }

    renderForm() {
        const { product, in_cart, size } = this.props;
        const { show_form, loading, message } = this.state;

        if (in_cart && show_form) {
            if (loading) {
                return (
                    <div className="quick-order-block">
                        <div className="close" onClick={this.handleQuickOrderHide.bind(this)}>x</div>
                        <div className="text">
                            <Loader />
                        </div>
                    </div>
                );
            }

            if (message) {
                return (
                    <div className="quick-order-block">
                        <div className="close" onClick={this.handleQuickOrderHide.bind(this)}>x</div>
                        <div className="text">
                            <h3>{this.state.message}</h3>
                        </div>
                    </div>
                );
            }

            return (
                <div className="quick-order-block">
                    <div className="close" onClick={this.handleQuickOrderHide.bind(this)}>x</div>
                    <div className="text">
                        Товар добавлен в <Link to="Cart" className='red'>корзину</Link>
                    </div>

                    <form id="quick-order-form" ref="form" onSubmit={this.handleQuickOrderSubmit.bind(this)}>
                        <div className="relative">
                            <PhoneInputMask name="QuickOrderForm[phone]"/>
                            <ul className="errors"></ul>
                        </div>

                        <input type="hidden" name="QuickOrderForm[size]" value={size}/>
                        <input type="hidden" name="QuickOrderForm[product]" value={product.id}/>
                        <input type="submit" value="Быстрый заказ" className="order-button maroon"/>
                        <div className="b-personal-data">
                            <span className="b-personal-data__check"><input type="checkbox" checked="checked"/></span>
                            Передавая информацию сайту вы принимаете условия
                            <a className="b-personal-data__link"
                               href="/page/private"
                               target="_blank">"политики защиты персональных данных".</a>
                        </div>
                    </form>
                </div>
            );
        }
    }

    render() {
        return (
            <span className={'link-add-cart-container ' + (this.props.className || '')}>
                {this.renderButton()}
                {this.renderForm()}
            </span>

        );
    }
}

export default connect((state, props) => {
    return {
        in_cart: state.cart.in_cart.indexOf(props.product.id) > -1
    };
}, dispatch => {
    return {
        cartActions: bindActionCreators(cartActions, dispatch)
    };
})(QuickOrderButton);
