import { cookie } from 'easy-storage';
import React, { PropTypes, Component } from 'react';
import { findDOMNode } from 'react-dom';
import api from 'lib/api';
import InputElement from 'react-input-mask';

export default class PhoneInput extends Component {
    _timer = undefined;
    _default = undefined;
    _key = 'phone';

    static propTypes = {
        onKeyUp: PropTypes.func
    };

    componentWillMount() {
        this._default = cookie.get(this._key);
    }

    sendPhoneValue(value) {
        if (this._timer) {
            clearTimeout(this._timer);
        }

        cookie.set(this._key, value);
        if (value.length == 18) {
            this._timer = setTimeout(function () {
                api.post('/api/ko/save-phone', { value });
            }, 150);
        }
    }

    handleChange(e) {
        if (this.props.onChange) {
            this.props.onChange(e);
        }
    }

    handleKeyUp(e) {
        this.sendPhoneValue(e.target.value);

        if (this.props.onKeyUp) {
            this.props.onKeyUp(e);
        }
    }

    render() {
        return <InputElement
            type="text"
            mask="+7 (999) 999-99-99"
            maskChar="_"
            required="required"
            minLength="9"
            ref="input"
            name={this.props.name || ''}
            onChange={this.handleChange.bind(this)}
            className={this.props.className || 'phone'}
            onKeyUp={this.handleKeyUp.bind(this)}
            placeholder={this.props.placeholder || "+7 (___) ___-__-__"}/>;
    }
}
