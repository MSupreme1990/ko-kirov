import $ from 'jquery';
import { cookie } from 'easy-storage';
import React from 'react';
import api from 'lib/api';
import loader from 'lib/loader';
import { modalWindowUnlock, dataLayer_push, sendToAmo, formValidate } from 'lib/utils';

var QuickOrderMixin = {
    componentDidMount: function() {
        var $form = $('#credit-order-form');
        $form.find('[type="text"]').first().focus();
    },
    getInitialState: function() {
        return {
            phone: '',
            extraOrder: false,
            extraCredit: false,
            pk: null,
            is_credit: null,
            product_id: this.props.product_id
        };
    },
    loadVkreditScript: function (callback) {
        if (typeof VkreditWidget == "undefined") {
            loader.loadFile(document, {
                src: 'https://www.kupivkredit.ru/widget/vkredit.js',
                tag: "script",
                type: "text/javascript"
            }, function () {
                callback();
            }.bind(this));
        } else {
            callback();
        }
    },
    handleQuickOrderSubmit: function(e) {
        e.preventDefault();

        var $form = $('#quick-order-form');

        api.post('/api/ko/quick_order', $form.serialize(), function(data) {
            sendToAmo($form);
            formValidate($form, data['errors']);

            if (data['complete']) {
                // google tag manager
                dataLayer_push({
                    'event': 'submit',
                    'action': 'buyNow'
                });
                this.hideOrderExtra();
                //$form.html('<h1>' + data['message'] + '</h1>');
                this.refs.modal.open();
            } else if (data['success']) {
                $form.find("#form_id").val(data['pk']);
                this.setState({
                    pk: data['pk']
                });
                this.openOrderExtra();
            }
        }.bind(this));
    },
    handleCreditOrderSubmit: function(e) {
        e.preventDefault();

        var $form = $('#credit-order-form');

        api.post('/api/ko/credit_order', $form.serialize(), function(data) {
            sendToAmo($form);
            formValidate($form, data['errors']);

            if (data['success']) {
                // google tag manager
                dataLayer_push({
                    'event': 'submit',
                    'action': 'credit'
                });
                $form.find("#form_id").val(data['pk']);
                this.hideCreditExtra();
                modalWindowUnlock();

                this.loadVkreditScript(function () {
                    var vkredit = new VkreditWidget(1, data['price'], {
                        order: data['base64'],
                        sig: data['sig']
                    });
                    vkredit.openWidget();
                }.bind(this));
            }
        }.bind(this));
    },
    hideCreditExtra: function() {
        this.setState({
            extraCredit: false
        });
    },
    hideOrderExtra: function() {
        this.setState({
            extraOrder: false
        });
    },
    openCreditExtra: function() {
        this.setState({
            extraCredit: true
        });
    },
    openOrderExtra: function() {
        this.setState({
            extraOrder: true
        });
    },
    handleClose: function(e) {
        e.preventDefault();
        this.hideOrderExtra();
        this.hideCreditExtra();
    },
    handleNameChange: function(e) {
        cookie.set('name', e.target.value);
    },
    handleEmailChange: function(e) {
        cookie.set('email', e.target.value);
    },
    getExtraForm: function(name, small) {
        var cls = "extra-form",
            nameField = null;

        var emailValue = cookie.get('email');

        if (small) {
            cls += " small";
        } else {
            nameField = (
                <div>
                    <input type="text" name={name + "[name]"} onChange={this.handleNameChange} value={name} placeholder="Введите ваше имя" />
                    <ul className="errors"></ul>
                </div>
            );
        }
        return (
            <div className={cls}>
                <div className="row">
                    <div className="columns medium-12">
                        <a href="#" className="extra-form-close" onClick={this.handleClose}>&times;</a>
                        <p className="extra-form-title">Ваша заявка успешно отправлена. Уточните, пожалуйста,<br/>
                            следующие данные:</p>
                    </div>
                    <div className="columns medium-6">
                        {nameField}
                        <div>
                            <input type="text" name={name + "[email]"} onChange={this.handleEmailChange} defaultValue={emailValue} placeholder="Введите ваш e-mail" />
                            <ul className="errors"></ul>
                        </div>
                        <div>
                            <input type="hidden" id="form_id" defaultValue={this.state.pk} name={name + "[id]"} />
                            <input type="submit" value="ЗАКАЗАТЬ В ОДИН КЛИК" className="button" />
                        </div>
                    </div>
                    <div className="columns medium-6">
                        <textarea name={name + "[comment]"} placeholder="Введите ваш комментарий" />
                        <ul className="errors"></ul>
                    </div>
                </div>
            </div>
        );
    }
};

export default QuickOrderMixin;