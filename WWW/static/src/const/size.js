const womanSizes = {
    40: false,
    42: false,
    44: false,
    46: false,
    48: false,
    50: false,
    52: false,
    54: false,
    56: false,
    58: false,
    60: false,
    62: false,
    64: false,
    66: false,
    68: false,
    70: false
};

const manSizes = {
    46: false,
    48: false,
    50: false,
    52: false,
    54: false,
    56: false,
    58: false,
    60: false,
    62: false,
    64: false,
    66: false,
    68: false,
    70: false
};

export {
    manSizes,
    womanSizes
}