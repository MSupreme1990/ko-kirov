import $ from 'jquery';
import api from 'lib/api';
import settings from 'config/settings';
import * as modalActions from 'actions/modal';
import store from 'store';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

export function bindConnect(reducers, actions = {}) {
    return connect(reducers, dispatch => {
        let reduxActions = {};
        for (let key in actions) {
            reduxActions[key] = bindActionCreators(actions[key], dispatch);
        }
        return reduxActions;
    });
}

export function AdWords_send(data = {}) {
    let conv_handler = window['google_trackConversion'];
    if (typeof(conv_handler) == 'function') {
        conv_handler({
            google_conversion_id: 955403784,
            google_custom_params: data,
            google_remarketing_only: true
        });
    }
}

export function chunk(arr, chunkSize) {
    arr = arr || [];
    return [].concat.apply([],
        arr.map(function (elem, i) {
            return i % chunkSize ? [] : [arr.slice(i, i + chunkSize)];
        })
    );
}

export function openModal(component, style, e) {
    if (!e) {
        e = style;
        style = {};
    }
    e.preventDefault();
    store.dispatch(modalActions.open(component, style));
}

export function closeModal(e) {
    if (e) e.preventDefault();
    store.dispatch(modalActions.close());
}

export function clearModal(e) {
    if (e) e.preventDefault();
    store.dispatch(modalActions.clear());
}

export function scrollBarWidth() {
    let width;
    document.body.style.overflow = 'hidden';
    width = document.body.clientWidth;

    document.body.style.overflow = 'scroll';
    width -= document.body.clientWidth;
    if (!width) {
        width = document.body.offsetWidth - document.body.clientWidth;
    }

    document.body.style.overflow = '';
    return width;
}

export function makeId(len = 5) {
    let text = "";
    let possible = "abcdefghijklmnopqrstuvwxyz0123456789";

    for (let i = 0; i < len; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}

export function parseYoutubeIframe(iframe) {
    let src = /src=["|'](.*?)["|']/.exec(iframe)[1];
    if (!src) {
        console.log('incorrect youtube url:', video);
        return;
    }

    let youtube_video_id = src.match(/youtube\.com.*(\?v=|\/embed\/)(.{11})/);
    if (youtube_video_id) {
        youtube_video_id = youtube_video_id.pop();

        let video_thumbnail;
        if (youtube_video_id.length == 11) {
            video_thumbnail = '//img.youtube.com/vi/' + youtube_video_id + '/0.jpg';
        }

        return {
            thumb: video_thumbnail,
            id: youtube_video_id
        };
    } else {
        return { thumb: '', id: '' }
    }
}

export function sendToAmo($form) {
    var formArray = $form.serializeArray(),
        data = {};

    for (var i = 0; i < formArray.length; i++) {
        var tmp = formArray[i],
            cleanName = tmp.name.replace(/\w+\[(\w+)\]/, "$1");
        if (cleanName == 'question') {
            cleanName = 'note';
        }
        data[cleanName] = tmp.value;
    }

    api.get('/cmdf5/addtoamo.php', data);
}

export function formValidate($form, errors = {}) {
    $form.find('input, select, textarea').each(function () {
        var $this = $(this),
            name = $this.attr('name'),
            $errors = $this.next('.errors');

        if ($errors.length) {
            $errors.html('');
            var name = $this.attr('name');
            if (typeof errors[name] != 'undefined') {
                $this.addClass('error');
                $errors.css('display', 'block');
                var inputErrors = errors[name];
                for (var key in inputErrors) {
                    var error = inputErrors[key];
                    $errors.append($('<li/>').text(error));
                }
            } else {
                $this.removeClass('error');
                $errors.css('display', 'none');
            }
        }
    });

    setTimeout(function () {
        $form.find('.errors').hide();
    }, 4000);
}

export function nl2br(str) {
    return str.replace(/([^>])\n/g, '$1<br/>')
}

export function getTimestamp() {
    return Math.random() * Math.random();
}

export function strtr(str, from, to) {
    var fr = '',
        i = 0,
        j = 0,
        lenStr = 0,
        lenFrom = 0,
        tmpStrictForIn = false,
        fromTypeStr = '',
        toTypeStr = '',
        istr = '';
    var tmpFrom = [];
    var tmpTo = [];
    var ret = '';
    var match = false;

    // Received replace_pairs?
    // Convert to normal from->to chars
    if (typeof from === 'object') {
        for (fr in from)
            if (from.hasOwnProperty(fr)) {
                tmpFrom.push(fr);
                tmpTo.push(from[fr]);
            }

        from = tmpFrom;
        to = tmpTo;
    }

    // Walk through subject and replace chars when needed
    lenStr = str.length;
    lenFrom = from.length;
    fromTypeStr = typeof from === 'string';
    toTypeStr = typeof to === 'string';

    for (i = 0; i < lenStr; i++) {
        match = false;
        if (fromTypeStr) {
            istr = str.charAt(i);
            for (j = 0; j < lenFrom; j++)
                if (istr == from.charAt(j)) {
                    match = true;
                    break;
                }
        } else
            for (j = 0; j < lenFrom; j++)
                if (str.substr(i, from[j].length) == from[j]) {
                    match = true;
                    // Fast forward
                    i = (i + from[j].length) - 1;
                    break;
                }
        if (match) ret += toTypeStr ? to.charAt(j) : to[j];
        else ret += str.charAt(i);
    }

    return ret;
}

export function dataLayer_push(data = false) {
    if (data) {
        var data_layer = window['dataLayer'];
        if (typeof(data_layer) == 'object') {
            data_layer.push(data);
        }
    }
}

export function modalWindowUnlock() {
    $('body').css({
        'overflow': '',
        'padding-right': ''
    });
}

export function fixBannerMediaUrl(str) {
    var pattern = /\/media/gi;
    if (str.match(pattern)) {
        return str.replace(pattern, settings.MEDIA_URL);
    } else {
        return settings.MEDIA_URL + '/' + str;
    }
}

export function fixRelativeMediaUrls(str) {
    return str.replace(/\/media/gi, settings.MEDIA_URL);
}

export function modalWindowLock() {
    $('body').css({
        'overflow': 'hidden',
        'padding-right': scrollBarWidth()
    });
}
