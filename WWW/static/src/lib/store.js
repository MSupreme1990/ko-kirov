import { EventEmitter } from 'events';

export default class Store extends EventEmitter {
    _state = {};

    constructor() {
        super();
        this.setMaxListeners(Number.POSITIVE_INFINITY);
        this._state = this.getInitialState();

        Object.defineProperty(this, "state", {
            get: () => {
                return this._state
            },
            set: (state) => {
                this.setState(state);
            }
        });
    }

    getInitialState() {
        return {};
    }

    getState() {
        return this._state;
    }

    setState(state = {}, callback = undefined) {
        this._state = {
            ...this._state,
            ...state
        };
        if (callback instanceof Function) {
            callback();
        }
        this.emit('change', this._state);
    }

    listen(callback) {
        this.removeListener('change', callback);
        this.addListener('change', callback);
        return () => {
            this.removeListener('change', callback);
        };
    }

    unlisten(callback) {
        this.removeListener('change', callback);
    }
};
