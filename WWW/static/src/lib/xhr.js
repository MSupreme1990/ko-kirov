import axios from 'axios';
import qs from 'qs';

const defaultHeaders = {
    "Accept": 'application/json'
};

const settings = {
    VERBOSE: true
};

function request(url, method, params = {}, data = {}, headers = defaultHeaders) {
    if (typeof data !== 'string') {
        data = qs.stringify(data);
    }

    if (false == /^(GET|HEAD|OPTIONS|TRACE)$/.test(method.toUpperCase())) {
        headers = {
            ...headers,
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
        }
    }

    return axios({
        url,
        params,
        data,
        method,
        headers
    }).then(response => {
        if (settings.VERBOSE) {
            console.group('fetch ' + url);
            console.log('%cQuery', 'color: #b6d655', params);
            console.log('%cBody', 'color: #b6d655', data);
            console.log('%cResponse', 'color: #55b6d6', response);
            console.groupEnd();
        }
        return response.data;
    }).catch(response => {
        if (settings.VERBOSE) {
            console.groupEnd();
            console.group('fetch error ' + url);
            console.log('%cData', 'color: #b6d655', data);
            if (response instanceof Error) {
                // Something happened in setting up the request that triggered an Error
                console.log('%cError', 'color: #a00', response.message);
            } else {
                if (response.status == 403) {
                    window.location = '/auth/login';
                } else {
                    // The request was made, but the server responded with a status code
                    // that falls out of the range of 2xx
                    console.log('%cError', 'color: #a00', response);
                }
            }
            console.groupEnd();
        }
    });
}

export default {
    get(url, data = {}, params = {}) {
        return request(url, 'GET', data, params);
    },

    patch(url, data = {}, params = {}) {
        return request(url, 'PATCH', data, params);
    },

    put(url, data = {}, params = {}) {
        return request(url, 'PUT', data, params);
    },

    delete(url, data = {}, params = {}) {
        return request(url, 'DELETE', data, params);
    },

    post(url, data = {}, params = {}) {
        return request(url, 'POST', data, params);
    }
}