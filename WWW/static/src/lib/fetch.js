import fetch from 'axios';
import Qs from 'query-string';
import urlJoin from 'url-join';
import settings from 'settings';

export default {
    _wrap(url, method, params = {}, data = {}) {
        let headers = {
                "Accept": 'application/json'
            },
            isGet = method.toLowerCase() === 'get';

        url = urlJoin(settings.BASE_URL, 'api', settings.API_VERSION, url);

        data = typeof data === 'string' ? data : Qs.stringify(data);

        let config = {
            url,
            params,
            data: data,
            method,
            headers: isGet ? headers : {
                ...headers,
                "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
            }
        };

        return fetch(config)
            .then(response => {
                if (settings.VERBOSE) {
                    console.group('fetch ' + url);
                    console.log('%cQuery', 'color: #b6d655', params);
                    console.log('%cBody', 'color: #b6d655', data);
                    console.log('%cResponse', 'color: #55b6d6', response);
                    console.groupEnd();
                }
                return response.data;
            })
            .catch(response => {
                console.groupEnd();
                console.group('fetch error ' + url);
                console.log('%cData', 'color: #b6d655', data);
                if (response instanceof Error) {
                    // Something happened in setting up the request that triggered an Error
                    console.log('%cError', 'color: #a00', response.message);
                } else {
                    // The request was made, but the server responded with a status code
                    // that falls out of the range of 2xx
                    console.log('%cError', 'color: #a00', response);
                }
                console.groupEnd();
            });
    },

    get(url, data = {}, params = {}) {
        return this._wrap(url, 'GET', data, params);
    },

    patch(url, data = {}, params = {}) {
        return this._wrap(url, 'PATCH', data, params);
    },

    put(url, data = {}, params = {}) {
        return this._wrap(url, 'PUT', data, params);
    },

    delete(url, data = {}, params = {}) {
        return this._wrap(url, 'DELETE', data, params);
    },

    post(url, data = {}, params = {}) {
        return this._wrap(url, 'POST', data, params);
    }
}