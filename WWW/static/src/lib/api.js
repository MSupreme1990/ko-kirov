import $ from 'jquery';
import settings from 'config/settings';

export default {
    _process: function (url, data, successFn, errFn, method, cache = false) {
        url = settings.BASE_URL + url || '';
        data = data || {};
        errFn = errFn || function (xhr, data, err) {
                if (window['showWarnings']) {
                    console.warn("Error:", err.toString(), xhr);
                }
            };
        method = method || 'GET';

        return $.ajax({
            url: url,
            cache: cache,
            crossDomain: true,
            method: method,
            data: data,
            success: successFn,
            error: errFn,
            beforeSend: function (jqXHR, settings) {
                jqXHR.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            }
        });
    },
    get: function (url, data, successFn, errFn) {
        return this._process(url, data, successFn, errFn, 'GET');
    },
    getCached: function (url, data, successFn, errFn) {
        return this._process(url, data, successFn, errFn, 'GET', true);
    },
    post: function (url, data, successFn, errFn) {
        return this._process(url, data, successFn, errFn, 'POST');
    },
    delete: function (url, data, successFn, errFn) {
        return this._process(url, data, successFn, errFn, 'DELETE');
    }
};