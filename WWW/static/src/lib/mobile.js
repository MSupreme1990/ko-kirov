class MobileDetect {
    Android() {
        return navigator.userAgent.match(/Android/ig);
    }

    BlackBerry() {
        return navigator.userAgent.match(/BlackBerry/ig);
    }

    iOS() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/ig);
    }

    Opera() {
        return navigator.userAgent.match(/Opera Mini/ig);
    }

    Windows() {
        return navigator.userAgent.match(/IEMobile|Windows Phone/ig);
    }

    any() {
        return (this.Android() || this.BlackBerry() || this.iOS() || this.Opera() || this.Windows());
    }
}

const mobile = new MobileDetect();

export default mobile;