import React, { Component, PropTypes } from 'react';
import { GisTileLayer, YandexTileLayer } from 'map/tiles';
import { findDOMNode } from 'react-dom';
import { nl2br } from 'lib/utils';
import mobile from 'lib/mobile';
import L from 'leaflet';
import settings from 'config/settings';

L.Icon.Default.imagePath = '/static/dist/images/main/';

export default class Map extends Component {
    map = null;
    markers = {};
    layer = null;

    icon = L.icon({
        iconUrl: '/static/dist/images/main/placemark.png',
        shadowUrl: null,
        iconSize: mobile.any() ? [25, 27] : [50, 54], // size of the icon
        shadowSize: [0, 0], // size of the shadow
        iconAnchor: mobile.any() ? [-2.5, -13.5] : [-5, -27], // point of the icon which will correspond to marker's location
        shadowAnchor: [0, 0],  // the same for the shadow
        popupAnchor: mobile.any() ? [12.5, 11] : [25, 22] // point from which the popup should open relative to the iconAnchor
    });

    static defaultProps = {
        id: 'make_id_for_me',
        objects: [],
        lat: 0.0,
        lng: 0.0,
        zoom: 12
    };

    getMarker(id) {
        return this.markers[id];
    }

    componentDidMount() {
        const { lat, lng, zoom, objects } = this.props;

        this.layer = new L.TileLayer("//vec0{s}.maps.yandex.net/tiles?l=map&x={x}&y={y}&z={z}", {
            attribution: "Map data &copy; <a href='http://maps.yandex.ru'>Yandex.Maps</a>",
            subdomains: [1, 2, 3, 4],
            // see https://github.com/Leaflet/Leaflet/pull/2020
            // crs: L.CRS.EPSG3395
            crs: L.CRS.EPSG3395
        });

        if (this.map) {
            this.destroyMap();
        }

        this.map = L.map(findDOMNode(this.refs.map), {
            center: new L.LatLng(lat, lng),
            zoom: zoom,
            minZoom: 4,
            maxZoom: 17,
            scrollWheelZoom: false,
            crs: L.CRS.EPSG3395,
            attributionControl: false
        });
        this.map.on('click', this.onMapClick.bind(this));
        // this.map.fitWorld();

        objects.map(model => {
            let popup = '<p><strong>' + model.name + '</strong></p>' +
                '<div>' + model.phone + "<br/>" + nl2br(model.worktime) + '</div>';

            this.markers[model.id] = L.marker([model.lat, model.lng], { icon: this.icon })
                .addTo(this.map)
                .bindPopup(popup);
        });

        this.map.removeLayer(this.layer);
        this.layer.addTo(this.map);
    }

    componentWillReceiveProps(nextProps) {

    }

    destroyMap() {
        this.map.remove();
        this.map = null;
    }

    componentWillUnmount() {
        this.destroyMap();
    }

    onMapClick() {
        // Do some wonderful map things...
    }

    render() {
        return <div id={this.props.id} ref="map"></div>;
    }
}