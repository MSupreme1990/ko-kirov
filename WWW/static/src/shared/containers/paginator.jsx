import React, { Component, PropTypes } from 'react';
import { range } from 'lodash';

export default class Paginator extends Component {
    static propTypes = {
        numPages: PropTypes.number.isRequired,
        page: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number
        ]),
        onClick: PropTypes.func.isRequired
    };

    static defaultProps = {
        numPages: 0,
        full: false,
        reverse: false,
        splitTemplate: false,
        lastText: "Последняя",
        firstText: "Первая",
        prevText: "Предыдущая",
        nextText: "Следующая",
        onClick: (e, n) => {
        }
    };

    state = {
        page: undefined
    };

    componentWillMount() {
        this.setState({
            page: Number(this.props.page) || 1
        });
    }

    /**
     * Triggered by any button click within the paginator.
     */
    handleClick(n, e) {
        e.preventDefault();

        // n is out of range, don't do anything
        if (n > this.props.numPages || n < 1) {
            return;
        }

        if (this.props.onClick instanceof Function) {
            let status = this.props.onClick(e, n);
            if (status === false) {
                return;
            }
        }

        this.setState({
            page: n
        });
    }

    /**
     * Returns a range [start, end]
     */
    getPageRange() {
        let pageCount = 5,
            displayCount = this.props.numPages,
            page = this.state.page,
        // Check position of cursor, zero based
            idx = (page - 1) % displayCount,
        // list should not move if cursor isn't passed this part of the range
            start = page - idx,
        // remaining pages
            remaining = this.props.numPages - page;

        // Don't move cursor right if the range will exceed the number of pages
        // in other words, we've reached the home stretch
        if (page > displayCount && remaining < displayCount) {
            // add 1 due to the implementation of `range`
            start = this.props.numPages - displayCount + 1;
        }

        let value = range(start, start + displayCount);
        if (!this.props.full) {
            let index = value.indexOf(page);
            // Right part
            value.splice(index + pageCount - 1, value.length - index);
            // Left part
            value.splice(0, value.length - pageCount - 2);
        }
        return this.props.reverse ? value.reverse() : value;
    }

    renderSplittedTemplate(pageNodes) {
        return (
            <div>
                <ul className="pager">
                    {pageNodes}
                </ul>
                <ul className='pager-nav'>
                    <li className='page--first'>
                        <a href='#' onClick={this.handleClick.bind(this, 1)}>
                            {this.props.firstText}
                        </a>
                    </li>
                    <li className='page--prev'>
                        <a href='#' onClick={this.handleClick.bind(this, this.state.page - 1)}>
                            {this.props.prevText}
                        </a>
                    </li>
                    <li className='page--next'>
                        <a href='#' onClick={this.handleClick.bind(this, this.state.page + 1)}>
                            {this.props.nextText}
                        </a>
                    </li>
                    <li className='page--last'>
                        <a href='#' onClick={this.handleClick.bind(this, this.props.numPages)}>
                            {this.props.lastText}
                        </a>
                    </li>
                </ul>
            </div>
        );
    }

    renderTemplate(pageNodes) {
        return (
            <div>
                <ul className='pager full'>
                    <li className='page--first'>
                        <a href='#' onClick={this.handleClick.bind(this, 1)}>
                            {this.props.firstText}
                        </a>
                    </li>
                    <li className='page--prev'>
                        <a href='#' onClick={this.handleClick.bind(this, this.state.page - 1)}>
                            {this.props.prevText}
                        </a>
                    </li>
                    {pageNodes}
                    <li className='page--next'>
                        <a href='#' onClick={this.handleClick.bind(this, this.state.page + 1)}>
                            {this.props.nextText}
                        </a>
                    </li>
                    <li className='page--last'>
                        <a href='#' onClick={this.handleClick.bind(this, this.props.numPages)}>
                            {this.props.lastText}
                        </a>
                    </li>
                </ul>
            </div>
        );
    }

    render() {
        if (this.props.numPages <= 1) {
            return null;
        }

        let prevClassName = this.state.page === 1 ? 'disabled prev-page' : 'prev-page',
            nextClassName = this.state.page >= this.props.numPages ? 'disabled next-page' : 'next-page';

        let pageNodes = this.getPageRange().map((n, i) => {
            let cls = '', link;
            if (this.state.page == n) {
                cls = 'active';
                link = <a href='#' onClick={(e) => e.preventDefault()}>{n}</a>;
            } else {
                link = <a href='#' onClick={this.handleClick.bind(this, n)}>{n}</a>;
            }
            return <li key={'page_' + i} className={cls}>{link}</li>;
        });

        let html;
        if (this.props.splitTemplate) {
            html = this.renderSplittedTemplate(pageNodes, prevClassName, nextClassName);
        } else {
            html = this.renderTemplate(pageNodes, prevClassName, nextClassName);
        }

        return (
            <div className="pager-container">
                {html}
            </div>
        );
    }
}
