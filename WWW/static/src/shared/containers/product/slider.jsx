import React, { Component, PropTypes } from 'react';
import Slider from 'react-slick';
import { makeId } from 'lib/utils';
import Product from 'mobile/components/catalog/product';

export default class ProductSlider extends Component {
    static propTypes = {
        title: PropTypes.string,
        prefix: PropTypes.string,
        data: PropTypes.array.isRequired
    };

    static defaultProps = {
        title: '',
        prefix: makeId()
    };

    render () {
        const { data, prefix, title } = this.props;

        let nodes = data.map(product => (
            <div key={prefix + product.id}>
                <Product product={product} group={data}/>
            </div>
        ));

        const sliderConfig = {
            dots: false,
            arrows: true,
            infinite: false,
            speed: 500,
            slidesToShow: 2,
            slidesToScroll: 2
        };

        return (
            <div className="product-slider">
                <h3 className="product-list-title">{title}</h3>
                <Slider className='slider' {...sliderConfig}>{nodes}</Slider>
            </div>
        );
    }
}
