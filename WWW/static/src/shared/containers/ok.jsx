import React, { Component } from 'react';
import loader from 'lib/loader';
import { findDOMNode } from 'react-dom';

export default class OkCommunity extends Component {
    okInit(callback) {
        if (typeof OK == "undefined") {
            loader.loadFile(document, {
                src: 'https://connect.ok.ru/connect.js',
                tag: "script",
                type: "text/javascript"
            }, callback);
        } else {
            callback();
        }
    }

    initOk() {
        setTimeout(() => {
            document.executed = true;
        }, 0);
    }

    componentDidMount() {
        if (typeof VK == "undefined") {
            this.okInit(() => {
                this.initOk();
            });
        } else {
            this.initOk();
        }
    }

    render() {
        return <span />;
    }
}
