import $ from 'jquery';
import React, { PropTypes, Component } from 'react';
import { findDOMNode } from 'react-dom';
import { loadVk } from 'lib/utils';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as scriptsActions from 'actions/scripts';

class VkCommunity extends Component {
    static defaultProps = {
        id: 'vk_groups'
    };

    shouldComponentUpdate() {
        return false;
    }

    componentWillUnmount() {
        $('#' + this.props.id).html('');
    }

    componentDidMount() {
        const { id, scriptsActions } = this.props;

        scriptsActions.loadVk(() => {
            VK.Widgets.Group(id, {
                mode: 0,
                // width: "302",
                width: 240,
                height: "260",
                color1: 'FFFFFF',
                color2: '830000',
                color3: '830000'
            }, 2539070);
        });
    }

    render() {
        return <div ref="vk" id={this.props.id}></div>;
    }
}

export default connect(state => {
    return {}
}, dispatch => {
    return {
        scriptsActions: bindActionCreators(scriptsActions, dispatch)
    };
})(VkCommunity);