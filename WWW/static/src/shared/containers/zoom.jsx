import React from 'react';
import PropTypes from 'prop-types';
import ImageZoom from './js-zoom';
import shallowequal from 'shallowequal';

export default class Zoom extends React.Component {
    container = undefined;

    static propTypes = {
        img: PropTypes.string.isRequired,
        height: PropTypes.number.isRequired,
        width: PropTypes.number.isRequired,
        zoomWidth: PropTypes.number,
        scale: PropTypes.number,
        offset: PropTypes.object,
        zoomStyle: PropTypes.string,
    };

    componentDidMount() {
        this.imageZoom = new ImageZoom(this.container, this.props);
    }

    componentWillUnmount() {
        this.kill();
    }

    componentWillReceiveProps(nextProps) {
        if (false === shallowequal(this.props, nextProps)) {
            this.kill();
            this.imageZoom = new ImageZoom(this.container, nextProps);
        }
    }

    kill() {
        console.log('kill');
        this.imageZoom.kill();
        this.imageZoom = void 0;
    }

    getRef = ref => {
        this.container = ref;
    };

    render() {
        return <div ref={this.getRef} />;
    }
}
