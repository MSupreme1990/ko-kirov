import React from 'react';

export default props => {
    return (
        <div className="row">
            <div className="columns small-12">
                <div className="error-container">
                    {props.message}
                </div>
            </div>
        </div>
    );
}