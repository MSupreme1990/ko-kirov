import React, { Component, PropTypes } from 'react';
import ReviewsSlideImage from 'shared/components/reviews_slide_image';
import { ModalLink } from 'shared/components/modal';
import { parseYoutubeIframe, openModal } from 'lib/utils';
import classNames from 'classnames';
import mobile from 'lib/mobile';

export default props => {
    let model = props.model,
        images = model.images ? model.images : [];

    let imageNodes = images.map(image => (
        <li key={image.id}>
            <ReviewsSlideImage image={image} model={model}/>
        </li>
    ));

    let videoNode = null;
    if (model.video) {
        let youtube = parseYoutubeIframe(model.video),
            videoHtml = {
                __html: '<iframe width="975" height="644" src="https://www.youtube.com/embed/' + youtube.id + '?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>'
            },
            videoInner = <img src={youtube.thumb}/>;

        videoNode = (
            <li>
                <a href="#" className="video"
                   onClick={openModal.bind(this, <div dangerouslySetInnerHTML={videoHtml}></div>)}>
                    {videoInner}
                </a>
            </li>
        );
    }

    let classMap = {};

    let panel = null;
    if (props.is_expert == false && (images.length > 0 || model.video)) {
        classMap["with-panel"] = true;
        panel = (
            <div className="columns small-12">
                <div className="reviews-full-item-panel">
                    <ul className="reviews-full-item-panel-list">
                        {imageNodes}
                        {videoNode}
                    </ul>
                    <p className="clear"/>
                </div>
            </div>
        );
    }

    let content = null;
    if (model.image) {
        content = (
            <div className="row">
                <div className={classNames("columns", {"small-2": !mobile.any(), "small-4": mobile.any()})}>
                    <div className="reviews-full-item-image">
                        <img src={model.image.image.thumb}/>
                    </div>
                </div>
                <div className={classNames("columns", {"small-10": !mobile.any(), "small-8": mobile.any()})}>
                    <div className={classNames("reviews-full-item-content", classMap)}>
                        <p className="reviews-full-item-title">{model.name}</p>
                        <p className="reviews-full-item-author">{model.author}</p>
                        <div dangerouslySetInnerHTML={{__html: model.content}}></div>
                    </div>
                </div>
                {panel}
            </div>
        );
    } else {
        content = (
            <div className="row">
                <div className="columns small-12">
                    <div className={classNames("reviews-full-item-content", classMap)}>
                        <p className="reviews-full-item-title">{model.name}</p>
                        <p className="reviews-full-item-author">{model.author}</p>
                        <div dangerouslySetInnerHTML={{__html: model.content}}></div>
                    </div>
                </div>
                {panel}
            </div>
        );
    }

    return <div key={model.id} className="reviews-full-item">{content}</div>;
}