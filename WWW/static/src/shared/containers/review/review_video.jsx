import React from 'react';
import { openModal, parseYoutubeIframe } from 'lib/utils';

export default props => {
    const { model } = props;

    const width = 800;
    const height = 450;

    let youtube = parseYoutubeIframe(model.video),
        videoHtml = {
            __html: `<iframe src="https://www.youtube.com/embed/${youtube.id}?rel=0&amp;showinfo=0" width="${width}" height="${height}" frameborder="0" allowfullscreen></iframe>`
        },
        component = <div dangerouslySetInnerHTML={videoHtml}></div>;

    return (
        <a href="#" className="video" onClick={openModal.bind(this, component, {padding: 0, width, height})}>
            <img src={youtube.thumb}/>
        </a>
    );
}
