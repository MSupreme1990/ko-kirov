import React from 'react';
import ReviewVideo from 'shared/containers/review/review_video';

export default props => {
    const { reviews, count } = props;

    if (reviews.length == 0 || count == 0) {
        return <span />;
    }

    var reviewNodes = reviews.slice(0, count).map(model => (
        <li key={model.id}>
            <ReviewVideo model={model}/>
        </li>
    ));

    return (
        <ul className={"small-block-grid-" + count + " reviews-block-video-list"}>
            {reviewNodes}
        </ul>
    );
};
