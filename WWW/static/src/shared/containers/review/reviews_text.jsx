import React, { PropTypes, Component } from 'react';
import { Link } from 'react-easy-router';
import Review from 'shared/containers/review/review_text';

export default props => {
    const { reviews } = props;

    let nodes = reviews.map((model, i) => (
        <li key={i}>
            <Review model={model} is_expert={true}/>
        </li>
    ));

    return (
        <ul className="small-block-grid-1 reviews-block-video-list">
            {nodes}
        </ul>
    );
}
