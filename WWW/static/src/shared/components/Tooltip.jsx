import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class Tooltip extends Component {
    static propTypes = {
        children: PropTypes.any.isRequired,
        content: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.array,
        ]),
    };

    state = {
        visible: false
    };

    show = e => {
        this.setState({ visible: true });
    };

    hide = e => {
        this.setState({ visible: false });
    };

    handleTouch = () => {
        this.show();
        this.assignOutsideTouchHandler();
    };

    assignOutsideTouchHandler = () => {
        const handler = (e) => {
            let currentNode = e.target;
            const componentNode = findDOMNode(this.refs.instance);
            while (currentNode.parentNode) {
                if (currentNode === componentNode) {
                    return;
                }
                currentNode = currentNode.parentNode;
            }
            if (currentNode !== document) {
                return;
            }
            this.hide();
            document.removeEventListener('touchstart', handler);
        };
        document.addEventListener('touchstart', handler);
    };

    renderTooltip() {
        const { content } = this.props;

        const cls = classnames('b-tooltip__tooltip', {
            'b-tooltip__tooltip_open': this.state.visible
        });

        return (
            <div ref="tooltip" className={cls}>
                <div ref="content" className="b-tooltip__content">{content}</div>
                <div ref="arrow" className="b-tooltip__arrow"></div>
                <div ref="gap" className="b-tooltip__gap"></div>
            </div>
        );
    }

    render() {
        const { children } = this.props;

        return (
            <div
                onMouseEnter={this.show}
                onMouseLeave={this.hide}
                onTouchStart={this.handleTouch}
                ref="wrapper"
                className="b-tooltip__wrapper">
                {children}
                {this.renderTooltip()}
            </div>
        )
    }
}