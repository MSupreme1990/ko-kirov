import React, { Component, PropTypes } from 'react';
import HeaderMenuItem from 'shared/components/header/header_menu_item';
import { Link } from 'react-easy-router';
import Autocomplete from 'shared/components/product/autocomplete';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as menuActions from 'actions/menu';

class HeaderMenu extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    static propTypes = {
        q: PropTypes.string
    };

    static defaultProps = {
        q: ''
    };

    state = {
        search_models: []
    };

    componentWillMount() {
        const { menuActions } = this.props;
        menuActions.fetchListIfNeeded();
    }

    render() {
        const { query, q } = this.props;

        const { loading, menu } = this.props.menu;

        if (loading) {
            return <p>Загрузка...</p>;
        }

        let objects = [];
        if ('header-menu' in menu) {
            objects = menu['header-menu'];
        }

        return (
            <div className="menu-container">
                <div className="menu-image">
                    <div className="row">
                        <div className="columns large-12">
                            {(() => {
                                if (false) {
                                    return (
                                        <a href="#" className="request-call" onClick={openModal.bind(this, <RequestCall />)}>Заказать звонок</a>
                                    );
                                }
                            })()}

                            <Autocomplete q={q} />

                            <ul id="menu">
                                {objects.map(model => (
                                    <HeaderMenuItem key={model.id} model={model}></HeaderMenuItem>
                                ))}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(state => {
    return {
        menu: state.menu
    }
}, dispatch => {
    return {
        menuActions: bindActionCreators(menuActions, dispatch)
    };
})(HeaderMenu);