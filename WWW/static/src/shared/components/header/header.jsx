import React, { Component, PropTypes } from 'react';
import { Link } from 'react-easy-router';
import { ModalLink } from '../modal';
import CartPopup from '../cart/cart_popup';
import Geo from 'shared/components/geo';
import SubscribeModal from '../subscribe_modal';
import HeaderMenu from './header_menu';
import { GetCouponForm } from 'shared/components/forms';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as geoActions from 'actions/geo';
import { openModal } from 'lib/utils';
import Logo from 'shared/containers/logo';

class Header extends Component {
    static contextTypes = {
        router: React.PropTypes.object
    };

    static propTypes = {
        q: PropTypes.string
    };

    static defaultProps = {
        q: '',
        subscribe_timeout: 120000
    };

    componentWillMount() {
        const { geoActions } = this.props;
        geoActions.fetchListIfNeeded();
    }

    componentDidMount() {
        //временно отрубаем всплывашку
        //if (!Cookie.get('popup_fired')) {
        //    setTimeout(() => {
        //        this.refs.subscribe.open();
        //        Cookie.set('popup_fired', true);
        //    }, this.props.subscribe_timeout);
        //}
    }

    handlerShowCouponForm(e) {
        if (e) {
            e.preventDefault();
        }

        if (this.refs.coupon) {
            this.refs.coupon.open();
        }
    }

    render() {
        const { geo, q } = this.props;

        if (geo.loading || !geo.received) {
            return null;
        }

        let address = geo.city.id == 1 ? (
            <div>
                <p>
                    <span><Link to="KoContacts">Магазины</Link> в г.</span>
                    <a href="#" onClick={openModal.bind(this, <Geo />)}>{geo.city.name}</a>:
                    ТЦ "Лето", Привокзальная пл. 1,<br/>ТЦ "Куб", Горького 5а
                    <strong> {geo.site.phone_main}</strong>
                </p>
            </div>
        ) : (
            <div>
                <p>
                    <a href="#" onClick={openModal.bind(this, <Geo />)}>{geo.city.name}</a>:
                    <strong>{geo.site.phone_main}</strong>
                </p>
            </div>
        );

        return (
            <div id="header">
                <div className="header-container">
                    <div className="row">
                        <div className="columns medium-4 text-left left-side">
                            {address}
                            <p>
                                <span className="whatsapp">WhatsApp</span>
                                <span
                                    className="viber">Viber: <strong>8 (982) 811-77-77</strong> (24 часа онлайн)</span>
                            </p>
                        </div>
                        <div className="columns medium-4 logo-container">
                            <h1 id="logo">
                                <Link to="Index" title="Кожаная одежда">
                                    <Logo />
                                </Link>
                            </h1>
                        </div>
                        <div className="columns medium-4">
                            <div className="text-right right-side">
                                <CartPopup handlerCoupon={this.handlerShowCouponForm.bind(this)}/>

                                <div className="contact-container">
                                    <p>Для заказа по России</p>

                                    <p className="header-phone"><a href="tel:+78005001422">8 (800) 500-14-22</a></p>

                                    <p>Звоните бесплатно</p>
                                    <ModalLink ref="subscribe"
                                               name="Форма подписки"
                                               linkClassName="hide subscribe-modal"
                                               className="subscribe-modal">
                                        <SubscribeModal />
                                    </ModalLink>

                                    <ModalLink ref="coupon"
                                               name="форма получения купона"
                                               linkClassName="hide"
                                               className="feedback-modal">
                                        <GetCouponForm />
                                    </ModalLink>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <HeaderMenu q={q}/>
            </div>
        );
    }
}

export default connect(state => {
    return {
        geo: state.geo
    };
}, dispatch => {
    return {
        geoActions: bindActionCreators(geoActions, dispatch)
    };
})(Header);
