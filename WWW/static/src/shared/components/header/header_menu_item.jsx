import React from 'react';
import { Link } from 'react-easy-router';
import { openModal } from 'lib/utils';
import OrderStatus from '../order_status';
import HowOrderModal from '../how_order_modal';

var HeaderMenuItem = React.createClass({
    render: function () {
        var model = this.props.model;
        var link;

        if (model.id == 42) {
            link = (
                <a onClick={openModal.bind(this, <OrderStatus/>, {width: 540, padding: 0})} className="order-status" href="#">
                    Узнать статус заказа
                </a>
            );
        } else if (model.id == 43) {
            link = (
                <a href="#" className="how-order-modal" onClick={openModal.bind(this, <HowOrderModal />, {padding: 0})}>
                    Как сделать заказ?
                </a>
            );
        } else if (model.react_route.data instanceof Array) {
            link = (
                <Link to={model.react_route.route}>
                    {model.name}
                </Link>
            );
        } else {
            link = (
                <Link to={model.react_route.route} params={model.react_route.data}>
                    {model.name}
                </Link>
            );
        }


        return (
            <li className={model.items.length > 0 ? 'has-nested' : ''}>
                {link}

                {(()=> {
                    if (model.items.length > 0) {
                        return (
                            <ul className="menu-nested">
                                {(()=> {
                                    return model.items.map(item => {
                                        return (
                                            <HeaderMenuItem
                                                key={item.id}
                                                model={item}>
                                            </HeaderMenuItem>
                                        );
                                    })
                                })()}
                            </ul>
                        );
                    }
                })()}
            </li>);
    }
});

export default HeaderMenuItem;