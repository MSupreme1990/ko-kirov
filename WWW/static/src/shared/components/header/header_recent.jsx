import $ from 'jquery';
import api from 'lib/api';
import { formValidate, sendToAmo } from 'lib/utils';
import { findDOMNode } from 'react-dom';
import React, { Component } from 'react';
import { Link } from 'react-easy-router';
import ProductSlider from 'shared/components/product/product_slider';
import { local } from 'easy-storage';
import EmailInput from 'inputs/email_input';

import * as recentActions from 'actions/recent';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class HeaderRecent extends Component {
    state = {
        show: false,
        showed: false,
        success: false
    };

    showOnHover() {
        this.setState({
            show: true,
            success: false
        });
    }

    hide() {
        this.setState({
            show: false
        });

    }

    handlerSubmit(e) {
        e.preventDefault();

        const { models } = this.props;

        let $form = $(findDOMNode(this.refs.form));
        let data = $form.serialize(),
            modelIds = models.map(model => model.id).join(',');

        data = data + '&products=' + modelIds;

        api.post('/api/ko/send-to-email', data, (data) => {
            formValidate($form, data['errors']);
            sendToAmo($form);

            if (data['success']) {
                this.setState({ success: true }, ()=> {
                    setTimeout(() => this.hide(), 4000);
                });
            }
        });
    }

    render() {
        const { models } = this.props;

        const { showed, show } = this.state;

        if (models.length < 3) {
            return null;
        }

        if (showed) {
            return null;
        }

        if (show == false) {
            return <div className="header-recent-me" onMouseOver={this.showOnHover.bind(this)}></div>;
        }

        return (
            <div className={"header-recent" + (this.state.show ? ' show' : '')}>
                <div className="row">
                    <div className="columns large-12">
                        <div className="product-list-container ">
                            <h3 className="product-list-title">
                                Мы подготовили все изделия, которые Вы смотрели, и можем отправить их на ваш адрес
                                E-mail, чтобы
                                Вам не пришлось снова искать их в нашем каталоге!
                            </h3>
                            <ProductSlider
                                data={models.reverse()}
                                title=''
                                prefix="hr"/>
                        </div>
                    </div>
                </div>
                <div className="form-line">
                    <div className="row">
                        <div className="columns large-12">
                            <form ref="form" onSubmit={this.handlerSubmit.bind(this)}>
                                {(()=> {
                                    if (this.state.success) {
                                        return (
                                            <span className="text">
                                                Товары были отправлены сообщением на указанный email.
                                            </span>
                                        );
                                    }

                                    return (
                                        <span>
                                            <span className="text">
                                                Отправить изделия на почту:
                                            </span>

                                            <EmailInput name="RecentEmailForm[email]"/>

                                            <button className="button"> Отправить </button>
                                            <span className="close" onClick={this.hide.bind(this)}> </span>
                                        </span>
                                    );
                                })()}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(state => state.recent, dispatch => {
    return {
        recentActions: bindActionCreators(recentActions, dispatch)
    };
})(HeaderRecent);