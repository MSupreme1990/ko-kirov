import React from 'react';
import PhoneInput from 'inputs/phone_input';
import { cookie } from 'easy-storage';
import { sendToAmo, formValidate, dataLayer_push } from 'lib/utils';

var SubscribeModal = React.createClass({
    getInitialState: function () {
        return {
            message_extra: '',
            message_base: ''
        };
    },
    handleSubmit: function (e) {
        e.preventDefault();

        dataLayer_push({
            'event': 'submit',
            'action': 'subscribeGift'
        });

        var $form = $('#subscribe-modal-form');
        api.post('/api/mail/subscribe', $form.serialize(), function (data) {
            sendToAmo($form);
            formValidate($form, data['errors']);

            if (data['success']) {
                this.setState({
                    pk: data['pk'],
                    message_base: 'Благодарим Вас за подписку на ko-kirov.ru!'
                });
            }
        }.bind(this));
    },
    handleExtraSubmit: function(e) {
        e.preventDefault();

        var $form = $('#subscribe-question-form-modal');

        api.post('/api/mail/subscribe_extra', $form.serialize(), function(data) {
            sendToAmo($form);
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    message_extra: data['message']
                });

                setTimeout(function() {
                    this.setState({pk: null});
                }.bind(this), 3500);
            }
        }.bind(this));
    },
    handleClose: function(e) {
        e.preventDefault();
        this.setState({pk: null});
    },
    renderExtra: function() {
        if (this.state.pk == null) {
            return <span />;
        }
        var email = cookie.get('email');
        return (
            <div className={this.state.message_extra == '' ? 'extra-form-popup' : 'extra-form-popup complete'}>
                <h1 className={this.state.message_extra == '' ? 'hide' : ''}>{this.state.message_extra}</h1>
                <a href="#" className="extra-form-close" onClick={this.handleClose}>&times;</a>
                <form id="subscribe-question-form-modal" className={this.state.message_extra == '' ? 'row' : 'row hide'} onSubmit={this.handleExtraSubmit} >
                    <div className="row">
                        <div className="columns large-10">
                            <p className="extra-form-title">Введите Ваш номер телефона, чтобы мы смогли удостовериться, что Вы получили подарок:</p>
                        </div>
                    </div>

                    <div className="row">
                        <div className="columns large-10">
                            <div className="subscribe-call-form-row form-row">
                                <PhoneInput name="phone" />
                                <ul className="errors"></ul>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="columns large-10">
                            <input type="hidden" name="id" value={this.state.pk} />
                            <input type="submit" value="ОТПРАВИТЬ" className="button" />
                        </div>
                    </div>

                </form>
            </div>
        );
    },
    render: function () {
        var modal = this.renderExtra();
        var emailValue = cookie.get('email');
        return (
            <div class="subscribe-modal">
                {modal}
                <form id="subscribe-modal-form" onSubmit={this.handleSubmit}>
                    <h1 className={this.state.message_base == '' ? 'hide' : ''}>{this.state.message_base}</h1>
                    <div className={this.state.message_base == '' ? '' : 'hide'}>
                        <h3>Подпишитесь на
                            <br/>
                            новости и получите
                            <br/>
                            приятный подарок!</h3>
                        <div className="form-row">
                            <input type="text" name="email" defaultValue={emailValue} placeholder="Введите ваш e-mail" />
                            <ul className="errors"></ul>
                        </div>
                        <div className="form-row">
                            <input type="submit" value="ПОЛУЧИТЬ ПОДАРОК" className="button" />
                        </div>
                    </div>
                </form>
            </div>
        );
    }
});

export default SubscribeModal;