import React from 'react';
import $ from 'jquery';

export default props => (
    <div className="show-reviews-container">
        <a href="#" onClick={e => {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: $(".reviews-block-container").offset().top
            }, 500);
        }} role="button" className="show-reviews-link">Посмотрите отзывы наших клиентов</a>
    </div>
);