import React, { Component, PropTypes } from 'react';
import RangeSlider from 'react-slider';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as productActions from 'actions/product';

class SizeRange extends Component {
    static propTypes = {
        productActions: PropTypes.object.isRequired,
        min: PropTypes.number,
        max: PropTypes.number,
        onChange: PropTypes.func.isRequired
    };

    static defaultProps = {
        minLimit: 40,
        min: 40,
        maxLimit: 70,
        max: 70,
        step: 2
    };

    onChangeSlider(value) {
        const { onChange } = this.props;
        onChange(value[0], value[1]);
    }

    render() {
        const { step, minLimit, maxLimit } = this.props;

        let min = this.props.min || this.props.minLimit,
            max = this.props.max || this.props.maxLimit;

        return (
            <div>
                <input className="min" readOnly="true" type="text" value={min}/>
                <RangeSlider
                    min={minLimit} max={maxLimit} minDistance={step} step={step}
                    onAfterChange={this.onChangeSlider.bind(this)}
                    value={[min, max]} withBars/>
                <input className="max" readOnly="true" type="text" value={max}/>
            </div>
        );
    }
}

export default connect(state => {
    return {
        category: state.product.category
    };
}, dispatch => {
    return {
        productActions: bindActionCreators(productActions, dispatch)
    };
})(SizeRange);