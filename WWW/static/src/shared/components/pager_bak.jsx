import React, { Component } from 'react';

/**
 * React Paginator
 *
 * @prop {number} numPages - Available number of pages
 * @prop {number} [maxPages] - Max number of pages to display
 * @prop {function} [onClick] - Fired on every click and passes the page number
 */
class Paginator extends Component {
    static propTypes = {
        numPages: React.PropTypes.number.isRequired,
        page: React.PropTypes.number.isRequired,
        onClick: React.PropTypes.func.isRequired
    };

    static defaultProps = {
        numPages: 0,
        full: false,
        reverse: false,
        splitTemplate: false,
        prevText: "Prev",
        nextText: "Next",
        onClick: (e, n) => {
        }
    };

    state = {
        page: undefined
    };

    componentWillMount() {
        this.setState({
            page: this.props.page || 1
        });
    }

    /**
     * Triggered by any button click within the paginator.
     */
    handleClick(e) {
        e.preventDefault();

        var n = e.target.getAttribute('data-page');
        // n is out of range, don't do anything
        if (n > this.props.numPages || n < 1) {
            return;
        }

        if (this.props.onClick instanceof Function) {
            var status = this.props.onClick(e, n);
            if (status === false) {
                return;
            }
        }

        this.setState({
            page: n
        });
    }

    /**
     * Returns a range [start, end]
     */
    getPageRange() {
        var pageCount = 5,
            displayCount = this.props.numPages,
            page = this.state.page,
        // Check position of cursor, zero based
            idx = (page - 1) % displayCount,
        // list should not move if cursor isn't passed this part of the range
            start = page - idx,
        // remaining pages
            remaining = this.props.numPages - page;

        // Don't move cursor right if the range will exceed the number of pages
        // in other words, we've reached the home stretch
        if (page > displayCount && remaining < displayCount) {
            // add 1 due to the implementation of `range`
            start = this.props.numPages - displayCount + 1;
        }

        var value = this.range(start, start + displayCount);
        if (!this.props.full) {
            var index = value.indexOf(page);
            // Right part
            value.splice(index + pageCount - 1, value.length - index);
            // Left part
            value.splice(0, value.length - pageCount - 2);
        }
        return this.props.reverse ? value.reverse() : value;
    }

    // Slighlty modified from underscore source
    range(start, stop) {
        if (arguments.length <= 1) {
            stop = start || 0;
            start = 0;
        }

        var length = Math.max(stop - start, 0);
        var idx = 0;
        var arr = new Array(length);

        while (idx < length) {
            arr[idx++] = start;
            start += 1;
        }

        return arr;
    }

    renderSplittedTemplate(pageNodes, prevClassName, nextClassName) {
        return (
            <div>
                <ul className="pager">
                    {pageNodes}
                </ul>
                <ul className='pager-nav'>
                    <li className={prevClassName}>
                        <a href='#' onClick={this.handleClick.bind(this)} data-page={this.state.page - 1}>
                            {this.props.prevText}
                        </a>
                    </li>
                    <li className={nextClassName}>
                        <a href='#' onClick={this.handleClick.bind(this)} data-page={this.state.page + 1}>
                            {this.props.nextText}
                        </a>
                    </li>
                </ul>
            </div>
        );
    }

    renderTemplate(pageNodes, prevClassName, nextClassName) {
        return (
            <div>
                <ul className='pager full'>
                    <li className={prevClassName}>
                        <a href='#' onClick={this.handleClick.bind(this)} data-page={this.state.page - 1}>
                            {this.props.prevText}
                        </a>
                    </li>
                    {pageNodes}
                    <li className={nextClassName}>
                        <a href='#' onClick={this.handleClick.bind(this)} data-page={this.state.page + 1}>
                            {this.props.nextText}
                        </a>
                    </li>
                </ul>
            </div>
        );
    }

    render() {
        if (this.props.numPages <= 1) {
            return null;
        }

        var prevClassName = this.state.page === 1 ? 'disabled prev-page' : 'prev-page',
            nextClassName = this.state.page >= this.props.numPages ? 'disabled next-page' : 'next-page';

        var pageNodes = this.getPageRange().map(function (n, i) {
            var cls = '', link;
            if (this.state.page == n) {
                cls = 'active';
                link = (
                    <a href='#' onClick={(e) => e.preventDefault()} data-page={n}>{n}</a>
                );
            } else {
                link = (
                    <a href='#' onClick={this.handleClick.bind(this)} data-page={n}>{n}</a>
                );
            }
            return (
                <li key={'page_' + i} className={cls}>
                    {link}
                </li>
            );
        }.bind(this));

        var html;
        if (this.props.splitTemplate) {
            html = this.renderSplittedTemplate(pageNodes, prevClassName, nextClassName);
        } else {
            html = this.renderTemplate(pageNodes, prevClassName, nextClassName);
        }

        return (
            <div className="pager-container">
                {html}
            </div>
        );
    }
}

export default Paginator;