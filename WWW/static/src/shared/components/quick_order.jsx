import $ from 'jquery';
import React from 'react';
import Cookie from 'js-cookie';
import PhoneInputMask from 'inputs/phone_input_mask';
import PhoneInput from 'inputs/phone_input';
import QuickOrderMixin from 'mixins/quick_order_mixin';
import SocialModal from 'shared/components/social_modal';

let QuickOrder = React.createClass({
    mixins: [QuickOrderMixin],
    handleTabClick: function (e) {
        e.preventDefault();
        let $this = $(e.target),
            $li = $this.parent(),
            index = $li.index(),
            $tabs = $this.closest('.tabs'),
            $panels = $tabs.find('.tab-panel');
        $panels.removeClass(' is-active');
        $panels.eq(index).addClass(' is-active');

        $tabs.find('.tabs-menu li').removeClass('is-active');
        $li.addClass('is-active');
    },
    handlePhoneChange: function (e) {
        this.setState({
            phone: e.target.value
        });
    },
    render: function () {
        let extraOrder = null,
            extraCredit = null,
            tabActive = 1;
        if (this.state.extraOrder) {
            extraOrder = this.getExtraForm('QuickOrderForm', true);
        }
        if (this.state.extraCredit) {
            tabActive = 2;
            extraCredit = this.getExtraForm('CreditOrderForm', true);
        }

        let name = Cookie.get('name');
        let currentSize = Cookie.get('current_size');

        return (
            <div className="quick-order">
                <SocialModal ref="modal" />
                <div className="tabs">
                    <div className="tabs-navigation">
                        <ul className="tabs-menu">
                            <li className="tabs-menu-item">
                                <a onClick={this.handleTabClick} href="javascript:;">Быстрый заказ</a>
                            </li>
                            <li className="tabs-menu-item">
                                <a onClick={this.handleTabClick} href="javascript:;">Купить в кредит</a>
                            </li>
                        </ul>
                    </div>
                    <div className="tab-panel">
                        <p>Для бесплатной примерки введите свой номер телефона и нажмите «Заказать в один клик».
                            Менеджер позвонит Вам и уточнит детали заказа.</p>

                        <form id="quick-order-form" onSubmit={this.handleQuickOrderSubmit}>
                            {extraOrder}
                            <div className="columns medium-4">
                                <input type="text" name="QuickOrderForm[name]" onChange={this.handleNameChange}
                                       defaultValue={name} placeholder="Введите ваше имя"/>
                                <ul className="errors"></ul>
                            </div>
                            <div className="columns medium-4">
                                <PhoneInput name="QuickOrderForm[phone]" onChange={this.handlePhoneChange} />
                                <ul className="errors"></ul>
                            </div>
                            <div className="columns medium-4">
                                <input type="hidden" name="QuickOrderForm[size]" value={currentSize}/>
                                <input type="hidden" name="QuickOrderForm[product]" value={this.state.product_id}/>
                                <input type="submit" value="ЗАКАЗАТЬ В ОДИН КЛИК"
                                       className={this.state.phone.length > 0 ? "order-button yellow" : "order-button maroon"}/>
                            </div>
                            <div className="columns small-12">
                                <div className="b-personal-data">
                                    <span className="b-personal-data__check"><input type="checkbox" checked="checked"/></span>
                                    Передавая информацию сайту вы принимаете условия
                                    <a className="b-personal-data__link"
                                       href="/page/private"
                                       target="_blank">"политики защиты персональных данных".</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className="tab-panel">
                        <p>Для того, чтобы купить изделие в рассрочку или кредит, Вам не нужно тратить много сил и
                            времени. Заполните заявку, затем с Вами свяжется наш специалист банка и подберет более
                            выгодную программу для Вас!</p>

                        <form id="credit-order-form" onSubmit={this.handleCreditOrderSubmit}>
                            {extraCredit}
                            <div className="row">
                                <div className="columns medium-4">
                                    <input type="text" name="CreditOrderForm[name]" onChange={this.handleNameChange}
                                           defaultValue={name} placeholder="Введите ваши Фамилию Имя Отчество"/>
                                    <ul className="errors"></ul>
                                </div>
                                <div className="columns medium-4">
                                    <PhoneInputMask name="CreditOrderForm[phone]"/>
                                    <ul className="errors"></ul>
                                </div>
                                <div className="columns small-4">
                                    <input type="hidden" name="CreditOrderForm[size]" value={currentSize}/>
                                    <input type="hidden" name="CreditOrderForm[product]" value={this.state.product_id}/>
                                    <input type="submit" value="КУПИТЬ В КРЕДИТ" className="button"/>
                                </div>
                                <div className="columns small-12">
                                    <div className="b-personal-data">
                                        <span className="b-personal-data__check"><input type="checkbox" checked="checked"/></span>
                                        Передавая информацию сайту вы принимаете условия
                                        <a className="b-personal-data__link"
                                           href="/page/private"
                                           target="_blank">"политики защиты персональных данных".</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
});

export default QuickOrder;
