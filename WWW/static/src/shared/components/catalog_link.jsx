import React from 'react';
import { Link } from 'react-easy-router';

var CatalogLink = React.createClass({
    render: function() {
        return (
            <div className="catalog-link">
                <Link to="CatalogList" params={{url: "woman"}}>Перейдите в каталог для покупок →</Link>
            </div>
        );
    }
});

export default CatalogLink;