import React, { Component } from 'react';
import settings from 'config/settings';
import { nl2br } from 'lib/utils';
import * as scriptsActions from 'actions/scripts';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class Map extends Component {
    map = undefined;
    markers = {};
    done = false;

    static defaultProps = {
        id: 'map',
        lat: undefined,
        lng: undefined,
        zoom: 12,
        objects: [],
        controls: ['zoomControl', 'typeSelector'],
        icon: {
            iconLayout: 'default#image',
            iconImageHref: '/static/dist/images/main/placemark.png',
            iconImageSize: [50, 54],
            iconImageOffset: [-5, -27]
        }
    };

    componentDidMount() {
        const { scriptsActions } = this.props;
        const props = this.props;

        scriptsActions.loadYandex(() => {
            ymaps.ready(() => {
                this.map = new ymaps.Map(props.id, {
                    center: [props.lat, props.lng],
                    zoom: props.zoom,
                    controls: props.controls
                });
                this.map.behaviors.disable("scrollZoom");

                props.objects.map(model => {
                    let marker = new ymaps.Placemark([model.lat, model.lng], {
                        balloonContentHeader: model.name,
                        balloonContentBody: model.phone + "<br/>" + nl2br(model.worktime)
                    }, this.props.icon);
                    this.markers[model.id] = marker;
                    this.map.geoObjects.add(marker);
                });
            });
        });
    }

    shouldComponentUpdate() {
        return false;
    }

    getMarker(id) {
        return this.markers[id];
    }

    componentWillUnmount() {
        if (this.map) {
            this.map.destroy();
        }
    }

    render() {
        return this.props.objects.length == 0 ? null : <div id={this.props.id}></div>;
    }
}

export default connect(state => {
    return {};
}, dispatch => {
    return {
        scriptsActions: bindActionCreators(scriptsActions, dispatch)
    };
})(Map);
