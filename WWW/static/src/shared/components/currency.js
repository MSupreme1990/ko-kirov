import accounting from 'accounting';
import { getState } from 'store';

function format(symbol, obj = {}, old = false) {
    let symbolLower = symbol.toLowerCase(),
        defaultOptions = {
            format: {
                pos: "%v %s",
                neg: "(%v) %s",
                zero: "--"
            }
        };

    let price;
    if (old) {
        if (Number(obj['price_old_' + symbolLower]) > 0) {
            price = Number(obj['price_old_' + symbolLower]);
        } else {
            price = Number(obj['price_' + symbolLower]);
        }
    } else {
        if (Number(obj['price_' + symbolLower]) > 0) {
            price = Number(obj['price_' + symbolLower]);
        } else {
            price = Number(obj['price_old_' + symbolLower]);
        }
    }

    const data = {
        usd: { symbol: '$', precision: 2, thousand: " " },
        eur: { symbol: '€', precision: 2, thousand: " " },
        uah: { symbol: 'гр.', precision: 2, thousand: " " },
        kzt: { symbol: 'т.', precision: 0, thousand: " " },
        byr: { symbol: 'руб.', precision: 0, thousand: " " },
        rub: { symbol: 'руб.', precision: 0, thousand: " " }
    };

    return accounting.formatMoney(price, {
        ...defaultOptions,
        ...data[symbolLower]
    });
}

function hasOldPrice(symbol, obj = {}) {
    return Number(obj['price_' + symbol.toLowerCase()]) > 0;
}

export default {
    format,
    hasOldPrice
};