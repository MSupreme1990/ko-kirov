import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as modalActions from 'actions/modal';
import { SkyLightStateless } from 'react-skylight';

const modal = props => {
    const { queue, actions } = props;

    if (queue.length === 0) {
        return null;
    }

    const { component, isVisible, style } = queue[queue.length - 1];

    const titleStyle = {
        display: 'none'
    };

    const overlayStyles = {
        zIndex: 1002
        // pointerEvents: 'none'
    };

    const closeButtonStyle = {
        cursor: 'pointer',
        position: 'absolute',
        fontSize: '1.8em',
        right: -40,
        top: 0,
        borderRadius: '100%',
        border: '3px solid #fff',
        width: 32,
        height: 32,
        textAlign: 'center',
        lineHeight: '29px',
        color: '#fff',
        zIndex: 200
    };

    let dialogStyles = {
        zIndex: 1003,
        padding: 20,
        borderRadius: 4,
        position: "relative",
        // top: (document.documentElement.scrollTop || document.body.scrollTop) + 60,
        // left: "50%",
        top: 'auto',
        left: 'auto',
        margin: "60px auto 60px",
        maxWidth: 980,
        width: 'auto',
        height: 'auto',
        ...style || {}
    };

    const closeCallback = () => {
        if (queue.length > 0) {
            queue.pop();
            actions.close();
        } else {
            actions.close();
        }
    };

    const cfg = {
        isVisible,
        dialogStyles,
        titleStyle,
        closeButtonStyle,
        overlayStyles,
        onCloseClicked: closeCallback,
        onOverlayClicked: closeCallback
    };

    return <SkyLightStateless {...cfg}>{component}</SkyLightStateless>;
};

export default connect(state => state.modal, dispatch => {
    return {
        actions: bindActionCreators(modalActions, dispatch)
    };
})(modal);