import React from 'react';
import $ from 'jquery';
import PhoneInput from 'inputs/phone_input';
import settings from 'config/settings';
import { cookie } from 'easy-storage';
import api from 'lib/api';
import { sendToAmo, formValidate } from 'lib/utils';

var DetectSize = React.createClass({

    woman: [
        [
            "Российский<br/>размер",
            "40", "42", "44", "46", "48", "50", "52", "54", "56", "58", "60"
        ],
        [
            "Обхват груди",
            "75-85", "85-92", "90-95", "95-100", "100-105", "105-108", "107-110", "110-113", "113-117", "117-122", "122-128"
        ],
        [
            "Обхват талии",
            "55-62", "60-65", "63-67", "67-72", "72-75", "75-82", "82-86", "86-90", "90-97", "97-103", "103-110"
        ],
        [
            "Обхват бедер",
            "73-85", "85-92", "90-95", "95-100", "100-105", "105-108", "107-110", "110-113", "113-117", "117-122", "124-130"
        ],
        [
            "Европейский <br/>размер",
            "XS", "S", "M", "L", "XL", "2XL", "3XL", "4XL", "5XL", "6XL", "7XL"
        ]
    ],

    man: [
        [
            "Российский<br/>размер",
            "46", "48", "50", "52", "54", "56", "58", "60", "62", "64", "66", "68", "70"
        ],
        [
            "Обхват груди",
            "92", "96", "100", "104", "108", "112", "116", "120", "124", "128", "132", "136", "140"
        ],
        [
            "Обхват талии",
            "76", "82", "88", "94", "100", "106", "112", "118", "120", "124", "128", "132", "136"
        ],
        [
            "Обхват бедер",
            "96", "100", "104", "108", "112", "116", "120", "124", "128", "132", "134", "136", "138"
        ],
        [
            "Европейский <br/>размер",
            "XS", "S", "M", "L", "XL", "2XL", "3XL", "3XL", "3XL", "4XL", "4XL", "5XL", "5XL"
        ]
    ],

    getInitialState: function () {
        return {
            message: ''
        };
    },
    getDefaultProps: function () {
        return {
            woman: true,
            mobile: false
        };
    },
    handleSubmit: function (e) {
        e.preventDefault();

        var $form = $('#detect-size-form');

        api.post('/api/ko/detect_size', $form.serialize(), function (data) {
            sendToAmo($form);
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    message: data['message']
                });
            }
        }.bind(this));
    },
    handleNameChange: function (e) {
        cookie.set('name', e.target.value);
    },

    renderForm: function () {
        if (this.props.mobile) {
            return null;
        }

        var name = cookie.get('name');
        return (
            <div className="detect-size-description">
                <h4>Не можете определить<br/>свой размер?</h4>
                <p>Заполните форму, наш менеджер свяжется с Вами и поможет определить ваш размер</p>
                <form id="detect-size-form" onSubmit={this.handleSubmit}>
                    <h1 className={this.state.message == '' ? 'hide' : ''}>{this.state.message}</h1>
                    <div className={this.state.message == '' ? '' : 'hide'}>
                        <div>
                            <input type="text" name="DetectSizeForm[name]" onChange={this.handleNameChange}
                                   defaultValue={name} placeholder="Введите ваше имя"/>
                            <ul className="errors"></ul>
                        </div>
                        <div>
                            <PhoneInput name="DetectSizeForm[phone]"/>
                            <ul className="errors"></ul>
                        </div>
                        <div>
                            <input type="submit" value="ОПРЕДЕЛИТЬ РАЗМЕР" className="button"/>
                        </div>
                    </div>
                </form>
            </div>
        );
    },

    renderTable: function (arr) {
        let table = [];
        let x = arr.length;
        let y = arr[0].length;

        let funcRenderText = (i, t, text) => {
            if (i === 0) {
                return <th key={'d-' + i +' - ' + t}>{text}</th>;
            } else {
                return <td key={'d-' + i +' - ' + t}>{text}</td>;
            }
        };

        if (this.props.mobile) {
            for (let i = 0; i < y; i++) {
                let row = [];
                for (let t = 0; t < x; t++) {
                    let text = <span dangerouslySetInnerHTML={{__html: (arr[t][i] || "")}}></span>;

                    row.push(funcRenderText(i, t, text));
                }
                table.push(<tr key={'mr-' + i}>{row}</tr>);
            }
        } else {
            for (let i = 0; i < x; i++) {
                let row = [];
                for (let t = 0; t < y; t++) {
                    let text = <span dangerouslySetInnerHTML={{__html: (arr[i][t] || "")}}></span>;
                    row.push(funcRenderText(t, i, text));
                }
                table.push(<tr key={'mr-' + i}>{row}</tr>);
            }
        }

        return table;
    },
    render: function () {
        let image, arr;

        if (this.props.woman) {
            image = (
                <img src={"/static/dist/images/main/detect-size-woman.png"}/>
            );
            arr = this.woman;

        }
        else {
            image = (
                <img src={"/static/dist/images/main/detect-size-man.png"}/>
            );
            arr = this.man;
        }

        return (
            <div className="detect-size-container">
                <div className="row">
                    <div className="columns medium-3">
                        <div className="detect-size-image-container">
                            {image}
                            <p className="detect-size-shadow"></p>
                        </div>
                        {this.renderForm()}
                    </div>
                    <div className="columns medium-9">
                        <h2>Как определить свой размер?</h2>
                        <p>
                            Вам понадобится провести измерения с помощью сантиметровой ленты.
                            Для определения подходящего размера необходимо соотнести полученные измерения с размерами в
                            таблице.
                        </p>
                        <ul className="detect-size-list">
                            <li className="one">При измерении обхвата груди лента должна плотно прилегать к телу,
                                спереди проходить по наиболее выступающим точкам, сбоку через подмышечные впадины, сзади
                                обхватывая лопатки.
                            </li>
                            <li className="two">Обхват талии измеряется строго горизонтально по самой узкой части тела,
                                проходя через самую выступающую точку живота.
                            </li>
                            <li className="three">При измерении обхвата бедер лента должна находиться горизонтально,
                                проходя посредине бедра и сзади по наиболее выступающим точкам ягодиц.
                            </li>
                        </ul>
                        <h2>Таблица размеров</h2>
                        <table className="detect-size-table">
                            <tbody>
                            {this.renderTable(arr)}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
});

export default DetectSize;