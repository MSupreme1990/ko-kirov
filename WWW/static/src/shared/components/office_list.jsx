import React, { Component } from 'react';
import $ from 'jquery';
import Feedback from 'shared/components/feedback';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as officeActions from 'actions/office';
import Loader from 'shared/components/loader';
import Map from 'shared/containers/map';
import { nl2br, openModal } from 'lib/utils';
import { SalesBackForm } from 'shared/components/product/categories_sales';
import { Link } from 'react-easy-router';
import Error from 'shared/containers/error';
import PropTypes from 'prop-types';

class OfficeList extends Component {
    static propTypes = {
        site_id: PropTypes.oneOfType([
            PropTypes.number,
            PropTypes.string,
        ])
    };

    componentWillMount() {
        const { site_id, officeActions } = this.props;

        officeActions.fetchList(site_id);
    }

    componentWillReceiveProps(nextProps) {
        const { officeActions } = this.props;

        if (nextProps.site_id != this.props.site_id) {
            officeActions.fetchList(nextProps.site_id);
        }
    }

    handleOpenMarker(map, id, e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: $("#" + map).offset().top
        }, 500);

        let marker = this.refs[map].getMarker(id);
        if (marker) {
            marker.openPopup();
        }
    }

    renderMain(objects = [], map = null, params = {}) {
        let officeNodes = objects.map(model => {
            let imageNodes = model.images.map(image => {
                return (
                    <li key={'cimg' + image.id}>
                        <a className="fancybox" rel={model.id} href={image.image.original}>
                            <img src={image.image.thumb}/>
                        </a>
                    </li>
                );
            });

            let imageContainer = model.images.length > 0 ? (
                    <li className="map-images">
                        <ul className="map-images-list">
                            {imageNodes}
                        </ul>
                    </li>
                ) : null;

            let path = model.path ? (
                    <a href="#"
                       onClick={openModal.bind(this, <div dangerouslySetInnerHTML={{ __html: model.path }}></div>)}>Как
                        добраться?</a>
                ) : null;

            return (
                <ul className="map-office-list" key={'of' + model.id}>
                    <li className="map-icon-map">
                        <p>{model.name}</p>

                        <p>
                            <a onClick={this.handleOpenMarker.bind(this, map, model.id)} href="#">Показать
                                на карте</a>
                            {path}
                        </p>
                    </li>
                    <li className="map-icon-phone">{model.phone}</li>
                    <li className="map-icon-worktime" dangerouslySetInnerHTML={{ __html: nl2br(model.worktime) }}></li>
                    {imageContainer}
                    <li className="map-divider"></li>
                </ul>
            );
        });

        let offices = objects.length == 0 ? (
                <ul className="address-list">
                    <li className="empty">Адреса отсутствуют</li>
                </ul>
            ) : <ul className="address-list">{officeNodes}</ul>;

        return (
            <div className="row">
                <div className="map-container">
                    <div className="columns medium-4 large-4">
                        <div className="map-sidebar">{offices}</div>
                    </div>
                    <div className="columns medium-8 large-8">
                        <Map lat={params.lat} lng={params.lng} zoom={params.zoom}
                             ref={map} id={map} objects={objects}/>
                    </div>
                </div>
            </div>
        );
    }

    renderExtra(objects, map, params = {}) {
        const { geo, site_id } = this.props;

        let extraMap = '';
        if (site_id != 1) {
            extraMap = (
                <div>
                    <h2 className="text-center">Центральные магазины "Кожанная Одежда" в Кирове</h2>
                    {this.renderMain(objects, map, params)}
                </div>
            );
        }

        return (
            <div className="contacts__extra">
                <div className="row">
                    <div className="columns medium-12">
                        {extraMap}
                        <div className="extra__container">
                            <div className="padding-container">
                                <div className="row">
                                    <div className="columns medium-12">
                                        <h2>Отдел продаж</h2>
                                    </div>
                                    <div className="columns small-5">
                                        <ul className="sale__list">
                                            <li className="phone">
                                                <p className="title">Горячая линия: +7 (800) 500-14-22</p>
                                                <p className="title-help">Звоните бесплатно со всех телефонов</p>
                                            </li>
                                            <li className="time">
                                                ПН-ВС: с 9:00 до 21:00
                                            </li>
                                            <li className="mail">
                                                aamt@ko-kirov.ru
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="columns small-7">
                                        <div className="container__yellow-border">
                                            <p>Телефон в Москве: +7 (499) 703-38-68</p>
                                            <p>Телефон в Санкт-Петербурге: +7 (812) 309-35-22</p>
                                            <p>Телефон в Нижнем Новгороде: +7 (831) 429-05-99</p>
                                            <p>Телефон в Кирове: +7 (8332) 68-03-41</p>
                                        </div>
                                    </div>
                                    <div className="columns small-12">
                                        <p className="contacts__feedback-link">
                                            <a onClick={openModal.bind(this, <Feedback />)} href="#">Связаться с
                                                нами</a>
                                        </p>
                                    </div>
                                    <div className="columns small-12">
                                        <table className="sales__table">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <h2>Оптовый отдел</h2>
                                                    <ul className="sale__list small">
                                                        <li className="phone">
                                                            +7 (912) 734-43-39<br/>
                                                            Руководитель отдела Бажанов Александр Андреевич
                                                        </li>
                                                        <li className="time">
                                                            ПН-ПТ: с 9:00 до 21:00<br/>
                                                            СБ-ВС: с 11:00 до 19:00
                                                        </li>
                                                        <li className="mail">
                                                            opt@ko-kirov.ru
                                                        </li>
                                                    </ul>
                                                    <p className="contacts__feedback-link small">
                                                        <a href="#" onClick={openModal.bind(this, <SalesBackForm />)}>Связаться
                                                            с нами</a>
                                                    </p>
                                                </td>
                                                <td>
                                                    <h2>Отдел качества</h2>
                                                    <ul className="sale__list small">
                                                        <li className="phone">
                                                            +7 (912) 734-43-39<br/>
                                                            Руководитель отдела Бажанова Анастасия Анатольевна
                                                        </li>
                                                        <li className="time">
                                                            ПН-ПТ: с 9:00 до 19:00<br/>
                                                            По московскому времени
                                                        </li>
                                                        <li className="mail">
                                                            good@ko-kirov.ru
                                                        </li>
                                                    </ul>
                                                    <p className="contacts__feedback-link small">
                                                        <Link to="KoReviews">
                                                            Оставить отзыв
                                                        </Link>

                                                    </p>
                                                </td>
                                                <td>
                                                    <h2>Отдел маркетинга и рекламы</h2>
                                                    <ul className="sale__list small">
                                                        <li className="time">
                                                            ПН-ПТ: с 9:00 до 19:00
                                                        </li>
                                                        <li className="mail">
                                                            marketing@ko-kirov.ru
                                                        </li>
                                                        <li>
                                                            Все предложения Вы можете отправлять на электронную почту,
                                                            мы обязательно с ними ознакомимся.
                                                        </li>
                                                    </ul>
                                                    <p className="contacts__feedback-link small">
                                                        <a onClick={openModal.bind(this, <Feedback />)} href="#">Связаться
                                                            с нами</a>
                                                    </p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const {
            geo,
            defaultObjects,
            objects,
            loading,
            error,
            message
        } = this.props;

        if (loading || geo.loading) {
            return <Loader />;
        }

        if (error || geo.error) {
            return <Error message={message}/>;
        }

        let params = {
            lat: parseFloat(geo.site.lat),
            lng: parseFloat(geo.site.lng),
            zoom: parseInt(geo.site.zoom) || 12
        };

        // this.renderExtra(defaultObjects, 'map_extra', { lat: 58.6029, lng: 49.6681 })

        return (
            <div>
                {this.renderMain(objects, 'map', params)}
                {this.renderExtra(defaultObjects, 'map_extra', {
                    ...params,
                    lat: 58.6029,
                    lng: 49.6681,
                })}
            </div>
        );
    }
}

export default connect(state => {
    return {
        ...state.office,
        geo: state.geo
    };
}, dispatch => {
    return {
        officeActions: bindActionCreators(officeActions, dispatch),
    };
})(OfficeList);
