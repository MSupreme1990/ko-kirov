import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import SkyLight from 'react-skylight';
import PropTypes from 'prop-types';
import PayLateForm from './PayLateForm';

export default class PayLate extends Component {
    static propTypes = {
        style: PropTypes.object,
        products: PropTypes.array.isRequired
    };

    static defaultProps = {
        products: [],
        modalStyle: {
            width: '450px',
            height: 'auto',
            minHeight: '340px',
            marginLeft: '-15%'
        },
        className: "b-button b-button_empty"
    };

    state = {
        id: undefined,
        form: {},
        errors: {},
        error: undefined
    };

    render() {
        const {
            className,
            modalStyle,
            products,
            style
        } = this.props;

        return (
            <div>
                <SkyLight dialogStyles={modalStyle}
                          hideOnOverlayClicked
                          ref="order" title="Оформление в рассрочку">
                    <PayLateForm products={products} />
                </SkyLight>
                <button className={className}
                        style={style} onClick={() => this.refs.order.show()}>
                    В рассрочку
                </button>
            </div>
        );
    }
}
