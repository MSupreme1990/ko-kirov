import $ from 'jquery';
import React from 'react';
import { findDOMNode } from 'react-dom';
import loader from 'lib/loader';

var OriginalSocialLikes = React.createClass({
    vk: function () {
        VK.init({
            apiId: 2299641,
            onlyWidgets: true
        });
        VK.Widgets.Like("vk_like", {
            type: "button"
        });
    },
    ok: function () {
        !function (d, id, did, st) {
            var js = d.createElement("script");
            js.src = "http://connect.ok.ru/connect.js";
            js.onload = js.onreadystatechange = function () {
                if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
                    if (!this.executed) {
                        this.executed = true;
                        setTimeout(function () {
                            OK.CONNECT.insertShareWidget(id, did, st);
                        }, 0);
                    }
                }
            };
            d.documentElement.appendChild(js);
        }(document, "ok_shareWidget", window.location.href, "{width:170,height:30,st:'oval',sz:20,ck:3}");
    },
    componentDidMount: function () {
        if (typeof VK == "undefined") {
            loader.loadFile(document, {
                src: '//vk.com/js/api/openapi.js?116',
                tag: "script",
                type: "text/javascript"
            }, function () {
                this.vk();
            }.bind(this));
        } else {
            this.vk();
        }

        this.ok();
    },
    render: function () {
        return (
            <div className="social-widgets">
                <div id="vk_like"></div>
                <div id="ok_shareWidget"></div>
            </div>
        );
    }
});

var SocialLikes = React.createClass({
    componentDidMount: function () {
        $(findDOMNode(this)).socialLikes();
    },
    render: function () {
        return (
            <div className="social-likes" data-counters="no">
                <div className="facebook" title="Поделиться ссылкой на Фейсбуке">
                </div>
                <div className="twitter" title="Поделиться ссылкой в Твиттере">
                </div>
                <div className="vkontakte" title="Поделиться ссылкой во Вконтакте">
                </div>
                <div className="odnoklassniki" title="Поделиться ссылкой в Одноклассниках">
                </div>
                <p className="clear"></p>
            </div>
        );
    }
});

export { OriginalSocialLikes, SocialLikes };