import $ from 'jquery';
import React, { Component, PropTypes } from 'react';
import { Link } from 'react-easy-router';
import { reverse } from 'react-easy-router';
import PhoneInput from 'inputs/phone_input';
import api from 'lib/api';
import { sendToAmo, formValidate, modalWindowUnlock, dataLayer_push } from 'lib/utils';

export class GetCouponForm extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    state = {
        code: null,
        callback: function () {},
        message: ''
    };

    handleSubmit(e) {
        e.preventDefault();
        var $form = $('#GetCoupon-form');

        api.post('/api/ko/get_coupon', $form.serialize(), data => {

            sendToAmo($form);
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    code: data['code']
                }, () => {
                    modalWindowUnlock();
                    dataLayer_push({'event': 'cartGift'});

                    let url = reverse("Coupon", {code: this.state.code}, {});
                    window.open(url, '_blank');
                });
            }
        });
    }

    render() {
        let name = '';
        let email = '';
        let phone = '';

        return (
            <form target="_blank" id="GetCoupon-form" className="row" onSubmit={this.handleSubmit.bind(this)}>
                <h1 className={this.state.message == '' ? 'hide' : ''}>{this.state.message}</h1>
                <div className={this.state.message == '' ? '' : 'hide'}>

                    <h3>
                        Заполните форму и получите подарок.
                    </h3>

                    <div className="inputs">
                        <div className="form-row">
                            <label htmlFor="sbf_email">Введите Ваше имя</label>
                            <div className="input-line">
                                <input id="sbf_email" type="text" name="CouponForm[name]" defaultValue={name} placeholder="Ваше имя" />
                                <ul className="errors"></ul>
                            </div>
                            <ul className="errors"></ul>
                            <div className="hint"></div>
                        </div>

                        <div className="form-row">
                            <label>Введите свой телефон</label>
                            <div className="input-line">
                                <PhoneInput name="CouponForm[phone]" />
                                <ul className="errors"></ul>
                            </div>
                            <div className="hint"></div>
                        </div>

                        <div className="form-row">
                            <label htmlFor="sbf_email">Введите свой e-mail</label>
                            <div className="input-line">
                                <input id="sbf_email" type="text" name="CouponForm[email]" defaultValue={email} placeholder="example@example.ru" />
                                <ul className="errors"></ul>
                            </div>
                            <div className="hint"></div>
                        </div>

                        <div className="form-row">
                            <input type="submit" value="Получить подарок" className="button" />
                        </div>
                        <div className="b-form-row">
                            <div className="b-personal-data">
                                <span className="b-personal-data__check"><input type="checkbox" checked="checked"/></span>
                                Передавая информацию сайту вы принимаете условия
                                <a className="b-personal-data__link"
                                   href="/page/private"
                                   target="_blank">"политики защиты персональных данных".</a>
                            </div>
                        </div>

                        <div className="comment"></div>
                    </div>

                </div>
            </form>
        );
    }
}
