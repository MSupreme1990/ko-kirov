import { modalWindowUnlock, closeModal, clearModal, openModal } from 'lib/utils';
import React, { Component, PropTypes } from 'react';
import QuickOrder from 'shared/components/quick_order';
import { Link } from 'react-easy-router';
import { cookie } from 'easy-storage';
import currency from 'shared/components/currency';
import Sizes from 'shared/components/sizes';
import ProductQuickImageSlider from 'shared/components/product/product_quick_image_slider';
import Features from 'shared/components/features';
import DetectSize from 'shared/components/detect_size';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as recentActions from 'actions/recent';
import * as cartActions from 'actions/cart';
import PayLate from 'shared/components/PayLate';

class CatalogProductQuickLookHandler extends Component {
    static propTypes = {
        product: PropTypes.object.isRequired
    };

    componentWillMount() {
        const { recentActions, product } = this.props;
        recentActions.add(product);
    }

    handleAddToCart(e) {
        e.preventDefault();

        const { product, cartActions } = this.props;
        cartActions.addToCart(product.id);
    }

    handleCloseModal(e) {
        clearModal();
    }

    render() {
        const { in_cart, symbol, product } = this.props;

        let price_old = currency.hasOldPrice(symbol, product) ? (
                <span className="product-detail-old-price">{currency.format(symbol, product, true)}</span>
            ) : null;

        let cartButton = in_cart ? (
                <a href="/cart" className="b-button b-button_empty">Перейти в корзину</a>
            ) : (
                <a className="b-button b-button_empty" href="#" onClick={this.handleAddToCart.bind(this)}>Добавить в
                    корзину</a>
            );

        return (
            <div>
                <div className="row product-detail">
                    <div className="columns medium-4">
                        <ProductQuickImageSlider cls="product-quick-images-slider-container"
                                                 prefix="slickquick" product={product} small={true}/>
                    </div>
                    <div className="columns medium-8">
                        <h1>{product.name}</h1>

                        <p className="product-detail-article">Модель № {product.code}</p>
                        <div className="product-detail-prices">
                            <div className="row">
                                <div className="columns small-5">
                                    <h4 className="product-detail-price">
                                        Цена: {currency.format(symbol, product)}
                                    </h4>
                                    {price_old}
                                </div>
                                <div className="columns small-7">
                                    <div className="b-input-group b-input-group_end">
                                        <PayLate className="b-button b-button_empty"
                                                 style={{ marginRight: '5px' }}
                                                 products={
                                                     [{
                                                         quantity: 1,
                                                         price: product.price_rub || product.price_old_rub,
                                                         object: product
                                                     }]
                                                 }/>
                                        {cartButton}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <Features small={this.props.small || true}/>

                        <div>
                            <div className="product-detail-sizes">
                                <p>Размеры в наличии:</p>
                                <Sizes product={product} className="product-detail-sizes-list"/>
                            </div>
                            <p className="clear"></p>
                            <p className="detect-size-link">
                                <a className="detect-size" href="#"
                                   onClick={openModal.bind(this, <DetectSize />, { padding: 0 })}>Определить свой
                                    размер</a>
                            </p>
                        </div>
                        <QuickOrder product_id={product.id}/>
                        <div dangerouslySetInnerHTML={{ __html: product.content_short }}></div>
                        <p>
                            <Link to="CatalogProduct" params={{ url: product.url }}
                                  onClick={this.handleCloseModal.bind(this)}
                                  className="product-detail-read-more">Подробнее &rarr;</Link>
                        </p>
                    </div>
                </div>
                <div className="row">
                    <div className="columns medium-3">&nbsp;</div>
                    <div className="columns medium-6">
                        <div className="product-detail-link">
                            <Link to="CatalogProduct" onClick={this.handleCloseModal.bind(this)}
                                  params={{ url: product.url }}>Больше информации о товаре →</Link>
                        </div>
                    </div>
                    <div className="columns medium-3">&nbsp;</div>
                </div>
            </div>
        );
    }
}

export default connect((state, props) => {
    return {
        in_cart: state.cart.in_cart.indexOf(props.product.id) > -1,
        symbol: state.currency.symbol,
        recent: state.recent
    }
}, dispatch => {
    return {
        cartActions: bindActionCreators(cartActions, dispatch),
        recentActions: bindActionCreators(recentActions, dispatch)
    };
})(CatalogProductQuickLookHandler);
