import React, { Component } from 'react';
import ProductSlider from 'shared/components/product/product_slider';
import Loader from 'shared/components/loader';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as productActions from 'actions/product';

class NewModels extends Component {
    componentWillMount() {
        const { productActions } = this.props;
        productActions.fetchNew();
    }

    render() {
        const { objects, loading } = this.props;

        if (loading) {
            return <Loader />;
        }

        if (objects.length == 0) {
            return null;
        }

        return (
            <div className="product-list-container slider-new">
                <ProductSlider data={objects} models={objects}
                               title="Новые поступления" prefix="new" />
            </div>
        )
    }
}

export default connect(state => state.product.new, dispatch => {
    return {
        productActions: bindActionCreators(productActions, dispatch)
    };
})(NewModels);