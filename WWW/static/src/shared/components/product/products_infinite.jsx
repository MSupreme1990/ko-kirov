import React from 'react';

var ProductsInfinite = React.createClass({
    currentPage: 1,
    perPage: 16,
    numPages: 1,
    params: {},
    getDefaultProps: function() {
        return {
            count: "5",
            params: {}
        };
    },
    getInitialState: function() {
        return {
            group: [],
            models: undefined,
            force: false,
            init: false
        };
    },
    componentWillReceiveProps: function(nextProps) {
        this.params = nextProps.params;
        this.setState({
            force: true
        });

        this.getProducts(nextProps.url, 1, function(data) {
            this.numPages = Math.ceil(data.total / this.perPage);
            this.setState({
                models: data.products,
                total: data.total
            });
        }.bind(this));
    },
    getProducts: function(url, page, callback) {
        page = page || 1;
        var params = this.params;
        params['page'] = page || 1;
        api.get("/api/catalog/get/" + url, params, function(data) {
            callback(data);
        }.bind(this));
    },
    componentDidMount: function() {
        this.getProducts(this.props.url, 1, function(data) {
            this.numPages = parseInt(data.total / this.perPage);
            this.setState({
                models: data.products,
                total: data.total
            });
        }.bind(this));
    },
    onPaginate: function () {
        this.currentPage += 1;
        this.getProducts(this.props.url, this.currentPage, function(data) {
            this.setState({
                models: this.state.models.concat(data.products)
            });
        }.bind(this));
    },
    render: function() {
        var prefix = this.props.url,
            count = this.props.count || 5,
            cls = "infinite small-block-grid-" + count,
            group = [],
            products;

        var loader = (
            <div className="loader">
                <p>Загрузка</p>
            </div>
        );

        if (this.state.models && this.state.models.length > 0) {
            this.state.models.map(function (model) {
                group.push(model.id);
            });

            var productNodes = this.state.models.map(function (model) {
                if (!model) {
                    console.error("Problem with: ", model);
                    return;
                }

                var key = prefix + model.id;
                return (
                    <li key={'pinf_' + key}>
                        <Product model={model} group={group} models={this.state.models} />
                    </li>
                );
            }.bind(this));

            products = (
                <InfiniteScroll
                    className={cls}
                    pageStart="0"
                    loadMore={this.onPaginate}
                    hasMore={this.currentPage < this.numPages}
                    loader={loader}>
                  {productNodes}
                </InfiniteScroll>
            );
        } else if (this.state.models && this.state.models.length == 0) {
            products = (
                <div>Товары отсутствуют</div>
            );
        } else if (typeof this.state.models == "undefined") {
            products = loader;
        }

        return products;
    }
});

export default ProductsInfinite;