import React from 'react';
import Cookie from 'js-cookie';
import {
    manSizes,
    womanSizes
} from 'const/size.js';

let getSizes = (obj = {}, sizes = []) => {
    let i, newObject = {};
    for (i in obj) {
        newObject[i] = sizes.indexOf(i) !== -1;
    }
    return newObject;
};

export default (obj, model, clsContainer, callback) => {
    let sizes = {};
    let min = Number(model.min_size);
    let max = Number(model.max_size);
    if (min && max && (min < max)) {
        let i = min;
        while (i <= max) {
            sizes[i] = false;
            i = i + 2;
        }
    } else {
        sizes = model.woman ? womanSizes : manSizes;
    }

    sizes = getSizes(sizes, model.sizes || []);

    let currentSize = Cookie.get('current_size');

    let sizeNodes = Object.keys(sizes).map(size => {
        let cls = sizes[size] ? "available" : "not-available";
        if (currentSize == size) {
            cls += " active";
        }

        let title = null,
            rel = null;
        if (!sizes[size]) {
            title = "Под заказ";
            rel = "tooltip";

            return (
                <li key={size} className={cls} rel={rel} title={title} data-tooltipcls="small">
                    <span>
                        {size}
                    </span>
                </li>
            );
        }

        return (
            <li key={size} className={cls} rel={rel} title={title} data-tooltipcls="small">
                <a href="#" onClick={callback.bind(size)}>{size}</a>
            </li>
        );
    });

    return <ul className={clsContainer}>{sizeNodes}</ul>;
};