import React, { Component, PropTypes } from 'react';
import ProductSlider from 'shared/components/product/product_slider';
import Loader from 'shared/components/loader';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as productActions from 'actions/product';

class Popular extends Component {
    static propTypes = {
        count: PropTypes.number
    };

    static defaultProps = {
        count: 5
    };

    componentWillMount() {
        const { productActions } = this.props;
        productActions.fetchPopular();
    }

    render() {
        const { objects, loading, count } = this.props;

        if (loading) {
            return <Loader />;
        }

        if (objects.length == 0) {
            return null;
        }

        return (
            <div className="product-list-container slider-popular">
                <ProductSlider data={objects} count={count}
                               models={objects} title="Популярные модели" prefix="popular" />
            </div>
        )
    }
}

export default connect(state => state.product.popular, dispatch => {
    return {
        productActions: bindActionCreators(productActions, dispatch)
    };
})(Popular);