import React, { Component } from 'react';
import api from 'lib/api';
import { chunk, formValidate, sendToAmo } from 'lib/utils';
import { Link } from 'react-easy-router';
import Category from 'shared/components/product/category';
import { ModalLink } from 'shared/components/modal';
import PhoneInput from 'inputs/phone_input';
import { cookie } from 'easy-storage';
import { openModal } from 'lib/utils';

export class SalesBackForm extends Component {
    state = {
        pk: null,
        message: ""
    };

    handleSubmit(e) {
        e.preventDefault();
        var $form = $('#SalesBack-form');

        api.post('/api/ko/salesback', $form.serialize(), data => {

            sendToAmo($form);
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    message: data['message']
                });
            }
        });
    }

    handleEmailChange(e) {
        cookie.set('email', e.target.value);
    }

    handleNameChange(e) {
        cookie.set('name', e.target.value);
    }

    render() {
        var email = cookie.get('email');
        var name = cookie.get('name');
        return (
            <form id="SalesBack-form" className="row" onSubmit={this.handleSubmit.bind(this)}>
                <h1 className={this.state.message == '' ? 'hide' : ''}>{this.state.message}</h1>
                <div className={this.state.message == '' ? '' : 'hide'}>

                    <h3>
                        Заполните форму и мы <br/>
                        вышлем Вам оптовый <br/>
                        каталог в течение 5 минут!
                    </h3>

                    <div className="inputs">
                        <div className="form-row">
                            <label htmlFor="sbf_email">Введите свой e-mail</label>
                            <input id="sbf_email" type="text" onChange={this.handleEmailChange.bind(this)} name="SalesBackForm[email]"
                                   defaultValue={email} placeholder="example@example.ru" />
                            <ul className="errors"></ul>
                            <div className="hint">На эту почту мы отправим наш каталог</div>
                        </div>
                        <div className="form-row">
                            <label>Введите свой телефон</label>
                            <PhoneInput name="SalesBackForm[phone]" />
                            <ul className="errors"></ul>
                            <div className="hint">Мы убедимся что вы получили каталог</div>
                        </div>
                        <div className="form-row">
                            <input type="submit" value="Получить оптовый каталог" className="button" />
                        </div>
                        <div className="b-personal-data">
                                                            <span className="b-personal-data__check">
                                                                <input
                                                                    className="b-personal-data__input" type="checkbox"
                                                                    checked="checked"/>
                                                            </span>
                            Нажимая кнопку «Получить консультацию», вы принимаете
                            <a className="b-personal-data__link"
                               href="/page/private"
                               target="_blank">"условия обработки персональных
                                данных".</a>
                        </div>

                        <div className="comment">Заполнение заявки Вас ни к чему не обязывает!</div>
                    </div>

                </div>
            </form>
        );
    }
}

export class CategoriesSales extends Component {
    static defaultProps = {
        models: [],
        prefix: "",
        count: 5
    };

    render() {
        var products = chunk(this.props.models, this.props.count).map((models, i) => {
            var productNodes = models.map((model, t) => (
                <li key={i + '_' + t}><Category model={model} /></li>
            ));

            return (
                <div className="category-list-container" key={'csales' + i}>
                    <ul className={"medium-block-grid-" + (this.props.count - 2) + " large-block-grid-" + this.props.count} key={'pds' + i}>
                        {productNodes}
                    </ul>
                    <hr/>
                    <a href="#" className='button' onClick={openModal.bind(this, <SalesBackForm />)}>
                        Смотреть все
                    </a>
                </div>
            );
        });

        return <div>{products}</div>;
    }
}
