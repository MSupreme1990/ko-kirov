import React, { PropTypes, Component } from 'react';
import Loader from 'shared/components/loader';
import CatalogProductQuickLookHandler from 'shared/components/product/product_quick_look';

export default class ProductModal extends Component {
    mockTimer = 150;

    static propTypes = {
        product: PropTypes.object.isRequired,
        group: PropTypes.array
    };

    static defaultProps = {
        group: [],
    };

    state = {
        loading: false,
        index: 0,
        product: undefined
    };

    componentWillMount() {
        const { product, group } = this.props;

        let index = 0;
        for (let i = 0; i < group.length; i++) {
            if (group[i].id == product.id) {
                index = i;
            }
        }

        this.setState({
            product,
            index
        });
    }

    nextProduct(e) {
        e.preventDefault();

        if (this.hasNext()) {
            this.setState({
                loading: true
            }, () => {
                setTimeout(() => {
                    let index = this.state.index + 1;
                    this.setState({
                        loading: false,
                        index,
                        product: this.props.group[index]
                    });
                }, this.mockTimer);
            });
        }
    }

    prevProduct(e) {
        e.preventDefault();

        if (this.hasPrev()) {
            this.setState({
                loading: true
            }, () => {
                setTimeout(() => {
                    let index = this.state.index - 1;
                    this.setState({
                        loading: false,
                        index,
                        product: this.props.group[index]
                    });
                }, this.mockTimer);
            });
        }
    }

    hasNext() {
        const { group } = this.props;
        const { index } = this.state;

        return index < group.length - 1;
    }

    hasPrev() {
        const { group } = this.props;
        const { index } = this.state;

        return index > 0 && index <= (group.length - 1);
    }

    render() {
        const { product, loading } = this.state;

        if (loading) {
            return <Loader/>;
        }

        let prev = this.hasPrev() ?
                <a href="#" className="modal-prev-link" onClick={this.prevProduct.bind(this)}></a> : null,
            next = this.hasNext() ?
                <a href="#" className="modal-next-link" onClick={this.nextProduct.bind(this)}></a> : null;

        return (
            <div>
                {next}
                {prev}
                <CatalogProductQuickLookHandler small="true" product={product}/>
            </div>
        );
    }
}
