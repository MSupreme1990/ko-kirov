import React, { Component, PropTypes } from 'react';
import { Link } from 'react-easy-router';
import Loader from 'shared/components/loader';
import * as productActions from 'actions/product';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { findDOMNode } from 'react-dom';

class Autocomplete extends Component {
    _timer = null;

    static contextTypes = {
        router: PropTypes.object
    };

    static propTypes = {
        q: PropTypes.string
    };

    static defaultProps = {
        q: ''
    };

    /*
    componentWillMount() {
        $(document).on('esc_key', this.handleSearchClear.bind(this));
    }

    componentWillUnmount() {
        $(document).off('esc_key', this.handleSearchClear.bind(this));
    }
    */

    handleSearchKeyDown(e) {
        if (e.keyCode == 13) {
            this.context.router.to("Search", {}, {
                q: e.target.value
            });
        }
    }
    
    handleSearchChange(e) {
        let value = e.target.value;

        const { productActions } = this.props;

        clearTimeout(this._timer);
        this._timer = setTimeout(() => productActions.autocomplete(value), 350);
    }

    handleSearchClear(e) {
        findDOMNode(this.refs.search).value = '';
    }

    renderSearchItems() {
        const { loading, objects } = this.props;

        if (loading) {
            return <Loader />;
        }

        if (objects.length == 0) {
            return null;
        }

        let nodes = objects.map(model => (
            <li key={"autocomplete_" + model.id}>
                <Link onClick={this.handleSearchClear.bind(this)} to="CatalogProduct"
                      params={{url: model.url}}>{model.name}</Link>
            </li>
        ));

        return (
            <div className="search-popup-container">
                <ul className="search-popup">{nodes}</ul>
            </div>
        );
    }

    render() {
        const { q } = this.props;

        return (
            <div className="search-form-container">
                <input
                    ref="search" placeholder="Поиск..." type="text" defaultValue={q}
                    onChange={this.handleSearchChange.bind(this)}
                    onKeyDown={this.handleSearchKeyDown.bind(this)}/>
                {this.renderSearchItems()}
            </div>
        )
    }
}

export default connect(state => state.product.autocomplete, dispatch => {
    return {
        productActions: bindActionCreators(productActions, dispatch)
    };
})(Autocomplete);