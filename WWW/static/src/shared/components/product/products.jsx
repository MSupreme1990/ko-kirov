import React, { Component, PropTypes } from 'react';
import { makeId } from 'lib/utils';
import Product from 'shared/components/product/product';

export default class Products extends Component {
    static propTypes = {
        data: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.number
            ])
        }))
    };

    static defaultProps = {
        data: []
    };

    render() {
        let prefix = this.props.prefix || makeId(),
            count = this.props.count || 5,
            cls = "medium-block-grid-" + (count - 2) + " large-block-grid-" + count,
            data = this.props.data || [];

        let nodes = data.map(product => {
            return (
                <li key={prefix + product.id + '-product'}>
                    <Product product={product} group={this.props.group || []}/>
                </li>
            );
        });

        return <ul className={cls}>{nodes}</ul>;
    }
}