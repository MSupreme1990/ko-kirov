import React, { Component } from 'react';
import Category from 'shared/components/product/category';
import Loader from 'shared/components/loader';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as mainActions from 'actions/main';

class Categories extends Component {
    componentWillMount() {
        const { actions } = this.props;
        actions.fetchMainIfNeeded();
    }

    static defaultProps = {
        count: 5
    };

    render() {
        const { loading, categories } = this.props;

        if (loading) {
            return <Loader />;
        }

        let nodes = categories.map((model, i) => <li key={i}><Category model={model} /></li>);

        return (
            <div>
                <ul className={"medium-block-grid-" + (this.props.count - 2) + " large-block-grid-5"}>
                    {nodes}
                </ul>
            </div>
        );
    }
}

export default connect(state => state.main, dispatch => {
    return {
        actions: bindActionCreators(mainActions, dispatch)
    };
})(Categories);