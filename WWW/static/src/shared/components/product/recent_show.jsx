import React, { Component } from 'react';
import { Link } from 'react-easy-router';
import ProductSlider from 'shared/components/product/product_slider';
import Loader from 'shared/components/loader';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as recentActions from 'actions/recent';

class RecentShow extends Component {
    render() {
        const { loading, models } = this.props;

        if (loading) {
            return <Loader />
        }

        if (models.length == 0) {
            return null;
        }

        let link = <Link to="CatalogRecent">Недавно вы смотрели ({models.length})</Link>;

        return (
            <div className="recent__container">
                <div className="row">
                    <div className="columns large-12">
                        <div className="product-list-container slider-recent">
                            <ProductSlider data={models.reverse()}
                                title={link}
                                prefix="recent" />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(state => state.recent, dispatch => {
    return {
        actions: bindActionCreators(recentActions, dispatch)
    };
})(RecentShow);