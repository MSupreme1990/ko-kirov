import React, { Component, PropTypes } from 'react';
import { chunk, getTimestamp } from 'lib/utils';
import Paginator from 'shared/components/pager';
import { findDOMNode } from 'react-dom';
import ReactImageZoom from 'shared/containers/zoom';

export default class ProductQuickImageSlider extends Component {
    static propTypes = {
        product: PropTypes.object.isRequired
    };

    state = {
        currentPage: 1,
        numPages: 1,
        perPage: 4
    };

    static defaultProps = {
        small: false,
        el: undefined,
        cls: "product-quick-images-slider-container"
    };

    constructor(props) {
        super(props);

        let perPage = props.small ? 3 : 4;
        let imageSrc, zoomImage;

        const { images } = this.props.product;
        if (images.length > 1) {
            imageSrc = images[0].resize;
            zoomImage = images[0].original;
        }

        let data = chunk(images, perPage),
            currentPage = 1;

        this.state = {
            currentPage,
            perPage,
            numPages: Math.ceil(images.length / perPage),

            imageSrc,
            zoomImage,
            data,
            images: data[currentPage - 1]
        };
    }

    onPaginate(e, currentPage) {
        this.setState({
            currentPage,
            images: this.state.data[currentPage - 1]
        });
    }

    handlerMouseEnter(thumb, zoomImage, e) {
        // this.refs.zoom.kill();
        // console.log(this.refs.zoom.kill());

        this.setState({
            imageSrc: thumb + '?t=' + getTimestamp(),
            zoomImage
        });
    }

    render() {
        const { small, product, cls } = this.props;
        const { images, currentPage, perPage, numPages } = this.state;

        let sliderNodes = images.map((image, i) => (
            <li key={this.props.prefix + i}>
                <div className="product-images-item">
                    <img onMouseEnter={this.handlerMouseEnter.bind(this, image.resize, image.original)}
                         src={image.quick_look_slide_another}/>
                </div>
            </li>
        ));

        let discount = null,
            discountPercentage = null;

        if (product.discount && typeof product.discount.name != "undefined" && product.discount.name.length > 0) {
            discount = <p className="product-item-discount">{product.discount.name}</p>;
        }

        if (product.discount_percentage) {
            discountPercentage =
                <span className="product-detail-discount-percentage">-{product.discount_percentage}%</span>;
        }

        const { imageSrc, zoomImage } = this.state;

        const zoomDefaults = {
            img: zoomImage,
            zoomStyle: 'margin-left: 1em; z-index: 100; border: 1px solid #ccc'
        };

        const zoomProps = small ?
            { ...zoomDefaults, width: 295, height: 412, zoomWidth: 620 } :
            { ...zoomDefaults, width: 408, height: 570, zoomWidth: 850 };

        return (
            <div>
                <div className="product-detail-image-container">
                    <span className="product-detail-useless"></span>
                    <div style={{ position: 'relative', height: zoomProps.height }}>
                        <ReactImageZoom ref="zoom" {...zoomProps} />
                    </div>
                    {/*<img ref="img" className="product-detail-image"*/}
                    {/*src={imageSrc} alt={product.name} data-zoom-image={zoomImage}/>*/}
                    {discount}
                    {discountPercentage}
                </div>
                <div className="product-full-images-slider-container">
                    <div className={cls}>
                        <ul className={"medium-block-grid-" + perPage}>{sliderNodes}</ul>
                        <Paginator
                            page={currentPage}
                            numPages={numPages}
                            splitTemplate={true}
                            onClick={this.onPaginate.bind(this)}/>
                    </div>
                </div>
            </div>
        );
    }
}
