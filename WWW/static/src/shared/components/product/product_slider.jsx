import React, { Component, PropTypes } from 'react';
import Paginator from '../pager';
import Products from './products';
import { makeId } from 'lib/utils';

class ProductSlider extends Component {
    static propTypes = {
        count: PropTypes.number,
        data: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.number
            ])
        }))
    };

    static defaultProps = {
        count: 5,
        data: []
    };

    state = {
        currentPage: 1,
        perPage: 5,
        numPages: 1,
        maxPages: 4,
        prefix: makeId(),
        models: []
    };

    onPaginate(e, currentPage) {
        this.setState({
            currentPage
        });
    }

    componentWillMount() {
        this.setState({
            perPage: this.props.count
        });
    }

    componentDidMount() {
        this.loadData(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this.loadData(nextProps);
    }

    loadData(props) {
        let len = Math.ceil(props.data.length / this.state.perPage);

        this.setState({
            numPages: len,
            prefix: props.prefix || this.state.prefix,
            models: props.data
        });
    }

    render() {
        let splitTemplate = (typeof this.props.splitTemplate != 'undefined') ? this.props.splitTemplate : true;
        let pageModelsStart = this.state.perPage * (this.state.currentPage - 1);
        let products = this.state.models.slice(pageModelsStart, pageModelsStart + this.state.perPage);

        return (
            <div className="product-slider">
                <h3 className="product-list-title">{this.props.title}</h3>
                <Paginator
                    page={this.state.currentPage}
                    numPages={this.state.numPages}
                    maxPages={this.state.maxPages}
                    splitTemplate={splitTemplate}
                    reverse={true}
                    onClick={this.onPaginate.bind(this)}/>

                <Products
                    prefix={this.props.prefix || this.state.prefix}
                    data={products}
                    group={this.props.data}/>
            </div>
        );
    }
}

export default ProductSlider;