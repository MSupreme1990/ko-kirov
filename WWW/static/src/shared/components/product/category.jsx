import $ from 'jquery';
import { findDOMNode } from 'react-dom';
import React, { Component } from 'react';
import { Link } from 'react-easy-router';
import currency from 'shared/components/currency';
import { connect } from 'react-redux';

class Category extends Component {
    static defaultProps = {
        model: undefined
    };

    constructor(props, objs) {
        super(props, objs);

        let originalSrc,
            newSrc,
            model = this.props.model;

        if (model.image && typeof model.image.thumb != "undefined") {
            originalSrc = model.image.thumb;
        } else {
            originalSrc = "http://placehold.it/233x317";
        }

        if (model.image_hover && typeof model.image_hover.thumb != "undefined") {
            newSrc = model.image_hover.thumb;
        }

        this.state = {
            originalSrc: originalSrc,
            newSrc: newSrc
        };
    }

    handlerMouseLeave(e) {
        let $this = $(findDOMNode(this.refs.obj));
        $this.find('.product-image img').attr('src', this.state.originalSrc);
    }

    handlerMouseEnter(e) {
        let $this = $(findDOMNode(this.refs.obj));
        if (this.state.newSrc) {
            $this.find('.product-image img').attr('src', this.state.newSrc);
        }
    }

    render() {
        const { model, symbol } = this.props;

        let imagesHtml;
        return (
            <div ref="obj" className="product-item category"
                 onMouseEnter={this.handlerMouseEnter.bind(this)}
                 onMouseLeave={this.handlerMouseLeave.bind(this)}>
                <div className="product-item-overlay no-images">
                    {imagesHtml}
                    <div className="product-image">
                        <Link to="CatalogList" params={{url: model.url}}>
                            <img src={this.state.originalSrc} alt={model.name}/>
                        </Link>

                        <div className="preload">
                            <img src={this.state.newSrc} alt={model.name}/>
                        </div>
                    </div>
                    <p className="product-item-title">
                        <Link to="CatalogList" params={{url: model.url}}>
                            {model.name}
                        </Link>
                    </p>

                    <p className="product-item-price-container">
                        <span className="product-item-price">От {currency.format(symbol, model)}</span>
                    </p>
                </div>
            </div>
        );
    }
}

export default connect(state => {
    return {
        symbol: state.currency.symbol
    };
})(Category);