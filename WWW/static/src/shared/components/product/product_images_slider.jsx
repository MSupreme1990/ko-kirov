import $ from 'jquery';
import React, { Component } from 'react';
import Paginator from '../pager';

export default class ProductImagesSlider extends Component {
    state = {
        page: 1,
        pages: 0,
        per_page: 4
    };

    componentWillMount() {
        const { images } = this.props;
        this.setState({
            pages: images / this.state.per_page
        });
    }

    onPaginate(e, page) {
        this.setState({
            page
        });
    }

    handlerMouseEnter (e) {
        e.preventDefault();
        var $this = $(e.target),
            $item = $this.closest('.product-item'),
            $image = $item.find('.product-image img'),
            timestamp = Math.floor(Date.now() / 1000);

        $image.attr('src', $this.data('thumb') + '?' + timestamp);
    }

    render () {
        const { images } = this.props;
        if (images.length == 0) {
            return null;
        }

        let sliceStart = (this.state.page - 1) * this.state.per_page,
            sliceStop = this.state.per_page;
        let sliderNodes = [...images].slice(sliceStart, sliceStop).map((image, i) => (
            <div className="product-images-item" key={this.props.prefix + i}>
                <img onMouseEnter={this.handlerMouseEnter.bind(this)}
                     src={image.thumb_slide} data-thumb={image.thumb}/>
            </div>
        ));

        return (
            <div className="product-images-slider">
                {sliderNodes}
                <Paginator
                    page={this.currentPage}
                    numPages={this.numPages}
                    maxPages={this.maxPages}
                    splitTemplate={true}
                    onClick={this.onPaginate.bind(this)}/>
            </div>
        );
    }
}
