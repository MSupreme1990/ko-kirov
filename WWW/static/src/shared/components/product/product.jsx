import $ from 'jquery';
import React, { Component } from 'react';
import { Link } from 'react-easy-router';
import { findDOMNode } from 'react-dom';
import { dataLayer_push, openModal } from 'lib/utils';
import { ProductModalLink } from 'shared/components/modal';
import ProductImagesSlider from 'shared/components/product/product_images_slider';
import Sizes from 'shared/components/sizes';
import currency from 'shared/components/currency';
import ProductModal from 'shared/components/product/product_modal';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as cartActions from 'actions/cart';

class Product extends Component {
    static defaultProps = {
        woman: true
    };

    state = {
        size: undefined,
        slider: false,
        originalSrc: undefined,
        newSrc: undefined,
        additionalShow: false
    };

    componentWillMount() {
        const { images } = this.props.product;

        let newSrc, originalSrc;
        if (images && images.length > 0) {
            newSrc = originalSrc = images[0].thumb;
        }

        this.setState({
            originalSrc: originalSrc,
            newSrc: newSrc
        });
    }

    handlerMouseLeave(e) {
        let container = findDOMNode(this),
            $this = $(container);

        $this.find('.product-image img').attr('src', this.state.originalSrc);
    }

    handlerMouseEnter(e) {
        let container = findDOMNode(this),
            $this = $(container);

        if (!this.state.additionalShow) {
            this.setState({ additionalShow: true });
        }

        if (this.state.newSrc) {
            $this.find('.product-image img').attr('src', this.state.newSrc);
        }
    }

    handleAddToCart(e) {
        e.preventDefault();

        const { cartActions, product } = this.props;
        cartActions.addToCart(product.id);

        dataLayer_push({
            event: 'orderSubmit',
            type: 'fullCheckout'
        });
    }

    getProductModal() {
        const { product, group } = this.props;

        return <ProductModal
            product={product}
            name="Увеличить"
            group={group}/>;
    }

    getQuickOrderModal() {
        const { product, models, group } = this.props;
        return <ProductModal
            product={product}
            id={product.id}
            group={group}
            models={models}/>;
    }

    render() {
        const { in_cart, symbol } = this.props;

        let product = this.props.product;
        let overlayClass = "product-item-overlay no-images";

        if (product.images && product.images.length > 0) {
            overlayClass = "product-item-overlay";
        }

        return (
            <div className="product-item"
                 onMouseEnter={this.handlerMouseEnter.bind(this)}
                 onMouseLeave={this.handlerMouseLeave.bind(this)}>

                <div className={overlayClass}>
                    {(()=> {
                        if (this.state.additionalShow) {
                            if (product.images && product.images.length > 0) {
                                return <ProductImagesSlider prefix={product.id} images={product.images}/>;
                            }
                        }
                    })()}
                    <div className="product-image">
                        <Link to="CatalogProduct" params={{url: product.url}}>
                            <img src={this.state.originalSrc} alt={product.name}/>

                            <div className="preload hide">
                                <img src={this.state.newSrc} alt={product.name}/>
                            </div>
                        </Link>
                        {(()=> {
                            if (product.discount_percentage) {
                                return <span
                                    className="product-item-discount-percentage">-{product.discount_percentage}%</span>;
                            }
                        })()}
                        {(()=> {
                            if  (product.discount && typeof product.discount.name != "undefined" && product.discount.name.length > 0) {
                                return <p className="product-item-discount">{product.discount.name}</p>;
                            }
                        })()}
                        <p className="product-item-detail-container">
                            <a href="#" className="product-item-detail"
                               onClick={openModal.bind(this, this.getProductModal(), {height: 740})}>Увеличить</a>
                        </p>
                    </div>
                    <p className="product-item-title">
                        <Link to="CatalogProduct" params={{url: product.url}}>
                            {product.name}
                            <br/>
                            <span className="product-item-code">№ {product.code}</span>
                        </Link>
                    </p>

                    <p className="product-item-price-container">
                        <span className="product-item-price">
                            {currency.format(symbol, product)}
                        </span>
                        <br/>

                        {currency.hasOldPrice(symbol, product) ? <span className="product-item-old-price">{currency.format(symbol, product, true)}</span> : null}
                    </p>

                    {(()=> {
                        return (
                            <div className="product-item-additional-container">
                                <hr/>
                                <p className="product-item-sizes-title">Размеры в наличии:</p>
                                <Sizes product={product} className="product-item-sizes-list"/>

                                <div className="product-item-overlay-buttons">
                                    <a href="#" onClick={openModal.bind(this, this.getQuickOrderModal())}
                                       className="button">
                                        Быстрый заказ</a>

                                    {(()=> {
                                        if (in_cart) {
                                            return (
                                                <div>
                                                    <p>Товар добавлен в корзину</p>
                                                    <Link to="Cart" className="link">Перейти в корзину</Link>
                                                </div>
                                            );
                                        } else {
                                            return (
                                                <a href="#" className="link gray"
                                                   onClick={this.handleAddToCart.bind(this)}>
                                                    Добавить в корзину
                                                </a>
                                            );
                                        }
                                    })()}
                                </div>
                            </div>
                        );
                    })()}
                </div>
            </div>
        );
    }
}

export default connect((state, props) => {
    return {
        in_cart: state.cart.in_cart.indexOf(props.product.id) > -1,
        symbol: state.currency.symbol
    };
}, dispatch => {
    return {
        cartActions: bindActionCreators(cartActions, dispatch)
    };
})(Product);