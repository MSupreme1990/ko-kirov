import React, { Component, PropTypes } from 'react';
import Loader from 'shared/components/loader';
import ProductSlider from 'shared/components/product/product_slider';
import * as productActions from 'actions/product';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class RelatedSlider extends Component {
    static propTypes = {
        product: PropTypes.object.isRequired
    };

    componentWillMount() {
        const { actions, product } = this.props;
        actions.fetchRelatedIfNeeded(product.url);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.product.id != nextProps.product.id) {
            const { actions, product } = this.props;
            actions.fetchRelatedIfNeeded(product.url);
        }
    }

    render() {
        const { loading, error, objects } = this.props;

        if (error) {
            return null;
        }

        if (loading) {
            return <Loader/>;
        }

        return <ProductSlider data={objects} title="Вместе с этой моделью смотрят" prefix="also"/>;
    }
}

export default connect((state, props) => {
    const { loading, error, related } = state.product.related;
    return {
        loading,
        error,
        objects: related[props.product.url] || []
    };
}, dispatch => {
    return {
        actions: bindActionCreators(productActions, dispatch)
    }
})(RelatedSlider);