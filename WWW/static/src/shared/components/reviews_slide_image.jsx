import React from 'react';

var ReviewsSlideImage = React.createClass({
    render: function() {
        return (
            <a className="fancybox" rel={this.props.id} href={this.props.image.image.original}>
                <img src={this.props.image.image.thumb_slide} />
            </a>
        );
    }
});

export default ReviewsSlideImage;