import React, { Component, PropTypes } from 'react';

export default class Timer extends Component {
    _interval = null;

    static propTypes = {
        days: PropTypes.number.isRequired,
        hours: PropTypes.number.isRequired,
        minutes: PropTypes.number.isRequired,
        seconds: PropTypes.number.isRequired
    };

    static defaultProps = {
        days: 0,
        hours: 0,
        minutes: 60,
        seconds: 60
    };

    state = {
        days: 0,
        hours: 0,
        minutes: 0,
        seconds: 0
    };

    componentWillMount() {
        const { days, hours, minutes, seconds } = this.props;
        this.setState({ days, hours, minutes, seconds });

        let endtime = new Date(Date.parse(new Date()) + (days || 1) * (hours || 1) * (minutes || 1) * (seconds || 1) * 1000);

        this._interval = setInterval(() => {
            let t = this.getTimeRemaining(endtime);

            if (t.total <= 0) {
                clearInterval(this._interval);
                this.setState({
                    days: 0,
                    hours: 0,
                    minutes: 0,
                    seconds: 0
                });
            } else {
                this.setState({
                    days: t.days,
                    hours: ('0' + t.hours).slice(-2),
                    minutes: ('0' + t.minutes).slice(-2),
                    seconds: ('0' + t.seconds).slice(-2)
                });
            }
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this._interval);
    }

    getTimeRemaining(endtime) {
        let total = Date.parse(endtime) - Date.parse(new Date());
        return {
            total: total,
            days: Math.floor(total / (1000 * 60 * 60 * 24)),
            hours: Math.floor((total / (1000 * 60 * 60)) % 24),
            minutes: Math.floor((total / 1000 / 60) % 60),
            seconds: Math.floor((total / 1000) % 60)
        };
    }

    render() {
        const { days, hours, minutes, seconds } = this.state;

        return (
            <ul className="timer-list">
                <li>
                    <p className="timer-count-item">{days}</p>
                    <p className="timer-count-text">Дней</p>
                </li>
                <li>
                    <p className="timer-count-item">{hours}</p>
                    <p className="timer-count-text">Часов</p>
                </li>
                <li>
                    <p className="timer-count-item">{minutes}</p>
                    <p className="timer-count-text">Минут</p>
                </li>
                <li>
                    <p className="timer-count-item">{seconds}</p>
                    <p className="timer-count-text">Секунд</p>
                </li>
            </ul>
        );
    }
}