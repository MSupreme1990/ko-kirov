import { chunk } from 'lib/utils';
import React, { Component } from 'react';
import { Link } from 'react-easy-router';
import ReviewsText from 'shared/containers/review/reviews_text';
import ReviewVideo from 'shared/containers/review/review_video';
import VkCommunity from 'shared/containers/vk';
import Loader from 'shared/components/loader';
import { connect } from 'react-redux';
import Paginator from 'shared/containers/paginator';
import { bindActionCreators } from 'redux';
import * as reviewActions from 'actions/reviews';

class Reviews extends Component {
    state = {
        page: 1,
        text: false,
        video: true,
        small: false
    };

    componentWillMount() {
        const { reviewActions } = this.props;
        reviewActions.fetchListIfNeeded();
    }

    handleVideo(e) {
        e.preventDefault();
        this.setState({
            video: true,
            text: false
        });
    }

    handleText(e) {
        e.preventDefault();
        this.setState({
            text: true,
            video: false
        });
    }

    onPaginate(e, page) {
        this.setState({ page });
    }

    renderReviews() {
        const { loading, objects, video } = this.props;
        let data = chunk(objects, 1),
            reviews = data[this.state.page - 1] || [];

        if (loading) {
            return <Loader />;
        }

        var reviewVideoNodes = video.slice(0, 3).map(model => (
            <li key={"review_" + model.id}><ReviewVideo model={model}/></li>
        ));
        
        return (
            <div>
                <ul className="reviews-switch">
                    <li className={this.state.video ? 'active' : ''}>
                        <a href="#" onClick={this.handleVideo.bind(this)}>Видеоотзывы наших покупателей</a>
                    </li>
                    <li className={this.state.text ? 'active' : ''}>
                        <a href="#" onClick={this.handleText.bind(this)}>Текстовые отзывы наших покупателей</a>
                    </li>
                </ul>
                <div className="review-tabbed-container">
                    <div
                        className={this.state.video ? 'reviews-block-container' : 'reviews-block-container hide'}>
                        <ul className="small-block-grid-3 reviews-block-video-list">
                            {reviewVideoNodes}
                        </ul>
                    </div>
                    <div
                        className={this.state.text ? 'reviews-expert-container' : 'reviews-expert-container hide'}>
                        <ReviewsText reviews={reviews}/>
                        <Paginator
                            numPages={data.length}
                            onClick={this.onPaginate.bind(this)}/>
                    </div>
                </div>
            </div>
        )
    }

    render() {
        return (
            <div className="reviews-mixed-container">
                <div className="row">
                    <div className="columns medium-9 large-9">
                        {this.renderReviews()}
                    </div>
                    <div className="columns medium-3 large-3">
                        <h3>Наши счастливые покупатели<br/>Вконтакте</h3>
                        <VkCommunity />
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(state => {
    const { loading, objects, video } = state.reviews;

    return {
        loading,
        objects,
        video
    };
}, dispatch => {
    return {
        reviewActions: bindActionCreators(reviewActions, dispatch)
    };
})(Reviews);