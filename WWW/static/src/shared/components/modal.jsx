import React, { Component } from 'react';
import { render, findDOMNode, unmountComponentAtNode } from 'react-dom';
import { Link } from 'react-easy-router';
import { ModalLink, Modal } from 'shared/components/modal_dialog';
import { ProductModalLink, ProductModal } from 'shared/components/product_modal_dialog';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

export {
    ModalLink,
    ProductModalLink
};