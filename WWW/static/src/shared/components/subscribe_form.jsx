import $ from 'jquery';
import { findDOMNode } from 'react-dom';
import { cookie } from 'easy-storage';
import React, { Component } from 'react';
import api from 'lib/api';
import { dataLayer_push, sendToAmo, formValidate } from 'lib/utils';
import PhoneInput from 'inputs/phone_input.jsx';

export default class SubscribeForm extends Component {
    state = {
        message_extra: '',
        message_base: ''
    };

    handleSubmit(e) {
        e.preventDefault();

        let $form = $(findDOMNode(this.refs.form_base));
        api.post('/api/mail/subscribe', $form.serialize(), (data) => {
            formValidate($form, data['errors']);

            if (data['success']) {
                // google tag manager
                dataLayer_push({
                    event: 'submit',
                    action: 'subscribe'
                });

                sendToAmo($form);
                this.setState({
                    id: data['pk'],
                    message_base: 'Благодарим Вас за подписку на ko-kirov.ru!'
                });
                console.log(data);
            }
        });
    }

    renderExtra() {
        if (this.state.id == null) {
            return <span />;
        }

        return (
            <div
                className={this.state.message_extra == '' ? 'extra__form__container' : 'extra__form__container complete'}>
                <h1 className={this.state.message_extra == '' ? 'hide' : ''}>{this.state.message_extra}</h1>

                <form id="subscribe-question-form-modal" className={this.state.message_extra == '' ? 'row' : 'row hide'}
                      onSubmit={this.handleExtraSubmit.bind(this)}>
                    <a href="#" className="extra-form-close" onClick={this.handleClose.bind(this)}>&times;</a>
                    <div className="row">
                        <div className="columns large-12">
                            <p className="extra-form-title">Введите Ваш номер телефона, чтобы мы смогли удостовериться,
                                что Вы получили подарок:</p>
                        </div>
                    </div>

                    <div className="row">
                        <div className="columns large-12">
                            <div className="subscribe-call-form-row form-row">
                                <PhoneInput name="phone"/>
                                <ul className="errors"></ul>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="columns large-10">
                            <input type="hidden" name="id" value={this.state.id}/>
                            <input type="submit" value="ОТПРАВИТЬ" className="button"/>
                        </div>
                    </div>

                    <div className="row">
                        <div className="b-personal-data">
                                            <span className="b-personal-data__check">
                                                <input
                                                    className="b-personal-data__input" type="checkbox"
                                                    checked="checked"/>
                                            </span>
                            Нажимая кнопку «Получить консультацию», вы принимаете
                            <a className="b-personal-data__link"
                               href="/page/private"
                               target="_blank">"условия обработки персональных данных".</a>
                        </div>
                    </div>
                </form>
            </div>
        );
    }

    handleExtraSubmit(e) {
        e.preventDefault();

        var $form = $('#subscribe-question-form-modal');

        api.post('/api/mail/subscribe_extra', $form.serialize(), function (data) {
            sendToAmo($form);
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    message_extra: data['message']
                });

                setTimeout(function () {
                    this.setState({ pk: null });
                }.bind(this), 3500);
            }
        }.bind(this));
    }

    handleClose(e) {
        e.preventDefault();
        this.setState({
            id: null
        });
    }

    render() {
        let emailValue = cookie.get('email');

        return (
            <div id="subscribe-form">
                {this.renderExtra()}
                <form ref="form_base" onSubmit={this.handleSubmit.bind(this)}>
                    <h1 className={this.state.message_base == '' ? 'hide' : ''}>
                        {this.state.message_base}
                    </h1>

                    <div className="row">
                        <div className={this.state.message_base == '' ? '' : 'hide'}>
                            <div className="columns medium-7 large-9">
                                <input type="text" name="email" defaultValue={emailValue}
                                       placeholder="Введите ваш e-mail"/>
                                <ul className="errors"></ul>
                            </div>
                            <div className="columns medium-5 large-3">
                                <input type="submit" value="ПОДПИСАТЬСЯ" className="button"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}
