import $ from 'jquery';
import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import { Link } from 'react-easy-router';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from 'actions/category';

class SliderSidebar extends Component {
    componentWillMount() {
        const { actions } = this.props;
        actions.fetchListIfNeeded();
    }

    componentDidMount() {
        let $el = $(findDOMNode(this));
        if ($el.height() > 514) {
            $el.addClass('short');
        }
    }

    handleShowMore() {
        $(findDOMNode(this)).removeClass('short');
    }

    render() {
        const { loading, received, objects } = this.props;

        if (loading || !received) {
            return null;
        }

        let data = objects;
        let dl_count = 0;
        let dl = data.map(item => {
            dl_count = dl_count + item.items.length;
            return item.items.length;
        });

        let dlc = dl.map(item => item / dl_count * 100);

        if (dlc.length == 2) {
            if (dlc[0] > 60) {
                dlc[0] = 60;
            }
            if (dlc[1] < 40) {
                dlc[1] = 40;
            }
        }

        let menuNodes = data.map((item, i) => {
            let nodes = item.items.map(model => (
                    <li key={'ss' + model.id}>
                        <Link to="CatalogList" params={{url: model.url}}>
                            {model.name}
                        </Link>
                    </li>
                ));

            return (
                <div key={'ssr' + item.id} style={{height: dlc[i] + '%'}} className="root-menu">
                    <p><strong>{item.name}</strong></p>

                    <div className="bottom">
                        <div className="show_more">
                            <span className="text" onClick={this.handleShowMore.bind(this)}>
                                Смотреть все категории
                            </span>
                        </div>
                        {i != data.length - 1 ? <hr/> : null}
                    </div>

                    <ul className="sidebar-menu">{nodes}</ul>
                </div>
            );
        });

        return <div className="sidebar index short">{menuNodes}</div>;
    }
}

export default connect(state => state.category, dispatch => {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
})(SliderSidebar);