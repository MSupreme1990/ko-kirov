import React from 'react';
import SimpleSlider from 'shared/components/index/slider_simple.jsx';
import SliderSidebar from 'shared/components/index/slider_sidebar.jsx';

export default () => (
    <div className="slider-container">
        <div className="row">
            <div className="columns medium-3">
                <SliderSidebar />
            </div>
            <div className="columns medium-9">
                <SimpleSlider />
            </div>
        </div>
    </div>
)