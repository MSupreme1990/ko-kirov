import React, { Component } from 'react';
import { Link } from 'react-easy-router';

import Paginator from '../pager.jsx';
import Loader from 'shared/components/loader.jsx';

import SimpleSliderStore from 'store/simple_slider.js';

class SimpleSlider extends Component {
    state = SimpleSliderStore.getState();

    nextSlide(e) {
        e.preventDefault();
        SimpleSliderStore.nextSlide();
    }

    prevSlide(e) {
        e.preventDefault();
        SimpleSliderStore.prevSlide();
    }

    paginate(page, e) {
        e.preventDefault();
        SimpleSliderStore.setSlide(page);
    }

    componentWillMount() {
        this.unlisten = SimpleSliderStore.listen(this.setState.bind(this));
        SimpleSliderStore.list();
        SimpleSliderStore.startSlider();
    }

    componentWillUnmount() {
        SimpleSliderStore.stopSlider();
        this.unlisten();
    }

    renderPager() {
        let len = this.state.data.length - 1;
        return this.state.data.map((item, i) => {
            return (
                <li key={i} className={len - i == this.state.page - 1 ? "active": ""}>
                    <a href="#" onClick={this.paginate.bind(this, i)}>&nbsp;</a>
                </li>
            );
        })
    }

    render() {
        if (this.state.loading) {
            return <Loader />
        }

        const { styles, model } = this.state;

        return (
            <div className="slider-index" style={styles}>
                <div className="slider-index-item" key={model.id}>
                    <img src={model.image.resize}/>
                </div>
                <div className="pager-container">
                    <ul className="pager">
                        {this.renderPager()}
                    </ul>
                    <ul className="pager-nav">
                        <li className="prev-page">
                            <a href="#" onClick={this.prevSlide.bind(this)}>Prev</a>
                        </li>
                        <li className="next-page">
                            <a href="#" onClick={this.nextSlide.bind(this)}>Next</a>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default SimpleSlider;