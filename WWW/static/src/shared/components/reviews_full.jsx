import api from 'lib/api';
import { chunk } from 'lib/utils';
import React, { Component } from 'react';
import ReviewText from 'shared/containers/review/review_text';
import Paginator from './pager';
import CatalogLink from './catalog_link';

export default class ReviewsFull extends Component {
    currentPage = 1;
    numPages = 1;
    maxPages = 1;
    perPage = 3;

    static defaultProps = {
        pager: false
    };

    state = {
        data: [],
        models: []
    };

    componentDidMount() {
        api.get("/api/ko/reviews", {
            unlimit: true
        }, data => {
            var chunkData = chunk(data.reviews, this.perPage);
            this.numPages = chunkData.length;
            var r = {
                data: chunkData,
                models: chunkData[this.currentPage - 1]
            };
            this.setState(r);
        });
    }

    onPaginate (e, newPage) {
        this.currentPage = newPage;
        this.setState({
            models: this.state.data[this.currentPage - 1]
        });
        $('html, body').animate({
            scrollTop: $('.reviews-full-container').offset().top
        }, 500);
    }

    render() {
        var reviews = null;
        if (this.state.models.length > 0) {
            var reviewsNodes = this.state.models.map(model => (
                <li key={model.id}>
                    <ReviewText model={model} />
                </li>
            ));

            var pager = this.props.pager ? (
                <div>
                    <CatalogLink />
                    <Paginator
                      numPages={this.numPages}
                      maxPages={this.maxPages}
                      onClick={this.onPaginate.bind(this)} />
                </div>
            ) : null;
            reviews = (
                <div>
                    <ul className="small-block-grid-1 reviews-block-video-list">
                        {reviewsNodes}
                    </ul>
                    {pager}
                </div>
            );
        }

        var title = null;
        if (this.props.title) {
            title = (<h3>Отзывы наших Покупателей</h3>);
        }

        return (
            <div className="reviews-full-container">
                <div className="row">
                    <div className="columns small-12">
                        {title}
                        {reviews}
                    </div>
                </div>
            </div>
        );
    }
}
