import React from 'react';
import LazyLoad from 'react-lazy-load';
import { Link } from 'react-easy-router';
import Product from '../product/product';
import mobile from 'lib/mobile';

/*
export default props => {
    let lazyRenderCount = (mobile.any() ? 8 : 12),
        lazyPerLine = 4,
        lazyProductHeight = 424;

    if (props.products.length == 0) {
        return <h2>Ничего не найдено.</h2>;
    }

    let content = [],
        countLoop = Math.ceil(props.products.length / lazyRenderCount),
        iteration;

    for (iteration = 0; iteration <= countLoop; iteration++) {
        let prodStart = (iteration * lazyRenderCount),
            products = props.products.slice(prodStart, (prodStart + lazyRenderCount)),
            lazyHeight = lazyProductHeight * Math.ceil(products.length / lazyPerLine) + 'px';

        products = products.map(product => (
            <li key={'product_prev_' + product.id }>
                <Product product={product} group={props.group}/>
            </li>
        ));

        if (iteration == 0) {
            content.push(
                <ul className={"product-list small-block-grid-3 media-block-grid-" + (lazyPerLine - 1) + " " + "large-block-grid-" + lazyPerLine} key={'grid_bl_' + iteration}>{products}</ul>
            );
        } else {
            content.push(
                <LazyLoad height={lazyHeight} key={'lazy_' + 'grid_bl_' + iteration}>
                    <ul className={"product-list small-block-grid-3 media-block-grid-" + (lazyPerLine - 1) + " " + "large-block-grid-" + lazyPerLine}>{products}</ul>
                </LazyLoad>
            );
        }
    }

    return <div>{content}</div>;
}
*/

export default props => {
    if (props.products.length == 0) {
        return <h2>Ничего не найдено.</h2>;
    }

    let nodes = props.products.map(product => (
        <li key={'product_prev_' + product.id }>
            <Product product={product} group={props.group}/>
        </li>
    ));

    return <ul className="product-list medium-block-grid-3 large-block-grid-4">{nodes}</ul>;
}