import React, { Component, PropTypes } from 'react';
import { Link } from 'react-easy-router';
import { openModal } from 'lib/utils';
import HowOrderModal from 'shared/components/how_order_modal';
import DetectSize from 'shared/components/detect_size';
import GiftCounterModal from 'shared/components/gift_counter_modal';
import Loader from 'shared/components/loader';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as categoryActions from 'actions/category';

class SidebarMenu extends Component {
    static propTypes = {
        actions: PropTypes.object.isRequired,
        url: PropTypes.string
    };

    componentWillMount() {
        const { actions } = this.props;
        actions.fetchListIfNeeded();
    }

    renderMenuLevel(items) {
        const { url } = this.props;

        return items.map(category => {
            let active = false;
            let nodes = category.items.map(child => {
                if (child.url == url) {
                    active = true;
                }
                return (
                    <li key={"sidebar-menu-" + child.id}>
                        <Link className={child.url == url ? "active" : ""} to="CatalogList" params={{url: child.url}}>{child.name}</Link>
                    </li>
                );
            });

            let menu = nodes.length ? (
                <ul className={"sidebar-nested-menu" + (active || url == category.url ? ' active' : '')}>{nodes}</ul>
            ) : null;

            return (
                <li key={"sidebar-menu-" + category.id}>
                    <Link className={active || category.url == url ? "active" : ""} to="CatalogList" params={{url: category.url}}>{category.name}</Link>
                    {menu}
                </li>
            );
        });
    }

    render() {
        const { loading, objects } = this.props;

        if (loading) {
            return <Loader />;
        }

        let nodes = objects.map(root => {
            return (
                <div key={'sidebar-menu-root-' + root.id}>
                    <p><strong>{root.name}</strong></p>
                    <ul className="sidebar-menu">
                        {this.renderMenuLevel(root.items)}
                    </ul>
                </div>
            );
        });

        return (
            <div>
                {nodes}
                <hr/>
                <p className="icon-order">
                    <a href="#" onClick={openModal.bind(this, <HowOrderModal />, {width: 820, padding: 0})}>Как сделать заказ?</a>
                </p>
                <hr/>
                <p className="icon-size">
                    <a href="#" onClick={openModal.bind(this, <DetectSize woman={this.props.woman}/>, {padding: 0})}>Определить размер</a> 
                </p>
                <hr/>
                <p className="icon-gift">
                    <a href="#" onClick={openModal.bind(this, <GiftCounterModal />, {width: 760, padding: 0})}>Получить подарок</a>
                </p>
            </div>
        );
    }
}

export default connect(state => state.category, dispatch => {
    return {
        actions: bindActionCreators(categoryActions, dispatch)
    };
})(SidebarMenu);