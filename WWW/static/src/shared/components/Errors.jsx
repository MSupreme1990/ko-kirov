import React, { Component } from 'react';
import PropTypes from 'prop-types';
import objectPath from 'object-path';

export default class Errors extends Component {
    static propTypes = {
        path: PropTypes.string.isRequired,
        errors: PropTypes.oneOfType([
            PropTypes.object,
            PropTypes.array
        ]).isRequired
    };

    static defaultProps = {
        errors: []
    };

    reformatErrors() {
        const { errors } = this.props;

        let objectErrors = {};
        if (Array.isArray(errors)) {
            objectErrors = errors.reduce((acc, cur, i) => {
                acc[i] = cur;
                return acc;
            }, {});
        } else {
            objectErrors = errors;
        }

        return objectErrors;
    }

    getErrors() {
        let err = objectPath(this.reformatErrors()).get(this.props.path);
        if (typeof err === "object") {
            return Object.keys(err).length > 0 ? err : [];
        } else {
            return err ? err : [];
        }
    }

    render() {
        let nodes = this.getErrors().map((err, i) => {
            return <div className="b-error__item" key={i}>{err}</div>
        });

        return <div className="b-error">{nodes}</div>;
    }
}
