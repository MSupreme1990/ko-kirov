import $ from 'jquery';
import React, { Component } from 'react';
import Tooltip from 'shared/components/Tooltip';

export default class Best extends Component {
    static defaultProps = {
        reviews: false
    };

    handleClick(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $(".reviews-block-container").offset().top
        }, 500);
    }

    render() {
        var reviews = this.props.reviews ? (
                <div className="columns large-10">
                    <a href="#" onClick={this.handleClick.bind(this)} role="button" className="show-reviews-link">
                        Посмотрите отзывы наших клиентов
                    </a>
                </div>
            ) : null;

        const data = [
            {
                tooltip: 'Мы используем лучшие мировые технологии производства, что позволяет держать цену максимально выгодной для Покупателя. Стандарт качества ISO 9001 - это такой способ управления всем предприятием в условиях рынка, который отличается скоординированной деятельностью всех его сотрудников и подразделений, полностью направленной на обеспечение качества продукции удовлетворяющей Покупателя.',
                image: '/static/dist/images/inner/best_big.png',
                content: 'Собственное производство. Мы используем лучшие мировые технологии производства, что позволяет держать цену максимально выгодной для Покупателя.'
            },
            {
                tooltip: 'Не нужно волноваться: о графике работы магазинов, успею - не успею, о долгих часах перед полками в поисках того чего вам хочется. Достаточно широкий выбор позволяет легко  и быстро сделать заказ — и мы привезем его к вам!',
                image: '/static/dist/images/inner/best_many.png',
                content: 'Более 5000 современных моделей меховых и кожаных изделий, в том числе представлены большие размеры в наличии и под заказ индивидуального пошива по Вашим параметрам.'
            },
            {
                tooltip: 'Мех — это самый женственный и нежный  материал. В гардеробе каждой женщины, если она хочет быть принцессой, просто должно быть меховое изделие, что подтверждают и наши звёзды. Для Вас у нас тоже есть эксклюзив. Выбирайте!',
                image: "/static/dist/images/inner/best_star.png",
                content: 'Наши изделия носят известные звёзды шоу-бизнеса «Азиза», Светлана Пермякова, Анна Семенович'
            },
            {
                image: "/static/dist/images/inner/best_geo.png",
                tooltip: 'Новые коллекции, актуальные тенденции, модные тренды сезона 2017/18 и свежий взгляд на моду, все это представлено в нашем каталоге и доступно Вам.',
                content: 'Наши изделия разрабатываются итальянскими дизайнерами Andrea Nicolette, Lorenzo Conti и участвуют в международных показах мод в Москве, Милане и Париже.'
            },
            {
                image: "/static/dist/images/inner/best_cert.png",
                tooltip: 'Вся наша продукция сертифицирована и соответствует ГОСТ 32084-2013 ТР ТС 017/2011',
                content: 'Вся наша продукция промаркирована RFID-чипами, сертифицирована и соответствует ГОСТ 32084-2013 ТР ТС 017/2011'
            }
        ];

        let nodes = data.map((el, i) => (
            <li className="best-item" key={i}>
                <Tooltip content={el.tooltip}>
                    <img src={el.image}/>
                    <p className="big">{el.content}</p>
                </Tooltip>
            </li>
        ));

        return (
            <div className="best-container">
                <div className="row">
                    <div className="columns large-12">
                        <div className="best-info-container">
                            <h2>15 лет на рынке меха и кожи</h2>
                            <p>Почему более 100000 клиентов выбрали нас?</p>
                        </div>

                        <ul className="medium-block-grid-2 large-block-grid-5">
                            {nodes}
                        </ul>

                        {reviews}
                    </div>
                </div>
            </div>
        );
    }
}
