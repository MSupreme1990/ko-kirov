import React from 'react';
import { sendToAmo, formValidate } from 'lib/utils';
import api from 'lib/api';
import PhoneInput from 'inputs/phone_input';
import { cookie } from 'easy-storage';
import Timer from 'shared/components/timer';
import { dataLayer_push } from 'lib/utils';

var GiftCounterModal = React.createClass({
    getInitialState: function() {
        return {
            pk: null,
            message: ""
        };
    },
    handleClose: function(e) {
        e.preventDefault();
        this.setState({pk: null});
    },
    handleExtraSubmit: function(e) {
        e.preventDefault();

        var $form = $('#gift-question-form-modal');

        api.post('/api/ko/gift_extra', $form.serialize(), function(data) {
            sendToAmo($form);
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    message: data['message']
                });

                setTimeout(function() {
                    this.setState({pk: null});
                }.bind(this), 3500);
            }
        }.bind(this));
    },
    handleEmailChange: function(e) {
        cookie.set('email', e.target.value);
    },
    renderExtra: function() {
        if (this.state.pk == null) {
            return <span />;
        }
        var email = cookie.get('email');
        return (
            <div className={this.state.message == '' ? 'extra-form-popup' : 'extra-form-popup complete'}>
                <h1 className={this.state.message == '' ? 'hide' : ''}>{this.state.message}</h1>
                <a href="#" className="extra-form-close" onClick={this.handleClose}>&times;</a>
                <form id="gift-question-form-modal" className={this.state.message == '' ? 'row' : 'row hide'} onSubmit={this.handleExtraSubmit} >
                    <div className="row">
                        <div className="columns large-10">
                            <p className="extra-form-title">Ваша заявка успешно отправлена. Уточните, пожалуйста,
                            следующие данные:</p>
                        </div>
                    </div>

                    <div className="row">
                        <div className="columns large-10">

                                <input type="text" onChange={this.handleEmailChange} name="GiftForm[email]" defaultValue={email} placeholder="Введите ваш e-mail" />
                                <ul className="errors"></ul>

                        </div>
                    </div>

                    <div className="row">
                        <div className="columns large-10">
                            <textarea name="GiftForm[question]" placeholder="Введите ваш вопрос"></textarea>
                            <ul className="errors"></ul>
                        </div>
                    </div>

                    <div className="row">
                        <div className="columns large-10">
                            <input type="hidden" name="GiftForm[id]" value={this.state.pk} />
                            <input type="submit" value="ОТПРАВИТЬ" className="button" />
                        </div>
                    </div>

                </form>
            </div>
        );
    },
    handleSubmit: function(e) {
        e.preventDefault();
        e.stopPropagation();

        var $form = $('#gift-form-modal');

        api.post('/api/ko/gift', $form.serialize(), function(data) {
            formValidate($form, data['errors']);
            if (data['success']) {
                // google tag manager
                dataLayer_push({
                    'event': 'submit',
                    'action': 'gift'
                });

                this.setState({
                    pk: data['pk']
                });
            }
        }.bind(this));
    },
    handleNameChange: function(e) {
        cookie.set('name', e.target.value);
    },
    render: function() {
        var modal = this.renderExtra();
        var name = cookie.get('name');
        return (
            <div className="gift-counter-modal">
                {modal}
                <div className="gift-counter-info">
                    <h3>
                        Получите фирменный <br/> чехол - сумку в подарок!
                    </h3>
                    <div className="gift-counter">
                        <p className="gift-title">До конца акции осталось:</p>
                        <div className="gift-block">
                            <div id="gift-counter-modal">
                                <Timer/>
                            </div>
                        </div>
                    </div>
                </div>
                <form id="gift-form-modal" className="row" onSubmit={this.handleSubmit} >
                    <div className="gift-form-row form-row">
                        <input type="text" onChange={this.handleNameChange} name="GiftForm[name]" defaultValue={name} placeholder="Введите ваше имя" />
                        <ul className="errors"></ul>
                    </div>
                    <div className="gift-form-row form-row">
                        <PhoneInput name="GiftForm[phone]" />
                        <ul className="errors"></ul>
                    </div>
                    <div className="gift-form-row form-row">
                        <input type="submit" value="ПОЛУЧИТЬ ПОДАРОК" className="button" />
                    </div>
                    <div className="b-personal-data">
                        <span className="b-personal-data__check"><input type="checkbox" checked="checked"/></span>
                        Передавая информацию сайту вы принимаете условия
                        <a className="b-personal-data__link"
                           href="/page/private"
                           target="_blank">"политики защиты персональных данных".</a>
                    </div>
                </form>
            </div>
        );
    }
});

export default GiftCounterModal;
