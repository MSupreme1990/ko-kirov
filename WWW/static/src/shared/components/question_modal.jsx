import $ from 'jquery';
import React from 'react';
import PhoneInput from 'inputs/phone_input';
import { cookie } from 'easy-storage';
import api from 'lib/api';
import { dataLayer_push, sendToAmo, formValidate } from 'lib/utils';

var QuestionModal = React.createClass({
    getInitialState: function() {
        return {
            pk: null,
            message: ""
        };
    },
    handleSubmit: function(e) {
        e.preventDefault();

        var $form = $('#question-modal-form');

        api.post('/api/ko/question', $form.serialize(), function(data) {
            sendToAmo($form);
            formValidate($form, data['errors']);
            if (data['success']) {
                // google tag manager
                dataLayer_push({
                    'event': 'submit',
                    'action': 'consult'
                });

                this.setState({
                    pk: data['pk']
                });
            }
        }.bind(this));
    },
    handleClose: function(e) {
        e.preventDefault();
        this.setState({pk: null});
    },
    handleExtraSubmit: function(e) {
        e.preventDefault();

        var $form = $('#question-extra-form');

        api.post('/api/ko/question_extra', $form.serialize(), function(data) {
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    message: data['message']
                });

                setTimeout(function() {
                    this.setState({pk: null});
                }.bind(this), 2500);
            }
        }.bind(this));
    },
    handleNameChange: function(e) {
        cookie.set('name', e.target.value);
    },
    handleEmailChange: function(e) {
        cookie.set('email', e.target.value);
    },
    renderExtra: function() {
        if (this.state.pk == null) {
            return <span />;
        }
        var email = cookie.get('email');
        return (
            <div className={this.state.message == '' ? 'extra-form-popup' : 'extra-form-popup complete'}>
                <h1 className={this.state.message == '' ? 'hide' : ''}>{this.state.message}</h1>
                <a href="#" className="extra-form-close" onClick={this.handleClose}>&times;</a>
                <form id="question-extra-form" className={this.state.message == '' ? '' : 'hide'} onSubmit={this.handleExtraSubmit} >
                    <div className="row">
                        <div className="columns large-12">
                            <div className="question-extra__item">
                                <p className="extra-form-title">Ваша заявка успешно отправлена. Уточните, пожалуйста, следующие данные:</p>
                            </div>
                        </div>
                        <div className="columns large-12">
                            <div className="question-extra__item">
                                <input type="text" onChange={this.handleEmailChange} name="QuestionForm[email]" defaultValue={email} placeholder="Введите ваш e-mail" />
                                <ul className="errors"></ul>
                            </div>
                        </div>
                        <div className="columns large-12">
                            <div className="question-extra__item">
                                <textarea name="QuestionForm[question]" placeholder="Введите ваш вопрос"></textarea>
                                <ul className="errors"></ul>
                            </div>
                        </div>
                        <div className="columns large-12">
                            <div className="question-extra__item">
                                <input type="hidden" name="QuestionForm[id]" value={this.state.pk} />
                                <input type="submit" value="ОТПРАВИТЬ" className="button" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    },
    render: function() {
        var modal = this.renderExtra();
        var name = cookie.get('name');
        return (
            <div className="question-container-modal">
                <div className="row">
                    {modal}
                    <div className="columns medium-12">
                        <h3>Появились вопросы?</h3>
                        <p>Оставьте номер телефона и наш менеджер примет
                        Ваш заказ<br/>и проконсультирует по всем интересующим вопросам</p>
                    </div>
                    <div className="columns medium-12">
                        <form id="question-modal-form" onSubmit={this.handleSubmit} >
                            <div className="form-row">
                                <input type="text" onChange={this.handleNameChange} defaultValue={name} name="QuestionForm[name]" placeholder="Введите ваше имя" />
                                <ul className="errors"></ul>
                            </div>
                            <div className="form-row">
                                <PhoneInput name="QuestionForm[phone]" />
                                <ul className="errors"></ul>
                            </div>
                            <div className="form-row">
                                <input type="submit" value="ЗАКАЗАТЬ ЗВОНОК" className="button" />
                            </div>
                            <div className="form-row">
                                <div className="b-personal-data">
                                            <span className="b-personal-data__check">
                                                <input
                                                    className="b-personal-data__input" type="checkbox"
                                                    checked="checked"/>
                                            </span>
                                    Нажимая кнопку «Получить консультацию», вы принимаете
                                    <a className="b-personal-data__link"
                                       href="/page/private"
                                       target="_blank">"условия обработки персональных данных".</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
});

export default QuestionModal;
