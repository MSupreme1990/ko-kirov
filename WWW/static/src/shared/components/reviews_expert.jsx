import React, { Component } from 'react';
import api from 'lib/api';
import Review from 'shared/containers/review/review_text';
import VkCommunity from 'shared/containers/vk';

export default class ReviewsExpert extends Component {
    state = {
        data: []
    };

    componentDidMount() {
        api.get("/api/ko/reviews_expert", {
            expert: true
        }, data => {
            this.setState({ data });
        });
    }

    render() {
        if (this.state.data.length == 0) {
            return (<span />);
        }

        var reviewNodes = this.state.data.map(model => (
            <li key={"review_expert_" + model.id}>
                <Review model={model} is_expert={true}/>
            </li>
        ));

        return (
            <div className="reviews-expert-container">
                <div className="row">
                    <div className="columns large-8">
                        <h3 className="reviews-block-video-title">Мнение эксперта</h3>
                        <ul className="small-block-grid-1 reviews-block-video-list">
                            {reviewNodes}
                        </ul>
                    </div>
                    <div className="columns large-2">
                        <h3>Наши счастливые покупатели<br/>Вконтакте</h3>
                        <VkCommunity />
                    </div>
                </div>
            </div>
        );
    }
}