import React, { Component } from 'react';

import { ModalLink } from './modal.jsx';

export default class SocialModal extends Component {
    static defaultProps = {
        className: 'social__modal',
        page_url: 'http://ko-kirov.ru/gift'
    };

    open() {
        this.refs.modal.open();
        setTimeout(() => {
            VK.Widgets.Like("vk_like123", {
                type: "button",
                pageTitle: "1000 рублей для моих друзей! Кому-нибудь нужна большая скидка в ko-kirov.ru? Только что подарили промо-код на 1000 рублей…",
                pageDescription: "1000 рублей для моих друзей! Кому-нибудь нужна большая скидка в ko-kirov.ru? Только что подарили промо-код на 1000 рублей…",
                pageUrl: 'http://ko-kirov.ru/gift',
                pageImage: "http://ko-kirov.ru/static/dist/1.0.4/images/main/logo.png",
                text: "1000 рублей для моих друзей! Кому-нибудь нужна большая скидка в ko-kirov.ru? Только что подарили промо-код на 1000 рублей…"
            }, this.props.page_url);

            OK.CONNECT.insertShareWidget(
                "ok_like123",
                this.props.page_url,
                "{width:225,height:50,st:'oval',sz:16,ck:1}"
            );
        }, 100);
    }

    render() {
        return (
            <ModalLink
                className={this.props.className}
                ref="modal"
                name={null}>
                <h1>Ваша заявка успешно отправлена!</h1>

                <p>В ближайшее время с вами свяжется наш консультант</p>

                <div className="social__container">
                    <div className="social__container-dark">
                        <p>Хотите получить дополнительную скидку 1000 рублей? Расскажите о своем заказе ВКонтакте!</p>
                    </div>
                    <div className="right-side">Нажмите "Мне нравится" и "рассказать друзьям</div>
                    <div className="left-side">
                        <div id="vk_like123" className="left"></div>
                        <div id="ok_like123" className="left"></div>
                    </div>
                </div>
            </ModalLink>
        );
    }
}