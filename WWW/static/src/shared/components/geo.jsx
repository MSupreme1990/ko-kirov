import React, { Component } from 'react';
import Loader from 'shared/components/loader';
import settings from 'config/settings';
import { cookie } from 'easy-storage';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as geoActions from 'actions/geo';

class Geo extends Component {
    componentWillMount() {
        const { actions } = this.props;
        actions.fetchListIfNeeded();
    }

    handlerClick(city, e) {
        e.preventDefault();

        const { id, sites } = this.props;

        if (city.id == id) {
            return;
        }

        let site = sites[city.id];
        cookie.set('city', site.domain, {
            domain: settings.BASE_DOMAIN,
            patch: '/'
        });

        this.callbackGeo(city.id, site);
    }

    handlerRegionClick(id, e) {
        e.preventDefault();

        const { actions } = this.props;
        actions.setCurrentCities(this.props.regions[id]);
    }

    callbackGeo(id, site) {
        //window.location = "http://" + site.domain;
        if (settings.DEBUG) {
            alert('Move to: ' + site.domain);
        } else {
            window.location.host = site.domain;
        }
    }

    renderRegions() {
        const { cities, region_id } = this.props;
        let nodes = cities.map(region => (
            <li key={'region_' + region.id} className={region_id == region.id ? 'active' : ''}>
                <a onClick={this.handlerRegionClick.bind(this, region.id)} href="#">{region.name}</a>
            </li>
        ));

        return (
            <div>
                <h3>Федеральный округ</h3>
                <ul className="small-block-grid-2 medium-block-grid-1 geo-region-list">
                    {nodes}
                </ul>
            </div>
        );
    }

    renderCities() {
        const { current_cities, id } = this.props;

        let nodes = current_cities.map(city => (
            <li key={'current_city_' + city.id}>
                <a href="#" className={id == city.id ? 'active' : ''}
                   onClick={this.handlerClick.bind(this, city)}>{city.name}</a>
            </li>
        ));

        return (
            <div>
                <h3>Город</h3>
                <ul className="small-block-grid-2 medium-block-grid-3 city-list">
                    {nodes}
                </ul>
            </div>
        );
    }

    renderPopular() {
        const { popular, id } = this.props;

        if (popular.length == 0) {
            return null;
        }

        let nodes = popular.map(city => (
            <li key={'popular_' + city.id}>
                <a href="#" className={id == city.id ? 'active' : ''}
                   onClick={this.handlerClick.bind(this, city)}>{city.name}</a>
            </li>
        ));

        return (
            <div className="geo-popular">
                <h3>Популярные города</h3>
                <ul className="small-block-grid-2 medium-block-grid-3 city-list">
                    {nodes}
                </ul>
            </div>
        );
    }

    render() {
        const { loading } = this.props;

        if (loading) {
            return <Loader />
        }

        return (
            <div className="geo-container">
                <div className="row">
                    <div className="columns small-12 medium-12">
                        <div>
                            <h1>Выберите Ваш город</h1>
                            {this.renderPopular()}
                        </div>
                    </div>
                </div>
                <div className="geo-all">
                    <div className="row">
                        <div className="columns small-12 medium-4">
                            {this.renderRegions()}
                        </div>
                        <div className="columns small-12 medium-8">
                            {this.renderCities()}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(state => state.geo, dispatch => {
    return {
        actions: bindActionCreators(geoActions, dispatch)
    };
})(Geo);