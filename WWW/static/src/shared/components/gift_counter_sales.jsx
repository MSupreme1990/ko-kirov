import $ from 'jquery';
import api from 'lib/api';
import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import PhoneInput from 'inputs/phone_input';
import { cookie } from 'easy-storage';
import { sendToAmo, formValidate } from 'lib/utils';
import { dataLayer_push } from 'lib/utils';

class GiftCounterSales extends Component {
    state = {
        pk: null,
        message: ""
    };

    handleClose(e) {
        e.preventDefault();
        this.setState({
            pk: null
        });
    }

    handleExtraSubmit(e) {
        e.preventDefault();

        var $form = $(findDOMNode(this.refs.form_extra));

        api.post('/api/ko/gift_sales_extra', $form.serialize(), function (data) {
            sendToAmo($form);
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    message: data['message']
                });

                setTimeout(function () {
                    this.setState({ pk: null });
                }.bind(this), 2500);
            }
        }.bind(this));
    }

    handleEmailChange(e) {
        cookie.set('email', e.target.value);
    }

    renderExtra() {
        if (!this.state.success) {
            return <span />;
        }

        return (
            <div className='extra-form-popup complete'>
                <h1>{this.state.message}</h1>
            </div>
        );
    }

    handleSubmit(e) {
        e.preventDefault();

        var $form = $('#gift-form');

        api.post('/api/ko/sales', $form.serialize(), function (data) {
            formValidate($form, data['errors']);
            if (data['success']) {
                // google tag manager
                dataLayer_push({
                    'event': 'submit',
                    'action': 'sales'
                });

                this.setState({
                    success: true,
                    message: data['message']
                });
            }
        }.bind(this));
    }

    handleNameChange(e) {
        cookie.set('name', e.target.value);
    }

    render() {
        var modal = this.renderExtra();
        var name = cookie.get('name');
        return (
            <div className="gift-counter-container">
                <div className="gift-counter-img">
                    <div className="row">
                        {modal}
                        <div className="columns medium-6 left-side">
                            <h4>Получите доступ к оптовым ценам</h4>
                            <p><strong>+ БЕСПЛАТНО проверенную систему «Увеличение прибыли магазина меховых изделий в
                                2,5 раза»</strong></p>
                        </div>
                        <div className="columns medium-6">
                            <form ref="form" id="gift-form" className="row" onSubmit={this.handleSubmit.bind(this)}>
                                <div className="columns medium-4">
                                    <div className="form-row">
                                        <input type="text" onChange={this.handleNameChange.bind(this)}
                                               name="SalesForm[name]" defaultValue={name}
                                               placeholder="Введите ваше имя"/>
                                        <ul className="errors"></ul>
                                    </div>
                                </div>
                                <div className="columns medium-4">
                                    <div className="form-row">
                                        <PhoneInput name="SalesForm[phone]"/>
                                        <ul className="errors"></ul>
                                    </div>
                                </div>
                                <div className="columns medium-4">
                                    <input type="submit" value="ПОЛУЧИТЬ" className="button"/>
                                </div>
                                <div className="columns medium-12 large-12">
                                    <div className="b-personal-data">
                                            <span className="b-personal-data__check">
                                                <input
                                                    className="b-personal-data__input" type="checkbox"
                                                    checked="checked"/>
                                            </span>
                                        Нажимая кнопку «Получить консультацию», вы принимаете
                                        <a className="b-personal-data__link"
                                           href="/page/private"
                                           target="_blank">"условия обработки персональных данных".</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default GiftCounterSales;
