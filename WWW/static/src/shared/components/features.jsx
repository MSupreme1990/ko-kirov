import React, { Component } from 'react';
import { Link } from 'react-easy-router';
import Tooltip from 'shared/components/Tooltip';
import classnames from 'classnames';

export default class Features extends Component {
    static defaultProps = {
        small: false,
        half: false
    };

    render() {
        let html;

        const cls = classnames('features-container', {
            'small': this.props.small,
            'half': this.props.half,
        });

        if (this.props.small) {
            html = (
                <ul className="features-list">
                    <li className="multiline">
                        <Tooltip content="Быстро бесплатно доставим Ваш заказ курьером по всей России">
                            <p className="delivery">
                                <Link to="Delivery">
                                    Бесплатная доставка на дом
                                    <br/>
                                    по всей России
                                </Link>
                            </p>
                        </Tooltip>
                    </li>
                    <li>
                        <Tooltip content="Примерка мехового изделия бесплатна. Если товар Вам не подошёл, Вы можете сразу же вернуть его курьеру.">
                            <p className="prepare">
                                <Link to="Delivery">
                                    Примерка перед покупкой
                                </Link>
                            </p>
                        </Tooltip>
                    </li>
                </ul>
            );
        } else if (this.props.half) {
            html = (
                <ul className="features-list">
                    <li className="multiline">
                        <Tooltip content="Быстро бесплатно доставим Ваш заказ курьером по всей России">
                            <p className="delivery">
                                <Link to="Delivery">
                                    Бесплатная доставка на
                                    <br/>дом по всей России
                                </Link>
                            </p>
                        </Tooltip>
                    </li>
                    <li>
                        <Tooltip content="Примерка мехового изделия бесплатна. Если товар Вам не подошёл, Вы можете сразу же вернуть его курьеру.">
                            <p className="prepare">
                                <Link to="Delivery">
                                    Примерка перед<br/>
                                    покупкой
                                </Link>
                            </p>
                        </Tooltip>
                    </li>
                    <li className="multiline">
                        <Tooltip content="Вся наша продукция сертифицирована и соответствует ГОСТ 8765-93. Мы уверены в качестве нашей продукции и даем гарантию 12 месяцев. Вы уверены в своей неотразимости!">
                            <p className="cert">
                                Вся продукция<br/>
                                сертифицирована
                            </p>
                        </Tooltip>
                    </li>
                </ul>
            );
        } else {
            html = (
                <ul className="features-list">
                    <li className="multiline">
                        <Tooltip content="Бесплатная доставка на дом по всей России">
                            <p className="delivery">
                                <Link to="Delivery">
                                    Бесплатная доставка<br/>
                                    на дом по всей России
                                </Link>
                            </p>
                        </Tooltip>
                    </li>
                    <li>
                        <Tooltip
                            content="Примерка мехового изделия бесплатна. Если товар Вам не подошёл, Вы можете сразу же вернуть его курьеру.">
                            <p className="prepare">
                                <Link to="Delivery">
                                    Примерка перед<br/>покупкой
                                </Link>
                            </p>
                        </Tooltip>
                    </li>
                    <li>
                        <Tooltip
                            content="На сайте Вы можете оставить заказ круглосуточно. Менеджеры работают каждый день с 9:00 до 22:00 по МСК.">
                            <p className="orders">
                                Принимаем заказы<br/>круглосуточно
                            </p>
                        </Tooltip>
                    </li>
                    <li>
                        <Tooltip
                            content="Вы можете заказать доставку домой курьером или в ближайший пункт примерки в Вашем городе.">
                            <p className="points">
                                Более 1000 пунктов<br/>
                                выдачи по России
                            </p>
                        </Tooltip>
                    </li>
                </ul>
            );
        }

        return <div className={cls}>{html}</div>;
    }
}