import $ from 'jquery';
import api from 'lib/api';
import { sendToAmo, formValidate } from 'lib/utils';
import React from 'react';
import PhoneInput from 'inputs/phone_input';
import { cookie } from 'easy-storage';
import { dataLayer_push } from 'lib/utils';

var RequestCall = React.createClass({
    getInitialState: function() {
        return {
            complete: false
        };
    },
    handleNameChange: function(e) {
        cookie.set('name', e.target.value);
    },
    handleEmailChange: function(e) {
        cookie.set('email', e.target.value);
    },
    handleSubmit: function(e) {
        e.preventDefault();

        var $form = $('#request-call-form');

        api.post('/api/ko/request-call', $form.serialize(), function(data) {
            sendToAmo($form);
            formValidate($form, data['errors']);
            if (data['success']) {
                // google tag manager
                dataLayer_push({
                    'event': 'submit',
                    'action': 'call'
                });

                $form.find('h1').text(data['message']);
                this.setState({
                    complete: true
                });
            }
        }.bind(this));
    },
    render: function() {
        var name = cookie.get('name');
        var email = cookie.get('email');
        return (
            <div>
                <form id="request-call-form" className="row" onSubmit={this.handleSubmit} >
                    <h1>Заказать звонок</h1>
                    <div className={this.state.complete ? 'hide' : ''}>
                        <p>Оставьте номер телефона и наш менеджер проконсультирует по всем интересующим вопросам</p>

                        <div className="request-call-form-row form-row">
                            <input type="text" name="RequestCallForm[name]" onChange={this.handleNameChange} defaultValue={name} placeholder="Введите ваше имя" />
                            <ul className="errors"></ul>
                        </div>
                        <div className="request-call-form-row form-row">
                            <PhoneInput name="RequestCallForm[phone]" />
                            <ul className="errors"></ul>
                        </div>
                        <div className="request-call-form-row form-row">
                            <input type="text" name="RequestCallForm[email]" onChange={this.handleEmailChange} defaultValue={email} placeholder="Введите ваш e-mail" />
                            <ul className="errors"></ul>
                        </div>
                        <div className="request-call-form-row form-row">
                            <textarea name="RequestCallForm[question]" placeholder="Введите ваш вопрос" />
                            <ul className="errors"></ul>
                        </div>
                        <div className="request-call-form-row form-row">
                            <input type="submit" value="ЗАКАЗАТЬ ЗВОНОК" className="button" />
                        </div>
                        <div className="request-call-form-row form-row">
                            <div className="b-personal-data">
                                            <span className="b-personal-data__check">
                                                <input
                                                    className="b-personal-data__input" type="checkbox"
                                                    checked="checked"/>
                                            </span>
                                Нажимая кнопку «Получить консультацию», вы принимаете
                                <a className="b-personal-data__link"
                                   href="/page/private"
                                   target="_blank">"условия обработки персональных данных".</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
});

export default RequestCall;
