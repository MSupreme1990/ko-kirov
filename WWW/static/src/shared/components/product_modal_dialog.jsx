import React, { Component, PropTypes } from 'react';
import ProductModal from 'shared/components/product/product_modal';
import { ModalLink, Modal } from 'shared/components/modal_dialog';

class ProductModalLink extends ModalLink {
    renderLayer() {
        if (!this.state.show) {
            return null;
        }
        return (
            <Modal className={this.props.className} onRequestClose={this.handleClick.bind(this)}>
                <ProductModal model={this.props.model} group={this.props.group} />
            </Modal>
        );
    }

    render() {
        return (
            <a href="#"
               data-id={this.state.id}
               role="button"
               className={this.props.linkClassName || ''}
               onClick={this.handleClick.bind(this)}>
                {this.props.name || this.props.children}
            </a>
        );
    }
}

export {
    ProductModalLink
}
