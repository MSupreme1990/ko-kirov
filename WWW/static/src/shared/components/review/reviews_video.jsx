import React, { Component, PropTypes } from 'react';
import api from 'lib/api';
import ReviewsVideo from 'shared/containers/review/reviews_video';

export default class ReviewsVideoComponent extends Component {
    static propTypes = {
        count: PropTypes.number.isRequired
    };

    state = { 
        data: []
    };

    componentWillMount() {
        api.get("/api/ko/video_reviews", {}, data => this.setState({ data }));
    }

    render() {
        const { count } = this.props;
        const { data } = this.state;

        return <ReviewsVideo reviews={data} count={count} />;
    }
}
