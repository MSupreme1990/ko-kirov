import React, { Component } from 'react';
import api from 'lib/api';
import { chunk } from 'lib/utils';
import { Link } from 'react-easy-router';
import Paginator from 'shared/components/pager';
import ReviewsText from 'shared/containers/review/reviews_text';

export default class ReviewsTextComponent extends Component {
    currentPage = 1;
    numPages = 1;
    maxPages = 1;
    perPage = 1;

    state = {
        reviews: [],
        video_reviews: [],
        small: false
    };

    onPaginate (e, page) {
        e.preventDefault();

        this.currentPage = Number(page);
        this.setState({
            reviews: this.state.data[this.currentPage - 1]
        });
    }

    componentDidMount () {
        api.get("/api/ko/reviews", {}, data => {
            var chunkData = chunk(data.reviews, this.perPage);
            this.numPages = chunkData.length;
            this.setState({
                data: chunkData,
                reviews: chunkData[this.currentPage - 1]
            });
        });
    }

    render () {
        return (
            <div>
                <ReviewsText reviews={this.state.reviews} />
                <div className="reviews-all-link">
                    <Link to="Reviews">Смотреть все отзывы</Link>
                </div>
                <Paginator
                    page={Number(this.currentPage)}
                    small={this.props.small}
                    splitTemplate={true}
                    numPages={this.numPages}
                    maxPages={this.maxPages}
                    onClick={this.onPaginate.bind(this)} />
            </div>
        );
    }
}
