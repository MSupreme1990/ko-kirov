import React from 'react';
import settings from 'config/settings';

export default props => (
    <div className="how-order-container">
        <div className="row">
            <div className="columns medium-12 how-order-info-container">
                <h1>Как сделать заказ?</h1>
            </div>
            <ul className="medium-block-grid-5">
                <li>
                    <div className="how-order-item">
                        <img src={"/static/dist/images/main/how-order-product.png"}/>
                        <p className="how-order-product">
                            Вы выбираете изделие<br/>из каталога
                        </p>
                    </div>
                </li>
                <li>
                    <div className="how-order-item">
                        <img src={"/static/dist/images/main/how-order-request.png"}/>
                        <p className="how-order-request">
                            Оставляете бесплатную заявку
                        </p>
                    </div>
                </li>
                <li>
                    <div className="how-order-item">
                        <img src={"/static/dist/images/main/how-order-phone.png"}/>
                        <p className="how-order-phone">
                            С Вами связывается персональный<br/>
                            менеджер, подтверждает заявку и<br/>
                            консультирует по всем вопросам
                        </p>
                    </div>
                </li>
                <li>
                    <div className="how-order-item">
                        <img src={"/static/dist/images/main/how-order-delivery.png"}/>
                        <p className="how-order-delivery">
                            Доставка заказа до двери для<br/>
                            бесплатной примерки
                        </p>
                    </div>
                </li>
                <li>
                    <div className="how-order-item">
                        <img src={"/static/dist/images/main/how-order-payment.png"}/>
                        <p className="how-order-payment">
                            Расчет на месте удобным для Вас<br/>
                            способом. Вы в статусе постоянный<br/>
                            покупатель.
                        </p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
);
