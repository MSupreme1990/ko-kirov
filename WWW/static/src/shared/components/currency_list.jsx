import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as currencyActions from 'actions/currency';

class CurrencyList extends Component {
    componentWillMount() {
        const { actions } = this.props;
        actions.fetchListIfNeeded();
    }

    renderList() {
        const { actions, objects } = this.props;

        let nodes = Object.keys(objects).map(abr => {
            return (
                <li key={abr} onClick={e => actions.setSymbol(abr)} className={abr.toLowerCase()}>
                    {objects[abr]}
                </li>
            );
        });

        return <ul className="currency-list">{nodes}</ul>;
    }

    render() {
        const { symbol, objects, loading, received } = this.props;

        if (loading || !received) {
            return null;
        }

        return (
            <div className="currency__container">
                Валюта:
                <div className="currency-selected">
                    <span className={'current ' + symbol.toLowerCase()}>{objects[symbol]}</span>
                    {this.renderList()}
                </div>
            </div>
        );
    }
}

export default connect(state => state.currency, dispatch => {
    return {
        actions: bindActionCreators(currencyActions, dispatch)
    };
})(CurrencyList);