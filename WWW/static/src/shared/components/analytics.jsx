import React, { Component } from 'react';
import loader from 'lib/loader';
import settings from 'config/settings';

class Analytics extends Component {
    constructor(props) {
        super(props);

        if (!settings.DEBUG) {
            this.preInit();
            if (typeof window[this.props.key] == "undefined") {
                this.load(this.props.src, this.init);
            } else {
                this.send();
            }
        }
    }

    componentWillReceiveProps() {
        if (!settings.DEBUG) {
            this.send();
        }
    }

    load(src = '', callback = () => {}) {
        loader.loadFile(document, {
            src: src,
            tag: "script",
            type: "text/javascript"
        }, callback.bind(this));
    }

    send() {

    }

    preInit() {

    }

    render() {
        return (
            <span />
        )
    }
}

class Ga extends Analytics {
    static defaultProps = {
        id: 'UA-XXXXXXXX-1',
        key: 'GoogleAnalyticsObject',
        src: 'https://www.google-analytics.com/analytics.js'
    };

    init() {
        ga('create', this.props.id, 'auto');
    }

    send() {
        ga('send', 'pageview');
    }
}

class AdWords extends Analytics {
    static defaultProps = {
        id: 123456789,
        key: '',
        src: 'http://www.googleadservices.com/pagead/conversion.js'
    };

    send() {
        let conv_handler = window['google_trackConversion'];
        if (typeof(conv_handler) == 'function') {
            conv_handler({
                google_conversion_id: this.props.id,
                google_custom_params: window.google_tag_params,
                google_remarketing_only: true
            });
        }
    }
}

class Ya extends Analytics {
    static defaultProps = {
        id: 123,
        options: {
            defer: true,
            clickmap: true,
            trackLinks: true,
            accurateTrackBounce: true,
            webvisor: true,
            trackHash: true
        },
        key: 'ya_metrika',
        src: 'https://mc.yandex.ru/metrika/watch.js'
    };

    preInit() {
        var key = "yandex_metrika_callbacks";

        window[key] = window[key] || [];
        window[key].push(this.initYa.bind(this));
    }

    initYa() {
        var options = this.props.options;
        options['id'] = Number(this.props.id);

        window['yaCounter' + this.props.id] = new window.Ya.Metrika(options);
    }

    send() {
        if ('yaCounter' + this.props.id in window) {
            window['yaCounter' + this.props.id].hit(window.location.pathname);
        }
    }
}

export { Ga, Ya, AdWords };