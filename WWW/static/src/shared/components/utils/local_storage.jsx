export default {
    get: (key) => {
        let item = localStorage.getItem(key);
        try {
            item = JSON.parse(item);
        } catch (e) {

        }
        return item;
    },
    set: (key, value) => {
        if (value instanceof Object || value instanceof Array) {
            value = JSON.stringify(value);
        }
        try {
            localStorage.setItem(key, value);
        } catch (e) {
            if (window['showWarnings']) {
                console.warn(e.code, e.name, e.message);
                console.warn(value.length / 1024 / 1024);
            }

        }

    },
    remove: (key) => {
        localStorage.removeItem(key);
    }
};