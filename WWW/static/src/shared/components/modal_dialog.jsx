import $ from 'jquery';
import React, { Component, PropTypes } from 'react';
import { render, findDOMNode, unmountComponentAtNode } from 'react-dom';
import { dispatch } from 'store';
import * as modalActions from 'actions/modal';

export class LayerBase extends Component {
    _layer = null;

    componentDidMount() {
        this._layer = document.createElement('div');
        document.body.appendChild(this._layer);
        this._renderLayer();
    }

    componentDidUpdate() {
        this._renderLayer();
    }

    componentWillUnmount() {
        this._unrenderLayer();
        document.body.removeChild(this._layer);
    }

    _renderLayer() {
        var layerElement = this.renderLayer();
        if (layerElement === null) {
            render(<noscript />, this._layer);
        } else {
            render(layerElement, this._layer);
        }

        if (this.layerDidMount) {
            this.layerDidMount(this._layer);
        }
    }

    _unrenderLayer() {
        if (this.layerWillUnmount) {
            this.layerWillUnmount(this._layer);
        }
        unmountComponentAtNode(this._layer);
    }
}

export class ModalLink extends LayerBase {
    static propTypes = {
        onRequestClose: PropTypes.func
    };

    static defaultProps = {
        onRequestClose: undefined
    };

    state = {
        show: false
    };

    handleClick(e) {
        if (e) e.preventDefault();

        this.setState({ show: !this.state.show }, () => {
            if (this.state.show) {
                this.open();
            } else {
                this.close();
            }
        });
    }

    close() {
        // dispatch(modalActions.modalClose());
        this.setState({ show: false });
    }

    open() {
        // dispatch(modalActions.modalOpen());
        this.setState({ show: true });
    }

    renderLayer() {
        if (!this.state.show) {
            return null;
        }
        return (
            <Modal className={this.props.className} onRequestClose={this.close.bind(this)}>
                <div>{this.props.children}</div>
            </Modal>
        );
    }

    render() {
        return (
            <a role="button" className={this.props.linkClassName || ''}
               onClick={this.handleClick.bind(this)}>{this.props.name}</a>
        );
    }
}

export class Modal extends Component {
    touches = [];

    componentWillMount() {
        this.touches = {
            'startingY': 0,
            'startingX': 0
        }
    }

    handleTouchStart(e) {
        this.touches = {
            'startingY': e.originalEvent.touches[0].pageY,
            'startingX': e.originalEvent.touches[0].pageX
        };
    }

    handleToucnMove(e) {
        var $this = $(this);

        var deltaY = e.originalEvent.touches[0].pageY - this.touches.startingY;
        var deltaX = e.originalEvent.touches[0].pageX - this.touches.startingX;

        $this.scrollTop($this.scrollTop() - deltaY);
        $this.scrollLeft($this.scrollLeft() - deltaX);

        this.touches.startingY = e.originalEvent.touches[0].pageY;
        this.touches.startingX = e.originalEvent.touches[0].pageX;

        return false;
    }

    killClick(e) {
        // clicks on the content shouldn't close the modal
        e.stopPropagation();
    }

    handleClose(e) {
        e.preventDefault();
        if (this.props.onRequestClose) {
            this.props.onRequestClose();
        }
    }

    render() {
        return (
            <div className={"modal-container " + (this.props.className || '')}
                 onClick={this.handleClose.bind(this)}
                 onTouchStart={this.handleTouchStart.bind(this)}
                 onTouchMove={this.handleToucnMove.bind(this)}>
                <div className={"modal-content " + (this.props.className || '')} onClick={this.killClick.bind(this)}>
                    <a role="button" className="modal-close" onClick={this.handleClose.bind(this)}></a>
                    {this.props.children}
                </div>
            </div>
        );
    }
}