import React, { Component, PropTypes } from 'react';
import * as bannerActions from 'actions/banner';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fixBannerMediaUrl } from 'lib/utils';
import { find } from 'lodash';

class Banner extends Component {
    static propTypes = {
        url: PropTypes.string.isRequired
    };

    componentWillMount() {
        const { bannerActions } = this.props;
        bannerActions.fetchListIfNeeded();
    }

    render() {
        const { url, loading, objects } = this.props;

        if (loading) {
            return null;
        }

        let banner = find(objects, { url: window.location.pathname });

        if (!banner) {
            return null;
        }

        return (
            <div className="banner-container">
                <a target="_blank" className="banner" href={banner.target_url}
                   title={banner.name}>
                    <img src={fixBannerMediaUrl(banner.image)} alt={banner.name}/>
                </a>
            </div>
        );
    }
}

export default connect(state => state.banner, dispatch => {
    return {
        bannerActions: bindActionCreators(bannerActions, dispatch)
    };
})(Banner);
