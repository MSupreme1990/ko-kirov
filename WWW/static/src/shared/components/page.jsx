import React, { Component } from 'react';
import { Link } from 'react-easy-router';
import { fixRelativeMediaUrls } from 'lib/utils';
import CatalogLink from 'shared/components/catalog_link';

export default class Page extends Component {
    static defaultProps = {
        page: undefined,
        catalog_link: true,
        h1: true
    };

    render() {
        const { page } = this.props;

        let img = typeof page.file.resize != "undefined" ? (
            <div className="page-file">
                <a href={page.file.original} rel="lightbox">
                    <img src={page.file.resize} />
                </a>
            </div>
        ) : null;

        let childrenNodes = page.children.map(function(child) {
            let childImg = typeof child.file.resize != "undefined" ? (
                <div className="page-file">
                    <a href={child.file.original} rel="lightbox">
                        <img src={child.file.resize} />
                    </a>
                </div>
            ) : null;

            return (
                <div className="page-content">
                    {childImg}
                    <h2><Link to="PagesView" params={{url: child.url}}>{child.name}</Link></h2>
                    <p className="page-date">{child.created_at}</p>
                    <div dangerouslySetInnerHTML={{__html: fixRelativeMediaUrls(child.content_short)}}></div>
                    <p className="page-read-more"><Link to="PagesView" params={{url: child.url}}>Подробнее...</Link></p>
                </div>
            );
        });

        let h1 = this.props.h1 ? <h1>{page.name}</h1> : null;
        let catalog_link = this.props.catalog_link ? <CatalogLink /> : null;
 
        let content = fixRelativeMediaUrls(page.content);

        return (
            <div className="page-container">
                <div className="row">
                    <div className="columns medium-12">
                        <div className="page-content">
                            {h1}
                            {img}
                            <div dangerouslySetInnerHTML={{__html: content}} className={page.file ? 'file' : ''}></div>
                        </div>

                        <div className="page-childrens">
                            {childrenNodes}
                        </div>

                        {catalog_link}
                    </div>
                </div>
            </div>
        );
    }
}