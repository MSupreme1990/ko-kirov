import $ from 'jquery';
import React, { Component } from 'react';
import { strtr } from 'lib/utils';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as geoActions from 'actions/geo';
import * as seoActions from 'actions/seo';

class SeoText extends Component {
    state = {
        url: '',
        collapsed: true
    };

    handleClick(e) {
        e.preventDefault();

        let $link = $(e.target),
            $parent = $link.closest('.seo-item'),
            $gradient = $parent.find('.seo-gradient'),
            $content = $parent.find('.seo-content');
        $gradient.remove();
        $link.remove();
        $content.css({
            'overflow': '',
            'height': 'auto'
        });
    }

    componentWillMount() {
        this.setState({
            url: window.location.pathname
        }, () => {
            const { seoActions } = this.props;
            seoActions.fetchViewIfNeeded(this.state.url.substring(1));
        });
    }

    componentWillReceiveProps() {
        if (this.state.url != window.location.pathname) {
            this.setState({
                url: window.location.pathname
            }, () => {
                const { seoActions } = this.props;
                seoActions.fetchViewIfNeeded(this.state.url.substring(1));
            });
        }
    }

    render() {
        const { geo, loading } = this.props;
        const { collapsed } = this.state;

        let temp = this.props.objects,
            url = this.state.url.substring(1);

        let objects = temp[url] || [];

        if (objects.length == 0) {
            return null;
        }

        let geoName = geo.city.name,
            geoName2 = geo.city.name2,
            strtrParams = { '{{ geo.name }}': geoName, '{{ geo.name2 }}': geoName2 };

        if (objects.length > 0) {
            let seoNodes = objects.map((model) => {
                // <h1>{strtr(model.name, strtrParams)}</h1>
                if (!collapsed) {
                    return (
                        <div key={'seo-coll-' + model.id} className="seo-item">
                            <div className="seo-content"
                                 dangerouslySetInnerHTML={{__html: strtr(model.text, strtrParams)}}
                                 style={{height: 'auto'}}
                            ></div>
                        </div>
                    );
                }

                // <h1>{strtr(model.name, strtrParams)}</h1>
                return (
                    <div key={'seo' + model.id} className="seo-item">
                        <div className="seo-content"
                             dangerouslySetInnerHTML={{__html: strtr(model.text, strtrParams)}}></div>
                        <div className="seo-gradient"></div>
                        <p className="seo-readmore">
                            <a href="#" onClick={this.handleClick.bind(this)}>Читать далее &rarr;</a>
                        </p>
                    </div>
                );
            });

            return (
                <div className="seo-container">
                    <div className="row">
                        <div className="columns large-12">
                            {seoNodes}
                        </div>
                    </div>
                </div>
            );

        }

        return (
            <div></div>
        );
    }
}

export default connect((state, props) => {
    return {
        ...state.seo,
        geo: state.geo
    };
}, dispatch => {
    return {
        seoActions: bindActionCreators(seoActions, dispatch),
        geoActions: bindActionCreators(geoActions, dispatch)
    };
})(SeoText);