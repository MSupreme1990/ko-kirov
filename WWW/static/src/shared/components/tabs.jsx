import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

export default class Tabs extends Component {
    static propTypes = {
        tabs: PropTypes.object.isRequired,
        onClick: PropTypes.func
    };

    static defaultProps = {
        tabs: {},
        onClick: () => {
        }
    };

    state = {
        index: 1
    };

    handleTabClick(index, e) {
        e.preventDefault();
        this.setState({index});
        this.props.onClick();
    }

    renderMenu() {
        let nodes = Object.keys(this.props.tabs).map((name, i) => {
            return (
                <li key={i + 1}>
                    <a
                        className={classNames('item', {'active': this.state.index == i + 1})}
                        onClick={this.handleTabClick.bind(this, i + 1)}>
                        {name}
                    </a>
                </li>
            );
        });

        return <ul className="tab-menu-list">{nodes}</ul>;
    }

    renderContent() {
        const { tabs } = this.props;

        return Object.keys(tabs).map((name, i) => {
            return (
                <div key={name}
                     className={classNames('tab-content', {'active': this.state.index == i + 1})}>
                    {tabs[name]}
                </div>
            );
        });
    }

    render() {
        return (
            <div class="tab-container">
                {this.renderMenu()}
                {this.renderContent()}
            </div>
        );
    }
}