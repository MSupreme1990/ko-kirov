import $ from 'jquery';
import api from 'lib/api';
import React, { Component, PropTypes } from 'react';
import PhoneInput from 'inputs/phone_input';
import { Link } from 'react-easy-router';
import { sendToAmo, dataLayer_push, formValidate } from 'lib/utils';
import { cookie } from 'easy-storage';
import { findDOMNode } from 'react-dom';
import Timer from 'shared/components/timer';

export default class GiftCounter extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    state = {
        pk: null,
        message: ""
    };

    handleClose(e) {
        e.preventDefault();
        this.setState({ pk: null });
    }

    handleExtraSubmit(e) {
        e.preventDefault();

        let form = findDOMNode(this.refs.extra);
        var $form = $(form);

        api.post('/api/ko/gift_extra', $form.serialize(), function (data) {
            sendToAmo($form);
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    message: data['message']
                });

                setTimeout(function () {
                    this.setState({ pk: null });
                }.bind(this), 2500);
            }

            if (data['code']) {
                this.context.router.to("Coupon", { 'code': data['code'] }, {});
            }
        }.bind(this));
    }

    handleEmailChange(e) {
        cookie.set('email', e.target.value);
    }

    renderExtra() {
        if (this.state.pk == null) {
            return <span />;
        }
        var email = cookie.get('email');
        return (
            <div className={this.state.message == '' ? 'extra-form-popup' : 'extra-form-popup complete'}>
                <h1 className={this.state.message == '' ? 'hide' : ''}>{this.state.message}</h1>
                <a href="#" className="extra-form-close" onClick={this.handleClose.bind(this)}>&times;</a>
                <form ref="extra" className={this.state.message == '' ? 'row' : 'row hide'}
                      onSubmit={this.handleExtraSubmit.bind(this)}>
                    <div className="row">
                        <div className="columns large-12">
                            <p className="extra-form-title">Заявка на подарок успешно отправлена. Для того чтобы
                                распечатать купон на скидку, введите следующие данные:</p>
                        </div>
                        <div className="columns large-6">
                            <div>
                                <input type="text" onChange={this.handleEmailChange.bind(this)} name="GiftForm[email]"
                                       defaultValue={email} placeholder="Введите ваш e-mail"/>
                                <ul className="errors"></ul>
                            </div>
                            <div>
                                <input type="hidden" name="GiftForm[id]" value={this.state.pk}/>
                                <input type="submit" value="ОТПРАВИТЬ" className="button"/>
                            </div>
                        </div>
                        <div className="columns large-6">
                            <textarea name="GiftForm[question]" placeholder="Введите ваш вопрос"></textarea>
                            <ul className="errors"></ul>
                        </div>
                    </div>
                </form>
            </div>
        );
    }

    handleSubmit(e) {
        e.preventDefault();

        let form = findDOMNode(this.refs.form);
        var $form = $(form);

        api.post('/api/ko/gift', $form.serialize(), function (data) {
            formValidate($form, data['errors']);
            if (data['success']) {
                // google tag manager
                dataLayer_push({
                    'event': 'submit',
                    'action': 'gift'
                });

                this.setState({
                    pk: data['pk']
                });
            }
        }.bind(this));
    }

    handleNameChange(e) {
        cookie.set('name', e.target.value);
    }

    render() {
        var modal = this.renderExtra();
        var name = cookie.get('name');
        return (
            <div className="gift-counter-container">
                <div className="gift-counter-img">
                    <div className="row">
                        {modal}
                        <div className="columns medium-6 left-side">
                            <div className="gift-counter">
                                <p className="gift-title">До завершения осталось:</p>
                                <div className="gift-block">
                                    <Timer />
                                </div>
                            </div>
                            <h3>Получите фирменный <br/> чехол - сумку в подарок!</h3>
                        </div>
                        <div className="columns medium-6">
                            <form ref="form" onSubmit={this.handleSubmit.bind(this)}>
                                <div className="row">
                                    <div className="columns medium-4">
                                        <div className="form-row">
                                            <input type="text" onChange={this.handleNameChange.bind(this)}
                                                   name="GiftForm[name]"
                                                   defaultValue={name} placeholder="Введите ваше имя"/>
                                            <ul className="errors"></ul>
                                        </div>
                                    </div>
                                    <div className="columns medium-4">
                                        <div className="form-row">
                                            <PhoneInput name="GiftForm[phone]"/>
                                            <ul className="errors"></ul>
                                        </div>
                                    </div>
                                    <div className="columns medium-4">
                                        <input type="submit" value="ПОЛУЧИТЬ ПОДАРОК" className="button"/>
                                    </div>
                                    <div className="columns medium-12">
                                        <div className="b-personal-data">
                                            <span className="b-personal-data__check">
                                                <input
                                                    className="b-personal-data__input" type="checkbox"
                                                    checked="checked"/>
                                            </span>
                                            Нажимая кнопку «Получить консультацию», вы принимаете
                                            <a className="b-personal-data__link"
                                               href="/page/private"
                                               target="_blank">"условия обработки персональных данных".</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
