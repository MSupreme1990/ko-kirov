import React, { Component } from 'react';
import { Link } from 'react-easy-router';
import { ModalLink } from 'shared/components/modal';
import OrderStatus from 'shared/components/order_status';
import Geo from 'shared/components/geo';
import RequestCall from 'shared/components/request_call';
import SubscribeForm from 'shared/components/subscribe_form';
import Loader from 'shared/components/loader';
import Feedback from 'shared/components/feedback';
import { openModal } from 'lib/utils';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as geoActions from 'actions/geo';
import * as menuActions from 'actions/menu';
import * as categoryActions from 'actions/category';
import CurrencyList from 'shared/components/currency_list';

class Footer extends Component {
    state = {
        show_all: false
    };

    componentWillMount() {
        const { categoryActions, geoActions, menuActions } = this.props;
        menuActions.fetchListIfNeeded();
        geoActions.fetchListIfNeeded();
        categoryActions.fetchListIfNeeded();
    }

    renderLi(model) {
        let link = model.react_route.data instanceof Array ? (
            <Link to={model.react_route.route}>{model.name}</Link>
        ) : (
            <Link to={model.react_route.route} params={model.react_route.data}>{model.name}</Link>
        );

        return <li key={'ma' + model.id}>{link}</li>;
    }

    renderMenu(slug, name) {
        const { loading, menu } = this.props.menu;

        if (loading) {
            return <div className="footer-menu-item columns medium-6"><Loader /></div>;
        }

        let objects = slug in menu ? menu[slug] : [],
            nodes = objects.map(model => this.renderLi(model));

        let extra = null;
        if (slug == 'footer-service-menu') {
            extra = (
                <li>
                    <a href="#" onClick={openModal.bind(this, <OrderStatus />, {width: 540})}>Узнать статус заказа</a>
                </li>
            );
        }

        return (
            <ul className="footer-menu">
                <li className="title">{name}</li>
                {extra}
                {nodes}
            </ul>
        );
    }

    handleShowAll(e) {
        e.preventDefault();
        this.setState({ show_all: true });
    }

    renderCategoryMenu(key, name) {
        const { objects } = this.props.category;

        let categories = [];
        for (let i = 0; i < objects.length; i++) {
            if (objects[i].url == key) {
                categories = objects[i].items;
            }
        }

        const { show_all } = this.state;

        let nodes = (key == 'woman' && show_all ? categories : categories.slice(0, 6)).map((category, i) => {
            return (
                <li key={'mc' + category.id}>
                    <Link to="CatalogList" params={{url: category.url}}>{category.name}</Link>
                </li>
            );
        });

        if (key == 'woman' && !show_all) {
            nodes.push([
                <li>
                    <a className="footer-last" href="#" onClick={this.handleShowAll.bind(this)}>Смотреть все категории</a>
                </li>
            ]);
        }

        return (
            <ul className="footer-menu">
                <li className="title">{name}</li>
                {nodes}
            </ul>
        );
    }

    renderSocial() {
        return (
            <ul className="social-list">
                <li className="first">Следуйте за нами:</li>
                <li>
                    <a href="http://vk.com/kokirov" target="_blank" className="vk">Вконтакте</a>
                </li>
                <li>
                    <a href="http://instagram.com/kokirov.ru" target="_blank" className="instagram">Instagram</a>
                </li>
                <li>
                    <a href="http://ok.ru/kokirov" target="_blank" className="ok">Одноклассники</a>
                </li>
                <li>
                    <a href="http://facebook.com/groups/922330274477726" target="_blank" className="fb">Facebook</a>
                </li>
                <li>
                    <a href="http://twitter.com/Ko_kirovRu" target="_blank" className="twitter">Twitter</a>
                </li>
                <li>
                    <a href="https://plus.google.com/+%D0%9A%D0%BE%D0%B6%D0%B0%D0%BD%D0%B0%D1%8F%D0%BE%D0%B4%D0%B5%D0%B6%D0%B4%D0%B043"
                       target="_blank" className="google">Google+</a>
                </li>
                <li className="visible-for-large-up">
                    <a href="http://my.mail.ru/community/kokirov/" target="_blank" className="mir">Мой мир</a>
                </li>
                <li className="last visible-for-large-up">
                    <a href="http://kokirovru.livejournal.com/" target="_blank" className="lj">livejournal</a>
                </li>
            </ul>
        );
    }

    render() {
        const { geo } = this.props;

        if (geo.loading || !geo.received) {
            return null;
        }

        const year = new Date().getFullYear();

        let email = geo.site.email,
            emailLink = "mailto:" + email;

        return (
            <div id="footer">
                <div className="footer-container">
                    <div className="footer-menu-container">
                        <div className="footer-form row">
                            <div className="columns medium-7 large-6 left-side">
                                <h4>Не пропустите новинки и акции!</h4>

                                <p>Подпишитесь на рассылку и получите подарок при первом заказе!</p>
                            </div>
                            <div className="columns medium-5 large-6">
                                <SubscribeForm />
                            </div>
                        </div>
                        <div className="row">
                            <div className="footer-menu-item columns large-3 visible-for-large-up">
                                {this.renderCategoryMenu('woman', 'Женщинам')}
                            </div>
                            <div className="footer-menu-item columns large-3 visible-for-large-up">
                                {this.renderCategoryMenu('man', 'Мужчинам')}
                            </div>
                            <div className="footer-menu-item columns medium-5 large-2">
                                {this.renderMenu('footer-service-menu', 'Сервис и помощь')}
                            </div>
                            <div className="footer-menu-item columns medium-4 large-2">
                                {this.renderMenu('footer-about-menu', 'О компании')}
                            </div>
                            <div className="footer-menu-item columns medium-3 large-2">
                                <p className="title">
                                    Тел.: 8 800 500 14 22<br/>
                                    <span className="title-extra">Звоните по России бесплатно</span>
                                </p>

                                <div className="city-info">
                                    <div>
                                        <p>Телефоны в г. <a href="#"
                                                            onClick={openModal.bind(this, <Geo />)}>{geo.city.name}</a>:
                                        </p>
                                        <div className="city-phones">
                                            <div dangerouslySetInnerHTML={{__html: geo.site.phones}}></div>
                                        </div>
                                    </div>
                                    <p>
                                        E-mail: <a href={emailLink}>{email}</a>
                                    </p>

                                    <p>
                                        <a onClick={openModal.bind(this, <Feedback />, {width: 340})} href="#">Написать
                                            директору</a>
                                    </p>
                                </div>
                                <CurrencyList />
                                <a href="#" className="request-call"
                                   onClick={openModal.bind(this, <RequestCall />, {width: 340})}>
                                    Заказать звонок
                                </a>
                            </div>
                        </div>
                        <div className="row">
                            <div className="columns medium-12">
                                {this.renderSocial()}
                            </div>
                        </div>
                    </div>
                    <div className="row owner-container">
                        <div className="columns medium-3">
                            <p className="owner">
                                &copy; 2003-{year} Сеть магазинов «Мехарди»
                            </p>
                        </div>
                        <div className="columns medium-3">
                            <p className="owner">
                                <a href="/page/private">Политика защиты персональных данных</a>
                            </p>
                        </div>
                        <div className="columns medium-3">
                            <p className="author">
                                Разработка сайта: <a href="http://studio107.ru" target="_blank">Студия 107</a> © {year}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(state => {
    return {
        geo: state.geo,
        menu: state.menu,
        category: state.category
    }
}, dispatch => {
    return {
        geoActions: bindActionCreators(geoActions, dispatch),
        menuActions: bindActionCreators(menuActions, dispatch),
        categoryActions: bindActionCreators(categoryActions, dispatch)
    };
})(Footer);
