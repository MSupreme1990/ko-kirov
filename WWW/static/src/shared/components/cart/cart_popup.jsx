import React, { Component } from 'react';
import { Link } from 'react-easy-router';
import Loader from 'shared/components/loader.jsx';
import currency from 'shared/components/currency';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as cartActions from 'actions/cart';
import { openModal } from 'lib/utils';
import { GetCouponForm } from 'shared/components/forms';

class CartPopup extends Component {
    static defaultProps = {};

    state = {
        popup: false
    };

    componentWillMount() {
        const { cartActions } = this.props;
        cartActions.fetchListIfNeeded();
    }

    handleRemove(index, e) {
        e.preventDefault();
        const { cartActions } = this.props;
        cartActions.removeByIndex(index);
    }

    renderItems() {
        const { symbol, objects } = this.props;

        return objects.map((item, i) => {
            let model = item.object;

            if (i > 4) {
                if (objects.length > 5) {
                    return (
                        <li className="cart-item-li-last" key='cart-last'>
                            Еще {objects.length - 5} товаров в корзине.
                            <Link to="Cart">Перейти в корзину</Link>
                        </li>
                    );
                }

                return;
            }

            let price_old = currency.hasOldPrice(symbol, model) ? (
                <span className="cart-item-price-old">
                {currency.format(symbol, model, true)}
                </span>
            ) : null;

            let price = currency.format(symbol, model);

            return (
                <li key={'cart' + model.id}>
                    <a className="cart-item-remove" href="#" data-id={model.id}
                       onClick={this.handleRemove.bind(this, i)}>
                    </a>

                    <div className="cart-item-image">
                        <img
                            src={model.image.image.thumb_slide}
                            alt={model.name}/>
                    </div>
                    <p className="cart-item-name">
                        <Link to="CatalogProduct" params={{url: model.url}}>{model.name}</Link>
                        <span className="cart-item-article">№ {model.code}</span>
                    </p>

                    <p className="cart-item-price">
                        <span className="cart-item-price-new">{price}</span><br/>
                        {price_old}
                    </p>
                </li>
            );
        });
    }

    handleCartPopup(e) {
        e.preventDefault();

        if (!this.state.popup) {
            const { cartActions } = this.props;
            cartActions.fetchList();
        }

        this.setState({
            popup: !this.state.popup
        });
    }

    renderPopup() {
        if (!this.state.popup) {
            return null;
        }

        const { objects } = this.props;

        return (
            <div
                className="cart-popup">
                <div className="cart-popup-header">
                    <div className="cart-popup-count">{objects.length}</div>
                    Корзина
                </div>

                <div className="cart-popup-banner">
                    <a href="#" onClick={openModal.bind(this, <GetCouponForm />, {width: 320})}>
                        Получите фирменный чехол - сумку в подарок!
                    </a>
                </div>

                {this.renderCartContent()}
            </div>
        );
    }

    renderCartContent() {
        const { objects, loading } = this.props;

        let items = loading ? (
            <div className="loader__padding">
                <Loader />
            </div>
        ) : <ul className="cart-popup-list">{this.renderItems()}</ul>;

        return objects.length > 0 ? (
            <div>
                {items}
                <div className="cart-popup-order-container">
                    <Link className="cart-popup-order" to="Cart">Перейти к оформлению заказа</Link>
                </div>
            </div>
        ) : (
            <div className="cart-empty">Товары в корзине отсутствуют</div>
        );
    }

    handleMouseLeave() {
        this.setState({
            popup: false
        });
    }

    render() {
        const { objects } = this.props;
        return (
            <div>
                <div className="cart-popup-container">
                    <Link className="cart-link" to="Cart">
                        <span className="cart-count">{objects.length}</span>
                        Вам подарок!
                    </Link>
                </div>
                {this.renderPopup()}
            </div>
        );
    }
}

export default connect(state => {
    return {
        ...state.cart,
        symbol: state.currency.symbol
    };
}, dispatch => {
    return {
        cartActions: bindActionCreators(cartActions, dispatch)
    };
})(CartPopup);
