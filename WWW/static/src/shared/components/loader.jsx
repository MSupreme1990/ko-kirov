import React from 'react';

export default props => {
    return (
        <div className="loader">
            <div className="ui segment">
                <div className="ui active loader"></div>
                <p></p>
            </div>
        </div>
    );
}
