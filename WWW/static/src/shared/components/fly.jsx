import $ from 'jquery';
import React, { Component } from 'react';
import { Link } from 'react-easy-router';
import { findDOMNode } from 'react-dom';

class Fly extends Component {
    unlisten = undefined;

    state = {
        position: 0
    };

    componentWillMount() {
        let callback = this.fly.bind(this);
        $(window).on('scroll', callback);
        this.unlisten = () => {
            $(window).off('scroll', callback);
        }
    }

    fly() {
        let { position } = this.state;

        var $node = $(findDOMNode(this.refs.fly));
        var leftOffset = 3,
            rightOffset = 6,
            offset = $node.offset().top;

        position = parseInt(window.pageYOffset / offset * 100);
        if (position >= 100) {
            position = 100;
        }
        position = position / 100 * (100 - (leftOffset + rightOffset)) + leftOffset;
        this.setState({
            position
        });
    }

    componentWillUnmount() {
        this.unlisten();
    }

    render() {
        const { position } = this.state;
        let style = {
            marginLeft: position + '%'
        };
        return (
            <div>
                <div className="fly-container">
                    <div ref="fly" className="fly" style={style}></div>
                    <Link to="PagesView" params={{ url: "delivery" }}>Бесплатная доставка на дом по России с примеркой
                        перед покупкой!</Link>
                </div>
            </div>
        );
    }
}

export default Fly;