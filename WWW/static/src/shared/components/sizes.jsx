import React, { PropTypes, Component } from 'react';
import { manSizes, womanSizes } from 'const/size.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as sizeActions from 'actions/size';

let getSizes = (obj = {}, sizes = []) => {
    let i, newObject = {};
    for (i in obj) {
        newObject[i] = sizes.indexOf(i) !== -1;
    }
    return newObject;
};

class Sizes extends Component {
    static propTypes = {
        actions: PropTypes.object.isRequired,
        product: PropTypes.object.isRequired
    };

    selectSize(size, e) {
        e.preventDefault();

        const { actions } = this.props;
        actions.setSize(size);
    }

    render() {
        const { product, currentSize, className } = this.props;

        let sizes = {},
            min = Number(product.min_size),
            max = Number(product.max_size);

        if (min && max && (min < max)) {
            let i = min;
            while (i <= max) {
                sizes[i] = false;
                i = i + 2;
            }
        } else {
            sizes = product.woman ? womanSizes : manSizes;
        }

        sizes = getSizes(sizes, product.sizes || []);

        let sizeNodes = Object.keys(sizes).map(size => {
            let cls = sizes[size] ? "available" : "not-available";
            if (currentSize == size && sizes[size]) {
                cls += " active";
            }

            let title = null,
                rel = null;
            if (!sizes[size]) {
                title = "Под заказ";
                rel = "tooltip";

                return (
                    <li key={size} className={cls} rel={rel} title={title} data-tooltipcls="small">
                        <span>{size}</span>
                    </li>
                );
            }

            return (
                <li key={size} className={cls} rel={rel} title={title} data-tooltipcls="small">
                    <a href="#" onClick={this.selectSize.bind(this, size)}>{size}</a>
                </li>
            );
        });

        return <ul className={className}>{sizeNodes}</ul>;
    }
}

export default connect(state => state.sizes, dispatch => {
    return {
        actions: bindActionCreators(sizeActions, dispatch)
}
})(Sizes);