import React, { Component } from 'react';
import api from 'lib/api';
import PhoneInput from 'inputs/phone_input';
import { dataLayer_push } from 'lib/utils';
import { cookie } from 'easy-storage';
export default class OrderQuick extends Component {
    state = {
        phone_cls: null,
        complete: false,
        form: {}
    };

    handleSubmitQuick = e => {
        e.preventDefault();

        let params = {};
        for (let key in this.state.form) {
            params[`OrderQuickForm[${key}]`] = this.state.form[key];
        }
        api.post('/api/ko/cart-order-quick', params, data => {
            if (data.status) {
                dataLayer_push({
                    event: 'CartOrder',
                    type: 'oneClick'
                });

                this.setState({ complete: true });
            }
        });
    };

    handleInputChange = name => e => {
        this.setState({
            form: {
                ...this.state.form,
                [name]: e.target.value
            }
        }, () => {
            if (this.state.form.phone && this.state.form.phone.length < 6) {
                this.setState({ phone_cls: 'error' });
            }
        });
        cookie.set(name, e.target.value);
    };

    render() {
        const { phone_cls, complete } = this.state;

        return complete ? (
                <div>
                    <h1 className="text-center">
                        Заявка на звонок успешно отправлена. В ближайшее время с вами свяжется наш менеджер.
                    </h1>
                </div>
            ) : (
                <form
                    ref="quick_form"
                    className="quick-cart-order"
                    onSubmit={this.handleSubmitQuick}>
                    <h1>Заказать в 1 клик</h1>

                    <p>Заполните эту форму и наш менеджер свяжется с Вами для подтверждения заказа</p>

                    <div className="form-row">
                        <input type="text"
                               name="OrderQuickForm[name]"
                               onChange={this.handleInputChange('name')}
                               placeholder="Введите Ваше имя"
                               value={this.state.form.name || ''}/>
                        <ul className="errors"></ul>
                    </div>
                    <div className="form-row">
                        <PhoneInput type="text"
                                    name="OrderQuickForm[phone]"
                                    className={phone_cls}
                                    placeholder="Введите Ваш телефон"
                                    onChange={this.handleInputChange('phone')}
                                    value={this.state.form.phone || ''}/>
                        <ul className="errors">
                            {phone_cls && <li>Укажите ваш телефон</li>}
                        </ul>
                    </div>
                    <div className="form-row">
                        <input type="submit" value="ЗАКАЗАТЬ В 1 КЛИК" className="button"/>
                    </div>
                    <div className="b-form-row">
                        <div className="b-personal-data">
                            <span className="b-personal-data__check"><input type="checkbox" checked="checked"/></span>
                            Передавая информацию сайту вы принимаете условия
                            <a className="b-personal-data__link"
                               href="/page/private"
                               target="_blank">"политики защиты персональных данных".</a>
                        </div>
                    </div>
                </form>
            );
    }
}
