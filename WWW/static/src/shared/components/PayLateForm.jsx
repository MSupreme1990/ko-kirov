import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';
import xhr from 'lib/xhr';
import PhoneInput from 'inputs/phone_input';

const encode = str => encodeURIComponent(str).replace(/[!'()*]/g, function (c) {
    return '%' + c.charCodeAt(0).toString(16);
});

export default class PayLateForm extends Component {
    static propTypes = {
        products: PropTypes.array.isRequired,
    };

    static defaultProps = {
        clientId: '73624042017',
        category: 'Изделия из меха и кожи',
    };

    state = {
        id: undefined,
        form: {},
        errors: {},
        error: undefined
    };

    getPaylateGoods() {
        const products = this.props.products.map(item => ({
            Name: `${item.object.name} №${item.object.code}`,
            Category: "Изделия из меха и кожи",
            Price: item.price,
            Count: item.quantity,
        }));

        return encode(JSON.stringify(products));
    }

    renderForm() {
        const { clientId, category } = this.props;

        return (
            <form ref="paylate" action="https://paylate.ru/bypartner" method="post">
                <input type="hidden" name="client_id" value={clientId}/>
                <input type="hidden" name="order_id" value={this.state.id}/>
                <input type="hidden" name="category" value={category}/>
                <input type="hidden" name="goods" value={this.getPaylateGoods()}/>
                <input type="hidden" name="token" value={this.state.token}/>
                <input type="hidden" name="action" value="by_partner"/>
                <input type="submit" value="В рассрочку" style={{ display: 'none' }}/>
            </form>
        );
    }

    onSubmit = e => {
        e.preventDefault();

        let price = 0;
        this.props.products.map(item => {
            price += item.quantity * item.price;
        });

        let data = {
            ...this.state.form,
            price,
            products: JSON.stringify(this.props.products)
        };

        xhr
            .post('/api/ko/paylate/create', {}, data)
            .then(data => {
                if (data.status) {
                    this.setState({
                        id: data.id,
                        token: data.token,
                        errors: {}
                    }, () => {
                        findDOMNode(this.refs.paylate).submit();
                    });
                } else {
                    this.setState({
                        errors: data.errors || {}
                    });
                }
            })
            .catch(err => {
                this.setState({ error: true });
            });
    };

    linkState = key => e => {
        this.setState({
            form: {
                ...this.state.form,
                [key]: e.target.value
            }
        });
    };

    renderErrors(key) {
        return (this.state.errors[key] || []).map((err, i) => {
            return <div className="b-error" key={i}>{err}</div>
        });
    }

    render() {
        return (
            <div>
                <form action="" onSubmit={this.onSubmit}>
                    <div className="b-form__row">
                        <label htmlFor="name">Ваше имя</label>
                        <input type="text"
                               name="name"
                               required="required"
                               value={this.state.form.name}
                               onChange={this.linkState('name')}
                               id="name" placeholder="Ваше имя"/>
                        {this.renderErrors('name')}
                    </div>
                    <div className="b-form__row">
                        <label htmlFor="phone">Ваше номер телефона</label>
                        <PhoneInput name="phone"
                                    minLength="9"
                                    required="required"
                                    value={this.state.form.phone}
                                    onChange={this.linkState('phone')}/>
                        {this.renderErrors('phone')}
                    </div>
                    <div className="b-form__row">
                        <label htmlFor="email">Ваше электронная почта</label>
                        <input type="text"
                               name="email"
                               value={this.state.form.email}
                               onChange={this.linkState('email')}
                               id="email" placeholder="Ваша электронная почта"/>
                        {this.renderErrors('email')}
                    </div>
                    <div className="b-form__row">
                        <input type="submit" className="b-button b-button_primary"
                               value="Оформить заказ"/>
                    </div>
                    <div className="b-form__row">
                        <div className="b-personal-data">
                            <span className="b-personal-data__check"><input type="checkbox" checked="checked"/></span>
                            Передавая информацию сайту вы принимаете условия
                            <a className="b-personal-data__link"
                               href="/page/private"
                               target="_blank">"политики защиты персональных данных".</a>
                        </div>
                    </div>
                </form>
                {this.renderForm()}
            </div>
        );
    }
}
