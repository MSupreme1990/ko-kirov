import $ from 'jquery';
import React from 'react';
import PhoneInput from 'inputs/phone_input';
import { cookie } from 'easy-storage';
import api from 'lib/api';
import { sendToAmo, formValidate } from 'lib/utils';

var Feedback = React.createClass({
    getInitialState: function() {
        return {
            pk: null,
            message: ""
        };
    },
    handleSubmit: function(e) {
        e.preventDefault();

        var $form = $('#feedback-form');

        api.post('/api/ko/feedback', $form.serialize(), function(data) {
            sendToAmo($form);
            formValidate($form, data['errors']);
            if (data['success']) {
                this.setState({
                    message: data['message']
                });
            }
        }.bind(this));
    },
    handleEmailChange: function(e) {
        cookie.set('email', e.target.value);
    },
    handleNameChange: function(e) {
        cookie.set('name', e.target.value);
    },
    render: function() {
        var email = cookie.get('email');
        var name = cookie.get('name');
        return (
            <form id="feedback-form" className="row" onSubmit={this.handleSubmit}>
                <h1 className={this.state.message == '' ? 'hide' : ''}>{this.state.message}</h1>
                <div className={this.state.message == '' ? '' : 'hide'}>
                    <h3>Связаться с администрацией</h3>
                    <div className="form-row">
                        <input type="text" onChange={this.handleNameChange} name="FeedbackForm[name]" defaultValue={name} placeholder="Введите ваше имя" />
                        <ul className="errors"></ul>
                    </div>
                    <div className="form-row">
                        <input type="text" onChange={this.handleEmailChange} name="FeedbackForm[email]" defaultValue={email} placeholder="Введите ваш e-mail" />
                        <ul className="errors"></ul>
                    </div>
                    <div className="form-row">
                        <PhoneInput name="FeedbackForm[phone]" />
                        <ul className="errors"></ul>
                    </div>
                    <div className="form-row">
                        <textarea name="FeedbackForm[comment]" placeholder="Введите текст сообщения"></textarea>
                        <ul className="errors"></ul>
                    </div>
                    <div className="form-row">
                        <input type="submit" value="СВЯЗАТЬСЯ С АДМИНИСТРАЦИЕЙ" className="button" />
                    </div>
                    <div className="form-row">
                        <div className="b-personal-data">
                            <span className="b-personal-data__check"><input type="checkbox" checked="checked"/></span>
                            Передавая информацию сайту вы принимаете условия
                            <a className="b-personal-data__link"
                               href="/page/private"
                               target="_blank">"политики защиты персональных данных".</a>
                        </div>
                    </div>
                </div>
            </form>
        );
    }
});

export default Feedback;
