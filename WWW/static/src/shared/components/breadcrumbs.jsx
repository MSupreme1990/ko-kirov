import React, { Component, PropTypes } from 'react';
import { Link } from 'react-easy-router';

export default class Breadcrumbs extends Component {
    static propTypes = {
        breadcrumbs: PropTypes.array,
        model: PropTypes.object
    };

    static defaultProps = {
        model: {
            name: ""
        },
        breadcrumbs: []
    };

    state = {
        breadcrumbs: []
    };

    componentWillMount() {
        this.setState({
            breadcrumbs: this.props.breadcrumbs,
            model: this.props.model
        });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            breadcrumbs: nextProps.breadcrumbs,
            model: nextProps.model
        });
    }

    render() {
        const { breadcrumbs, model } = this.state;

        let nodes;
        if (breadcrumbs.length > 0) {
            nodes = breadcrumbs.map((bc, i) => {
                return bc.route ? (
                    <li key={i}>
                        <Link to={bc.route} params={bc.params}>{bc.name}</Link>
                    </li>
                ) : (
                    <li key={i}><span>{bc.name}</span></li>
                )
            });
        } else {
            nodes = <li><span>{model.name}</span></li>;
        }

        return (
            <div className="row">
                <div className="columns large-10">
                    <div className="breadcrumbs-container">
                        <ul className="breadcrumbs">
                            <li><Link to="Index">Главная</Link></li>
                            {nodes}
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}