'use strict';

let gulp = require('gulp'),
    path = require('path'),
    bs = require('browser-sync').create(),
    concat = require('gulp-concat'),
    watch = require('gulp-watch'),
    flatten = require('gulp-flatten'),
    autoprefixer = require('autoprefixer'),
    postcss = require('gulp-postcss'),
    flexbugs = require('postcss-flexbugs-fixes'),
    csso = require('postcss-csso'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    gulpif = require('gulp-if'),
    webp = require('gulp-webp'),
    imagemin = require('gulp-imagemin');

const isProd = process.env.NODE_ENV === 'production';

let settings = {
    csso: {
        comments: false,
        restructure: false
    },
    paths: {
        images: [
            'images/**/*'
        ],
        fonts: [
            'fonts/**/*{.eot,.woff,.woff2,.ttf,.svg}'
        ],
        css: [
            'fonts/**/*.css',
            'scss/**/*.scss',
            'vendor/jquery.mnotifyajax/css/jquery.mnotifyajax.css',
            'vendor/animate.css/animate.min.css',
            'components/mtooltip/mtooltip.css'
        ],
    },
    dst: {
        css: 'dist/css',
        images: 'dist/images',
        fonts: 'dist/fonts'
    },
    imagesOpts: {
        optimizationLevel: 5,
        progressive: true,
        interlaced: true
    },
    scssOpts: {
        errLogToConsole: true,
        outputStyle: 'expanded',
        indentWidth: 4,
        includePaths: [
            path.join(__dirname, '/vendor/mindy-sass'),
            path.join(__dirname, '/node_modules/flexy-framework'),
        ]
    }
};

gulp.task('webp', () => {
    return gulp.src(settings.paths.images)
        .pipe(changed(settings.dst.images))
        .pipe(gulpif(isProd, webp()))
        .pipe(gulp.dest(settings.dst.images))
        .pipe(bs.stream());
});

gulp.task('build', () => {
    return gulp.start('css', 'images', 'fonts');
});

gulp.task('css', () => {
    const plugins = [
        flexbugs,
        autoprefixer({
            browsers: [
                '>1%',
                'last 4 versions',
                'Firefox ESR',
                'not ie < 9', // React doesn't support IE8 anyway
            ],
            // cascade: false,
            flexbox: 'no-2009'
        }),
        csso(settings.csso)
    ];

    return gulp.src(settings.paths.css)
        .pipe(plumber())
        .pipe(sass(settings.scssOpts))
        .pipe(postcss(plugins))
        .pipe(gulp.dest(settings.dst.css))
        .pipe(bs.stream());
});

gulp.task('fonts', () => {
    return gulp.src(settings.paths.fonts)
        .pipe(flatten())
        .pipe(gulp.dest(settings.dst.fonts));
});

gulp.task('images', () => {
    gulp.src(settings.paths.images)
        .pipe(gulpif(isProd, imagemin([
            imagemin.gifsicle({ interlaced: true }),
            imagemin.jpegtran({ progressive: true }),
            imagemin.optipng({ optimizationLevel: 5 }),
            // imagemin.svgo({ plugins: [{ removeViewBox: false }] })
        ])))
        .pipe(gulp.dest(settings.dst.images));
});

gulp.task('watch', ['build'], () => {
    bs.init({
        proxy: "localhost:8000",
        open: false
    });

    watch(settings.paths.images, () => {
        gulp.start(['images']);
        bs.reload();
    });
    watch('./dist/**/*.js', () => {
        bs.reload();
    });

    watch(settings.paths.css, () => {
        gulp.start('css');
    });
});

gulp.task('default', ['build']);
