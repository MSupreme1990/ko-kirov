$(document).ajaxComplete(function (e, xhr, settings) {
    if (xhr.status == 278) {
        var location = xhr.getResponseHeader("Location");
        if (location) {
            window.location.href = xhr.getResponseHeader("Location");
        }
    }
});

$(document).on("contextmenu", 'a img, img', function (e) {
    e.preventDefault();
    return false;
});

$(document).on('click', 'a.mmodal', function (e) {
    e.preventDefault();
    var $this = $(this);
    $this.mmodal({
        width: $this.data('width')
    });
    return false;
});

// Handle search clear
$(document).on('keydown', function (e) {
    if (e.keyCode == 27) {
        $(document).trigger('esc_key');
    }
});

$(document).on('submit', '.mmodal-form', function (e) {
    e.preventDefault();
    var $this = $(this);
    $.ajax({
        type: 'post',
        url: $this.attr('action'),
        data: $this.serialize(),
        success: function (data) {
            if (data.success) {
                $.mnotify({
                    title: data.title
                });
                $this.find('input').val('');
            } else {
                var message = '';
                for (var fieldName in data.errors) {
                    message += "<br/>" + data.errors[fieldName].join("<br/>");
                }
                $.mnotify({
                    title: data.title,
                    message: message
                });
            }
        }
    });
    return false;
});


//$(document).on('click', '.errors', function (e) {
//    e.preventDefault();
//    $(this).css('display', 'none');
//    return false;
//});

(function () {
    var mobile = isMobile.any();
    var mobile_mode = $.cookie('MOBILE');

    if (mobile && mobile_mode == null) {
        $.cookie('MOBILE', true);
        window['mobile_mode'] = true;
    } else if (!mobile && mobile_mode == null) {
        $.cookie('MOBILE', false);
        window['mobile_mode'] = false;
    }

    if (window['mobile_mode'] == false) {
        (function ($) {
            FlipClock.Lang.Russian = {
                'years': 'лет',
                'months': 'месяцев',
                'days': 'дней',
                'hours': 'час.',
                'minutes': 'мин.',
                'seconds': 'сек.'
            };

            FlipClock.Lang['ru'] = FlipClock.Lang.Russian;
            FlipClock.Lang['ru-ru'] = FlipClock.Lang.Russian;
            FlipClock.Lang['russian'] = FlipClock.Lang.Russian;
        }(jQuery));


        $.mtooltip('[rel~=tooltip]');

        var position = 0;
        $(window).on('scroll', function () {
            var $node = $('.fly-container .fly');
            if ($node.length > 0) {
                var leftOffset = 3,
                    rightOffset = 6,
                    offset = $node.offset().top;

                position = parseInt(window.pageYOffset / offset * 100);
                if (position >= 100) {
                    position = 100;
                }
                position = position / 100 * (100 - (leftOffset + rightOffset)) + leftOffset;

                $node.css({
                    marginLeft: position + '%'
                });
            }
        });
    }
})();
