(function($) {
    'use strict';

    $.fn.extend({
        totop: function(options) {
            this.defaultOptions = {
                container: '#wrapper',
                offset: 60,
                margin: 0,
                width: 100,
                animation: true,
                animationSpeed: 300
            };

            var settings = $.extend({}, this.defaultOptions, options);

            var ToTop = function(link, options) {
                this.init(link, options);
            };

            ToTop.prototype = {
                $linkContainer: undefined,
                $contentContainer: undefined,
                init: function(link, options) {
                    var me = this;

                    this.options = $.extend({
                        offset: 100,
                        animation: true,
                        animationSpeed: 300,
                        container: undefined
                    }, options);

                    this.$contentContainer = $(this.options.container);
                    this.$linkContainer = $(link);

                    // если вам не нужно, чтобы кнопка подстраивалась под ширину экрана - удалите следующие четыре строчки в коде
                    this._sizeHandler();
                    $(window)
                        .on('resize', function(e) {
                            return this._resizeHandler(e);
                        }.bind(this))
                        .on('scroll', function(e) {
                            return this._scrollHandler(e);
                        }.bind(this));


                    this.$linkContainer.on('click', this._clickHandler);
                },
                _sizeHandler: function () {
                    this.$linkContainer.css({
                        width: this.options.width
                    });
                },
                _resizeHandler: function() {
                    var h = this.$contentContainer.offset().left;

                    if (h < this.options.offset) {
                        // если кнопка не умещается, скрываем её
                        this.$linkContainer.hide();
                    } else {
                        if ($(window).scrollTop() >= this.options.offset) {
                            this.$linkContainer.show();
                        }

                        this.$linkContainer.css({
                            width: h - this.options.margin
                        });
                    }
                },
                _scrollHandler: function(e) {
                    var options = this.options;

                    if ($(window).scrollTop() >= options.offset) {
                        this.$linkContainer.removeClass('hide').addClass('show');
                    } else {
                        this.$linkContainer.removeClass('show').addClass('hide');
                    }
                },
                _clickHandler: function(e) {
                    e.preventDefault();
                    var $top = (window.opera) ? ((document.compatMode == "CSS1Compat") ? $('html') : $('body')) : $('html,body');
                    $top.animate({
                        scrollTop: 0
                    }, 500);
                    return false;
                }
            };

            return this.each(function() {
                return new ToTop(this, settings);
            });
        }
    });
})(jQuery);
