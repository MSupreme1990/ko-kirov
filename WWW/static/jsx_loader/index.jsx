import settings from 'settings';

let version = process.env.version;

let start_loader = (data, callback) => {
    let loaded = 0;
    let cssLoader = (url) => {
        let link = document.createElement('link');
        let img = document.createElement('img');

        img.onerror = () => {
            link.type = 'text/css';
            link.rel = 'stylesheet';
            link.href = url;

            document.getElementsByTagName('head')[0].appendChild(link);

            loaded++;
        };
        img.src = url;
    };

    let jsLoader = (url) => {
        let script = document.createElement('script');
        script.src = url;
        document.body.appendChild(script);

        script.onload = () => {
            loaded++;
        };
    };

    let timer = setInterval(()=> {
        if (data.length == loaded) {
            clearInterval(timer);

            callback()
        }
    }, 500);

    data.map((item)=> {
        if (item[0] == 'css') {
            cssLoader(item[1]);
        }
        if (item[0] == 'js') {
            jsLoader(item[1]);
        }
    });
};


// if (window['mobile_mode'] == true) {
if (true) {
    start_loader([
            ['css', '/static/dist/' + version + '/css/mobile.css'],
            ['js', settings.BASE_URL + '/api/ko/v1-2/settings/mobile']
        ],
        () => {
            require.ensure(['../jsx_mobile/index.jsx'], function () {
                require('../jsx_mobile/index.jsx');
                document.getElementById('page-loader').remove();
            });
        }
    );
} else {
    start_loader([
            ['css', '/static/dist/' + version + '/css/screen.css'],
            ['js', settings.BASE_URL + '/api/ko/v1-2/settings/desktop']
        ],
        () => {
            require.ensure(['../jsx/index.jsx'], function () {
                require('../jsx/index.jsx');
                document.getElementById('page-loader').remove();
            });
        }
    );
}
