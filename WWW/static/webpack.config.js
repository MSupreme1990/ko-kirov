'use strict';

let webpack = require('webpack'),
    BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin,
    WatchMissingNodeModulesPlugin = require('react-dev-utils/WatchMissingNodeModulesPlugin'),
    CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin'),
    path = require('path');

const isProd = process.env.NODE_ENV === 'production';

const devServer = {
    contentBase: __dirname + '/dist',
    colors: true,
    quiet: false,
    noInfo: false,
    publicPath: '/static/dist/js/',
    historyApiFallback: true,
    host: '127.0.0.1',
    port: 8000,
    hot: true
};

const entryPoint = src => {
    const polyfils = require.resolve('./polyfills');

    // WebpackDevServer host and port
    // 'webpack-dev-server/client?http://' + devServer.host + ':' + devServer.port,
    // 'webpack/hot/only-dev-server',

    return isProd ? [
            polyfils,
            src
        ] : [
            polyfils,
            // 'webpack/hot/dev-server',
            // 'webpack-hot-middleware/client',
            src
        ];
};

module.exports = {
    target: "web",
    node: {
        fs: "empty"
    },
    devServer: devServer,
    devtool: process.env.NODE_ENV === 'production' ? 'source-map' : 'eval',
    entry: {
        mobile: entryPoint('./src/mobile/index'),
        desktop: entryPoint('./src/desktop/index')
    },
    output: {
        path: path.resolve(__dirname, 'dist/js'),
        filename: '[name].bundle.js',
        publicPath: devServer.publicPath
    },
    plugins: [
        /*
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            filename: 'vendor.bundle.js',
            minChunks: (module, count) => {
                return module.context && module.context.indexOf('node_modules') >= 0
            },
        }),
        */
        new webpack.DefinePlugin({
            'process.env': { NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development') }
        }),
        new WatchMissingNodeModulesPlugin,
        new CaseSensitivePathsPlugin,
        // new webpack.optimize.UglifyJsPlugin({
        //     compress: {
        //         screw_ie8: true, // React doesn't support IE8
        //         warnings: false
        //     },
        //     mangle: {
        //         screw_ie8: true
        //     },
        //     output: {
        //         comments: false,
        //         screw_ie8: true
        //     }
        // }),
        new webpack.NoEmitOnErrorsPlugin(),
        // new BundleAnalyzerPlugin({ analyzerMode: 'static' }),
    ].concat(isProd ? [] : []),
    resolve: {
        modules: [
            "node_modules",
            path.resolve(__dirname, "src")
        ],
        extensions: [".js", ".json", ".jsx"]
    },
    module: {
        rules: [
            {
                test: require.resolve('jquery'),
                use: [
                    { loader: 'expose-loader', options: 'jQuery' },
                    { loader: 'expose-loader', options: '$' }
                ]
            },
            {
                test: /\.json$/,
                use: 'json-loader'
            },
            {
                test: /\.(js|jsx)$/,
                include: path.join(__dirname, 'src'),
                use: 'babel-loader',
            }
        ]
    }
};
