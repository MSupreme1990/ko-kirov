<?php

/*
 * (c) Studio107 <mail@studio107.ru> http://studio107.ru
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

include __DIR__.'/../vendor/autoload.php';

$sentry = new Raven_Client('https://ba5e4b4320ef4c1088b2a5d4182dc8ea:359161efdba04cf19350728536ab1728@sentry.studio107.ru/6');
$sentry->install();

if (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/(?i)msie [1-8]/', $_SERVER['HTTP_USER_AGENT'])) {
    echo file_get_contents('ie.html');
    die();
}

defined('MINDY_PATH') or define('MINDY_PATH', dirname(__FILE__).DIRECTORY_SEPARATOR);

$config = MINDY_PATH.'../app/config/settings';

if (strpos(gethostname(), 'studio107') !== false) {
    $debug = true;
    $config .= '_stage';
    define('MAIN_DOMAIN', 'http://ko.dev.studio107.ru');
} else {
    $debug = is_file($config.'_local.php');
    if (is_file($config.'_local.php')) {
        $config .= '_local';
        define('MAIN_DOMAIN', 'http://localhost');
    } else {
        define('MAIN_DOMAIN', 'http://ko-kirov.ru');
    }
}
defined('MINDY_DEBUG') or define('MINDY_DEBUG', $debug);

if ($debug || $_SERVER['REMOTE_ADDR'] == '92.39.76.2') {
    ini_set('error_reporting', -1);
    (new \Whoops\Run())
        ->pushHandler(new \Whoops\Handler\PrettyPageHandler())
        ->register();
}

$app = \Mindy\Base\Mindy::getInstance($config.'.php');
$app->run();
