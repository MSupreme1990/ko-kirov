<?php
/** 
 * Обновляет статусы трек-номеров.
 */

//error_reporting( E_ALL );
//ini_set( 'display_errors', 1);

define( 'ROOT', dirname(__FILE__) );

include ROOT.'/class/db.class.php';
include ROOT.'/class/amocrm/amo.class.php';
include ROOT.'/class/carrier/carrier.dpd.class.php';
include ROOT.'/class/carrier/carrier.ems.class.php';


$db = new DB( 'localhost', 'u1249_dev', 'jZ4aA8u', 'u1249_dev' );

$activeTrackings = $db->getActiveTrackings();

/*
[id] => 3
[num] => 04071472GOJ
[carrier] => DPD
[status_id] => 0
[lead_id] => 31368901
[active] => 1
[added] => 2015-04-22 11:05:01
[last_checked] => 0000-00-00 00:00:00'*/

if ( !$activeTrackings || !count( $activeTrackings ) ) exit( 'No active trackings.' );

$amo = new amoCRM();

foreach( $activeTrackings as $tracking )
{
    switch( $tracking['carrier'] )
    {
        case 'DPD': $carrier = new CarrierDPD( $tracking['num'] ); break;
        case 'EMS': $carrier = new CarrierEMS( $tracking['num'] ); break;
    }

    if ( $carrier->checkStatus() )
    {
        $tr_status      = $carrier->getStatus();
        $tr_status_id   = $carrier->getStatusId();
        $tr_status_call = $carrier->getStatusCall();

        if ( $tracking['status_id'] == $tr_status_id )
        {
            // Статус отправления не изменился.
            
            // Дополнительня проверка отправлений EMS,
            // у которых статус не меняется несколько дней.
            if ( $tracking['carrier'] == 'EMS' )
            {
                $task_text = '';
                $days = floor( ( strtotime( $tracking['last_checked'] ) - time() ) / 86400 );
                
                if ( $tracking['status_id'] == 3 && $days > 10 )
                {
                    $task_text = 'Звонить на почту и клиенту. Посылка возможно прибыла в место вручения.';
                }
                elseif ( $tracking['status_id'] == 5 && ( $days > 3 || $days <= 31 ) )
                {
                    $task_text = 'Звонить клиенту. Чтобы шел забирать. Написать точное время когда заберет.';
                }
                elseif ( $tracking['status_id'] == 9 )
                {
                    $task_text = 'Звонить клиенту. Выяснить причину почему не забрал, возможно планировать повторную отправку.';
                }
                
                // Получаем информацию о сделке.
                $leadGet = $amo->leads_Get;
                $l = $leadGet->byId( $tracking['lead_id'] );
                
                // Добавляем задачу.
                $taskSet = $amo->tasks_Set;
                $taskSet->text( $task_text );
                $taskSet->elemType( 'lead' );
                $taskSet->elemId( $tracking['lead_id'] );
                $taskSet->type( 'Follow-up' );
                $taskSet->toTime( 2 );
                $taskSet->respId( $l->responsible_user_id );
                $taskSet->run();
            }
            
            // Обновляем дату проверки.
            $db->updateTrackingDateLastChecked( $tracking['num'] );
            
            continue;
        }
        else
        {
            // Статус отправления изменился.
            $db->setTrackingStatusId( $tracking['num'], $tr_status_id );
            
            // Получаем информацию о сделке.
            $leadGet = $amo->leads_Get;
            $l = $leadGet->byId( $tracking['lead_id'] );
            
            // Изменяем статус сделки.
            $leadUpdate = $amo->leads_Update;
            $leadUpdate->setLead( $l );
            $leadUpdate->id( $l->id );
            $leadUpdate->statusId( 7602832 );
            $leadUpdate->custom( 'Статус отправления', $tr_status );
            $leadUpdate->run();
            
            if ( !empty( $tr_status_call ) )
            {
                // Добавляем задачу.
                $taskSet = $amo->tasks_Set;
                $taskSet->text( $tr_status_call );
                $taskSet->elemType( 'lead' );
                $taskSet->elemId( $tracking['lead_id'] );
                $taskSet->type( 'Follow-up' );
                $taskSet->toTime( 2 );
                $taskSet->respId( $l->responsible_user_id );
                $taskSet->run();
            }
        }
        
        // Диактивация трек-номера.
        $disacivate = false;

        switch( $tracking['carrier'] )
        {
            case 'DPD':
                if ( $tr_status_id == 8 || $tr_status_id == 9 )
                {
                    $disacivate = true;
                }
                break;

            case 'EMS':
                if ( $tr_status_id == 6 )
                {
                    $disacivate = true;
                }
                break;
        }
        
        if ( $disacivate )
        {
            $db->disactivateTracking( $tracking['num'] );
        }
    }
}

?>