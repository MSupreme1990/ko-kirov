<?php
/**
 * amoCRM - Интегратор заявок в амо
 * Прием данных с сайтов
 *
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
define('ROOT', dirname(__FILE__));

include(ROOT . '/class/amocrm/amo.class.php');
$amo = new amoCRM();

// номер
if (!empty($_REQUEST['phone'])) $phone = preg_replace("/[^0-9]/", '', (string)$_REQUEST['phone']); else $phone = '';
// email
if (!empty($_REQUEST['email'])) $email = (string)$_REQUEST['email']; else $email = '';
// имя
if (!empty($_REQUEST['name'])) $name = (string)$_REQUEST['name']; else $name = 'Новый контакт';
//примечание
if (!empty($_REQUEST['note'])) $note = (string)$_REQUEST['note'];

// ищем контакт по номеру телефона
if (!empty($phone)) {
    $contact = $amo->contacts_Get->search($phone, 'Телефон');
// или по email
} elseif (!empty($email)) {
    $contact = $amo->contacts_Get->search($email, 'Email');
} else {
    exit('Empty search request');
}

// если контакт найден
if (is_object($contact) && isset($contact->id)) {
    $contact_id = $contact->id;
    $resp_id = $contact->responsible_user_id;
// если контакт не найден
} else {
    $contact_id = false;
    $resp_id = 1;
}

if (!empty($_COOKIE['utm_source'])) $utm_source = $_COOKIE['utm_source']; else $utm_source = '-';
if (!empty($_COOKIE['utm_campaign'])) $utm_campaign = $_COOKIE['utm_campaign']; else $utm_campaign = '-';
if (!empty($_COOKIE['utm_content'])) $utm_content = $_COOKIE['utm_content']; else $utm_content = '-';
if (!empty($_COOKIE['utm_medium'])) $utm_medium = $_COOKIE['utm_medium']; else $utm_medium = '-';
if (!empty($_COOKIE['utm_term'])) $utm_term = $_COOKIE['utm_term']; else $utm_term = '-';

if ($utm_source != '-') {
    if (strpos($utm_source, 'yand') !== false) {
        $utm_source = 'Яндекс';
    } else if (strpos($utm_source, 'google') !== false) {
        $utm_source = 'Гугл';
    }
}

// добавление контакта
if (!$contact_id) {

    //Добавление сделки
    $setLead = $amo->leads_Set;
    $setLead->name('Новая сделка');
    $setLead->status('Новая'); 
    $setLead->respId($resp_id);
    $leadId = $setLead->run();

    // создание контакта
    $setContact = $amo->contacts_Set;
    $setContact->name($name);
    $setContact->respId($resp_id);
    $setContact->phone($phone);
    $setContact->email($email);
    $setContact->leads($leadId);
    $setContact->custom('Источник', $utm_source);
    $setContact->custom('utm_campaign', $utm_campaign);
    $setContact->custom('utm_content', $utm_content);
    $setContact->custom('utm_term', $utm_term);
    $contact_id = $setContact->run();

    // создание задачи
    $taskSet = $amo->tasks_Set;
    $taskSet->respId($resp_id);
    $taskSet->text('Обработать контакт.');
    $taskSet->type('Follow-up');
    $taskSet->elemType('contact');
    $taskSet->elemId($contact_id);
    $taskSet->toTime(2);
    $taskSet->run();

    //создание заметки
    if (isset($note)) {
        $noteSet = $amo->notes_Set;
        $noteSet->text($note);
        $noteSet->elemType('contact');
        $noteSet->elemId($contact_id);
        $noteSet->type(4);
        $noteSet->respId($resp_id);
        $noteSet->run();
    }

// работа с найденным контактом
} else {

    // создание задачи
    $taskSet = $amo->tasks_Set;
    $taskSet->respId($resp_id);
    $taskSet->text('Повторная заявка.');
    $taskSet->type('Follow-up');
    $taskSet->elemType('contact');
    $taskSet->elemId($contact_id);
    $taskSet->toTime(1);
    $task_id = $taskSet->run();

    $uContact = $amo->contacts_Update;
    $uContact->setContact($contact);
    $uContact->run();
}