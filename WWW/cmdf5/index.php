<?php

define( 'ROOT', dirname(__FILE__) );

if ( empty( $_POST ) || empty( $_POST['leads'] ) ) exit();

foreach ( $_POST['leads'] as $val )
{
    $lead_arr = $val[0];
    break;
}


//include ROOT.'/class/logger/log.class.php';
include ROOT.'/class/db.class.php';
include ROOT.'/class/lead.class.php';
include ROOT.'/class/tracking.class.php';
include ROOT.'/class/amocrm/amo.class.php';


$lead;      // class Lead
$tracking;  // class Tracking
$db;        // class DB
$amo;       // class amoCRM


//Logger::log( '===== NEW EVENT =====' );

$tracking_number     = ''; // Трек-номер.
$tracking_number_old = '';
$tracking_carrier    = ''; // Код транспортной компании.

$lead = new Lead( $lead_arr );
$tracking_number = $lead->getCustom( 'Трек-номер' );

$db = new DB( 'localhost', 'u1249_1', 'PWFHKaE', 'u1249_1' );
$tracking_number_old = $db->getTrackingNumberByLeadId( $lead->getId() );

$tracking = new Tracking( $tracking_number );
$tracking_carrier = $tracking->getCarrier();

$amo = new amoCRM();

//Logger::log( $tracking_number );
//Logger::log( $tracking_number_old );
//Logger::log( $tracking_carrier );

if ( ( empty( $tracking_number ) && empty( $tracking_number_old ) ) ||
     ( $tracking_number == $tracking_number_old ) )
{
    // Трек-номер отсутствует или
    //            уже существует.
    exit();
}
elseif ( empty( $tracking_number ) && !empty( $tracking_number_old ) )
{
    // Трек-номер удален.
    $db->editTrackingByLeadId( $lead->getId(), $tracking_number, $tracking_carrier );
    $db->deleteTrackingByLeadId( $lead->getId() );

    // Добавить в CRM примечание сделки об удалении трек-номера.
    $noteSet = $amo->notes_Set;
    $noteSet->text( 'Удален трек-номер.' );
    $noteSet->elemType( 'lead' );
    $noteSet->elemId( $lead->getId() );
    $noteSet->type( 4 );
    $noteSet->run();
    
}
elseif ( empty( $tracking_number_old ) )
{
    // Трек-номер добавлен.
    $db->addTracking( $tracking_number, $tracking_carrier, $lead->getId() );
    
    // Получаем информацию о сделке.
    $leadGet = $amo->leads_Get;
    $l = $leadGet->byId( $lead->getId() );
    
    // Изменяем статус сделки.
    $leadUpdate = $amo->leads_Update;
    $leadUpdate->setLead( $l );
    $leadUpdate->id( $lead->getId() );
    $leadUpdate->statusId( 7602832 ); // Статус "Отправка"
    $leadUpdate->run();
    
    // Добавить в CRM примечание сделки о добавлении трек-номера.
    $noteSet = $amo->notes_Set;
    $noteSet->text( 'Добавлен трек-номер: '.$tracking_number.' - '.$tracking_carrier );
    $noteSet->elemType( 'lead' );
    $noteSet->elemId( $lead->getId() );
    $noteSet->type( 4 );
    $noteSet->run();
}
elseif ( $tracking_number != $tracking_number_old )
{
    // Трек-номер изменен.
    $db->editTrackingByLeadId( $lead->getId(), $tracking_number, $tracking_carrier );

    // Добавить в CRM примечание сделки об изменении трек-номера.
    $noteSet = $amo->notes_Set;
    $noteSet->text( 'Изменен трек-номер: '.$tracking_number.' - '.$tracking_carrier );
    $noteSet->elemType( 'lead' );
    $noteSet->elemId( $lead->getId() );
    $noteSet->type( 4 );
    $noteSet->run();
}

?>