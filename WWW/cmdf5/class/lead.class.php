<?php

class Lead
{
    private $_id        = null;
    private $_name      = '';
    private $_status_id = 0;
    private $_price     = 0;
    private $_custom   = array();
    
    
    public function __construct( $data )
    {
        $this->set( $data );
    }
    
    /**
     * Инициализация из массива данных.
     */
    public function set( $data )
    {
        if ( is_array( $data ) && !empty( $data ) )
        {
            $this->_id        = $data['id'];
            $this->_name      = $data['name'];
            $this->_status_id = $data['status_id'];
            $this->_price     = $data['price'];
            
            if ( $data['custom_fields'] )
            {
                $this->_custom = $data['custom_fields'];
            }
        }
    }
    
    /**
     * Возвращает Id сделки.
     * 
     * @return integer
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Возвращает значение custom поля.
     * 
     * @param  string $name
     * @return string OR false
     */
    public function getCustom( $name )
    {
        if ( $name )
        {
            $name = (string)$name;
            
            foreach ( $this->_custom as $field )
            {
                if ( $field['name'] == $name )
                {
                    return $field['values'][0]['value'];
                }
            }
        }
        
        return false;
    }
}