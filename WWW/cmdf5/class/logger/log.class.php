<?php
/**
 * Logger
 * Класс для логирования
 *
 * @name Logger
 * @author Vlad Ionov <vlad@f5.com.ru>
 * @version 1.00
 */
class Logger {
    /**
	 * Имя лога
     */
	private static $_name = 'events';
    /**
	 * Путь к логам
     */
	private static $_path = 'log';
    
	//=============================================
	
    /**
	 * Запись в лог
     */
	public static function log( $data ){
		
		$data = date( 'd M Y H:i:s', time()).' - '.$data."\r\n";
		file_put_contents( ROOT.'/'.self::$_path.'/'.self::$_name.'.txt', $data, FILE_APPEND | LOCK_EX );
	}
	
    /**
	 * Установка имени лога
     */
	public static function setName( $name ){
		
		if( !empty( $name )) self::$_name = $name;
	}
}