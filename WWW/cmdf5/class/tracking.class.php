<?php

class Tracking
{
    private $_number = '';
    private $_carrier = '';
    
    /**
     * Список паттернов трек-номеров транспортных компаний.
     */
    private $_number_patterns = array(
        'DPD' => '/\d{8}[A-Z]{3}/i',
        'EMS' => '/[A-Z]{2}\d{9}[A-Z]{2}/i'
    );
    
    /**
     * Проверка трек-номера по шаблону.
     */
    private function _checkCarrier()
    {
        if ( $this->_number )
        {
            foreach ( $this->_number_patterns as $key => $val )
            {
                if ( preg_match( $val, $this->_number ) )
                {
                    $this->_carrier = $key;
                    
                    break;
                }
            }
            
            if ( !$this->_carrier )
            {
                $this->_number = '';
            }
        }
    }
    
    public function __construct( $number )
    {
        $this->setNumber( $number );
    }
    
    /**
     * Устанавиливает трекинг-номер.
     */
    public function setNumber( $number )
    {
        if ( !empty( $number ) )
        {
            $this->_number = $number;
        }
        
        $this->_checkCarrier();
    }
    
    /**
     * Возвращает код перевозчика.
     */
    public function getCarrier()
    {
        return $this->_carrier;
    }
}