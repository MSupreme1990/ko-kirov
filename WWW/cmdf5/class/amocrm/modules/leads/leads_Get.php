<?php
/**
 * Получение сделок
 *
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
class leads_Get extends get {
	/**
	 * Хранение данных
	 */
	private $data = array();
	/**
	 * Хранение ID для получения
	 */
	private $ids = array();
	
	/**
	 * Получение по ID
	 */
	public function byId( $id ){
		
		$this->clear();
		$this->setParam( 'id', $id );
		
		if( is_array( $id )){
			
			$max = 349;
			$this->ids = $id;
			
			while( count($this->ids) > 0 ){
			
				$i = 0;
				$part = array();
				
				foreach( $this->ids as $k=>$lid ){
					
					$part[] = $lid;
					unset( $this->ids[$k] );
					$i++;
					if( $i == $max ) break;
				}
				
				$this->setParam( 'id', $part );
				$resp = $this->run();
				
				foreach( $resp as $item ){
					
					$this->data[] = $item;
				}
			}
			return $this->data;
			
		} elseif( $data = $this->run()){
			
			if( is_array($data) && count($data) === 1 ) return $data[0];
			return $data;
		}
		return '';
	}

}
?>