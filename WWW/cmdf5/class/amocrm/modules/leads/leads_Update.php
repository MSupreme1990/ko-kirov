<?php
/**
 * Обновление сделок
 *
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
class leads_Update extends set {
    /**
	 * Данные по сделке
     */
	protected $lead;
    /**
	 * Добавление или обновление
     */
	protected $mode = 'update';
	
	/**
	 * Сделка для обновления
	 */
	public function setLead( $lead ){
		
		$this->lead = json_decode( json_encode( $lead ), true );
		$this->setModif();
		$this->id( $this->lead['id'] );

		foreach( $this->lead['custom_fields'] as $cf_field ){
		
			$this->lead['update']['custom'][$cf_field['name']] = array();

			foreach( $cf_field['values'] as $cf_item ){
			
				$this->lead['setted'][$cf_field['name']][$cf_item['value']] = 1;
				$field = array( $cf_item['value'] );
				
				if( isset( $cf_item['enum'] ) && isset( $this->enum[$cf_item['enum']] )){
				
					$field[1] = $this->enum[$cf_item['enum']];
				}
				$this->lead['update']['custom'][$cf_field['name']][] = $field;
			}
		}
	}
	
	/**
	 * Идентификатор контакта
	 */
	public function id( $id ){
		
		$this->setValue( 'id', $id );
	}
	
	/**
	 * Время изменения
	 */
	public function setModif( $time = false ){
	
		if( !$time && $this->lead['last_modified'] < time() ){
			$time = time();
		} elseif( !$time ){
			$time = $this->lead['last_modified']+= 3;
		}
		$this->setValue( 'last_modified', $time );
	}
	
	/**
	 * Имя контакта
	 */
	public function name( $value ){
		
		$this->setValue( 'name', $value );
	}
	
	/**
	 * Теги
	 */
	public function tags( $value ){
		
		$this->setValue( 'tags', $value );
	}
	
	/**
	 * Ключевые слова
	 */
	public function leads( $values ){
	
		if( !is_array( $values )){
			$values = array( $values );
		}
		$this->setValue( 'linked_leads_id', $values );
	}
	
	
	/**
	 * Установка временных custom данных для отправки
	 */
	public function custom( $name, $values ){
	
		if( !is_array( $values )){
			$values = array( $values );
		}
		$this->setCustom( $name, $values );
	}
        
        /**
	 * ID статуса сделки
	 */
	public function statusId( $value ){
	
		if(!empty( $value )) $this->setValue( 'status_id', $value );
	}
	
	/**
	 * Статус сделки
	 */
	public function status( $value ){
	
		if( isset( $this->lead_status[$value] )) $this->setValue( 'status_id', $this->lead_status[$value] );
	}
	
	/**
	 * Ответственный
	 */
	public function respId( $value ){

		$this->setValue( 'responsible_user_id', $value );
	}
	
}
?>