<?php
/**
 * amoCRM �����
 * ��������� ���������� � ����������
 *
 * @author Vlad Ionov <vlad@f5.com.ru>
 * @version 1.00
 */
class userinfo {
    /**
	 * ��������� �������� �� id
     */
	private $user_id = array();
    /**
	 * ��������� �������� �� �����
     */
	private $user_names = array();
	
	//=======================================================
	
	public function __construct(){}
	
	/**
	 * ��������� ����������
	 */
	public function setConfig( $cnf ){
	
		foreach( $cnf['account']->users as $user ){
		
			$this->user_id[$user->id] = $user;
			if( !empty( $user->last_name )) $user->name = $user->name.' '.$user->last_name;
			$this->user_name[trim($user->name)] = $user->id;
			
		}
	}
	
	/**
	 * ��������� ���������� � ��������� �� id
	 */
	public function byId( $user_id ){
		
		if( isset( $this->user_id[$user_id] )) return $this->user_id[$user_id];
		return (object) array( 'name' => $user_id );
	}
	
	/**
	 * ��������� ���������� � ��������� �� �����
	 */
	public function byName( $user_name, $info = true ){
		
		if( isset( $this->user_name[trim($user_name)] )){
			if( $info ) return $this->byId($this->user_name[$user_name]); else return $this->user_name[$user_name];
		}
		return false;
	}
	
	/**
	 * ��������� ������� �������������
	 */
	public function users(){
		
		return $this->user_id;
	}
	
	/**
	 * �������������
	 */
	public function inits( $entity ){}
}
?>