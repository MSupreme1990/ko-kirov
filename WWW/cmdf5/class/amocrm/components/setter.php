<?php
/**
 * amoCRM класс
 * Добавление в amoCRM
 *
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
class set extends amoCRM {
    /**
	 * Сущность
     */
	protected $entity;
    /**
	 * Данные для отправки
     */
	protected $request;
    /**
	 * Временные данные
     */
	protected $temp;
    /**
	 * Дополнительные поля по name
     */
	protected $custom;
    /**
	 * Класс ответственных
     */
	protected $user;
    /**
	 * Добавление или обновление
     */
	protected $mode = 'add';
    /**
	 * Enum id и их значения
     */
	protected $enum = array();
    /**
	 * Статусы сделок
     */
	protected $lead_status = array();
    /**
	 * Типы примечаний
     */
	protected $note_type = array();
    /**
	 * Типы задач
     */
	protected $task_type = array();
	
	//=======================================================
	
	public function __construct( $entity = false ){
		/**
		 * С чем работаем
		 */
		$this->setEntity( $entity );
		/**
		 * Установка начальных параметров
		 */
		$this->clear();
	}
	
	/**
	 * Установка временных данных для отправки
	 */
	public function setValue( $key, $value ){
		
		$this->temp[$key] = $value;
	}
	
	/**
	 * Установка временных custom данных для отправки
	 */
	public function setCustom( $name, $values ){

		if( !isset( $this->custom[$name] )) $this->error( 1003, $name );
		if( !is_array( $values[0] )) $values = array( $values );
		$custom_values = array();
		
		foreach( $values as $val ){
			if( !isset( $val[1] )) $val[1] = 'OTHER';
			$custom_values[] = array(
				'value' => $val[0],
				'enum' => $val[1]
			);
		}
		$this->temp['custom_fields'][] = array(
												'id' => $this->custom[$name]->id,
												'values' => $custom_values
												);
	}
	
	/**
	 * Множественное добавление
	 */
	public function bind(){
		
		if( !empty( $this->temp )){
			$this->request[] = $this->temp;
			$this->temp = array();

			return true;
		}
	}
	
	/**
	 * Добавление
	 */
	public function run( $request = false ){
			
		$this->bind();
		$data = array();
		if( $request ) $this->request = $request;
		if( empty( $this->request )) $this->error( 1002 );
		$data['request'][$this->entity][$this->mode] = $this->request;

		$resp = $this->query( 'post', '/private/api/v2/json/'.$this->entity.'/set', $data );
		$resp = $resp->getResp();

        if( !isset( $resp->server_time )) {
            $this->error( 1004, $this->entity, $this->mode );
        }
        $this->clear();

        $temp = $resp->{$this->entity}->{$this->mode};
        if (count($temp) > 0) {
            return $temp[0]->id;
        }
		return true;
	}
	
	/**
	 * Установка сущности
	 */
	protected function setEntity( $entity ){
	
		if( $entity ) $this->entity = $entity;
	}
	
	/**
	 * Инициализация
	 */
	protected function inits( $entity ){

		if( $entity ) $this->entity = $entity;
		$custom_entity = $this->entity;
		if( $this->entity == 'company' ) $custom_entity = 'companies';
		// дополнительные поля
		if( isset( $this->config['account']->custom_fields->{$custom_entity} )){
		
			foreach( $this->config['account']->custom_fields->{$custom_entity} as $custom_field ){
			
				if( !empty( $custom_field->enums )){
				
						foreach( $custom_field->enums as $eid=>$enam ){
							$eid = (int) $eid;
							$this->enum[$eid] = $enam;
						}
					$custom_field->enums = $this->objFlip( $custom_field->enums );
				}
				$this->custom[$custom_field->name] = $custom_field;
			}
		}
		
		// статусы сделок
		foreach( $this->config['account']->leads_statuses as $least ){
			
			$this->lead_status[$least->name] = $least->id;
		}
		
		// типы примечаний
		foreach( $this->config['account']->note_types as $noty ){
			
			$this->note_type[$noty->code] = $noty->id;
		}
		
		// типы задач
		foreach( $this->config['account']->task_types as $tasky ){
			
			$this->task_type[$tasky->name] = $tasky->id;
		}
	}
	
	/**
	 * Очистка данных для отправки
	 */
	public function clear(){
		
		$this->request = array();
		$this->temp = array();
	}
	
	/**
	 * Очистка данных для отправки
	 */
	protected function objFlip( $obj ){
		$obj = (array) $obj;
		return (object) array_flip( $obj );
	}
}
 ?>