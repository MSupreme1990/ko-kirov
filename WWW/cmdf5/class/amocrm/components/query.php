<?php
/**
 * amoCRM класс
 * Запросы к серверу
 *
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
class amoCRM_Query {
    /**
	 * curl
     */
	private $curl;
    /**
	 * Заголовок LastModified
     */
	private $modif;
    /**
	 * last time
     */
	private $last_time;
	
	//=======================================================

	public function __construct(){
		/**
		 * start time
		 */
		$this->last_time = microtime(1);
	}
	
	/**
	 * Инициализация curl
	 */
	private function curlInit( $domain ){
		
		if( !is_null( $this->curl )) curl_close( $this->curl );
		$this->curl = curl_init();
		
		curl_setopt( $this->curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0' );
		curl_setopt( $this->curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $this->curl, CURLOPT_SSL_VERIFYPEER, 0 );
		curl_setopt( $this->curl, CURLOPT_SSL_VERIFYHOST, 0 );
		curl_setopt( $this->curl, CURLOPT_COOKIEFILE, $this->cookiesFile( $domain ));
		curl_setopt( $this->curl, CURLOPT_COOKIEJAR, $this->cookiesFile( $domain ));
	}
	
    /**
	 * Заголовок LastModified
	 * По умолчанию - минус год
     */
	public function setModified( $time ){
		
		$datetime = new DateTime( date( 'Y-m-d H:i:s', $time ));
		$this->modif = gmdate("D, d M Y H:i:s", $datetime->getTimestamp() );
		return $this->modif;
	}
	
    /**
	 * Формирование ссылки
     */
    private function createUrl( $domain, $url, $data = false ){
	
		$link = 'https://'.$domain.'.amocrm.ru'.$url;
		if( $data && !isset( $data['request'] )){
			$link .= '?'.http_build_query( $data );
		}
		  return $link;
    }
	
    /**
	 * GET запрос
     */
	public function get( $domain, $url, $args, $modified = false ){
	
		$this->curlInit( $domain );
		curl_setopt( $this->curl, CURLOPT_URL, $this->createUrl( $domain, $url, $args ));
		if( $modified ){
			curl_setopt( $this->curl, CURLOPT_HTTPHEADER, array( 'if-modified-since: '.$this->setModified( $modified ) ));
		}
		
		$response = $this->response();
		return new amoCRM_Response( $response['data'], $response['code'] );
	}
	
    /**
	 * POST запрос
     */
	public function post( $domain, $url, $args, $modified ){
	
		$this->curlInit( $domain );
		curl_setopt( $this->curl, CURLOPT_URL, $this->createUrl( $domain, $url, $args ));
		if( !is_array( $args )){
			curl_setopt( $this->curl, CURLOPT_CUSTOMREQUEST, 'POST' );
			curl_setopt( $this->curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		} else {
			curl_setopt( $this->curl, CURLOPT_POST, true );
		}
		curl_setopt( $this->curl, CURLOPT_POSTFIELDS, json_encode( $args ));
		
		$response = $this->response();
		return new amoCRM_Response( $response['data'], $response['code'] );
	}
	
    /**
	 * Ответ сервера
     */
	private function response(){
	
		$latency = $this->getLatency();
		if( $latency > 0 && $latency < 1 ){
			sleep( 1-$latency );
		}
		$resp = array(
			'data' => curl_exec( $this->curl ),
			'code' => curl_getinfo( $this->curl, CURLINFO_HTTP_CODE )
		);
		$this->last_time = microtime(1);
		return $resp;
	}
	
    /**
	 * Получение времени запроса
     */
	private function getLatency(){
	
		return microtime(1) - $this->last_time;
	}
	
    /**
	 * Cookies временный файл
     */
	public function cookiesFile( $domain ){
		
		return AMOAPP.'/temp/'.$domain.'_amocookie.txt';
	}
}