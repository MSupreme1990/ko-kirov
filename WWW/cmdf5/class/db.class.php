<?php

class DB
{
    private $_db;
    private $_db_table_name = 'cmdf5_order_trackings';
    
    public function __construct( $host, $username, $password, $db )
    {
        $this->connect( $host, $username, $password, $db );
    }
    
    public function __destruct()
    {
        $this->_db->close();
    }
    
    /**
     * Соединение с БД.
     * 
     * @param string $host
     * @param string $username
     * @param string $password
     * @param string $db
     */
    public function connect( $host, $username, $password, $db )
    {
        $this->_db = new mysqli( $host, $username, $password, $db );

        if ( mysqli_connect_errno() )
        {
            exit();
        }
    }
    
    /**
     * Проверяет существование трек-номер в базе.
     * 
     * @param type $number
     * @return boolean
     */
    public function checkTracking( $number, $lead_id )
    {
        $rows = 0;
        
        $query = 'SELECT `id` '.
                 'FROM `'.$this->_db_table_name.'` '.
                 'WHERE `num` = "'.$number.'" AND '.
                 '      `lead_id` = '.$lead_id.' '.
                 'LIMIT 1';
        
        $result = $this->_db->query( $query );
        $rows = $result->num_rows;
        $result->close();
        
        return (bool)$rows;
    }
    
    /**
     * Добавляет трек-номер в базу.
     * 
     * @param  string  $carrier
     * @param  integer $lead_id
     * @return boolean
     */
    public function addTracking( $number, $carrier, $lead_id )
    {
        $query = 'INSERT INTO `'.$this->_db_table_name.'` (`num`, `carrier`, `lead_id`, `added`) '.
                 'VALUES ("'.$number.'", "'.$carrier.'", '.$lead_id.', NOW())';
        $this->_db->query( $query );
        
        return (bool)$this->_db->affected_rows;
    }
    
    /**
     * Обновляет трек-номер по переданному Id сделки.
     * 
     * @param  integer $lead_id
     * @param  integer $number
     * @param  string  $carrier
     * @return boolean
     */
    public function editTrackingByLeadId( $lead_id, $number, $carrier )
    {
        $query = 'UPDATE `'.$this->_db_table_name.'` '.
                 'SET `num`     = "'.$number.'", '.
                 '    `carrier` = "'.$carrier.'", '.
                 '    `added`   = NOW(), '.
                 '    `active`  = 1 '.
                 'WHERE `lead_id` = '.$lead_id;
        $this->_db->query( $query );
        
        return (bool)$this->_db->affected_rows;
    }
    
    /**
     * Возвращает статус ID трек-номера.
     * 
     * @param  integer $number
     * @return integer
     */
    public function getTrackingStatusId( $number )
    {
        $query = 'SELECT `status_id` '.
                 'FROM `'.$this->_db_table_name.'` '.
                 'WHERE `num` = "'.$number.'" '.
                 'LIMIT 1';
        $result = $this->_db->query( $query );
        $row = $result->fetch_row();
        $result->close();
        
        return $row['status_id'];
    }
    
    /**
     * Устанавливает статус ID трек-номера.
     * 
     * @param  integer $number
     * @param  integer $status_id
     * @return boolean
     */
    public function setTrackingStatusId( $number, $status_id )
    {
        $query = 'UPDATE `'.$this->_db_table_name.'` '.
                 'SET `status_id`    = '.$status_id.', '.
                 '    `last_checked` = NOW() '.
                 'WHERE `num` = "'.$number.'"';
        $this->_db->query( $query );
        
        return (bool)$this->_db->affected_rows;
    }
    
    /**
     * Обновляет дату проверки трек-номера.
     * 
     * @param  integer $number
     * @return boolean
     */
    public function updateTrackingDateLastChecked( $number )
    {
        $query = 'UPDATE `'.$this->_db_table_name.'` '.
                 'SET `last_checked` = NOW() '.
                 'WHERE `num` = "'.$number.'"';
        $this->_db->query( $query );
        
        return (bool)$this->_db->affected_rows;
    }
    
    /**
     * 
     * 
     * @param  integer $lead_id
     * @return boolean OR array
     */
    public function getTrackingNumberByLeadId( $lead_id )
    {
        $number = false;
        
        $query = 'SELECT `num` '.
                 'FROM `'.$this->_db_table_name.'` '.
                 'WHERE `lead_id` = '.$lead_id.' '.
                 'LIMIT 1';
        $result = $this->_db->query( $query );

        if ( $result->num_rows )
        {
            $row = $result->fetch_assoc();
            $number = $row['num'];
        }

        $result->close();
        
        return $number;
    }
    
    /**
     * Удаляет трек-номер по переданному Id сделки.
     * 
     * @param  integer $lead_id
     * @return boolean
     */
    public function deleteTrackingByLeadId( $lead_id ) {
        $query = 'DELETE FROM `'.$this->_db_table_name.'` '.
                 'WHERE `lead_id` = '.$lead_id;
        $this->_db->query( $query );
        
        return (bool)$this->_db->affected_rows;
    }
    
    /**
     * Возвращает список активных трек-номеров.
     * 
     * @return boolean OR array
     */
    public function getActiveTrackings()
    {
        $rows = false;
        
        $query = 'SELECT * '.
                 'FROM `'.$this->_db_table_name.'` '.
                 'WHERE `active` = 1';
        $result = $this->_db->query( $query );

        if ( $result->num_rows )
        {
            while ( $row = $result->fetch_assoc() )
            {
                $rows[] = $row;
            }
        }

        $result->close();
        
        return $rows; 
    }
    
    /**
     * Деактивирует трек-номер для исключения его из списка проверки статуса отправки.
     * 
     * @param  integer $number
     * @return boolean
     */
    public function disactivateTracking( $number )
    {
        $query = 'UPDATE `'.$this->_db_table_name.'` '.
                 'SET `active` = 0 '.
                 'WHERE `num` = "'.$number.'"';
        $this->_db->query( $query );

        return (bool)$this->_db->affected_rows;
    }
}