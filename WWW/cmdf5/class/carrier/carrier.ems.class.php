<?php

class CarrierEMS
{
    private $_url         = '';
    private $_orderNum    = '';
    private $_orderStatus = array(
        1 => 'Следует по маршруту',
        2 => 'Следует по маршруту',
        3 => 'Следует по маршруту',
        4 => 'Следует по маршруту',
        5 => 'Почта (Прибыло в место вручения)',
        6 => 'Куплено (Доставлено, Вручено)',
        7 => 'Почта (Прибыло в место вручения)',
        8 => 'Условный отказ',
        9 => 'Идёт возврат'
    );
    private $_orderStatusCall = array(
        5 => 'Звонить получателю, сообщить о том что заказ пришел и он получал его.',
        8 => 'Звонить получателю, уточнять информацию почему отказ.'
    );
    private $_orderStatusId = 0;
    
    /**
     * 
     * @return array
     */
    private function _getContent()
    {
        include_once 'russianpost.lib.php';
        
        $client = new RussianPostAPI();

        return $client->getOperationHistory( $this->_orderNum );
    }
    
    private function _setStatusId( $status )
    {
        foreach ( $this->_orderStatus as $key => $val )
        {
            if ( $status === $val )
            {
                $this->_orderStatusId = $key;
                break;
            }
        }
    }
    
    public function __construct( $orderNum )
    {
        $this->_orderNum = $orderNum;
    }
    
    public function checkStatus()
    {
        $content = $this->_getContent();
        $status  = '';

        if ( !$content || !count( $content ) )
        {
            return false;
        }
        
        $status = $content[count( $content ) - 1]->operationAttribute;
        $this->_setStatusId( $status );
        
        return true;
    }
    
    public function getStatus()
    {
        return isset( $this->_orderStatus[$this->_orderStatusId] )
                    ? $this->_orderStatus[$this->_orderStatusId]
                    : '';
    }
    
    public function getStatusId()
    {
        return $this->_orderStatusId;
    }
    
    public function getStatusCall()
    {
        return isset( $this->_orderStatusCall[$this->_orderStatusId] )
                    ? $this->_orderStatusCall[$this->_orderStatusId]
                    : '';
    }
}