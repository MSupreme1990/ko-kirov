<?php

class CarrierDPD
{
    private $_url         = 'http://dpd.ru/ols/trace2/standard.do2?method:search';
    private $_orderNum    = '';
    private $_orderStatus = array(
        1 => 'Подбор размера',
        2 => 'Следует по маршруту',
        3 => 'Следует по маршруту',
        4 => 'Следует по маршруту',
        5 => 'На терминале (Готово для доставки получателю)',
        6 => 'Осуществляется доставка получателю',
        7 => 'Условный отказ',
        8 => 'Куплено (Доставлено, Вручено)',
        9 => 'Идёт возврат'
    );
    private $_orderStatusCall = array(
        5 => 'Звонить DPD и выводить на доставку посылку',
        7 => 'Звонить получателю, для уточнения информации, почему перенесли доставку или оформили возврат'
    );
    private $_orderStatusId = 0;
    
    private function _getContent()
    {
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $this->_url );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_HEADER, 0 );
        curl_setopt( $ch, CURLOPT_ENCODING, 'UTF-8' );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, 'orderNum='.$this->_orderNum );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
            'Accept:*/*',
            'Accept-Encoding:gzip, deflate',
            'Accept-Language:ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
            'Connection:keep-alive',
            'Content-Type:application/x-www-form-urlencoded',
            'Host:dpd.ru',
            'Origin:http://dpd.ru',
            'Referer:http://dpd.ru/ols/trace2/standard.do2',
            'User-Agent:Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36',
            'X-Requested-With:XMLHttpRequest') );
        
        $content = curl_exec( $ch );
        curl_close( $ch );

        return $content;
    }
    
    private function _setStatusId( $status )
    {
        foreach ( $this->_orderStatus as $key => $val )
        {
            if ( $status === $val )
            {
                $this->_orderStatusId = $key;
                break;
            }
        }
    }
    
    public function __construct( $orderNum )
    {
        $this->_orderNum = $orderNum;
    }
    
    public function checkStatus()
    {
        $content = $this->_getContent();
        $status  = '';

        if ( !$content )
        {
            return false;
        }
        
        if ( preg_match( '/current-status">([^\.\(]+)/', $content, $matches ) )
        {
            $status = trim( $matches[1] );
            $this->_setStatusId( $status );
            
            return true;
        }
        
        return false;
    }
    
    public function getStatus()
    {
        return isset( $this->_orderStatus[$this->_orderStatusId] )
                    ? $this->_orderStatus[$this->_orderStatusId]
                    : '';
    }
    
    public function getStatusId()
    {
        return $this->_orderStatusId;
    }
    
    public function getStatusCall()
    {
        return isset( $this->_orderStatusCall[$this->_orderStatusId] )
                    ? $this->_orderStatusCall[$this->_orderStatusId]
                    : '';
    }
}