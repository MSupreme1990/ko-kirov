<?php

defined('MINDY_PATH') or define('MINDY_PATH', dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR);


if (!is_file(MINDY_PATH . '../livetex/index.php')) {
    throw new Exception("Livetex files not found.");
}

include(MINDY_PATH . '../livetex/index.php');
